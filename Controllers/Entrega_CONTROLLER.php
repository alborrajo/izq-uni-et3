
<?php
/* Controller de ENTREGAs y las acciones que se le podrán realizar
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	session_start();
	include_once '../Functions/Autenticacion.php';
	if(!autenticado()){//No se puede verni aquí sin estar logeado
		header('Location: ../../index.php');
	}
	
	include '../Models/ENTREGA.php';
	include '../Views/Entrega/Entrega_ADD.php';
	include '../Views/Entrega/Entrega_EDIT.php';
	include '../Views/Entrega/Entrega_SEARCH.php';
	include '../Views/Entrega/Entrega_DELETE.php';
	include '../Views/Entrega/Entrega_SHOWCURRENT.php';
	include '../Views/Entrega/Entrega_SHOWALL.php';
	include '../Views/MESSAGE.php';
	
function get_data_form(){
	
	
    $login = $_REQUEST['login'];
	$IdTrabajo = $_REQUEST['IdTrabajo'];
	$Alias = $_REQUEST['Alias'];
	$Horas = $_REQUEST['Horas'];

	if(isset($_FILES['Ruta'])) { //Si se ha subido un fichero
		preg_match('/.*\.rar$/',$_FILES['Ruta']['name'],$matches,PREG_OFFSET_CAPTURE); //Regex para ver si es un .rar
		if(sizeof($matches) > 0) { //Si es un rar
			if(strlen($_FILES['Ruta']['name'])!=0){  //se sube una entrega
				
				mkdir("../Files/".$Alias);	//Crear carpeta con el alias por nombre
				$ruta="../Files/".$Alias."/".$_FILES['Ruta']['name']; //Ruta en la que se guardará el rar ../Files/alias/nombredel.rar

				if(strlen($_REQUEST['rutaGuardada'])!=0){ //se esta editando la entrega y se esta subiendo un nueva entrega
					//como se esta subiendo una nueva entrega eliminamos la entrega antigua del servidor antes de subir una nuea entrega
					unlink(trim($_REQUEST['rutaGuardada']));
				}

				if(is_uploaded_file($_FILES['Ruta']['tmp_name'])){
					move_uploaded_file($_FILES['Ruta']['tmp_name'],$ruta);  //subimos la entrega al servidor
				}

			}else{
				//no se sube una nueva entrega  --> se almacena la ruta que ya habia en la BD
				$ruta=$_REQUEST['rutaGuardada'];

			}

			$Ruta = $ruta;

			$Entrega = new Entrega($login,$IdTrabajo,$Alias,$Horas,$Ruta);

			return $Entrega;
		}
		else { //Si no es un rar
			return null;
		}
	}
	else if (isset($_REQUEST['Ruta'])) { //Si no se ha subido un fichero
		$Ruta = $_REQUEST['Ruta'];

		$Entrega = new Entrega($login,$IdTrabajo,$Alias,$Horas,$Ruta);
		
		return $Entrega;
	}
}
	

if (!isset($_REQUEST['orden'])){ //si no viene del formulario, no existe array POST
	$_REQUEST['orden'] = 'SHOWALL';
}
		switch ($_REQUEST['orden']){
			case 'ADD':
				if(tienePermisosPara('ENTREG', 'ADD')){
					if(!$_POST){//Si GET
						$muestraADD = new Entrega_ADD();//Mostrar vista add
					}else{
						$entrega = get_data_form();//Si post cogemos usuario
						
						if($entrega == null){
							new Mensaje('Error con la extensión del fichero o nombre no válido', '../Controllers/Entrega_CONTROLLER.php');// y a ver qué ha pasado en la BD								
						}else{
							$respuesta = $entrega->ADD();//Y lo añadimos
							new Mensaje($respuesta, '../Controllers/Entrega_CONTROLLER.php');// y a ver qué ha pasado en la BD							
						}
					}				
				}else{
					new Mensaje('Permisos insuficientes', '../Controllers/Trabajo_CONTROLLER.php');
				}
			break;
				
			case 'EDIT':
				if(tienePermisosPara('ENTREG', 'EDIT')){ //Si tiene permisos (ADMIN)
					if(!$_POST){//Si GET
						$entrega = new Entrega($_REQUEST['login'],$_REQUEST['IdTrabajo'],'','','');//Editar usuario seleccionado
						$entrega->_getDatosGuardados();//Rellenar con los datos de la BD
						new Entrega_EDIT($entrega);//Mostrar vista
					}else{	
						$entrega = get_data_form();//Coger datos					
						
						if($entrega == null){
							new Mensaje('Error con la extensión del fichero o nombre no válido', '../Controllers/Entrega_CONTROLLER.php');// y a ver qué ha pasado en la BD								
						}else{
							$respuesta = $entrega->EDIT();//Actualizarlos
							new Mensaje($respuesta, '../Controllers/Entrega_CONTROLLER.php');//A ver que pasa con la BD, qué intrigante
						}
					}
				}else{ //Si no tiene permisos (USER regular)
					if(!$_POST){//Si GET
						$entrega = new Entrega($_SESSION['login'],$_REQUEST['IdTrabajo'],'','','');//Editar trabajo seleccionado del usuario de la sesión
						$entrega->_getDatosGuardados();//Rellenar con los datos de la BD
						new Entrega_EDIT($entrega);//Mostrar vista
					}else{
						$entrega = get_data_form();//Coger datos
						
						if($entrega == null){
							new Mensaje('Error con la extensión del fichero o nombre no válido', $_SERVER['HTTP_REFERER']);// y a ver qué ha pasado en la BD								
						}else{
							$entrega->_setLogin($_SESSION['login']); //Asegurarse de que solo se edite a simismo									
							if($entrega->ENPLAZO()){//Si está en plazo								
								$respuesta = $entrega->EDIT();//Actualizarlos
								new Mensaje($respuesta, '../Controllers/Trabajo_CONTROLLER.php');//A ver que pasa con la BD, qué intrigante
							}else{
								new Mensaje('Fuera de plazo', '../Controllers/Trabajo_CONTROLLER.php');//A ver que pasa con la BD, qué intrigante
							}
						}
					}
				}
				break;
				
			case 'SEARCH':
				if(tienePermisosPara('ENTREG', 'SEARCH')){
					if(!$_POST){//Si GET
					$muestraSEARCH = new Entrega_SEARCH();//Mostrar vista buscadora
					}else{
						$entrega = get_data_form();//Creamos un usuario con los datos introducidos (que no insertarlo en la BD)
					
						$respuesta = $entrega->SEARCH();//Buscamos los datos que se parezcan a los introducidos
						new Entrega_SHOWALL($respuesta, array('','EDIT'));//Mostramos todos los datos recuperados de la BD (showall muestra todos los datos que se le pasan)
					}
				}else{
					new Mensaje('Permisos insuficientes', '../Controllers/Trabajo_CONTROLLER.php');
				}				
			break;
				
			case 'DELETE':
				if(tienePermisosPara('ENTREG', 'DELETE')){
					if(!$_POST){//Si GET
						$entrega = new Entrega($_REQUEST['login'],$_REQUEST['IdTrabajo'],'','','');//Coger usuario guardado a eliminar
						$entrega->_getDatosGuardados();//Rellenar datos
						new Entrega_DELETE($entrega);//Mostrar vissta 
					}else{//Si confirma borrado llega por post
						$entrega = new Entrega($_REQUEST['login'],$_REQUEST['IdTrabajo'],'','','');//Clave
						$respuesta = $entrega->DELETE();//Borrar usuario con dicha clave
						//eliminamos la entrega (ruta) del servidor
						unlink($_REQUEST['rutaGuardada']);
						new Mensaje($respuesta, '../Controllers/Entrega_CONTROLLER.php');//A ver qué pasa en la BD
					}
				}else{
					new Mensaje('Permisos insuficientes', '../Controllers/Trabajo_CONTROLLER.php');
				}				
			break;
				
			case 'SHOWCURRENT':
				if(tienePermisosPara('ENTREG', 'SHOWCU')){
					if(!$_POST){//Si GET
						$entrega = new Entrega($_REQUEST['login'],$_REQUEST['IdTrabajo'],'','','');//Coger clave del usuario
						$respuesta = $entrega->SHOWCURRENT();
						if(!is_string($respuesta)){//NO debería ser posible pedir un showcurrent de algo no existente pero si esp osible retornará un string, así que si no es un string es un usuario
							$entrega->_getDatosGuardados();
							new Entrega_SHOWCURRENT($entrega);//Mostrar al usuario rellenado
						}else{//sino
							new Mensaje($respuesta, '../Controllers/Entrega_CONTROLLER.php');//Mensaje de error, que hay muchos
						}
					}
				}else{
					new Mensaje('Permisos insuficientes', '../Controllers/Trabajo_CONTROLLER.php');
				}
			break;
				
			case 'SHOWALL':
				if(tienePermisosPara('ENTREG', 'SHOWAL')){ //Si tiene permisos (ADMIN)
					$entrega = new Entrega('','','','','');//No necesitamos usuario para buscar (pero sí para acceder a la BD)
					$respuesta = $entrega->SHOWALL();//Todos los datos de la BD estarán aqúi
					new Entrega_SHOWALL($respuesta, array('','EDIT'));//Le pasamos todos los datos de la BD

				}else{ //Si no tiene permisos (USUARIO NORMAL)								
					if(!isset($_REQUEST['IdTrabajo'])){
						new Mensaje('Permisos insuficientes', '../Controllers/Entrega_CONTROLLER.php?IdTrabajo='.$_SESSION["login"]);
					}
					$entrega = new Entrega($_SESSION["login"],$_REQUEST['IdTrabajo'],'','','');//No necesitamos usuario para buscar (pero sí para acceder a la BD)
					$respuesta = $entrega->SEARCH();//Buscamos los datos que se parezcan a los introducidos

					if(is_string($respuesta)) { //Si no se encuentran datos, devuelve string, y si es asi...
						$entrega = new Entrega($_SESSION["login"],$_REQUEST['IdTrabajo'],'','',''); //Se crea una entrega nueva vacía
						$entrega->genAlias(); //Se genera un alias nuevo
						$respuestaAdd = $entrega->ADD(); //Se añade a la BD la entrega vacía

						$respuesta = $entrega->SEARCH();//Buscamos de nuevo los datos, ahora ya insertados
					}
					if($entrega->ENPLAZO()){//Si está en plazo
						new Entrega_SHOWALL($respuesta, array('', 'EDIT'));//Mostramos todos los datos recuperados de la BD (showall muestra todos los datos que se le pasan)
					}else{
						new Entrega_SHOWALL($respuesta, array(''));//Mostramos todos los datos recuperados de la BD (showall muestra todos los datos que se le pasan)
					}
				}
			break;
			
			default:
				if(tienePermisosPara('ENTREG', 'SHOWAL')){ //Si tiene permisos (ADMIN)
					$entrega = new Entrega('','','','','');//No necesitamos usuario para buscar (pero sí para acceder a la BD)
					$respuesta = $entrega->SHOWALL();//Todos los datos de la BD estarán aqúi
					new Entrega_SHOWALL($respuesta, array('', 'EDIT'));//Le pasamos todos los datos de la BD

				}else{ //Si no tiene permisos (USUARIO NORMAL)							
					if(!isset($_REQUEST['IdTrabajo'])){
						new Mensaje('Permisos insuficientes', '../Controllers/Entrega_CONTROLLER.php?IdTrabajo='.$_SESSION["login"]);
					}
					$entrega = new Entrega($_SESSION["login"],$_REQUEST['IdTrabajo'],'','','');//No necesitamos usuario para buscar (pero sí para acceder a la BD)
					$respuesta = $entrega->SEARCH();//Buscamos los datos que se parezcan a los introducidos

					if(is_string($respuesta)) { //Si no se encuentran datos, devuelve string, y si es asi...
						$entrega = new Entrega($_SESSION["login"],$_REQUEST['IdTrabajo'],'','',''); //Se crea una entrega nueva vacía
						$entrega->genAlias(); //Se genera un alias nuevo
						$respuestaAdd = $entrega->ADD(); //Se añade a la BD la entrega vacía

						$respuesta = $entrega->SEARCH();//Buscamos de nuevo los datos, ahora ya insertados
					}
					if($entrega->ENPLAZO()){//Si está en plazo
						new Entrega_SHOWALL($respuesta, array('', 'EDIT'));//Mostramos todos los datos recuperados de la BD (showall muestra todos los datos que se le pasan)
					}else{
						new Entrega_SHOWALL($respuesta, array(''));//Mostramos todos los datos recuperados de la BD (showall muestra todos los datos que se le pasan)
					}
				}
			break;
		}
?>