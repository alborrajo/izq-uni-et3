<?php
/* Controller de las notas de los trabajos y las acciones que se le podrán realizar
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	session_start();
	include_once '../Functions/Autenticacion.php';
	if(!autenticado()){//No se puede verni aquí sin estar logeado
		header('Location: ../../index.php');
	}
	
	include '../Models/NOTATRABAJO.php';
	include '../Views/Notatrabajo/Notatrabajo_ADD.php';
	include '../Views/Notatrabajo/Notatrabajo_EDIT.php';
	include '../Views/Notatrabajo/Notatrabajo_SEARCH.php';
	include '../Views/Notatrabajo/Notatrabajo_DELETE.php';
	include '../Views/Notatrabajo/Notatrabajo_SHOWCURRENT.php';
	include '../Views/Notatrabajo/Notatrabajo_SHOWALL.php';
	include '../Views/MESSAGE.php';
	
function get_data_form(){

	$login = $_REQUEST['login'];
	$IdTrabajo = $_REQUEST['IdTrabajo'];
	$NotaTrabajo = $_REQUEST['NotaTrabajo'];

	$trabajo = new NotaTrabajo($login, $IdTrabajo, $NotaTrabajo);
 
	return $trabajo;
}
	

if (!isset($_REQUEST['orden'])){ //si no viene del formulario, no existe array POST
	$_REQUEST['orden'] = 'SHOWALL';
}
		switch ($_REQUEST['orden']){
			case 'ADD':
				if(tienePermisosPara('NOTATR', 'ADD')){
					if(!$_POST){//Si GET
						$muestraADD = new Notatrabajo_ADD();//Mostrar vista add
					}else{
						$trabajo = get_data_form();//Si post cogemos usuario
						$respuesta = $trabajo->ADD();//Y lo añadimos
						new Mensaje($respuesta, '../Controllers/Notatrabajo_CONTROLLER.php');// y a ver qué ha pasado en la BD
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
				
			case 'EDIT':
				if(tienePermisosPara('NOTATR', 'EDIT')){
					if(!$_POST){//Si GET
						$trabajo = new NotaTrabajo($_REQUEST['login'],$_REQUEST['IdTrabajo'],'');//Editar usuario seleccionado
						$trabajo->_getDatosGuardados();//Rellenar con los datos de la BD
						new Notatrabajo_EDIT($trabajo);//Mostrar vista
					}else{
						$trabajo = get_data_form();//Coger datos
						$respuesta = $trabajo->EDIT();//Actualizarlos
						new Mensaje($respuesta, '../Controllers/Notatrabajo_CONTROLLER.php');//A ver que pasa con la BD, qué intrigante
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
				
			case 'SEARCH':
				if(tienePermisosPara('NOTATR', 'SEARCH')){
					if(!$_POST){//Si GET
						$muestraSEARCH = new Notatrabajo_SEARCH();//Mostrar vista buscadora
					}else{
						$trabajo = get_data_form();//Creamos un usuario con los datos introducidos (que no insertarlo en la BD)
						$respuesta = $trabajo->SEARCH();//Buscamos los datos que se parezcan a los introducidos
						new Notatrabajo_SHOWALL($respuesta, '');//Mostramos todos los datos recuperados de la BD (showall muestra todos los datos que se le pasan)
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
				
			case 'DELETE':
				if(tienePermisosPara('NOTATR', 'DELETE')){
					if(!$_POST){//Si GET
						$trabajo = new NotaTrabajo($_REQUEST['login'],$_REQUEST['IdTrabajo'],'');//Coger usuario guardado a eliminar
						$trabajo->_getDatosGuardados();//Rellenar datos
						new Notatrabajo_DELETE($trabajo);//Mostrar vissta 
					}else{//Si confirma borrado llega por post
						$trabajo = new NotaTrabajo($_REQUEST['login'],$_REQUEST['IdTrabajo'],'');//Clave
						$respuesta = $trabajo->DELETE();//Borrar usuario con dicha clave
						new Mensaje($respuesta, '../Controllers/Notatrabajo_CONTROLLER.php');//A ver qué pasa en la BD
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
				
			case 'SHOWCURRENT':
				if(tienePermisosPara('NOTATR', 'SHOWCU')){
					if(!$_POST){//Si GET
						$trabajo = new NotaTrabajo($_REQUEST['login'],$_REQUEST['IdTrabajo'],'');//Coger clave del usuario
						$respuesta = $trabajo->SHOWCURRENT();
						if(!is_string($respuesta)){//NO debería ser posible pedir un showcurrent de algo no existente pero si esp osible retornará un string, así que si no es un string es un usuario
							$trabajo->_getDatosGuardados();
							new Notatrabajo_SHOWCURRENT($trabajo);//Mostrar al usuario rellenado
						}else{//sino
							new Mensaje($respuesta, '../Controllers/Notatrabajo_CONTROLLER.php');//Mensaje de error, que hay muchos
						}
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
				
			case 'SHOWALL':
				if(tienePermisosPara('NOTATR', 'SHOWAL')){ //Si tiene permisos (ADMIN)
					$trabajo = new NotaTrabajo('','','');//No necesitamos usuario para buscar (pero sí para acceder a la BD)
					$respuesta = $trabajo->SHOWALL();//Todos los datos de la BD estarán aqúi
					new Notatrabajo_SHOWALL($respuesta, '');//Le pasamos todos los datos de la BD
				}else{ //Si no (USER, usuario regular)
					$trabajo = new NotaTrabajo('','','');//No necesitamos usuario para buscar (pero sí para acceder a la BD)
					$respuesta = $trabajo->SHOWALL_U();//Todos los datos de la BD estarán aqúi
					new Notatrabajo_SHOWALL($respuesta, '');//Le pasamos todos los datos de la BD
				}
				break;

			case 'GENOTAS':
				if(tienePermisosPara('NOTATR', 'GENOTA')){
					if(!$_POST){//Si GET
						$trabajo = new NotaTrabajo('','','');
						$respuesta = $trabajo->GENOTAS();
						new Mensaje($respuesta, '../Controllers/Notatrabajo_CONTROLLER.php'); //Mensaje confirmando o mensaje de error
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;

		}
?>