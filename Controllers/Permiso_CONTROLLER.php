<?php
/* Controller del permiso y las acciones que se le podrán realizar
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	session_start();
	include_once '../Functions/Autenticacion.php';
	if(!autenticado()){//No se puede verni aquí sin estar logeado
		header('Location: ../../index.php');
	}
	
	include '../Models/PERMISO.php';
	include '../Views/Permiso/Permiso_ADD.php';
	include '../Views/Permiso/Permiso_DELETE.php';
	include '../Views/Permiso/Permiso_SEARCH.php';
	include '../Views/Permiso/Permiso_SHOWALL.php';
	include '../Views/MESSAGE.php';
	
function get_data_form(){

	$IdGrupo = $_REQUEST['IdGrupo'];
	$IdFuncionalidad = $_REQUEST['IdFuncionalidad'];
	$IdAccion = $_REQUEST['IdAccion'];
	
	$permiso = new Permiso($IdGrupo,$IdFuncionalidad,$IdAccion);
 
	return $permiso;
}
	

if (!isset($_REQUEST['orden'])){ //si no viene del formulario, no existe array POST
	$_REQUEST['orden'] = 'SHOWALL';
}
		switch ($_REQUEST['orden']){
			//Asignar permiso
			case 'ADD':
				if(tienePermisosPara('PERMIS', 'ADD')){
					if(!$_POST){//Si GET
						$permiso = new Permiso('','',''); //Creamos un Permiso vacio para acceder a sus métodos
						$muestraADD = new Permiso_ADD($permiso->grupos(), $permiso->funcs(), $permiso->acciones(), $permiso->func_accion());//Mostrar vista add con las listas de grupos, funcionalidades y acciones
					}else{
						$permiso = get_data_form();//cogemos permiso
						$respuesta = $permiso->ADD();//Y lo añadimos
						new Mensaje($respuesta, '../Controllers/Permiso_CONTROLLER.php');// y a ver qué ha pasado en la BD
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;

			case 'SEARCH':
				if(tienePermisosPara('PERMIS', 'SEARCH')){
					if(!$_POST){//Si GET
						$muestraSEARCH = new Permiso_SEARCH();//Mostrar vista buscadora
					}else{
						$permiso = get_data_form();//Creamos un permiso con los datos introducidos (que no insertarlo en la BD)
						$respuesta = $permiso->SEARCH();//Buscamos los datos que se parezcan a los introducidos
						new Permiso_SHOWALL($respuesta, '');//Mostramos todos los datos recuperados de la BD (showall muestra todos los datos que se le pasan)
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;

			case 'DELETE':
				if(tienePermisosPara('PERMIS', 'DELETE')){
					if(!$_POST){//Si GET
						$permiso = new Permiso($_REQUEST['IdGrupo'],$_REQUEST['IdFuncionalidad'],$_REQUEST['IdAccion']);//Coger permiso guardado a eliminar
						new Permiso_DELETE($permiso);//Mostrar vista 
					}else{//Si confirma borrado llega por post
						$permiso = new Permiso($_REQUEST['IdGrupo'],$_REQUEST['IdFuncionalidad'],$_REQUEST['IdAccion']);//Clave
						$respuesta = $permiso->DELETE();//Borrar usuario con dicha clave
						new Mensaje($respuesta, '../Controllers/Permiso_CONTROLLER.php');//A ver qué pasa en la BD
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
				
			case 'SHOWALL':
				if(tienePermisosPara('PERMIS', 'SHOWAL')){
					$permiso = new Permiso('','','');//No necesitamos permiso para buscar (pero sí para acceder a la BD)
					$respuesta = $permiso->SHOWALL();//Todos los datos de la BD estarán aqúi
					new Permiso_SHOWALL($respuesta, '');//Le pasamos todos los datos de la BD
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
				
			default:
				if(tienePermisosPara('PERMIS', 'SHOWAL')){
					$permiso = new Permiso('','','');//No necesitamos permiso para buscar (pero sí para acceder a la BD)
					$respuesta = $permiso->SHOWALL();//Todos los datos de la BD estarán aqúi
					new Permiso_SHOWALL($respuesta, '');//Le pasamos todos los datos de la BD
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
		}
?>