<?php
/* Controller de GRUPO y las acciones que se le podrán realizar 
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	session_start();
	include_once '../Functions/Autenticacion.php';
	if(!autenticado()){//No se puede verni aquí sin estar logeado
		header('Location: ../../index.php');
	}
	
	include '../Models/GRUPO.php';
	include '../Views/Grupo/Grupo_ADD.php';
	include '../Views/Grupo/Grupo_EDIT.php';
	include '../Views/Grupo/Grupo_SEARCH.php';
	include '../Views/Grupo/Grupo_DELETE.php';
	include '../Views/Grupo/Grupo_SHOWCURRENT.php';
	include '../Views/Grupo/Grupo_SHOWALL.php';
	include '../Views/MESSAGE.php';
	
function get_data_form(){

	$IdGrupo = $_REQUEST['IdGrupo'];
	$NombreGrupo = $_REQUEST['NombreGrupo'];
	$DescripGrupo = $_REQUEST['DescripGrupo'];
	

	$grupo = new Grupo($IdGrupo, $NombreGrupo, $DescripGrupo);
 
	return $grupo;
}
	

if (!isset($_REQUEST['orden'])){ //si no viene del formulario, no existe array POST
	$_REQUEST['orden'] = 'SHOWALL';
}
		switch ($_REQUEST['orden']){
			case 'ADD':
				if(tienePermisosPara('GRUPO', 'ADD')){
					if(!$_POST){//Si GET
						$muestraADD = new Grupo_ADD();//Mostrar vista add
					}else{
						$grupo = get_data_form();//Si post cogemos grupo
						$respuesta = $grupo->ADD();//Y lo añadimos
						new Mensaje($respuesta, '../Controllers/Grupo_CONTROLLER.php');// y a ver qué ha pasado en la BD
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
				
			case 'EDIT':
				if(tienePermisosPara('GRUPO', 'EDIT')){
					if(!$_POST){//Si GET
						$grupo = new Grupo($_REQUEST['IdGrupo'],'','');//Editar grupo seleccionado
						$grupo->_getDatosGuardados();//Rellenar con los datos de la BD
						new Grupo_EDIT($grupo);//Mostrar vista
					}else{
						$grupo = get_data_form();//Coger datos
						$respuesta = $grupo->EDIT();//Actualizarlos
						new Mensaje($respuesta, '../Controllers/Grupo_CONTROLLER.php');//A ver que pasa con la BD, qué intrigante
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
				
			case 'SEARCH':
				if(tienePermisosPara('GRUPO', 'SEARCH')){
					if(!$_POST){//Si GET
						$muestraSEARCH = new Grupo_SEARCH();//Mostrar vista buscadora
					}else{
						$grupo = get_data_form();//Creamos un usuario con los datos introducidos (que no insertarlo en la BD)
						$respuesta = $grupo->SEARCH();//Buscamos los datos que se parezcan a los introducidos
						new Grupo_SHOWALL($respuesta, '');//Mostramos todos los datos recuperados de la BD (showall muestra todos los datos que se le pasan)
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
				
			case 'DELETE':
				if(tienePermisosPara('GRUPO', 'DELETE')){
					if(!$_POST){//Si GET
						$grupo = new Grupo($_REQUEST['IdGrupo'],'','');//Coger grupo guardado a eliminar
						$grupo->_getDatosGuardados();//Rellenar datos
						new Grupo_DELETE($grupo);//Mostrar vissta 
					}else{//Si confirma borrado llega por post
						$grupo = new Grupo($_POST['IdGrupo'],'','');//Clave
						$respuesta = $grupo->DELETE();//Borrar usuario con dicha clave
						new Mensaje($respuesta, '../Controllers/Grupo_CONTROLLER.php');//A ver qué pasa en la BD
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
				
			case 'SHOWCURRENT':
				if(tienePermisosPara('GRUPO', 'SHOWCU')){
					if(!$_POST){//Si GET
						$grupo = new Grupo($_REQUEST['IdGrupo'],'','');//Coger clave del usuario
						$respuesta = $grupo->SHOWCURRENT();
						if(!is_string($respuesta)){//NO debería ser posible pedir un showcurrent de algo no existente pero si esp osible retornará un string, así que si no es un string es un usuario
							$grupo->_getDatosGuardados();
							new Grupo_SHOWCURRENT($grupo);//Mostrar al usuario rellenado
						}else{//sino
							new Mensaje($respuesta, '../Controllers/Grupo_CONTROLLER.php');//Mensaje de error, que hay muchos
						}
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
				
			case 'SHOWALL':
				if(tienePermisosPara('GRUPO', 'SHOWAL')){
					$grupo = new Grupo('','','');//No necesitamos usuario para buscar (pero sí para acceder a la BD)
					$respuesta = $grupo->SHOWALL();//Todos los datos de la BD estarán aqúi
					new Grupo_SHOWALL($respuesta, '');//Le pasamos todos los datos de la BD
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
				
			case 'ASIGNAR':
				if(tienePermisosPara('GRUPO', 'ASIGNA')){
					$grupo = new Grupo($_REQUEST['IdGrupo'],'','');
					if(!isset($_REQUEST['IdFuncionalidad']) || !isset($_REQUEST['IdAccion'])){
						new Mensaje('No se ha indicado funcionalidad o accion', '../Controllers/Grupo_CONTROLLER.php');//Mensaje de el grupo no existe
					}else{
						$respuesta = $grupo->DARPERMISO($_REQUEST['IdFuncionalidad'], $_REQUEST['IdAccion']);//Cambiar el grupo del usuario
						new Mensaje($respuesta, '../Controllers/Grupo_CONTROLLER.php');
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
				
			CASE 'DESASIGNAR':
				if(tienePermisosPara('GRUPO', 'DESASI')){
					$grupo = new Grupo($_REQUEST['IdGrupo'],'','');
					if(!isset($_REQUEST['IdFuncionalidad']) || !isset($_REQUEST['IdAccion'])){
						new Mensaje('No se ha indicado funcionalidad o accion', '../Controllers/Grupo_CONTROLLER.php');//Mensaje de el grupo no existe
					}else{
						$respuesta = $grupo->QUITARPERMISO($_REQUEST['IdFuncionalidad'], $_REQUEST['IdAccion']);//Cambiar el grupo del usuario
						new Mensaje($respuesta, '../Controllers/Grupo_CONTROLLER.php');
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
				
			default:
				if(tienePermisosPara('GRUPO', 'SHOWAL')){
					$grupo = new Grupo('','','');//No necesitamos usuario para buscar (pero sí para acceder a la BD)
					$respuesta = $grupo->SHOWALL();//Todos los datos de la BD estarán aqúi
					new Grupo_SHOWALL($respuesta, '');//Le pasamos todos los datos de la BD
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
		}
?>