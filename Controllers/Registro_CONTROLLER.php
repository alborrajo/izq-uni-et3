<?php
/*Aquí se podrá registrar una persona
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/

session_start();
include_once '../Functions/Autenticacion.php';
if(autenticado()){//Si está autenticado, no pinta nada aquí
	header('Location: ../index.php');
}

function get_data_form(){

	$login = $_REQUEST['login'];
	$password = $_REQUEST['password'];
	$DNI = $_REQUEST['DNI'];
	$Nombre = $_REQUEST['Nombre'];
	$Apellidos = $_REQUEST['Apellidos'];
	$Correo = $_REQUEST['Correo'];
	$Direccion = $_REQUEST['Direccion'];
	$Telefono = $_REQUEST['Telefono'];

	$usuario = new Usuario($login, $password, $DNI, $Nombre, $Apellidos, $Correo, $Direccion, $Telefono);
 
	return $usuario;
}

if(!$_POST){//Si no se ha llegado mediante el formulario de vistaRegistro
	include '../Views/Usuario/Usuario_REGISTRO.php';//Incluir vistaRegistro
	new Usuario_REGISTRO();//Mostrar vistaRegistro	
}else{//Sino
	include '../Models/USUARIO.php';
	include '../Views/MESSAGE.php';
	$usuario = get_data_form();//Se crea un usuario
	$respuesta = $usuario->REGISTRAR();//Se registra
	
	if($respuesta != 'Usuario añadido correctamente'){//Si es un mensaje de error
		new Mensaje($respuesta, '../Controllers/Registro_CONTROLLER.php');//Se muestra
	}else{//Sino
		new Mensaje($respuesta, '../index.php');//Mensaje de registro
	}
}
?>