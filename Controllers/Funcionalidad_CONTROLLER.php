<?php
/* Controller de FUNCIONALIDADes y las acciones que se le podrán realizar
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	session_start();
	include_once '../Functions/Autenticacion.php';
	if(!autenticado()){//No se puede verni aquí sin estar logeado
		header('Location: ../index.php');
	}
	
	include '../Models/FUNCIONALIDAD.php';
	include '../Views/Funcionalidad/Funcionalidad_ADD.php';
	include '../Views/Funcionalidad/Funcionalidad_EDIT.php';
	include '../Views/Funcionalidad/Funcionalidad_SEARCH.php';
	include '../Views/Funcionalidad/Funcionalidad_DELETE.php';
	include '../Views/Funcionalidad/Funcionalidad_SHOWCURRENT.php';
	include '../Views/Funcionalidad/Funcionalidad_SHOWALL.php';
	include '../Views/MESSAGE.php';
	
function get_data_form(){

	$IdFuncionalidad = $_REQUEST['IdFuncionalidad'];
	$NombreFuncionalidad = $_REQUEST['NombreFuncionalidad'];
	$DescripFuncionalidad = $_REQUEST['DescripFuncionalidad'];

	$funcionalidad = new Funcionalidad($IdFuncionalidad, $NombreFuncionalidad, $DescripFuncionalidad);
 
	return $funcionalidad;
}
	

if (!isset($_REQUEST['orden'])){ //si no viene del formulario, no existe array POST
	$_REQUEST['orden'] = 'SHOWALL';
}
		switch ($_REQUEST['orden']){
			case 'ADD':
				if(tienePermisosPara('FUNCIO', 'ADD')){
					if(!$_POST){//Si GET
						$muestraADD = new Funcionalidad_ADD();//Mostrar vista add
					}else{
						$funcionalidad = get_data_form();//Si post cogemos usuario
						$respuesta = $funcionalidad->ADD();//Y lo añadimos
						new Mensaje($respuesta, '../Controllers/Funcionalidad_CONTROLLER.php');// y a ver qué ha pasado en la BD
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
			break;
				
			case 'EDIT':
				if(tienePermisosPara('FUNCIO', 'EDIT')){
					if(!$_POST){//Si GET
						$funcionalidad = new Funcionalidad($_REQUEST['IdFuncionalidad'],'','');//Editar usuario seleccionado
						$funcionalidad->_getDatosGuardados();//Rellenar con los datos de la BD
						new Funcionalidad_EDIT($funcionalidad);//Mostrar vista
					}else{
						$funcionalidad = get_data_form();//Coger datos
						$respuesta = $funcionalidad->EDIT();//Actualizarlos
						new Mensaje($respuesta, '../Controllers/Funcionalidad_CONTROLLER.php');//A ver que pasa con la BD, qué intrigante
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
			break;
				
			case 'SEARCH':
				if(tienePermisosPara('FUNCIO', 'SEARCH')){
					if(!$_POST){//Si GET
						$muestraSEARCH = new Funcionalidad_SEARCH();//Mostrar vista buscadora
					}else{
						$funcionalidad = get_data_form();//Creamos un usuario con los datos introducidos (que no insertarlo en la BD)
						$respuesta = $funcionalidad->SEARCH();//Buscamos los datos que se parezcan a los introducidos
						new Funcionalidad_SHOWALL($respuesta, '');//Mostramos todos los datos recuperados de la BD (showall muestra todos los datos que se le pasan)
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
				
			case 'DELETE':
				if(tienePermisosPara('FUNCIO', 'DELETE')){
					if(!$_POST){//Si GET
						$funcionalidad = new Funcionalidad($_REQUEST['IdFuncionalidad'],'','');//Coger usuario guardado a eliminar
						$funcionalidad->_getDatosGuardados();//Rellenar datos
						new Funcionalidad_DELETE($funcionalidad);//Mostrar vissta 
					}else{//Si confirma borrado llega por post
						$funcionalidad = new Funcionalidad($_POST['IdFuncionalidad'],'','');//Clave
						$respuesta = $funcionalidad->DELETE();//Borrar usuario con dicha clave
						new Mensaje($respuesta, '../Controllers/Funcionalidad_CONTROLLER.php');//A ver qué pasa en la BD
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
				
			case 'SHOWCURRENT':
				if(tienePermisosPara('FUNCIO', 'SHOWCU')){
					if(!$_POST){//Si GET
						$funcionalidad = new Funcionalidad($_REQUEST['IdFuncionalidad'],'','');//Coger clave del usuario
						$respuesta = $funcionalidad->SHOWCURRENT();
						if(!is_string($respuesta)){//NO debería ser posible pedir un showcurrent de algo no existente pero si esp osible retornará un string, así que si no es un string es un usuario
							$funcionalidad->_getDatosGuardados();
							new Funcionalidad_SHOWCURRENT($funcionalidad);//Mostrar al usuario rellenado
						}else{//sino
							new Mensaje($respuesta, '../Controllers/Funcionalidad_CONTROLLER.php');//Mensaje de error, que hay muchos
						}
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
				
			case 'SHOWALL':
				if(tienePermisosPara('FUNCIO', 'SHOWAL')){
					$funcionalidad = new Funcionalidad('','','');//No necesitamos usuario para buscar (pero sí para acceder a la BD)
					$respuesta = $funcionalidad->SHOWALL();//Todos los datos de la BD estarán aqúi
					new Funcionalidad_SHOWALL($respuesta, '');//Le pasamos todos los datos de la BD
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
				
			case 'ASIGNAR':
				if(tienePermisosPara('FUNCIO', 'ASIGNA')){
					$funcionalidad = new Funcionalidad($_REQUEST['IdFuncionalidad'],'','');//Coger clave del usuario
					if(!isset($_REQUEST['IdAccion'])){
						new Mensaje('No se ha indicado accion', '../Controllers/Funcionalidad_CONTROLLER.php');//Mensaje de el grupo no existe
					}else{
						$respuesta = $funcionalidad->ASIGNARACCION($_REQUEST['IdAccion']);//Cambiar el grupo del usuario
						new Mensaje($respuesta, '../Controllers/Funcionalidad_CONTROLLER.php');//Mensaje de cambiar el grupo, tanto sea fallido como no
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
			
			case 'DESASIGNAR':
				if(tienePermisosPara('FUNCIO', 'DESASI')){
					$funcionalidad = new Funcionalidad($_REQUEST['IdFuncionalidad'],'','');//Coger clave del usuario
					if(!isset($_REQUEST['IdAccion'])){
						new Mensaje('No se ha indicado accion', '../Controllers/Funcionalidad_CONTROLLER.php');//Mensaje de el grupo no existe
					}else{
						$respuesta = $funcionalidad->DESASIGNARACCION($_REQUEST['IdAccion']);//Cambiar el grupo del usuario
						new Mensaje($respuesta, '../Controllers/Funcionalidad_CONTROLLER.php');//Mensaje de cambiar el grupo, tanto sea fallido como no					
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
				
			default:
				if(tienePermisosPara('FUNCIO', 'SHOWAL')){
					$funcionalidad = new Funcionalidad('','','');//No necesitamos usuario para buscar (pero sí para acceder a la BD)
					$respuesta = $funcionalidad->SHOWALL();//Todos los datos de la BD estarán aqúi
					new Funcionalidad_SHOWALL($respuesta, '');//Le pasamos todos los datos de la BD
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
		}
?>