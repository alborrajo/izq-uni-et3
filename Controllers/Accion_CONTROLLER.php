<?php
/* Controller del ACCION y las acciones que se le podrán realizar
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	session_start();
	include_once '../Functions/Autenticacion.php';
	if(!autenticado()){
		header('Location: ../index.php');
	}
	
	include '../Models/ACCION.php';
	include '../Views/Accion/Accion_ADD.php';
	include '../Views/Accion/Accion_EDIT.php';
	include '../Views/Accion/Accion_SEARCH.php';
	include '../Views/Accion/Accion_DELETE.php';
	include '../Views/Accion/Accion_SHOWCURRENT.php';
	include '../Views/Accion/Accion_SHOWALL.php';
	include '../Views/MESSAGE.php';
	
function get_data_form(){

	$IdAccion = $_REQUEST['IdAccion'];
	$NombreAccion = $_REQUEST['NombreAccion'];
	$DescripAccion = $_REQUEST['DescripAccion'];

	$accion = new Accion($IdAccion, $NombreAccion, $DescripAccion);
 
	return $accion;
}
	

if (!isset($_REQUEST['orden'])){ //si no viene del formulario, no existe array POST
	$_REQUEST['orden'] = 'SHOWALL';
}
		switch ($_REQUEST['orden']){
			case 'ADD':
				if(tienePermisosPara('ACCION', 'ADD')){
					if(!$_POST){//Si GET
						$muestraADD = new Accion_ADD();//Mostrar vista add
					}else{
						$accion = get_data_form();//Si post cogemos accion
						$respuesta = $accion->ADD();//Y lo añadimos
						new Mensaje($respuesta, '../Controllers/Accion_CONTROLLER.php');// y a ver qué ha pasado en la BD
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
				
			case 'EDIT':
				if(tienePermisosPara('ACCION', 'EDIT')){
					if(!$_POST){//Si GET
						$accion = new Accion($_REQUEST['IdAccion'],'', '');//Editar accion seleccionado
						$accion->_getDatosGuardados();//Rellenar con los datos de la BD
						new Accion_EDIT($accion);//Mostrar vista
					}else{
						$accion = get_data_form();//Coger datos
						$respuesta = $accion->EDIT();//Actualizarlos
						new Mensaje($respuesta, '../Controllers/Accion_CONTROLLER.php');//A ver que pasa con la BD, qué intrigante
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
				
			case 'SEARCH':
				if(tienePermisosPara('ACCION', 'SEARCH')){
					if(!$_POST){//Si GET
						$muestraSEARCH = new Accion_SEARCH();//Mostrar vista buscadora
					}else{
						$accion = get_data_form();//Creamos un accion con los datos introducidos (que no insertarlo en la BD)
						$respuesta = $accion->SEARCH();//Buscamos los datos que se parezcan a los introducidos
						new Accion_SHOWALL($respuesta, '');//Mostramos todos los datos recuperados de la BD (showall muestra todos los datos que se le pasan)
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
				
			case 'DELETE':
				if(tienePermisosPara('ACCION', 'DELETE')){
					if(!$_POST){//Si GET
						$accion = new Accion($_REQUEST['IdAccion'],'', '');//Coger accion guardado a eliminar
						$accion->_getDatosGuardados();//Rellenar datos
						new Accion_DELETE($accion);//Mostrar vissta 
					}else{//Si confirma borrado llega por post
						$accion = new Accion($_POST['IdAccion'],'','','','','','','','','');//Clave
						$respuesta = $accion->DELETE();//Borrar accion con dicha clave
						new Mensaje($respuesta, '../Controllers/Accion_CONTROLLER.php');//A ver qué pasa en la BD
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
				
			case 'SHOWCURRENT':
				if(tienePermisosPara('ACCION', 'SHOWCU')){
					if(!$_POST){//Si GET
						$accion = new Accion($_REQUEST['IdAccion'],'', '');//Coger clave del accion
						$respuesta = $accion->SHOWCURRENT();
						if(!is_string($respuesta)){//NO debería ser posible pedir un showcurrent de algo no existente pero si esp osible retornará un string, así que si no es un string es un accion
							$accion->_getDatosGuardados();
							new Accion_SHOWCURRENT($accion);//Mostrar al accion rellenado
						}else{//sino
							new Mensaje($respuesta, '../Controllers/Accion_CONTROLLER.php');//Mensaje de error, que hay muchos
						}
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
				
			case 'SHOWALL':
				if(tienePermisosPara('ACCION', 'SHOWAL')){
					$accion = new Accion('','','');//No necesitamos accion para buscar (pero sí para acceder a la BD)
					$respuesta = $accion->SHOWALL();//Todos los datos de la BD estarán aqúi
					new Accion_SHOWALL($respuesta, '');//Le pasamos todos los datos de la BD
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
				
			default:
				if(tienePermisosPara('ACCION', 'SHOWAL')){
					$accion = new Accion('','','');//No necesitamos accion para buscar (pero sí para acceder a la BD)
					$respuesta = $accion->SHOWALL();//Todos los datos de la BD estarán aqúi
					new Accion_SHOWALL($respuesta, '');//Le pasamos todos los datos de la BD
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
		}
?>