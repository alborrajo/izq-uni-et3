<?php
/* Controller de QA y las acciones que se le podrán realizar 
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	session_start();
	include_once '../Functions/Autenticacion.php';
	if(!autenticado()){//No se puede verni aquí sin estar logeado
		header('Location: ../../index.php');
	}
	
	include '../Models/QA.php';
	include '../Views/QA/QA_ADD.php';
	include '../Views/QA/QA_EDIT.php';
	include '../Views/QA/QA_SEARCH.php';
	include '../Views/QA/QA_DELETE.php';
	include '../Views/QA/QA_SHOWCURRENT.php';
	include '../Views/QA/QA_SHOWALL.php';
	include '../Views/MESSAGE.php';
	
function get_data_form(){

	$IdTrabajo = $_REQUEST['IdTrabajo'];
	$LoginEvaluador = $_REQUEST['LoginEvaluador'];
	$LoginEvaluado = $_REQUEST['LoginEvaluado'];
	$AliasEvaluado = $_REQUEST['AliasEvaluado'];
	

	$qa = new QA($IdTrabajo, $LoginEvaluador, $LoginEvaluado, $AliasEvaluado);
 
	return $qa;
}
	

if (!isset($_REQUEST['orden'])){ //si no viene del formulario, no existe array POST
	$_REQUEST['orden'] = 'SHOWALL';
}
		switch ($_REQUEST['orden']){
			case 'ADD':
				if(tienePermisosPara('QA', 'ADD')){
					if(!$_POST){//Si GET
						$muestraADD = new QA_ADD();//Mostrar vista add
					}else{
						$qa = get_data_form();//Si post cogemos grupo
						$LoginEvaluador = $qa->_getLoginEvaluador();
						$LoginEvaluado = $qa->_getLoginEvaluado();
						if ($LoginEvaluado == $LoginEvaluador) {
							new Mensaje('El LoginEvaluador no puede ser igual que LoginEvaluado', '../Controllers/QA_CONTROLLER.php');
						}
						else{
							$respuesta = $qa->ADD();//Y lo añadimos
							new Mensaje($respuesta, '../Controllers/QA_CONTROLLER.php');// y a ver qué ha pasado en la BD
						}
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
				
			case 'EDIT':
				if(tienePermisosPara('QA', 'EDIT')){
					if(!$_POST){//Si GET
						$qa = new QA($_REQUEST['IdTrabajo'],$_REQUEST['LoginEvaluador'],'',$_REQUEST['AliasEvaluado']);//Editar grupo seleccionado
						$qa->_getDatosGuardados();//Rellenar con los datos de la BD
						new QA_EDIT($qa);//Mostrar vista
					}else{
						$qa = get_data_form();//Coger datos
						$respuesta = $qa->EDIT();//Actualizarlos
						new Mensaje($respuesta, '../Controllers/QA_CONTROLLER.php');//A ver que pasa con la BD, qué intrigante
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
				
			case 'SEARCH':
				if(tienePermisosPara('QA', 'SEARCH')){
					if(!$_POST){//Si GET
						$muestraSEARCH = new QA_SEARCH();//Mostrar vista buscadora
					}else{
						$qa = get_data_form();//Creamos un usuario con los datos introducidos (que no insertarlo en la BD)
						$respuesta = $qa->SEARCH();//Buscamos los datos que se parezcan a los introducidos
						new QA_SHOWALL($respuesta, '');//Mostramos todos los datos recuperados de la BD (showall muestra todos los datos que se le pasan)
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
				
			case 'DELETE':
				if(tienePermisosPara('QA', 'DELETE')){
					if(!$_POST){//Si GET
						$qa = new QA($_REQUEST['IdTrabajo'],$_REQUEST['LoginEvaluador'],'',$_REQUEST['AliasEvaluado']);//Coger grupo guardado a eliminar
						$qa->_getDatosGuardados();//Rellenar datos
						new QA_DELETE($qa);//Mostrar vissta 
					}else{//Si confirma borrado llega por post
						$qa = new QA($_POST['IdTrabajo'],$_POST['LoginEvaluador'],'',$_POST['AliasEvaluado']);//Clave
						$respuesta = $qa->DELETE();//Borrar usuario con dicha clave
						new Mensaje($respuesta, '../Controllers/QA_CONTROLLER.php');//A ver qué pasa en la BD
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;
				
			case 'SHOWCURRENT':
				if(tienePermisosPara('QA', 'SHOWCU')){
					if(!$_POST){//Si GET
						$qa = new QA($_REQUEST['IdTrabajo'],$_REQUEST['LoginEvaluador'],'',$_REQUEST['AliasEvaluado']);//Coger clave del usuario
						$respuesta = $qa->SHOWCURRENT();
						if(!is_string($respuesta)){//NO debería ser posible pedir un showcurrent de algo no existente pero si esp osible retornará un string, así que si no es un string es un usuario
							$qa->_getDatosGuardados();
							new QA_SHOWCURRENT($qa);//Mostrar al usuario rellenado
						}else{//sino
							new Mensaje($respuesta, '../Controllers/QA_CONTROLLER.php');//Mensaje de error, que hay muchos
						}
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
				break;

			case 'SHOWALL':
				if(tienePermisosPara('QA', 'SHOWAL')){ //Si tiene permisos (ADMIN)
					$qa = new QA('','','','');//No necesitamos usuario para buscar (pero sí para acceder a la BD)
					$respuesta = $qa->SHOWALL();//Todos los datos de la BD estarán aqúi
					new QA_SHOWALL($respuesta, '');//Le pasamos todos los datos de la BD

				}else{ //Si no tiene permisos (USER)
					$qa = new QA('',$_SESSION["login"],'',''); //Objeto QA con el login del usuario como dato
					$respuesta = $qa->SHOWALL_USER(); //Se buscan las QA asignadas al usuario logeado
					new QA_SHOWALL($respuesta, ''); //Le pasamos los datos de la BD
				}
				break;

		}
?>