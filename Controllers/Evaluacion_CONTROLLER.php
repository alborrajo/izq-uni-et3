<?php
/* Controller de las evaluaciones y las acciones que se le podrán realizar
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	session_start();
	include_once '../Functions/Autenticacion.php';
	if(!autenticado()){//No se puede venir aquí sin estar logeado
		header('Location: ../../index.php');
	}
	
	include '../Models/EVALUACION.php';
	include '../Views/Evaluacion/Evaluacion_ADD.php';
	include '../Views/Evaluacion/Evaluacion_EDIT.php';
	include '../Views/Evaluacion/Evaluacion_SEARCH.php';
	include '../Views/Evaluacion/Evaluacion_DELETE.php';
	include '../Views/Evaluacion/Evaluacion_SHOWCURRENT.php';
	include '../Views/Evaluacion/Evaluacion_SHOWALL.php';
	include '../Views/MESSAGE.php';
	
function get_data_form(){
	$IdTrabajo = $_REQUEST['IdTrabajo'];
	$AliasEvaluado = $_REQUEST['AliasEvaluado'];
	$LoginEvaluador = $_REQUEST['LoginEvaluador'];
	$IdHistoria = $_REQUEST['IdHistoria'];
	$CorrectoA = $_REQUEST['CorrectoA'];
	$ComenIncorrectoA = $_REQUEST['ComenIncorrectoA'];
	$CorrectoP = $_REQUEST['CorrectoP'];
	$ComentIncorrectoP = $_REQUEST['ComentIncorrectoP'];
	$OK = $_REQUEST['OK'];
	
	$evaluacion = new Evaluacion($IdTrabajo, $AliasEvaluado, $LoginEvaluador, $IdHistoria, $CorrectoA, $ComenIncorrectoA, $CorrectoP, $ComentIncorrectoP, $OK);
 
	return $evaluacion;
}
	

if (!isset($_REQUEST['orden'])){ //si no viene del formulario, no existe array POST
	$_REQUEST['orden'] = 'SHOWALL';
}
		switch ($_REQUEST['orden']){
			case 'ADD':
				if(tienePermisosPara('EVALUA', 'ADD')){
					if(!$_POST){//Si GET
						$muestraADD = new Evaluacion_ADD();//Mostrar vista add
					}else{
						$evaluacion = get_data_form();//Si post cogemos usuario
						$respuesta = $evaluacion->ADD();//Y lo añadimos
						new Mensaje($respuesta, '../Controllers/Evaluacion_CONTROLLER.php');// y a ver qué ha pasado en la BD
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
			break;
				
			case 'EDIT':
				if(tienePermisosPara('EVALUA', 'EDIT')){ //Si tiene permisos (ADMIN)
					if(!$_POST){//Si GET
						$evaluacion = new Evaluacion($_REQUEST['IdTrabajo'], $_REQUEST['AliasEvaluado'], $_REQUEST['LoginEvaluador'], $_REQUEST['IdHistoria'],'','','','','');//Editar evaluacion seleccionada
						$evaluacion->_getDatosGuardados();//Rellenar con los datos de la BD
						new Evaluacion_EDIT($evaluacion);//Mostrar vista
					}else{
						$evaluacion = get_data_form();//Coger datos
						$respuesta = $evaluacion->EDIT();//Actualizarlos
						new Mensaje($respuesta, '../Controllers/Evaluacion_CONTROLLER.php');//A ver que pasa con la BD, qué intrigante
					}
				}else{ //Si no tiene (USER regular)
					if(!$_POST){//Si GET
						$evaluacion = new Evaluacion($_REQUEST['IdTrabajo'], $_REQUEST['AliasEvaluado'], $_SESSION['login'], $_REQUEST['IdHistoria'],'','','','','');//Formulario de editar evaluacion seleccionada, asegurandonos de que lo mostrado corresponda a una evaluacion del usuario logeado
						$evaluacion->_getDatosGuardados();//Rellenar con los datos de la BD
						new Evaluacion_EDIT($evaluacion);//Mostrar vista
					}else{
						$evaluacion = new Evaluacion($_REQUEST['IdTrabajo'], $_REQUEST['AliasEvaluado'], $_SESSION['login'], $_REQUEST['IdHistoria'],$_REQUEST['CorrectoA'],$_REQUEST['ComenIncorrectoA'],'','','');//Edición asegurandonos de que lo mostrado corresponda a una evaluacion del usuario logeado
						
						$respuesta = $evaluacion->EDIT_USER();//Actualizarlos
						new Mensaje($respuesta, '../Controllers/Evaluacion_CONTROLLER.php?IdTrabajo='.$_REQUEST['IdTrabajo'].'&AliasEvaluado='.$_REQUEST['AliasEvaluado']);//A ver que pasa con la BD, qué intrigante
					}
				}
			break;
				
			case 'SEARCH':
				if(tienePermisosPara('EVALUA', 'SEARCH')){			
					if(!$_POST){//Si GET
						$muestraSEARCH = new Evaluacion_SEARCH();//Mostrar vista buscadora
					}else{
						$evaluacion = get_data_form();//Creamos un obj evaluacion con los datos introducidos (que no insertarlo en la BD)
						$respuesta = $evaluacion->SEARCH();//Buscamos los datos que se parezcan a los introducidos
						new Evaluacion_SHOWALL($respuesta, array('','IdTrabajo', 'AliasEvaluado', 'LoginEvaluador', 'IdHistoria', 'CorrectoA', 'ComenIncorrectoA', 'CorrectoP', 'ComentIncorrectoP', 'OK', 'EDIT', 'DELETE', 'SHOWCURRENT'));//Mostramos todos los datos recuperados de la BD (showall muestra todos los datos que se le pasan)
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
			break;
				
			case 'DELETE':
				if(tienePermisosPara('EVALUA', 'DELETE')){		
					if(!$_POST){//Si GET
						$evaluacion = new Evaluacion($_REQUEST['IdTrabajo'], $_REQUEST['AliasEvaluado'], $_REQUEST['LoginEvaluador'], $_REQUEST['IdHistoria'],'','','','','');//Coger evaluacion guardada a eliminar
						$evaluacion->_getDatosGuardados();//Rellenar datos
						new Evaluacion_DELETE($evaluacion);//Mostrar vissta 
					}else{//Si confirma borrado llega por post
						$evaluacion = new Evaluacion($_REQUEST['IdTrabajo'], $_REQUEST['AliasEvaluado'], $_REQUEST['LoginEvaluador'], $_REQUEST['IdHistoria'],'','','','','');//Clave
						$respuesta = $evaluacion->DELETE();//Borrar evaluacion con dicha clave
						new Mensaje($respuesta, '../Controllers/Evaluacion_CONTROLLER.php');//A ver qué pasa en la BD
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
			break;
				
			case 'SHOWCURRENT':
				if(tienePermisosPara('EVALUA', 'SHOWCU')){
					if(!$_POST){//Si GET
						$evaluacion = new Evaluacion($_REQUEST['IdTrabajo'], $_REQUEST['AliasEvaluado'], $_REQUEST['LoginEvaluador'], $_REQUEST['IdHistoria'],'','','','','');//Coger clave
						$respuesta = $evaluacion->SHOWCURRENT();
						if(!is_string($respuesta)){//NO debería ser posible pedir un showcurrent de algo no existente pero si esp osible retornará un string, así que si no es un string es un usuario
							$evaluacion->_getDatosGuardados();
							new Evaluacion_SHOWCURRENT($evaluacion);//Mostrar al usuario rellenado
						}else{//sino
							new Mensaje($respuesta, '../Controllers/Evaluacion_CONTROLLER.php');//Mensaje de error, que hay muchos
						}
					}
				}else{
					new Mensaje('Permisos insuficientes', '../index.php');
				}
			break;
				
			case 'SHOWALL':
				if(tienePermisosPara('EVALUA', 'SHOWAL')){ //Si tiene permisos (ADMIN)
					$evaluacion = new Evaluacion('','','','','','','','','');//No necesitamos evaluacion para buscar (pero sí para acceder a la BD)
					$respuesta = $evaluacion->SHOWALL();//Todos los datos de la BD estarán aqúi
					new Evaluacion_SHOWALL($respuesta, array('','IdTrabajo', 'AliasEvaluado', 'LoginEvaluador', 'IdHistoria', 'CorrectoA', 'ComenIncorrectoA', 'CorrectoP', 'ComentIncorrectoP', 'OK', 'EDIT', 'DELETE', 'SHOWCURRENT'));//Le pasamos todos los datos de la BD
				}else{ //Si no (USER regular)									
					if(!isset($_REQUEST['IdTrabajo']) || !isset($_REQUEST['AliasEvaluado'])){
						new Mensaje('Permisos insuficientes', '../index.php');
					}
					$evaluacion = new Evaluacion($_REQUEST['IdTrabajo'], $_REQUEST['AliasEvaluado'], $_SESSION['login'],'','','','','',''); //Objeto Evaluacion con los datos del showall de QA
					$respuesta = $evaluacion->SEARCH(); //Busqueda de todas las tuplas con esos datos
					new Evaluacion_SHOWALL($respuesta, array('','IdTrabajo', 'AliasEvaluado', 'LoginEvaluador', 'IdHistoria', 'CorrectoA', 'ComenIncorrectoA', 'EDIT')); //Muestra de datos
				}
			break;
			
			case 'CORRECCION':
				if(tienePermisosPara('EVALUA','CORRECCION')){/*Por si se queire que el root acceda a las correcciones, pero no se ha implementado*/
					
				}else{
					if(!isset($_REQUEST['IdTrabajo'])){
						new Mensaje('Trabajo no válido', '../Controllers/Trabajo_CONTROLLER.php');
					}
					if(preg_match('/^ET/', $_REQUEST['IdTrabajo'])){//Si es una ET y no un QA, se peresentan las evaluaciones de la misma
						$evaluacion = new Evaluacion($_REQUEST['IdTrabajo'], $_SESSION['login'], '', '','','','','','');
						if(!$evaluacion->ENPLAZO()){
							$respuesta = $evaluacion->CORRECCION();
							if(is_string($respuesta)){
								new Mensaje($respuesta, '../Controllers/Trabajo_CONTROLLER.php');
							}
							new Evaluacion_SHOWALL($respuesta, array('','IdTrabajo', 'AliasEvaluado', 'IdHistoria', 'CorrectoA', 'ComenIncorrectoA'));
						}else{
							new Mensaje('No se pueden ver las correcciones mientras se puede cambiar la entrega', '../Controllers/Trabajo_CONTROLLER.php');
						}
					}else if(preg_match('/^QA/', $_REQUEST['IdTrabajo'])){//Si es una QA se quiere ver las correcciones del profesor comparadas con las unas propias, por lo tanto se hace un truco para que el alaisevaluado sea el login, que comparandolo en otra tabla saldrá el alias de uno mismo 
						$evaluacion = new Evaluacion($_REQUEST['IdTrabajo'], '', $_SESSION['login'],'','','','','','');
						if(!$evaluacion->ENPLAZO()){
							$respuesta = $evaluacion->CORRECCIONQA();
							if(is_string($respuesta)){
								new Mensaje($respuesta, '../Controllers/Trabajo_CONTROLLER.php');
							}
							new Evaluacion_SHOWALL($respuesta, array('','IdTrabajo', 'LoginEvaluador','AliasEvaluado', 'IdHistoria', 'CorrectoA', 'ComenIncorrectoA', 'CorrectoP', 'ComentIncorrectoP'));
						}else{
							new Mensaje('No se pueden ver las correcciones mientras se puede cambiar la entrega', '../Controllers/Trabajo_CONTROLLER.php');
						}
					}else{//Por si se quieren presentar otros trabajos
						$evaluacion = new Evaluacion($_REQUEST['IdTrabajo'], $_SESSION['login'], '', '','','','','','');
						if(!$evaluacion->ENPLAZO()){
							$respuesta = $evaluacion->CORRECCION();
							if(is_string($respuesta)){
								new Mensaje($respuesta, '../Controllers/Trabajo_CONTROLLER.php');
							}
							new Evaluacion_SHOWALL($respuesta, array('','IdTrabajo', 'AliasEvaluado', 'IdHistoria', 'CorrectoA', 'ComenIncorrectoA'));
						}else{
							new Mensaje('No se pueden ver las correcciones mientras se puede cambiar la entrega', '../Controllers/Trabajo_CONTROLLER.php');
						}
					}
				}
			break;
			
			default:
				if(tienePermisosPara('EVALUA', 'SHOWAL')){ //Si tiene permisos (ADMIN)
					$evaluacion = new Evaluacion('','','','','','','','','');//No necesitamos evaluacion para buscar (pero sí para acceder a la BD)
					$respuesta = $evaluacion->SHOWALL();//Todos los datos de la BD estarán aqúi
					new Evaluacion_SHOWALL($respuesta, array('','IdTrabajo', 'AliasEvaluado', 'LoginEvaluador', 'IdHistoria', 'CorrectoA', 'ComenIncorrectoA', 'CorrectoP', 'ComentIncorrectoP', 'OK', 'EDIT', 'DELETE', 'SHOWCURRENT'));//Le pasamos todos los datos de la BD
				}else{ //Si no (USER regular)
					new Mensaje('Permisos insuficientes', '../index.php');
				}
			break;
		}
?>