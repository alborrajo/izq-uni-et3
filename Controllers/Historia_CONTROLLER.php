
<?php
/* Controller de HISTORIAs y las acciones que se le podrán realizar
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	session_start();
	include_once '../Functions/Autenticacion.php';
	if(!autenticado()){//No se puede verni aquí sin estar logeado
		header('Location: ../../index.php');
	}
	
	include '../Models/HISTORIA.php';
	include '../Views/Historia/Historia_ADD.php';
	include '../Views/Historia/Historia_EDIT.php';
	include '../Views/Historia/Historia_SEARCH.php';
	include '../Views/Historia/Historia_DELETE.php';
	include '../Views/Historia/Historia_SHOWCURRENT.php';
	include '../Views/Historia/Historia_SHOWALL.php';
	include '../Views/MESSAGE.php';
	
function get_data_form(){
	
	
   
	$idTrabajo = $_REQUEST['IdTrabajo'];
	$idHistoria = $_REQUEST['IdHistoria'];
	$textoHistoria = $_REQUEST['TextoHistoria'];


	$historia = new Historia($idTrabajo,$idHistoria,$textoHistoria);
 
	return $historia;
}
	

if (!isset($_REQUEST['orden'])){ //si no viene del formulario, no existe array POST
	$_REQUEST['orden'] = 'SHOWALL';
}
		switch ($_REQUEST['orden']){
			case 'ADD':
				if(!$_POST){//Si GET
					$muestraADD = new vistaAdd();//Mostrar vista add
				}else{
					$historia = get_data_form();//Si post cogemos objeto historia
					$respuesta = $historia->ADD();//Y lo añadimos
					new Mensaje($respuesta, '../Controllers/Historia_CONTROLLER.php');// y a ver qué ha pasado en la BD
				}
				break;
				
			case 'EDIT':
				if(!$_POST){//Si GET
					$historia = new Historia($_REQUEST['IdTrabajo'],$_REQUEST['IdHistoria'],'');//Editar objeto historia seleccionado
					$historia->_getDatosGuardados();//Rellenar con los datos de la BD
					new vistaEdit($historia);//Mostrar vista
				}else{
					$historia = get_data_form();//Coger datos
					$respuesta = $historia->EDIT();//Actualizarlos
					new Mensaje($respuesta, '../Controllers/Historia_CONTROLLER.php');//A ver que pasa con la BD, qué intrigante
				}
				break;
				
			case 'SEARCH':
				if(!$_POST){//Si GET
					$muestraSEARCH = new vistaSearch();//Mostrar vista buscadora
				}else{
					$historia = get_data_form();//Creamos un objeto historia con los datos introducidos (que no insertarlo en la BD)
				    $respuesta = $historia->SEARCH();//Buscamos los datos que se parezcan a los introducidos
					new vistaShowall($respuesta, '');//Mostramos todos los datos recuperados de la BD (showall muestra todos los datos que se le pasan)
				}
				break;
				
			case 'DELETE':
				if(!$_POST){//Si GET
					$historia = new Historia($_REQUEST['IdTrabajo'],$_REQUEST['IdHistoria'],'');//Coger objeto historia guardado a eliminar
					$historia->_getDatosGuardados();//Rellenar datos
					new vistaDelete($historia);//Mostrar vissta 
				}else{//Si confirma borrado llega por post
					$historia = new Historia($_POST['IdTrabajo'],$_REQUEST['IdHistoria'],'');//Clave
					$respuesta = $historia->DELETE();//Borrar objeto historia con dicha clave
					new Mensaje($respuesta, '../Controllers/Historia_CONTROLLER.php');//A ver qué pasa en la BD
				}
				break;
				
			case 'SHOWCURRENT':
				if(!$_POST){//Si GET
					$historia= new Historia($_REQUEST['IdTrabajo'],$_REQUEST['IdHistoria'],'');//Coger clave del objeto historia
					$resultado = $historia->SHOWCURRENT();
					if(!is_string($historia)){//NO debería ser posible pedir un showcurrent de algo no existente pero si esp osible retornará un string, así que si no es un string es un objeto historia
						$historia->_getDatosGuardados();
						new vistaShowcurrent($historia);//Mostrar al objeto historia rellenado
					}else{//sino
						new Mensaje($respuesta, '../Controllers/Historia_CONTROLLER.php');//Mensaje de error, que hay muchos
					}
				}
				break;
				
			case 'SHOWALL':
				$historia = new Historia('','','');//No necesitamos objeto historia para buscar (pero sí para acceder a la BD)
				$respuesta = $historia->SHOWALL();//Todos los datos de la BD estarán aqúi
				new vistaShowall($respuesta, '');//Le pasamos todos los datos de la BD
				break;
				
			default:// default, se hace un showall
				$historia = new Historia('','','');
				$respuesta = $historia->SHOWALL();
				new vistaShowall($respuesta, '');
				break;
		}
?>