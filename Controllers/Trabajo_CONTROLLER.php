<?php
/* Controller de TRABAJOs y las acciones que se le podrán realizar
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	session_start();
	include_once '../Functions/Autenticacion.php';
	if(!autenticado()){//No se puede verni aquí sin estar logeado
		header('Location: ../../index.php');
	}
	
	include '../Models/TRABAJO.php';
	include '../Views/Trabajo/Trabajo_ADD.php';
	include '../Views/Trabajo/Trabajo_EDIT.php';
	include '../Views/Trabajo/Trabajo_SEARCH.php';
	include '../Views/Trabajo/Trabajo_DELETE.php';
	include '../Views/Trabajo/Trabajo_SHOWCURRENT.php';
	include '../Views/Trabajo/Trabajo_SHOWALL.php';
	include '../Views/MESSAGE.php';
	
function get_data_form(){

	$IdTrabajo = $_REQUEST['IdTrabajo'];
	$NombreTrabajo = $_REQUEST['NombreTrabajo'];
	$FechaIniTrabajo = $_REQUEST['FechaIniTrabajo'];
	$FechaFinTrabajo = $_REQUEST['FechaFinTrabajo'];
	$PorcentajeNota = $_REQUEST['PorcentajeNota'];

	$trabajo = new Trabajo($IdTrabajo, $NombreTrabajo, $FechaIniTrabajo, $FechaFinTrabajo, $PorcentajeNota);
 
	return $trabajo;
}
	

if (!isset($_REQUEST['orden'])){ //si no viene del formulario, no existe array POST
	$_REQUEST['orden'] = 'SHOWALL';
}
		switch ($_REQUEST['orden']){
			case 'ADD':
				if(tienePermisosPara('TRABAJ', 'ADD')){
					if(!$_POST){//Si GET
						$muestraADD = new Trabajo_ADD();//Mostrar vista add
					}else{
						$trabajo = get_data_form();//Si post cogemos usuario
						$respuesta = $trabajo->ADD();//Y lo añadimos	
						new Mensaje($respuesta, '../Controllers/Trabajo_CONTROLLER.php');// y a ver qué ha pasado en la BD
					}
				}else{
					new Mensaje('Permisos insuficientes', '../Controllers/Trabajo_CONTROLLER.php');
				}
				break;
				
			case 'EDIT':
				if(tienePermisosPara('TRABAJ', 'EDIT')){
					if(!$_POST){//Si GET
						$trabajo = new Trabajo($_REQUEST['IdTrabajo'],'','','','');//Editar usuario seleccionado
						$trabajo->_getDatosGuardados();//Rellenar con los datos de la BD
						new Trabajo_EDIT($trabajo);//Mostrar vista
					}else{
						$trabajo = get_data_form();//Coger datos
						$respuesta = $trabajo->EDIT();//Actualizarlos
						new Mensaje($respuesta, '../Controllers/Trabajo_CONTROLLER.php');//A ver que pasa con la BD, qué intrigante
					}
				}else{
					new Mensaje('Permisos insuficientes', '../Controllers/Trabajo_CONTROLLER.php');
				}
				break;
				
			case 'SEARCH':
				if(tienePermisosPara('TRABAJ', 'SEARCH')){
					if(!$_POST){//Si GET
						$muestraSEARCH = new Trabajo_SEARCH();//Mostrar vista buscadora
					}else{
						$trabajo = get_data_form();//Creamos un usuario con los datos introducidos (que no insertarlo en la BD)
						$respuesta = $trabajo->SEARCH();//Buscamos los datos que se parezcan a los introducidos
						new Trabajo_SHOWALL($respuesta, '');//Mostramos todos los datos recuperados de la BD (showall muestra todos los datos que se le pasan)
					}
				}else{
					new Mensaje('Permisos insuficientes', '../Controllers/Trabajo_CONTROLLER.php');
				}
				break;
				
			case 'DELETE':
				if(tienePermisosPara('TRABAJ', 'DELETE')){
					if(!$_POST){//Si GET
						$trabajo = new Trabajo($_REQUEST['IdTrabajo'],'','','','');//Coger usuario guardado a eliminar
						$trabajo->_getDatosGuardados();//Rellenar datos
						new Trabajo_DELETE($trabajo);//Mostrar vissta 
					}else{//Si confirma borrado llega por post
						$trabajo = new Trabajo($_POST['IdTrabajo'],'','','','');//Clave
						$respuesta = $trabajo->DELETE();//Borrar usuario con dicha clave
						new Mensaje($respuesta, '../Controllers/Trabajo_CONTROLLER.php');//A ver qué pasa en la BD
					}
				}else{
					new Mensaje('Permisos insuficientes', '../Controllers/Trabajo_CONTROLLER.php');
				}
				break;
				
			case 'SHOWCURRENT':
				if(tienePermisosPara('TRABAJ', 'SHOWCU')){
					if(!$_POST){//Si GET
						$trabajo = new Trabajo($_REQUEST['IdTrabajo'],'','','','');//Coger clave del usuario
						$respuesta = $trabajo->SHOWCURRENT();
						if(!is_string($respuesta)){//NO debería ser posible pedir un showcurrent de algo no existente pero si esp osible retornará un string, así que si no es un string es un usuario
							$trabajo->_getDatosGuardados();
							new Trabajo_SHOWCURRENT($trabajo);//Mostrar al usuario rellenado
						}else{//sino
							new Mensaje($respuesta, '../Controllers/Trabajo_CONTROLLER.php');//Mensaje de error, que hay muchos
						}
					}
				}else{
					new Mensaje('Permisos insuficientes', '../Controllers/Trabajo_CONTROLLER.php');
				}
				break;
				
			case 'AUTOQA':
				if(tienePermisosPara('TRABAJ', 'AUTOQA')){
					if(!$_POST){//Si GET
						$trabajo = new Trabajo($_REQUEST['IdTrabajo'],'','','','');
						$respuesta = $trabajo->AUTOQA();
						new Mensaje($respuesta, '../Controllers/Trabajo_CONTROLLER.php'); //Mensaje confirmando o mensaje de error
					}
				}else{
					new Mensaje('Permisos insuficientes', '../Controllers/Trabajo_CONTROLLER.php');
				}
				break;

			case 'SHOWALL':
				$trabajo = new Trabajo('','','','','');//No necesitamos usuario para buscar (pero sí para acceder a la BD)
				$respuesta = $trabajo->SHOWALL();//Todos los datos de la BD estarán aqúi
				new Trabajo_SHOWALL($respuesta, '');//Le pasamos todos los datos de la BD
				break;
				
			default:
				$trabajo = new Trabajo('','','','','');//No necesitamos usuario para buscar (pero sí para acceder a la BD)
				$respuesta = $trabajo->SHOWALL();//Todos los datos de la BD estarán aqúi
				new Trabajo_SHOWALL($respuesta, '');//Le pasamos todos los datos de la BD
				break;
		}
?>