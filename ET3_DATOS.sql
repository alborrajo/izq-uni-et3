-- Script de inserción de datos a la BD.
-- por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
-- 28/11/17

--
-- SELECCIONAMOS PARA USAR
--
USE `IUET32017`;

-- --------------------------------------------------------
-- --------------------------------------------------------

-- PERMISO
--    ADMIN

INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'ACCION','ADD');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'ACCION','EDIT');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'ACCION','DELETE');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'ACCION','SEARCH');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'ACCION','SHOWAL');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'ACCION','SHOWCU');

INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'ENTREG','ADD');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'ENTREG','EDIT');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'ENTREG','DELETE');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'ENTREG','SEARCH');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'ENTREG','SHOWAL');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'ENTREG','SHOWCU');

INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'EVALUA','ADD');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'EVALUA','EDIT');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'EVALUA','DELETE');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'EVALUA','SEARCH');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'EVALUA','SHOWAL');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'EVALUA','SHOWCU');

INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'FUNCIO','ADD');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'FUNCIO','EDIT');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'FUNCIO','DELETE');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'FUNCIO','SEARCH');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'FUNCIO','SHOWAL');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'FUNCIO','SHOWCU');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'FUNCIO','ASIGNA');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'FUNCIO','DESASI');

INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'GRUPO','ADD');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'GRUPO','EDIT');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'GRUPO','DELETE');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'GRUPO','SEARCH');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'GRUPO','SHOWAL');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'GRUPO','SHOWCU');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'GRUPO','ASIGNA');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'GRUPO','DESASI');

INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'HISTOR','ADD');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'HISTOR','EDIT');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'HISTOR','DELETE');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'HISTOR','SEARCH');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'HISTOR','SHOWAL');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'HISTOR','SHOWCU');

INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'NOTATR','ADD');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'NOTATR','EDIT');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'NOTATR','DELETE');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'NOTATR','SEARCH');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'NOTATR','SHOWAL');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'NOTATR','SHOWCU');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'NOTATR','GENOTA');

INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'PERMISO','ADD');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'PERMISO','DELETE');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'PERMISO','SEARCH');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'PERMISO','SHOWAL');

INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'QA','ADD');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'QA','EDIT');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'QA','DELETE');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'QA','SEARCH');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'QA','SHOWAL');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'QA','SHOWCU');

INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'TRABAJ','ADD');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'TRABAJ','EDIT');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'TRABAJ','DELETE');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'TRABAJ','SEARCH');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'TRABAJ','SHOWAL');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'TRABAJ','SHOWCU');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'TRABAJ','AUTOQA');

INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'USUARI','ADD');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'USUARI','EDIT');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'USUARI','DELETE');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'USUARI','SEARCH');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'USUARI','SHOWAL');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'USUARI','SHOWCU');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'USUARI','ASIGNA');
INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('ADMIN', 'USUARI','DESASI');


-- FUNC_ACCION
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('ACCION','ADD');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('ACCION','EDIT');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('ACCION','DELETE');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('ACCION','SEARCH');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('ACCION','SHOWAL');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('ACCION','SHOWCU');

INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('ENTREG','ADD');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('ENTREG','EDIT');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('ENTREG','DELETE');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('ENTREG','SEARCH');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('ENTREG','SHOWAL');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('ENTREG','SHOWCU');

INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('EVALUA','ADD');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('EVALUA','EDIT');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('EVALUA','DELETE');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('EVALUA','SEARCH');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('EVALUA','SHOWAL');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('EVALUA','SHOWCU');

INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('FUNCIO','ADD');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('FUNCIO','EDIT');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('FUNCIO','DELETE');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('FUNCIO','SEARCH');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('FUNCIO','SHOWAL');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('FUNCIO','SHOWCU');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('FUNCIO','ASIGNA');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('FUNCIO','DESASI');

INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('GRUPO','ADD');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('GRUPO','EDIT');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('GRUPO','DELETE');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('GRUPO','SEARCH');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('GRUPO','SHOWAL');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('GRUPO','SHOWCU');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('GRUPO','ASIGNA');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('GRUPO','DESASI');

INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('HISTOR','ADD');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('HISTOR','EDIT');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('HISTOR','DELETE');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('HISTOR','SEARCH');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('HISTOR','SHOWAL');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('HISTOR','SHOWCU');

INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('NOTATR','ADD');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('NOTATR','EDIT');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('NOTATR','DELETE');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('NOTATR','SEARCH');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('NOTATR','SHOWAL');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('NOTATR','SHOWCU');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('NOTATR','GENOTA');

INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('PERMISO','ADD');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('PERMISO','DELETE');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('PERMISO','SEARCH');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('PERMISO','SHOWAL');

INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('QA','ADD');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('QA','EDIT');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('QA','DELETE');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('QA','SEARCH');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('QA','SHOWAL');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('QA','SHOWCU');

INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('TRABAJ','ADD');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('TRABAJ','EDIT');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('TRABAJ','DELETE');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('TRABAJ','SEARCH');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('TRABAJ','SHOWAL');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('TRABAJ','SHOWCU');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('TRABAJ','AUTOQA');

INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('USUARI','ADD');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('USUARI','EDIT');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('USUARI','DELETE');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('USUARI','SEARCH');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('USUARI','SHOWAL');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('USUARI','SHOWCU');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('USUARI','ASIGNA');
INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('USUARI','DESASI');

-- USU_GRUPO
INSERT INTO USU_GRUPO (login, IdGrupo) VALUES ('gabo','USER');
INSERT INTO USU_GRUPO (login, IdGrupo) VALUES ('userlmao','USER');
INSERT INTO USU_GRUPO (login, IdGrupo) VALUES ('pepsiman','USER');
INSERT INTO USU_GRUPO (login, IdGrupo) VALUES ('cuaca','USER');
INSERT INTO USU_GRUPO (login, IdGrupo) VALUES ('ue','USER');
INSERT INTO USU_GRUPO (login, IdGrupo) VALUES ('mlem','USER');
INSERT INTO USU_GRUPO (login, IdGrupo) VALUES ('root','ADMIN');

-- ACCION
INSERT INTO ACCION (IdAccion, NombreAccion, DescripAccion) VALUES ('ADD','Añadir','Añadir a la base de datos');
INSERT INTO ACCION (IdAccion, NombreAccion, DescripAccion) VALUES ('DELETE','Borrar','Borrar de la base de datos');
INSERT INTO ACCION (IdAccion, NombreAccion, DescripAccion) VALUES ('EDIT','Editar','Editar datos');
INSERT INTO ACCION (IdAccion, NombreAccion, DescripAccion) VALUES ('SEARCH','Buscar','Buscar en la base de datos');
INSERT INTO ACCION (IdAccion, NombreAccion, DescripAccion) VALUES ('SHOWAL','Mostrar todo','Mostrar todas las tuplas');
INSERT INTO ACCION (IdAccion, NombreAccion, DescripAccion) VALUES ('SHOWCU','Vista en detalle','Mostrar todos los datos de la tupla');
INSERT INTO ACCION (IdAccion, NombreAccion, DescripAccion) VALUES ('ASIGNA','Asignar','Registrarse en la aplicación web');
INSERT INTO ACCION (IdAccion, NombreAccion, DescripAccion) VALUES ('DESASI','Desasignar','Registrarse en la aplicación web');
INSERT INTO ACCION (IdAccion, NombreAccion, DescripAccion) VALUES ('AUTOQA','Asignar QAs automáticamente','Asignar 5 QAs a cada usuario que haya entregado. Garantizando que cada QA sea asignada a 5 alumnos.');
INSERT INTO ACCION (IdAccion, NombreAccion, DescripAccion) VALUES ('GENOTA','Generar notas','Genera automáticamente las notas de las entregas');
-- --------------------------------------------------------

-- FUNCIONALIDAD
INSERT INTO FUNCIONALIDAD (IdFuncionalidad, NombreFuncionalidad, DescripFuncionalidad) VALUES ('ACCION','Gestión de Acciones','Gestión de las acciones que se pueden realizar en las funcionalidades de la aplicación');
INSERT INTO FUNCIONALIDAD (IdFuncionalidad, NombreFuncionalidad, DescripFuncionalidad) VALUES ('ENTREG','Gestión de Entregas','Gestión de las entregas subidas a la aplicación');
INSERT INTO FUNCIONALIDAD (IdFuncionalidad, NombreFuncionalidad, DescripFuncionalidad) VALUES ('EVALUA','Gestión de Evaluaciones','Gestión de las evaluaciones realizadas a las entregas');
INSERT INTO FUNCIONALIDAD (IdFuncionalidad, NombreFuncionalidad, DescripFuncionalidad) VALUES ('FUNCIO','Gestión de Funcionalidades','Gestión de las funcionalidades de la aplicación');
INSERT INTO FUNCIONALIDAD (IdFuncionalidad, NombreFuncionalidad, DescripFuncionalidad) VALUES ('GRUPO','Gestión de Grupos','Gestión de grupos de usuario');
INSERT INTO FUNCIONALIDAD (IdFuncionalidad, NombreFuncionalidad, DescripFuncionalidad) VALUES ('HISTOR','Gestión de Historias','Gestión de historias de usuario para cada trabajo');
INSERT INTO FUNCIONALIDAD (IdFuncionalidad, NombreFuncionalidad, DescripFuncionalidad) VALUES ('NOTATR','Gestión de Notas de trabajo','Gestión de las notas de cada usuario por cada entrega');
INSERT INTO FUNCIONALIDAD (IdFuncionalidad, NombreFuncionalidad, DescripFuncionalidad) VALUES ('PERMIS','Gestión de Permisos','Gestión de permisos por grupos para las distintas funcionalidades y acciones');
INSERT INTO FUNCIONALIDAD (IdFuncionalidad, NombreFuncionalidad, DescripFuncionalidad) VALUES ('QA','Asignación de QAs','Gestión de QAs, para decidir qué usuarios evaluan las entregas de qué usuarios');
INSERT INTO FUNCIONALIDAD (IdFuncionalidad, NombreFuncionalidad, DescripFuncionalidad) VALUES ('TRABAJ','Gestión de Trabajos','Gestión de trabajos de la aplicación, sobre los cuales los usuarios subiran sus entregas');
INSERT INTO FUNCIONALIDAD (IdFuncionalidad, NombreFuncionalidad, DescripFuncionalidad) VALUES ('USUARI','Gestión de Usuarios','Gestión de los datos de cada usuario registrado en la aplicación');

-- --------------------------------------------------------
-- GRUPO
INSERT INTO GRUPO (IdGrupo, NombreGrupo, DescripGrupo) VALUES ('USER','Usuarios','Usuarios regulares de la aplicación');
INSERT INTO GRUPO (IdGrupo, NombreGrupo, DescripGrupo) VALUES ('ADMIN','Administradores','Administradores de la aplicación');

-- --------------------------------------------------------

-- 'TRABAJO'
INSERT INTO TRABAJO (IdTrabajo, NombreTrabajo, FechaIniTrabajo, FechaFinTrabajo,  PorcentajeNota) VALUES ('ET1','Entrega 1','2001-01-01','2018-02-02','25.3');
INSERT INTO TRABAJO (IdTrabajo, NombreTrabajo, FechaIniTrabajo, FechaFinTrabajo,  PorcentajeNota) VALUES ('QA1','Quality Assurance 1','2018-02-02','2018-02-09','5.9');

INSERT INTO TRABAJO (IdTrabajo, NombreTrabajo, FechaIniTrabajo, FechaFinTrabajo,  PorcentajeNota) VALUES ('ET2','Entrega 2','2017-01-01','2018-03-03','23.5');
INSERT INTO TRABAJO (IdTrabajo, NombreTrabajo, FechaIniTrabajo, FechaFinTrabajo,  PorcentajeNota) VALUES ('QA2','Quality Assurance 2','2018-03-03','2018-03-10','7.2');

INSERT INTO TRABAJO (IdTrabajo, NombreTrabajo, FechaIniTrabajo, FechaFinTrabajo,  PorcentajeNota) VALUES ('ET3','Entrega 3','2017-12-12','2018-04-04','30.8');
INSERT INTO TRABAJO (IdTrabajo, NombreTrabajo, FechaIniTrabajo, FechaFinTrabajo,  PorcentajeNota) VALUES ('QA3','Quality Assurance 3','2018-04-04','2018-04-11','7.3');


-- --------------------------------------------------------

-- EVALUACION
-- Se generan automáticamente tuplas de EVALUACION al generar QA automáticamente
-- --------------------------------------------------------

-- HISTORIA
--		ET1
INSERT INTO HISTORIA (IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET1','1','Funciona');
INSERT INTO HISTORIA (IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET1','2','Funciona bien');
INSERT INTO HISTORIA (IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET1','3','No falla');

--		ET2
INSERT INTO HISTORIA (IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','1','Funciona');
INSERT INTO HISTORIA (IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','2','Funciona bien');
INSERT INTO HISTORIA (IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','3','No falla');
INSERT INTO HISTORIA (IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','4','No tiene fallos');
INSERT INTO HISTORIA (IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','5','Enciende');
INSERT INTO HISTORIA (IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET2','6','Arranca');

--		ET3
INSERT INTO HISTORIA (IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','1','Funciona');
INSERT INTO HISTORIA (IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','2','Funciona bien');
INSERT INTO HISTORIA (IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','3','No falla');
INSERT INTO HISTORIA (IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','4','No tiene fallos');
INSERT INTO HISTORIA (IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','5','Enciende');
INSERT INTO HISTORIA (IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','6','Arranca');
INSERT INTO HISTORIA (IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','7','Flipendo');
INSERT INTO HISTORIA (IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','8','Wingardium Leviosá');
INSERT INTO HISTORIA (IdTrabajo,IdHistoria,TextoHistoria) VALUES ('ET3','9','Entregado bien');

-- --------------------------------------------------------

-- NOTA_TRABAJO
--		Las notas se generarían desde la propia aplicación

-- --------------------------------------------------------

-- ASIGNAC_QA
--		Lo ideal es asignar automáticamente usando el botón de Asignar QA en la parte de trabajo.
--		Para generar QA1, se le da al botón asignar automáticamente de la ET1, etc.

-- --------------------------------------------------------

-- ENTREGA
INSERT INTO ENTREGA (login, IdTrabajo, Alias, Horas, Ruta) VALUES ('gabo','ET1','gabanz','1','/Files/gabanz/ET1_gabanz.rar');
INSERT INTO ENTREGA (login, IdTrabajo, Alias, Horas, Ruta) VALUES ('userlmao','ET1','lmao','2','/Files/lmao/lmao.rar');
INSERT INTO ENTREGA (login, IdTrabajo, Alias, Horas, Ruta) VALUES ('pepsiman','ET1','pepsi','3','/Files/pepsi/ET1_pepsi.rar');
INSERT INTO ENTREGA (login, IdTrabajo, Alias, Horas, Ruta) VALUES ('cuaca','ET1','bel','4','/Files/bel/bel.rar');
INSERT INTO ENTREGA (login, IdTrabajo, Alias, Horas, Ruta) VALUES ('ue','ET1','ueueue','5','/Files/ueueue/ET1_ueueue.rar');
INSERT INTO ENTREGA (login, IdTrabajo, Alias, Horas, Ruta) VALUES ('mlem','ET1','mrau','6','/Files/mrau/ET1_mrau.rar');
-- --------------------------------------------------------

-- USUARIO
--		User: root, Pass: root
INSERT INTO USUARIO (login, password, DNI, Nombre, Apellidos, Correo, Direccion, Telefono) VALUES ('root','63a9f0ea7bb98050796b649e85481845','12345678A','Root','McRooty','root@mcrooty.com','Calle del Rooteo, 0-R','902202122');

--		User: gabo, Pass: gabo
INSERT INTO USUARIO (login, password, DNI, Nombre, Apellidos, Correo, Direccion, Telefono) VALUES ('gabo','4a34588683fbb3c0cc3e53049048da05','99999999R','Gabo','Gaboal','abo@gabo.com','Aqui','988888888');

--		User: userlmao, Pass: ayy
INSERT INTO USUARIO (login, password, DNI, Nombre, Apellidos, Correo, Direccion, Telefono) VALUES ('userlmao','8843cfb8cbd3367b0dce5a5426a022d9','1979155K','ayy','lmao','ayy@lm.ao','marte','123456789');

--		User: pepsiman, Pass: pepsi
INSERT INTO USUARIO (login, password, DNI, Nombre, Apellidos, Correo, Direccion, Telefono) VALUES ('pepsiman','184b3f811e4e3f2eb9d763cd33dffdf6','64806480G','pepsi','man','pepsi@man.com','calle de la pesicola','111111111');

--		User: cuaca, Pass: cuacabelaria
INSERT INTO USUARIO (login, password, DNI, Nombre, Apellidos, Correo, Direccion, Telefono) VALUES ('cuaca','9e920a3c7cebb303d2fe4e2193ab36b4','80648064B','cuacabel','cuacabelaria','otro@mail.com','San Cristovo de Cea','902902902');

--		User: ue, Pass: ueueueueue
INSERT INTO USUARIO (login, password, DNI, Nombre, Apellidos, Correo, Direccion, Telefono) VALUES ('ue','1dabb333a3c03d20658cb7eab16e11b4','19119119K','Aoai', 'Ueaue','aoai@ueaue.io','ueeee','191919191');

--		User: mlem, Pass: mlem
INSERT INTO USUARIO (login, password, DNI, Nombre, Apellidos, Correo, Direccion, Telefono) VALUES ('mlem','9aae137a387a18b06bfde0eeaba5d6dc','12312312E','mlem','mlem','ml@e.m','mlem','123123123');