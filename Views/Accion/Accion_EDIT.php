<?php
/* Clase vista edit, con el fin de poder editar una Accion que se le pase como paramétro
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class Accion_EDIT{  // declaración de clase
	
	var $accion;//Accion a editar
	
	function __construct($accion){
		$this->accion = $accion;
		$this->toString();//Imprimir por pantalla el formulario
	} // fin del constructor

	// declaración de método pinta()
	// muestra por pantall html con los valores de los atributos de la clase
	// y un hiperenlace para volver al script php que la invocó
	function toString(){
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';?>
		<div class="general">		
		<fieldset><legend class="TituloFormulario"><?php echo $strings['Editar']; ?></legend>
			<form method="POST" accept-charset="UTF-8" id="formularioEdit" name="formularioEdit" style="display: inline-block;" action="../Controllers/Accion_CONTROLLER.php">
				<table class="formulario">
					<trp>
						<tdp><?php echo $strings['IdAccion']; ?></tdp><tdp><input readonly required type="text" id="IdAccionA" name="IdAccion" size="8" maxlength="6" onBlur="validarIdAccion('IdAccionA')" value="<?php echo $this->accion->_getIdAccion(); ?>"/></tdp><tdp><img id="IdAccionABot" height="20px" src="../img/red-button.png"/></tdp><tdp><texto-correccion id="IdAccionABotText"></texto-correccion></tdp>

					</trp>
					<tri>
						<tdi><?php echo $strings['NombreAccion']; ?></tdi><tdi><input required type="text" id="NombreAccionA" name="NombreAccion" size="65" maxlength="60" onBlur="validarNombreAccion(this)" value="<?php echo $this->accion->_getNombreAccion(); ?>"/></tdi><tdi><img id="NombreAccionABot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="NombreAccionABotText"></texto-correccion></tdi>

					</tri>
					<trp>
						<tdp><?php echo $strings['DescripAccion']; ?></tdp><tdp><input required type="text" id="DescripAccionA" name="DescripAccion" size="110" maxlength="100" onBlur="validarDescripAccion(this)" value="<?php echo $this->accion->_getDescripAccion(); ?>"/></tdp><tdp><img id="DescripAccionABot" height="20px" src="../img/red-button.png"/></tdp><tdp><texto-correccion id="DescripAccionABotText"></texto-correccion></tdp>
					</trp>
				
					
					
					<button onClick="return validarFormularioEvaluacionAE(document.getElementById('formularioEdit'), 'EDIT')" name="orden" value="EDIT"/><img src="../img/edit.png" height="30px"/>
				</table>
			</form>
		</fieldset>
		</div><?php
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
 ?>