<?php
/* Clase vista search, con el fin de buscar Acciones por cualquier campo y cualquier longitud 
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
class Accion_SEARCH{  // declaración de clase


	// declaración constructor de la clase
	// se inicializa con los valores del formulario y el valor del botón submit pulsado
	function __construct(){
		$this->toString();
	} // fin del constructor

	// declaración de método pinta()
	// muestra por pantall html con los valores de los atributos de la clase
	// y un hiperenlace para volver al script php que la invocó
	function toString(){
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';?>	
		<div class="general">
		<fieldset><legend class="TituloFormulario"><?php echo $strings['Buscar']; ?></legend>
			<form method="POST" accept-charset="UTF-8" id="formularioSearch" name="formularioSearch" style="display: inline-block;" action="../Controllers/Accion_CONTROLLER.php">
				<table class="formulario">
					<trp>
						<tdp><?php echo $strings['IdAccion']; ?></tdp><tdp><input type="IdAccion" id="IdAccionA" name="IdAccion" size="8" maxlength="6"/>

					</trp>
					<tri>
						<tdi><?php echo $strings['NombreAccion']; ?></tdi><tdi><input  type="text" id="NombreAccionA" name="NombreAccion" size="65" maxlength="60"/></tdi>

					</tri>
					<trp>
						<tdp><?php echo $strings['DescripAccion']; ?></tdp><tdp><input type="text" id="DescripAccionA" name="DescripAccion" size="110" maxlength="100"/></tdp>

					</trp>
					<button onClick="submit" type="submit" name="orden" value="SEARCH"/><img src="../img/search.png" height="30px"/>
				</table>
			</form>
		</fieldset>
		</div><?php
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
 ?>