<?php
/* Clase vista delete, con el fin de poder eliminar una Accion que se le pase como parámetro. Con la clave es suficiente
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class Accion_DELETE{  // declaración de clase
	
	var $accion;//Accion a Deletear
	
	function __construct($accion){
		$this->accion = $accion;
		$this->toString();//Imprimir por pantalla el formulario
	} // fin del constructor

	function toString(){		
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';?>
		<div class="general">
		
		<form method="POST" accept-charset="UTF-8" id="formularioDelete" name="formularioDelete" style="display: inline-block;" action="../Controllers/Accion_CONTROLLER.php">			
			<input type="hidden" id="IdAccionA" name="IdAccion" value="<?php echo $this->accion->_getIdAccion(); ?>"/>
			
			<table id="tuplaDetail">
					<th><?php echo $strings['IdAccion']; ?></th><td><?php echo $this->accion->_getIdAccion(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['NombreAccion']; ?></th><td><?php echo $this->accion->_getNombreAccion(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['DescripAccion']; ?></th><td><?php echo $this->accion->_getDescripAccion(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['Accion']; ?></th><td><button onClick="document.getElementById('IdAccionA').value='<?php echo $this->accion->_getIdAccion(); ?>'" onClick="submit" type="submit" name="orden" value="DELETE"/><img src="../img/erase.png" height="20px"/></td>
				</tr>
				<tr>
					<th><?php echo $strings['Volver']; ?></th><td><a href="../Controllers/Accion_CONTROLLER.php"><img src="../img/return.png" height="27px"/></a></td>
				</tr>
			</table>
		
		</form>
		</div><?php
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
 ?>