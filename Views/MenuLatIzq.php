<?php 
/* Menu lateral izquierda para su generación
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/


/* Menu lateral izquierda para su generación
	por actr4u 
	 8/ 0/ 7*/
include_once '../Locales/Strings_'. $_SESSION['idioma'].'.php'; 
include_once '../Functions/Autenticacion.php'; /*Autenticacion*/
if(autenticado()){//Se debería usar un switch que de los permisos pero de momento:
	//Esta sería la rama de ADMIN
	?>
	<div class="menulateralizq" >
		<?php 
		if(tienePermisosPara('ACCION', 'ADD') || tienePermisosPara('ACCION', 'DELETE') || tienePermisosPara('ACCION', 'EDIT') || tienePermisosPara('ACCION', 'SEARCH') || tienePermisosPara('ACCION', 'SHOWAL') || tienePermisosPara('ACCION', 'SHOWCU')){
			?>
			<div class="menudespleg">
				<a href='../Controllers/Accion_CONTROLLER.php'><button class="menu"><botonmenuizq><?php echo $strings['Acciones']; ?></botonmenuizq></button></a>
					<div class="opciones">
						<a href="../Controllers/Accion_CONTROLLER.php?orden=ADD"><?php echo $strings['Añadir']; ?>  </a>
						<a href="../Controllers/Accion_CONTROLLER.php?orden=SEARCH"><?php echo $strings['Buscar']; ?>  </a>
					</div>
			</div>
			<?php			
		}
		
		if(tienePermisosPara('ENTREG', 'ADD') || tienePermisosPara('ENTREG', 'DELETE') || tienePermisosPara('ENTREG', 'EDIT') || tienePermisosPara('ENTREG', 'SEARCH') || tienePermisosPara('ENTREG', 'SHOWAL') || tienePermisosPara('ENTREG', 'SHOWCU')){
			?>
			<div class="menudespleg">
				<a href='../Controllers/Entrega_CONTROLLER.php'><button class="menu"><?php echo $strings['Entregas']; ?> </button></a>
					<div class="opciones">
						<a href="../Controllers/Entrega_CONTROLLER.php?orden=ADD"><?php echo $strings['Añadir']; ?>  </a>
						<a href="../Controllers/Entrega_CONTROLLER.php?orden=SEARCH"><?php echo $strings['Buscar']; ?>  </a>
					</div>
			</div>
			<?php
		}		

		
		if(tienePermisosPara('EVALUA', 'ADD') || tienePermisosPara('EVALUA', 'DELETE') || tienePermisosPara('EVALUA', 'EDIT') || tienePermisosPara('EVALUA', 'SEARCH') || tienePermisosPara('EVALUA', 'SHOWAL') || tienePermisosPara('EVALUA', 'SHOWCU')){
			?>
			<div class="menudespleg">
				<a href='../Controllers/Evaluacion_CONTROLLER.php'><button class="menu"><?php echo $strings['Evaluaciones']; ?> </button></a>
					<div class="opciones">
						<a href="../Controllers/Evaluacion_CONTROLLER.php?orden=ADD"><?php echo $strings['Añadir']; ?>  </a>
						<a href="../Controllers/Evaluacion_CONTROLLER.php?orden=SEARCH"><?php echo $strings['Buscar']; ?>  </a>
					</div>
			</div>
			<?php			
		}
		
		if(tienePermisosPara('FUNCIO', 'ADD') || tienePermisosPara('FUNCIO', 'ASIGNA') || tienePermisosPara('FUNCIO', 'DELETE') || tienePermisosPara('FUNCIO', 'DESASI') || tienePermisosPara('FUNCIO', 'EDIT') || tienePermisosPara('FUNCIO', 'SEARCH') || tienePermisosPara('FUNCIO', 'SHOWAL') || tienePermisosPara('FUNCIO', 'SHOWCU')){
			?>
			<div class="menudespleg">
				<a href='../Controllers/Funcionalidad_CONTROLLER.php'><button class="menu"><?php echo $strings['Funcionalidades']; ?> </button></a>
					<div class="opciones">
						<a href="../Controllers/Funcionalidad_CONTROLLER.php?orden=ADD"><?php echo $strings['Añadir']; ?>  </a>
						<a href="../Controllers/Funcionalidad_CONTROLLER.php?orden=SEARCH"><?php echo $strings['Buscar']; ?>  </a>
					</div>
			</div>
			<?php			
		}
		
		if(tienePermisosPara('GRUPO', 'ADD') || tienePermisosPara('GRUPO', 'ASIGNA') || tienePermisosPara('GRUPO', 'DELETE') || tienePermisosPara('GRUPO', 'DESASI') || tienePermisosPara('GRUPO', 'EDIT') || tienePermisosPara('GRUPO', 'SEARCH') || tienePermisosPara('GRUPO', 'SHOWAL') || tienePermisosPara('GRUPO', 'SHOWCU')){
			?>
			<div class="menudespleg">
				<a href='../Controllers/Grupo_CONTROLLER.php'><button class="menu"><?php echo $strings['Grupos']; ?> </button></a>
					<div class="opciones">
						<a href="../Controllers/Grupo_CONTROLLER.php?orden=ADD"><?php echo $strings['Añadir']; ?>  </a>
						<a href="../Controllers/Grupo_CONTROLLER.php?orden=SEARCH"><?php echo $strings['Buscar']; ?>  </a>
					</div>
			</div>
			<?php			
		}
		
		if(tienePermisosPara('HISTOR', 'ADD') || tienePermisosPara('HISTOR', 'DELETE') || tienePermisosPara('HISTOR', 'EDIT') || tienePermisosPara('HISTOR', 'SEARCH') || tienePermisosPara('HISTOR', 'SHOWAL') || tienePermisosPara('HISTOR', 'SHOWCU')){
			?>
			<div class="menudespleg">
				<a href='../Controllers/Historia_CONTROLLER.php'><button class="menu"><?php echo $strings['Historias']; ?> </button></a>
					<div class="opciones">
						<a href="../Controllers/Historia_CONTROLLER.php?orden=ADD"><?php echo $strings['Añadir']; ?>  </a>
						<a href="../Controllers/Historia_CONTROLLER.php?orden=SEARCH"><?php echo $strings['Buscar']; ?>  </a>
					</div>
			</div>
			<?php			
		}
		
		if(tienePermisosPara('NOTATR', 'ADD') || tienePermisosPara('NOTATR', 'DELETE') || tienePermisosPara('NOTATR', 'EDIT') || tienePermisosPara('NOTATR', 'SEARCH') || tienePermisosPara('NOTATR', 'SHOWAL') || tienePermisosPara('NOTATR', 'SHOWCU')){
			?>
			<div class="menudespleg">			
				<a href='../Controllers/Notatrabajo_CONTROLLER.php'><button class="menu"><?php echo $strings['Notas']; ?> </button></a>
					<div class="opciones">
						<a href="../Controllers/Notatrabajo_CONTROLLER.php?orden=ADD"><?php echo $strings['Añadir']; ?>  </a>
						<a href="../Controllers/Notatrabajo_CONTROLLER.php?orden=SEARCH"><?php echo $strings['Buscar']; ?>  </a>
					</div>
			</div>
			<?php			
		}else{
			?>
			<div class="menudespleg">
				<a href='../Controllers/Notatrabajo_CONTROLLER.php'><button class="menu"><?php echo $strings['Notas']; ?> </button></a>
			</div>
			<?php
		}
		
		if(tienePermisosPara('PERMIS', 'ADD') || tienePermisosPara('PERMIS', 'DELETE') || tienePermisosPara('PERMIS', 'SEARCH') || tienePermisosPara('PERMIS', 'SHOWAL')){
			?>
			<div class="menudespleg">
				<a href='../Controllers/Permiso_CONTROLLER.php'><button class="menu"><?php echo $strings['Permisos']; ?> </button></a>
					<div class="opciones">
						<a href="../Controllers/Permiso_CONTROLLER.php?orden=SEARCH"><?php echo $strings['Buscar']; ?>  </a>
					</div>
			</div>
			<?php			
		}
		
		if(tienePermisosPara('QA', 'ADD') || tienePermisosPara('QA', 'DELETE') || tienePermisosPara('QA', 'EDIT') || tienePermisosPara('QA', 'SEARCH') || tienePermisosPara('QA', 'SHOWAL') || tienePermisosPara('QA', 'SHOWCU')){
			?>
			<div class="menudespleg">
				<a href='../Controllers/QA_CONTROLLER.php'><button class="menu"><?php echo $strings['QAs']; ?> </button></a>
					<div class="opciones">
						<a href="../Controllers/QA_CONTROLLER.php?orden=ADD"><?php echo $strings['Añadir']; ?>  </a>
						<a href="../Controllers/QA_CONTROLLER.php?orden=SEARCH"><?php echo $strings['Buscar']; ?>  </a>
					</div>
			</div>
			<?php			
		}else{
			?>
			<div class="menudespleg">
				<a href='../Controllers/QA_CONTROLLER.php'><button class="menu"><?php echo $strings['QAs']; ?> </button></a>
			</div>
			<?php	
		}
		
		if(tienePermisosPara('TRABAJ', 'ADD') || tienePermisosPara('TRABAJ', 'DELETE') || tienePermisosPara('TRABAJ', 'EDIT') || tienePermisosPara('TRABAJ', 'SEARCH') || tienePermisosPara('TRABAJ', 'SHOWAL') || tienePermisosPara('TRABAJ', 'SHOWCU')){
			?>
			<div class="menudespleg">
				<a href='../Controllers/Trabajo_CONTROLLER.php'><button class="menu"><?php echo $strings['Trabajos']; ?> </button></a>
					<div class="opciones">
						<a href="../Controllers/Trabajo_CONTROLLER.php?orden=ADD"><?php echo $strings['Añadir']; ?>  </a>
						<a href="../Controllers/Trabajo_CONTROLLER.php?orden=SEARCH"><?php echo $strings['Buscar']; ?>  </a>
					</div>
			</div>
			<?php			
		}else{
			?>				
			<div class="menudespleg">
				<a href='../Controllers/Trabajo_CONTROLLER.php'><button class="menu"><?php echo $strings['Trabajos']; ?> </button></a>
			</div>
			<?php		
		}
		
		if(tienePermisosPara('USUARI', 'ADD') || tienePermisosPara('USUARI', 'ASIGNA') || tienePermisosPara('USUARI', 'DELETE') || tienePermisosPara('USUARI', 'DESASI') || tienePermisosPara('USUARI', 'EDIT') || tienePermisosPara('USUARI', 'SEARCH') || tienePermisosPara('USUARI', 'SHOWAL') || tienePermisosPara('USUARI', 'SHOWCU')){
			?>
			<div class="menudespleg">
				<a href='../Controllers/Usuario_CONTROLLER.php'><button class="menu"><?php echo $strings['Usuarios']; ?> </button></a>
					<div class="opciones">
						<a href="../Controllers/Usuario_CONTROLLER.php?orden=ADD"><?php echo $strings['Añadir']; ?>  </a>
						<a href="../Controllers/Usuario_CONTROLLER.php?orden=SEARCH"><?php echo $strings['Buscar']; ?>  </a>
					</div>
			</div>
			<?php			
		}
		
		if(tienePermisosPara('USUARI', 'REGIST') || tienePermisosPara('USUARI', 'ADD')){
			?>
			<div class="menudespleg">
				<a href='../Controllers/Usuario_CONTROLLER.php?orden=ADD'><button class="menu"><?php echo $strings['Registrar']; ?> </button></a>
			</div>
			<?php			
		}
		?>
		<div class="menudespleg">
			<a href='../Controllers/Trabajo_CONTROLLER.php'><button class="menu"><?php echo $strings['Correcciones']; ?> </button></a>
			<div class="opciones">				
					<a href="../Controllers/Evaluacion_CONTROLLER.php?orden=CORRECCION&IdTrabajo=ET1"><?php echo $strings['Entrega']; ?>  ET1</a>
					<a href="../Controllers/Evaluacion_CONTROLLER.php?orden=CORRECCION&IdTrabajo=ET2"><?php echo $strings['Entrega']; ?>  ET2</a>
					<a href="../Controllers/Evaluacion_CONTROLLER.php?orden=CORRECCION&IdTrabajo=ET3"><?php echo $strings['Entrega']; ?>  ET3</a>
					<a href="../Controllers/Evaluacion_CONTROLLER.php?orden=CORRECCION&IdTrabajo=QA1"><?php echo $strings['QA']; ?>1</a>
					<a href="../Controllers/Evaluacion_CONTROLLER.php?orden=CORRECCION&IdTrabajo=QA2"><?php echo $strings['QA']; ?>2</a>
					<a href="../Controllers/Evaluacion_CONTROLLER.php?orden=CORRECCION&IdTrabajo=QA3"><?php echo $strings['QA']; ?>3</a>
				</div>
		</div>
		<?php
		
		?>
	</div>
	<?php
}else{
	?>	
	<div class="menulateralizq" >
		<div class="menudespleg">
			<a href='../Controllers/Registro_CONTROLLER.php'><button class="menu"><?php echo $strings['Registrarse']; ?> </button></a>
		</div>
	</div>
	<?php
}
?>