<?php 
/* Clase vista showcurrent, para mostrar una tupla en detalle
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class Entrega_SHOWCURRENT{  // declaración de clase
	var $entrega;//Usuario recibido
	
	// declaración constructor de la clase
	// se inicializa con los valores del formulario y el valor del botón submit pulsado
	function __construct($entrega){
		$this->entrega = $entrega;
		$this->toString();
	} // fin del constructor

	function toString(){
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php'; ?>	
		<div class="general">
		<table id="tuplaDetail">
			<tr>
				<th><?php echo $strings['Login']; ?></th><td><?php echo $this->entrega->_getLogin(); ?></td>
			</tr>
			<tr>
				<th><?php echo $strings['IdTrabajo']; ?></th><td><?php echo $this->entrega->_getIdTrabajo(); ?></td>
			</tr>
			<tr>
				<th><?php echo $strings['Alias']; ?></th><td><?php echo $this->entrega->_getAlias(); ?></td>
			</tr>
			<tr>
				<th><?php echo $strings['Horas']; ?></th><td><?php echo $this->entrega->_getHoras(); ?></td>
			</tr>
			<tr>
				<th><?php echo $strings['Ruta']; ?></th>
				<td>
					<a href="<?php echo $this->entrega->_getRuta();?>">
						<?php echo $this->entrega->_getRuta(); ?>
						<img src="../img/fichero.png" height="20px"/>
					</a>
				</td>
			</tr>
		
			<tr>
				<th><?php echo $strings['Volver']; ?></th><td><a href="../Controllers/Entrega_CONTROLLER.php"><img src="../img/return.png" height="27px"/></a></td>
			</tr>
		</table>
		</div>
		<?php 
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
?>