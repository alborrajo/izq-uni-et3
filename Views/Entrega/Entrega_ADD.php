<?php
/* Clase vista add, con el fin de poder añadir una entrega a la BD
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class Entrega_ADD{  // declaración de clase


	// declaración constructor de la clase
	// se inicializa con los valores del formulario y el valor del botón submit pulsado
	function __construct(){
		$this->toString();
	} // fin del constructor

	// declaración de método pinta()
	// muestra por pantall html con los valores de los atributos de la clase
	// y un hiperenlace para volver al script php que la invocó
	function toString(){
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';?>

		<!-- Javascript para la función de generar alias -->
		<script type="text/javascript" src="../js/Entrega_JAVASCRIPT.js"></script>

		<div class="general">	
		<fieldset><legend class="TituloFormulario"><?php echo $strings['Añadir']; ?></legend>
			<form method="POST" enctype="multipart/form-data" accept-charset="UTF-8" id="formularioAdd" name="formularioAdd" style="display: inline-block;" action="../Controllers/Entrega_CONTROLLER.php">
				<table class="formulario">
					<tri>
						<tdi><?php echo $strings['Login']; ?></tdi><tdi><input required type="text" id="loginA" name="login" size="9" maxlength="9" onBlur="validarLogin('loginA')"/></tdi><tdi><img id="loginABot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="loginABotText"></texto-correccion></tdi>
 
					</tri>
					
					<trp>
						<tdi><?php echo $strings['IdTrabajo']; ?></tdi><tdi><input required type="text" id="IdTrabajo" name="IdTrabajo" size="6" maxlength="6" onBlur="comprobarVacio(this) && comprobarTexto(this,6)"/></tdi><tdi><img id="IdTrabajoABot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="IdTrabajoABotText"></texto-correccion></tdi>
						
					</trp>
					<tri>
						<tdp><?php echo $strings['Alias']; ?></tdp><tdp><input required type="text" id="Alias" name="Alias" size="6" maxlength="6" onfocus="generarAlias('Alias');" onBlur="comprobarVacio(this) && comprobarTexto(this,6)"/></tdp><tdp><img id="AliasABoot" height="20px" src="../img/red-button.png"/></tdp><tdp><texto-correccion id="AliasABotText"></texto-correccion></tdp>

					</tri>
					
						
					<trp>
						<tdi><?php echo $strings['Horas']; ?></tdi><tdi><input required type="text" id="Horas" name="Horas" size="2" maxlength="2" onBlur="validarEntero('Horas')"/></tdi><tdi><img id="HorasABot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="HorasABotText"></texto-correccion></tdi>
						
					</trp>
					
					<tri>
						<tdp><?php echo $strings['Fichero']; ?></tdp><tdp><input required type="file" accept='application/zip.application/rar' id="Ruta" name="Ruta" onChange="validarRAR('Ruta');"/></tdp><tdp><img id="RutaABoot" height="20px" src="../img/red-button.png"/></tdp><tdp><texto-correccion id="RutaABotText"></texto-correccion></tdp>
						<input type="hidden" name="rutaGuardada" value="">

					</tri>

					<br/><br/>
					
					<button onClick="return validarFormularioAEREntrega(document.getElementById('formularioAdd'), 'ADD');" name="orden" value="ADD"/><img src="../img/add.png" height="30px"/>
				</table>
			</form>
		</fieldset>
		</div><?php
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
 ?>