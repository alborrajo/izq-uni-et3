<?php
/* Clase vista edit, con el fin de poder editar una entrega que se le pase como paramétro
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class Entrega_EDIT{  // declaración de clase
	
	var $entrega;//Usuario a editar
	
	function __construct($entrega){
		$this->entrega = $entrega;
		$this->toString();//Imprimir por pantalla el formulario
	} // fin del constructor

	// declaración de método pinta()
	// muestra por pantall html con los valores de los atributos de la clase
	// y un hiperenlace para volver al script php que la invocó
	function toString(){
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';?>
		<div class="general">		
		<fieldset><legend class="TituloFormulario"><?php echo $strings['Editar']; ?></legend>
			<form method="POST" enctype="multipart/form-data" accept-charset="UTF-8" id="formularioEdit" name="formularioEdit" style="display: inline-block;" action="../Controllers/Entrega_CONTROLLER.php">
				<table class="formulario">
						<tri>
						<tdi><?php echo $strings['Login']; ?></tdi><tdi><input required readonly type="text" id="loginA" name="login" size="9" maxlength="9" onBlur="validarlogin('loginA')" value="<?php echo $this->entrega->_getLogin(); ?>"/></tdi><tdi><img id="loginABot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="loginABotText"></texto-correccion></tdi>

					</tri>
					
					<trp>
						<tdi><?php echo $strings['IdTrabajo']; ?></tdi><tdi><input required readonly type="text" id="IdTrabajo" name="IdTrabajo" size="6" maxlength="6" onBlur="comprobarVacio(this) && comprobarTexto(this,6)" value="<?php echo $this->entrega->_getIdTrabajo(); ?>"/></tdi><tdi><img id="IdTrabajoABot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="IdTrabajoABotText"></texto-correccion></tdi>
						
					</trp>
					<tri>
						<tdp><?php echo $strings['Alias']; ?></tdp><tdp><input required readonly type="text" id="Alias" name="Alias" size="6" maxlength="6"  onBlur="comprobarVacio('Alias') && comprobarTexto(this,6)" value="<?php echo $this->entrega->_getAlias(); ?>"/></tdp><tdp><img id="AliasABoot" height="20px" src="../img/red-button.png"/></tdp><tdp><texto-correccion id="AliasABotText"></texto-correccion></tdp>

					</tri>						
					<trp>
						<tdi><?php echo $strings['Horas']; ?></tdi><tdi><input required type="text" id="Horas" name="Horas" size="2" maxlength="2" onBlur="validarEntero('Horas')" value="<?php echo $this->entrega->_getHoras(); ?>"/></tdi><tdi><img id="HorasABot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="HorasABotText"></texto-correccion></tdi>
						
					</trp>
					
					<tri>
						<tdp><?php echo $strings['Fichero']; ?></tdp>
						<tdp>
							<input type="file" accept='application/zip.application/rar' id="Ruta" name="Ruta" onChange="validarRAR('Ruta');"/>
							<a href="<?php echo $this->entrega->_getRuta();?>">
								<img src="../img/fichero.png" height="20px"/>
							</a>
						</tdp>
						<tdp>
							<img id="RutaABoot" height="20px" src="../img/red-button.png"/>
						</tdp>
						<tdp>
							<texto-correccion id="RutaABotText"></texto-correccion>
						</tdp>
						<input type="hidden" name="rutaGuardada" value="<?php echo $this->entrega->_getRuta(); ?>">

					</tri><br/><br/>
					
					<button onClick="document.getElementById('loginA').value='<?php echo $this->entrega->_getLogin(); ?>'; return validarFormularioAEREntrega(document.getElementById('formularioEdit'), 'EDIT');" name="orden" value="EDIT"/><img src="../img/edit.png" height="30px"/>
				</table>
			</form>
		</fieldset>
		</div><?php
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
 ?>