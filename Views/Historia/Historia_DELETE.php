<?php
/* Clase vista delete, con el fin de poder eliminar una historia que se le pase como parámetro. Con la clave es suficiente
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class vistaDelete{  // declaración de clase
	
	var $historia;//Usuario a Deletear
	
	function __construct($historia){
		$this->historia = $historia;
		$this->toString();//Imprimir por pantalla el formulario
	} // fin del constructor

	function toString(){		
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';?>
		<div class="general">
		
		<form method="POST" accept-charset="UTF-8" id="formularioDelete" name="formularioDelete" style="display: inline-block;" action="../Controllers/Historia_CONTROLLER.php">			
			
			<input type="hidden" id="IdTrabajoA" name="IdTrabajo" value="<?php echo $this->historia->_getIdTrabajo(); ?>"/>
			<input type="hidden" id="IdHistoriaA" name="IdHistoria" value="<?php echo $this->historia->_getIdHistoria(); ?>"/>
			
			<table id="tuplaDetail">
		    
		    <tr>
				<th><?php echo $strings['IdTrabajo']; ?></th><td><?php echo $this->historia->_getIdTrabajo(); ?></td>
			</tr>
			<tr>
				<th><?php echo $strings['IdHistoria']; ?></th><td><?php echo $this->historia->_getIdHistoria(); ?></td>
			</tr>
			<tr>
				<th><?php echo $strings['TextoHistoria']; ?></th><td><?php echo $this->historia->_getTextoHistoria(); ?></td>
			</tr>		
			<tr>
				<th><?php echo $strings['Accion']; ?></th><td><button type="submit" name="orden" value="DELETE"/><img src="../img/erase.png" height="20px"/></td>
			</tr>
			<tr>
				<th><?php echo $strings['Volver']; ?></th><td><a href="../Controllers/Controller.php"><img src="../img/return.png" height="27px"/></a></td>
			</tr>
			</table>
		
		</form>
		</div><?php
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
 ?>