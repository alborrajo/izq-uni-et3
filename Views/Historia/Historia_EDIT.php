<?php
/* Clase vista edit, con el fin de poder editar una historia que se le pase como paramétro
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class vistaEdit{  // declaración de clase
	
	var $historia;//Usuario a editar
	
	function __construct($historia){
		$this->historia = $historia;
		$this->toString();//Imprimir por pantalla el formulario
	} // fin del constructor

	// declaración de método pinta()
	// muestra por pantall html con los valores de los atributos de la clase
	// y un hiperenlace para volver al script php que la invocó
	function toString(){
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';?>
		<div class="general">		
		<fieldset><legend class="TituloFormulario"><?php echo $strings['Editar']; ?></legend>
			<form method="POST" accept-charset="UTF-8" id="formularioEdit" name="formularioEdit" style="display: inline-block;" action="../Controllers/Historia_CONTROLLER.php">
				<table class="formulario">
				
					<tri>
						<tdi><?php echo $strings['IdTrabajo']; ?></tdi><tdi><input readonly required type="text" id="IdTrabajo" name="IdTrabajo" size="6" maxlength="6" onBlur="comprobarTexto(this,6)" value="<?php echo $this->historia->_getIdTrabajo(); ?>"/></tdi><tdi><img id="IdTrabajoABot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="IdTrabajoABotText"></texto-correccion></tdi>
						
					</tri>
				
						
					<trp>
						<tdp><?php echo $strings['IdHistoria']; ?></tdp><tdp><input readonly required type="number" id="IdHistoria" name="IdHistoria" size="2" maxlength="2" onBlur="comprobarReal(this,0,99,0)" value="<?php echo $this->historia->_getIdHistoria(); ?>"/></tdp><tdp><img id="IdHistoriaABot" height="20px" src="../img/red-button.png"/></tdp><tdp><texto-correccion id="IdHistoriaABotText"></texto-correccion></tdp>
						
					</trp>
					
					<tri>
						<tdi><?php echo $strings['TextoHistoria']; ?></tdi>
						<tdi>
							<textarea required id="TextoHistoria" name="TextoHistoria" rows="5" cols="60" onBlur="comprobarTexto(this,300)"/><?php echo $this->historia->_getTextoHistoria(); ?></textarea>
						</tdi>
						<tdi>
							<img id="TextoHistoriaABot" height="20px" src="../img/red-button.png"/>
						</tdi>
						<tdi>
							<texto-correccion id="textoHistoriaABotText"></texto-correccion>
						</tdi>

					</tri><br/><br/>
					
					<button onClick="document.getElementById('loginA').value='<?php echo $this->historia->_getIdTrabajo(); ?>'; return validarFormularioAERHistoria(document.getElementById('formularioEdit'), 'EDIT');" name="orden" value="EDIT"/><img src="../img/edit.png" height="30px"/>
				</table>
			</form>
		</fieldset>
		</div><?php
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
 ?>