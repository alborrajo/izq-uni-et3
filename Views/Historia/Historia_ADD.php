<?php
/* Clase vista add, con el fin de poder añadir una historia de usuario a la BD
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class vistaAdd{  // declaración de clase


	// declaración constructor de la clase
	// se inicializa con los valores del formulario y el valor del botón submit pulsado
	function __construct(){
		$this->toString();
	} // fin del constructor

	// declaración de método pinta()
	// muestra por pantall html con los valores de los atributos de la clase
	// y un hiperenlace para volver al script php que la invocó
	function toString(){
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';?>	
		<div class="general">	
		<fieldset><legend class="TituloFormulario"><?php echo $strings['Añadir']; ?></legend>
			<form method="POST" accept-charset="UTF-8" id="formularioAdd" name="formularioAdd" style="display: inline-block;" action="../Controllers/Historia_CONTROLLER.php">
				<table class="formulario">
				
					
					<tri>
						<tdi><?php echo $strings['IdTrabajo']; ?></tdi><tdi><input required type="text" id="IdTrabajo" name="IdTrabajo" size="6" maxlength="6" onBlur="comprobarTexto(this,6)"/></tdi><tdi><img id="IdTrabajoABot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="IdTrabajoABotText"></texto-correccion></tdi>
						
					</tri>
				
						
					<trp>
						<tdp><?php echo $strings['IdHistoria']; ?></tdp><tdp><input required type="number" id="IdHistoria" name="IdHistoria" size="2" maxlength="2" onBlur="comprobarReal(this,0,99,0)"/></tdp><tdp><img id="IdHistoriaABot" height="20px" src="../img/red-button.png"/></tdp><tdp><texto-correccion id="IdHistoriaABotText"></texto-correccion></tdp>
						
					</trp>
					
					<tri>
						<tdi><?php echo $strings['TextoHistoria']; ?></tdi>
						<tdi>
							<textarea required id="TextoHistoria" name="TextoHistoria" rows="5" cols="60" onBlur="comprobarTexto(this,300)"/></textarea>
						</tdi>
						<tdi>
							<img id="TextoHistoriaABot" height="20px" src="../img/red-button.png"/>
						</tdi>
						<tdi>
							<texto-correccion id="textoHistoriaABotText"></texto-correccion>
						</tdi>

					</tri><br/><br/>
					
					<button onClick="return validarFormularioAERHistoria(document.getElementById('formularioAdd'), 'ADD')" name="orden" value="ADD"/><img src="../img/add.png" height="30px"/>
				</table>
			</form>
		</fieldset>
		</div><?php
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
 ?>