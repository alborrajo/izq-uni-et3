<?php
/* Clase vista add, con el fin de poder añadir un permiso a la BD
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class Permiso_ADD{  // declaración de clase


	// declaración constructor de la clase
	// se inicializa con los valores del formulario y el valor del botón submit pulsado
	function __construct($grupos, $funcs, $acciones,$func_accion){
		$this->toString($grupos, $funcs, $acciones,$func_accion);
	} // fin del constructor

	// declaración de método pinta()
	// muestra por pantall html con los valores de los atributos de la clase
	// y un hiperenlace para volver al script php que la invocó
	function toString($grupos, $funcs, $acciones, $func_accion){
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';?>

		<!-- Array func_accion con todos los pares funcionalidad-acción -->
		<script>
			// Convertir array $func_accion en array para JS
			<?php
				$php_array = $func_accion->fetch_all(MYSQLI_NUM);
				$js_array = json_encode($php_array);
			?>
			var func_accion = <?php echo $js_array; ?>;
		</script>
		<!-- Javascript con la funcion accionesPosibles() para mostrar en el formulario las acciones de cada funcionalidad -->
		<script type="text/javascript" src="../js/Permiso_JAVASCRIPT.js"></script>


		<div class="general">	
		<fieldset><legend class="TituloFormulario"><?php echo $strings['Añadir']; ?></legend>
			<form method="POST" accept-charset="UTF-8" id="formularioAdd" name="formularioAdd" style="display: inline-block;" action="../Controllers/Permiso_CONTROLLER.php">
				<table class="formulario">
					<tri>
						<tdi><?php echo $strings['Grupo']; ?></tdi>
						<tdi>
							<select name="IdGrupo">
								<?php 
								while($fila = $grupos->fetch_assoc()){ //Mientras haya filas
									?> <option value="<?php echo $fila["IdGrupo"]; ?>"><?php echo $fila["NombreGrupo"]; ?></option> <?php
								}
								?>
							</select>
						</tdi>
					</tri>

					<trp>
						<tdp><?php echo $strings['Funcionalidad']; ?></tdp>
						<tdi>
							<select name="IdFuncionalidad" id="IdFuncionalidad" onChange="accionesPosibles('IdFuncionalidad','IdAccion');">
								<?php 
								while($fila = $funcs->fetch_assoc()){ //Mientras haya filas
									?> <option value="<?php echo $fila["IdFuncionalidad"]; ?>"><?php echo $fila["NombreFuncionalidad"]; ?></option> <?php
								}
								?>
							</select>
						</tdi>
					</trp>

					<tri>
						<tdi><?php echo $strings['Accion']; ?></tdi>
						<tdi>
							<select name="IdAccion" id="IdAccion">
								<?php 
								while($fila = $acciones->fetch_assoc()){ //Mientras haya filas
									?>
										<option value="<?php echo $fila["IdAccion"]; ?>" id="<?php echo $fila["IdAccion"]; ?>">
											<?php echo $fila["NombreAccion"]; ?>
										</option>
									<?php
								}
								?>
							</select>
						</tdi>
					</tri>
					
					</trp><br/><br/>
					<button name="orden" value="ADD"/><img src="../img/add.png" height="30px"/>
				</table>
			</form>
		</fieldset>
		</div><?php
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
 ?>