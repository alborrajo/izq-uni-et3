<?php
/* Clase vista delete, con el fin de poder eliminar un permiso. Con la clave es suficiente
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class Permiso_DELETE{  // declaración de clase
	
	var $permiso;//Usuario a Deletear
	
	function __construct($permiso){
		$this->permiso = $permiso;
		$this->toString();//Imprimir por pantalla el formulario
	} // fin del constructor

	function toString(){		
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';?>
		<div class="general">
		
		<form method="POST" accept-charset="UTF-8" id="formularioDelete" name="formularioDelete" style="display: inline-block;" action="../Controllers/Permiso_CONTROLLER.php">			
			<input type="hidden" id="IdGrupoA" name="IdGrupo" value="<?php echo $this->permiso->_getIdGrupo(); ?>"/>
			<input type="hidden" id="IdFuncionalidadA" name="IdFuncionalidad" value="<?php echo $this->permiso->_getIdFuncionalidad(); ?>"/>
			<input type="hidden" id="IdAccionA" name="IdAccion" value="<?php echo $this->permiso->_getIdAccion(); ?>"/>
			
			<table id="tuplaDetail">
				<tr>
					<th><?php echo $strings['IdGrupo']; ?></th><td><?php echo $this->permiso->_getIdGrupo(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['NombreGrupo']; ?></th><td><?php echo $this->permiso->_getNombreGrupo(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['IdFuncionalidad']; ?></th><td><?php echo $this->permiso->_getIdFuncionalidad(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['IdAccion']; ?></th><td><?php echo $this->permiso->_getIdAccion(); ?></td>
				</tr>
				
				<tr>
					<th><?php echo $strings['Accion']; ?></th><td><button type="submit" name="orden" value="DELETE"/><img src="../img/erase.png" height="20px"/></td>
				</tr>
				<tr>
					<th><?php echo $strings['Volver']; ?></th><td><a href="../Controllers/Permiso_CONTROLLER.php"><img src="../img/return.png" height="27px"/></a></td>
				</tr>
			</table>
		
		</form>
		</div><?php
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
 ?>