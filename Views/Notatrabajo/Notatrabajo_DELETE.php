<?php
/* Clase vista delete, con el fin de poder eliminar una nota que se le pase como parámetro. Con la clave es suficiente
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class Notatrabajo_DELETE{  // declaración de clase
	
	var $notatrabajo;//Nota a Deletear
	
	function __construct($notatrabajo){
		$this->usuario = $notatrabajo;
		$this->toString();//Imprimir por pantalla el formulario
	} // fin del constructor

	function toString(){		
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';?>
		<div class="general">
		
		<form method="POST" accept-charset="UTF-8" id="formularioDelete" name="formularioDelete" style="display: inline-block;" action="../Controllers/Notatrabajo_CONTROLLER.php">			
			<input type="hidden" id="loginA" name="login" value="<?php echo $this->usuario->_getLogin(); ?>"/>
			<input type="hidden" id="IdTrabajoA" name="IdTrabajo" value="<?php echo $this->usuario->_getIdTrabajo(); ?>"/>

			<table id="tuplaDetail">
				<tr>
					<th><?php echo $strings['Login']; ?></th><td><?php echo $this->usuario->_getLogin(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['IdTrabajo']; ?></th><td><?php echo $this->usuario->_getIdTrabajo(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['NotaTrabajo']; ?></th><td><?php echo $this->usuario->_getNotaTrabajo(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['Accion']; ?></th><td><button onClick="document.getElementById('loginA').value='<?php echo $this->usuario->_getLogin(); ?>'" onClick="submit" type="submit" name="orden" value="DELETE"/><img src="../img/erase.png" height="20px"/></td>
				</tr>
				<tr>
					<th><?php echo $strings['Volver']; ?></th><td><a href="../Controllers/Notatrabajo_CONTROLLER.php"><img src="../img/return.png" height="27px"/></a></td>
				</tr>
			</table>
		
		</form>
		</div><?php
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
 ?>