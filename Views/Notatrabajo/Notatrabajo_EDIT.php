<?php
/* Clase vista edit, con el fin de poder editar una nota que se le pase como paramétro
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class Notatrabajo_EDIT{  // declaración de clase
	
	var $usuario;//Usuario a editar
	
	function __construct($usuario){
		$this->usuario = $usuario;
		$this->toString();//Imprimir por pantalla el formulario
	} // fin del constructor

	// declaración de método pinta()
	// muestra por pantall html con los valores de los atributos de la clase
	// y un hiperenlace para volver al script php que la invocó
	function toString(){
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';?>
		<div class="general">		
		<fieldset><legend class="TituloFormulario"><?php echo $strings['Editar']; ?></legend>
			<form method="POST" accept-charset="UTF-8" id="formularioEdit" name="formularioEdit" style="display: inline-block;" action="../Controllers/Notatrabajo_CONTROLLER.php">
				<table class="formulario">
					<tri>
						<tdi><?php echo $strings['Login']; ?></tdi><tdi><input readonly type="text" id="loginA" name="login" size="9" maxlength="9" onBlur="validarLogin('loginA')" value="<?php echo $this->usuario->_getLogin(); ?>"/></tdi><tdi><img id="loginABot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="loginABotText"></texto-correccion></tdi>

					</tri>
					<trp>
						<tdp><?php echo $strings['IdTrabajo']; ?></tdp><tdp><input readonly type="text" id="IdTrabajoA" name="IdTrabajo" size="6" maxlength="6" onBlur="validarIdTrabajo(this)" value="<?php echo $this->usuario->_getIdTrabajo(); ?>"/></tdp><tdp><img id="IdTrabajoABot" height="20px" src="../img/red-button.png"/></tdp><tdp><texto-correccion id="IdTrabajoABotText"></texto-correccion></tdp>

					</trp>
					<tri>
						<tdi><?php echo $strings['NotaTrabajo']; ?></tdi><tdi><input required type="text" id="NotaTrabajo" name="NotaTrabajo" min="0" max="10" onBlur="validarNota(this)" value="<?php echo $this->usuario->_getNotaTrabajo(); ?>"/></tdi><tdi><img id="NotaTrabajoBot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="NotaTrabajoBotText"></texto-correccion></tdi>

					</tri>

					</trp><br/><br/>
					<button onClick="return validarFormularioNotatrabajoAER(document.getElementById('formularioEdit'), 'EDIT');" name="orden" value="EDIT"/><img src="../img/edit.png" height="30px"/>
				</table>
			</form>
		</fieldset>
		</div><?php
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
 ?>