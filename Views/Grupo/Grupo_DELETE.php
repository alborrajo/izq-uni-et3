<?php
/* Clase vista delete para grupo, con el fin de poder eliminar un grupo que se le pase como parámetro. Con la clave es suficiente
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class Grupo_DELETE{  // declaración de clase
	
	var $grupo;//Usuario a Deletear
	
	function __construct($grupo){
		$this->grupo = $grupo;
		$this->toString();//Imprimir por pantalla el formulario
	} // fin del constructor

	function toString(){		
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';?>
		<div class="general">
		
		<form method="POST" accept-charset="UTF-8" id="formularioDelete" name="formularioDelete" style="display: inline-block;" action="../Controllers/Grupo_CONTROLLER.php">			
			<input type="hidden" id="IdGrupo" name="IdGrupo" value="<?php echo $this->grupo->_getIdGrupo(); ?>"/>
			
			<table id="tuplaDetail">
				<tr>
					<th><?php echo $strings['IdGrupo']; ?></th><td><?php echo $this->grupo->_getIdGrupo(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['NombreGrupo']; ?></th><td><?php echo $this->grupo->_getNombreGrupo(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['DescripcionGrupo']; ?></th><td><?php echo $this->grupo->_getDescripcionGrupo(); ?></td>
				</tr>
				
					<th><?php echo $strings['Accion']; ?></th><td><button onClick="document.getElementById('IdGrupo').value='<?php echo $this->grupo->_getIdGrupo(); ?>'" onClick="submit" type="submit" name="orden" value="DELETE"/><img src="../img/erase.png" height="20px"/></td>
				</tr>
				<tr>
					<th><?php echo $strings['Volver']; ?></th><td><a href="../Controllers/Grupo_CONTROLLER.php"><img src="../img/return.png" height="27px"/></a></td>
				</tr>
			</table>
		
		</form>
		</div><?php
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
 ?>