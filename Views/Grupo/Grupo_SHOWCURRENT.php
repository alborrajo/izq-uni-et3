<?php 
/* Clase vista showcurrent para grupo, para mostrar una tupla en detalle
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class Grupo_SHOWCURRENT{  // declaración de clase
	var $grupo;//Grupo recibido
	
	// declaración constructor de la clase
	// se inicializa con los valores del formulario y el valor del botón submit pulsado
	function __construct($grupo){
		$this->grupo = $grupo;
		$this->toString();
	} // fin del constructor

	function toString(){
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php'; ?>	
		<div class="general">
		<table id="tuplaDetail">
			<tr>
				<th><?php echo $strings['IdGrupo']; ?></th><td><?php echo $this->grupo->_getIdGrupo(); ?></td>
			</tr>
			<tr>
				<th><?php echo $strings['NombreGrupo']; ?></th><td><?php echo $this->grupo->_getNombreGrupo(); ?></td>
			</tr>
			<tr>
				<th><?php echo $strings['DescripcionGrupo']; ?></th><td><?php echo $this->grupo->_getDescripcionGrupo(); ?></td>
			</tr>
			<tr>
				<th><?php echo $strings['Volver']; ?></th><td><a href="../Controllers/Grupo_CONTROLLER.php"><img src="../img/return.png" height="27px"/></a></td>
			</tr>
		</table>
		</div>
		<?php 
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
?>