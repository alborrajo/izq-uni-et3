<?php
/* Clase vista showall para grupo, para mostrar tuplas y datos que se le pasen a mostrar
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class Grupo_SHOWALL
{  // declaración de clase
	
	var $resultado;//Las tuplas a mostrar
	var $ACC_NOM;
	var $FUNC_NOM;
	var $permisos;
	var $datosAMostrar;//Los datos de esas tuplas a mostrar

	// declaración constructor de la clase
	// se inicializa con los valores del formulario y el valor del botón submit pulsado
	function __construct($respuesta, $datosAMostrar){
		$this->datosAMostrar = $datosAMostrar;
		
		if(is_string($respuesta)){
			$this->resultado = $respuesta;
		}else{
			
			//RESPUESTA[0] = GRUPOS : Mostrarlos y editarlos	
			//RESPUESTA[1] = FUNC_NOM : Mostrarlos y asignarlos/desasignarlos
			//RESPUESTA[1] = ACC_NOM : Mostrarlos y asignarlos/desasignarlos
			//RESPUESTA[3] = PERMISOS : Mostrarlos
			$permisos = array();
			$contadorFilas = 0;
			while($contadorFilas < mysqli_num_rows($respuesta[3])){
				$fila = $respuesta[3]->fetch_row();
				$permisos[$contadorFilas]["IdGrupo"] = $fila[0];
				$permisos[$contadorFilas]["IdFuncionalidad"] = $fila[1];
				$permisos[$contadorFilas]["IdAccion"] = $fila[2];
				$contadorFilas++;
			}
			
			$resultado = array();			
			$i = 0;
			while($fila = $respuesta[0]->fetch_row()){
				$resultado[$i] = array();
				$resultado[$i]["IdGrupo"] = $fila[0];
				$resultado[$i]["NombreGrupo"] = $fila[1];
				$resultado[$i]["DescripGrupo"] = $fila[2];
				$resultado[$i]["Permisos"] = array();
				$j = 0;//Iterador de USU_accion
				$posicion = 0;
				while($j < sizeof($permisos)){//Mientras haya filas con la posibildad de que esté este usuario en algun accion
					if($permisos[$j]["IdGrupo"] == $resultado[$i]["IdGrupo"]){//Comprobar idfunc
						$resultado[$i]["Permisos"][$posicion]["IdFuncionalidad"] = $permisos[$j]["IdFuncionalidad"];//Y se inserta un func						
						$resultado[$i]["Permisos"][$posicion]["IdAccion"] = $permisos[$j]["IdAccion"];//Y se inserta un accion
						$posicion++;
					}
					$j++;
				}
				
				$i++;
			}
			
			$ACC_NOM = array();
			$contadorFilas = 0;
			while($fila = $respuesta[1]->fetch_row()){
				$ACC_NOM[$fila[0]]/*IdAccion*/ = $fila[1];/*NomAccion*/
				$contadorFilas++;
			}
			
			$FUNC_NOM = array();
			$contadorFilas = 0;
			while($fila = $respuesta[2]->fetch_row()){
				$FUNC_NOM[$fila[0]]/*IdAccion*/ = $fila[1];/*NomAccion*/
				$contadorFilas++;
			}
			
			$this->resultado = $resultado;
			$this->FUNC_NOM = $FUNC_NOM;
			$this->ACC_NOM = $ACC_NOM;
			$this->permisos = $permisos;		
		}
		$this->toString();
	} // fin del constructor

	// declaración de método pinta()
	// muestra por pantall html con los valores de los atributos de la clase
	// y un hiperenlace para volver al script php que la invocó
	function toString(){
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';
		echo "<div class='general'>";
		
		if(is_string($this->resultado)){
			echo '<table id="tablaDatos" name="SHOWALL">
					<tr style="margin-bottom: 20px">
						<td style="background-color: white;"></td>
						<td style="background-color: white;"><center><img src="../img/add.png" onClick=document.getElementById("ADD").submit() height="40px"> <form id="ADD" onSubmit="controlador.php"><input type="hidden" name="orden" value="ADD"></form> </center></td>
						<td style="background-color: white;"></td>						
						<td style="background-color: white;"><center><img src="../img/search.png" onClick=document.getElementById("SEARCH").submit() height="40px"> <form id="SEARCH" onSubmit="controlador.php"><input type="hidden" name="orden" value="SEARCH"></form> </center></td>
						<td style="background-color: white;"></td>
					</tr>';
			?>
			<table id="tuplaDetail">
				<tr>
					<th><?php echo $strings['Informacion']; ?></th><td><?php echo $this->resultado; ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['Volver']; ?></th><td><a href="../Controllers/Grupo_CONTROLLER.php"><?php echo $strings['Volver']; ?></a></td>
				</tr>
			</table>
			<?php
		}else{			
			echo '<table id="tablaDatos" name="SHOWALL">
					<tr style="margin-bottom: 20px">
						<td style="background-color: white;"></td>							
						<td style="background-color: white;"><center><img src="../img/add.png" onClick=document.getElementById("ADD").submit() height="40px"> <form id="ADD" onSubmit="controlador.php"><input type="hidden" name="orden" value="ADD"></form> </center></td>
						<td style="background-color: white;"></td>
						<td style="background-color: white;"><center><img src="../img/search.png" onClick=document.getElementById("SEARCH").submit() height="40px"> <form id="SEARCH" onSubmit="controlador.php"><input type="hidden" name="orden" value="SEARCH"></form> </center></td>
						<td style="background-color: white;"></td>		
					</tr>';
			echo   '<tr>
						<th>'. $strings['IdGrupo'] .'</th>
						<th>'. $strings['NombreGrupo'] .'</th>
						<th>'. $strings['DescripGrupo'] .'</th>
						<th style="width:400px;">'. $strings['Permisos'] .'</th>
						
						<th>'. $strings['Acciones'] .'</th>
					</tr>';/*Nombre de los datos*/
					
			$i = 0; //Variable para saber el numero de iteraciones e identificar formularios e inputs
			while($i < sizeof($this->resultado)){//Mientras haya filas, se coje una y se muestra
				?>
				<form id='formularioOpcion<?php echo $i ?>' method='GET' action='../Controllers/Grupo_CONTROLLER.php'>
					<tr>
						<input type='hidden' id="oculto<?php echo $i ?>" name='orden' value=''/>
						<input type='hidden' name='IdGrupo' value="<?php echo $this->resultado[$i]["IdGrupo"]; ?>"><td id='IdGrupo'><?php echo $this->resultado[$i]["IdGrupo"]; ?></td></input></form>				
						<td id='NombreGrupo'><?php echo $this->resultado[$i]["NombreGrupo"]; ?></td>
						<td id='DescripGrupo'><?php echo $this->resultado[$i]["DescripGrupo"]; ?></td>
						<td id='PermisosGrupo' style="width:400px;">
							<table>
								<?php
								$j = 0;
								while($j < sizeof($this->resultado[$i]["Permisos"])){
									?>
									<tr>
										<td style="width:400px;">
											<form id='formularioQuitarPermiso<?php echo $i; echo $j; ?>' method='POST' action='../Controllers/Grupo_CONTROLLER.php'>
													<input type='hidden' name='IdGrupo' value="<?php echo $this->resultado[$i]["IdGrupo"]; ?>"></input>
													<input type='hidden' name='IdAccion' value="<?php echo $this->resultado[$i]["Permisos"][$j]["IdAccion"]; ?>"></input>
													<input type='hidden' name='IdFuncionalidad' value="<?php echo $this->resultado[$i]["Permisos"][$j]["IdFuncionalidad"]; ?>"></input>
													<input type='hidden' name='orden' value="DESASIGNAR"></input>
												</form>
												<?php
												echo "- ";
												while($nombreFuncionalidad = current($this->FUNC_NOM)){
													if($this->resultado[$i]["Permisos"][$j]["IdFuncionalidad"] == key($this->FUNC_NOM)){
														echo $nombreFuncionalidad;
													}
													next($this->FUNC_NOM);
												}
												reset($this->FUNC_NOM);
												echo " / ";												
												while($nombreAccion = current($this->ACC_NOM)){
													if($this->resultado[$i]["Permisos"][$j]["IdAccion"] == key($this->ACC_NOM)){
														echo $nombreAccion;
													}
													next($this->ACC_NOM);
												}
												reset($this->ACC_NOM);
												?>
										</td>
										<td>
											<img onClick="document.getElementById('formularioQuitarPermiso<?php echo $i; echo $j; ?>').submit()" src="../img/delete.png" height="12px" style='cursor: pointer'></img>								
												<?php
												echo "\n";
												?>
										</td>
									</tr>
									<?php
									$j++;
								}
								?>
								<tr>
									<td>
										<form id='formularioDarPermiso<?php echo $i;?>' method="POST" action="../Controllers/Grupo_CONTROLLER.php">
											<input type='hidden' name='IdGrupo' value="<?php echo $this->resultado[$i]["IdGrupo"]; ?>"></input>
											<input type='hidden' name='orden' value="ASIGNAR"></input>
											<select name="IdFuncionalidad"  style="margin-top: 15%">
												<?php
												while($nombreFuncionalidad = current($this->FUNC_NOM)){//Mientras haya accions que permitir opcionar
													?>
													<img src="../img/erase.png"></img>
													<option value=<?php echo key($this->FUNC_NOM); ?>><?php echo $nombreFuncionalidad;?></option>
													<?php
													next($this->FUNC_NOM);
												}
												reset($this->FUNC_NOM);
												?>
											</select>
											<select name="IdAccion"  style="margin-top: 15%">
												<?php
												while($nombreAccion = current($this->ACC_NOM)){//Mientras haya accions que permitir opcionar
													?>
													<img src="../img/erase.png"></img>
													<option value=<?php echo key($this->ACC_NOM); ?>><?php echo $nombreAccion;?></option>
													<?php
													next($this->ACC_NOM);
												}
												reset($this->ACC_NOM);
												?>
											</select>
										</form>
									</td>							
									
									<td>
										<img onClick="document.getElementById('formularioDarPermiso<?php echo $i; ?>').submit()" src="../img/add.png" height="12px" style='cursor: pointer'></img>
									</td>	
								</tr>
							</table>
						</td>
						<td>
							<img onMouseOver="document.getElementById('oculto<?php echo $i ?>').value='EDIT'" onClick="document.getElementById('formularioOpcion<?php echo $i ?>').submit()" src='../img/edit.png' height='20px;' style='cursor: pointer'/>
							<img onMouseOver="document.getElementById('oculto<?php echo $i ?>').value='DELETE'" onClick="document.getElementById('formularioOpcion<?php echo $i ?>').submit()" src='../img/erase.png' height='20px;' style='cursor: pointer'/>
							<img onMouseOver="document.getElementById('oculto<?php echo $i ?>').value='SHOWCURRENT'" onClick="document.getElementById('formularioOpcion<?php echo $i ?>').submit()" src='../img/detail.png' height='20px;' style='cursor: pointer'/>
						</td> 
					</tr>
			<?php
			$i++;
			}//Escribir una celda en el orden en el que se presentan los datos del showall, ponemos un input hidden para que al ejecutar las acciones de edit, showcurrent o delete tengamos el input del login o lo que necesitemos. Las acciones al final en la ultima celda, pero además si es showCurrent se cambia a post para no tener una URL de la nasa
			echo '</table>';
		}
		echo '</div>';
		include '../Views/Footer.php';	
	}
		// fin método pinta()
} //fin de class muestradatos
 ?>