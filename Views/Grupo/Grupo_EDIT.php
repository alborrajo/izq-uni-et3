<?php
/* Clase vista edit para grupo, con el fin de poder editar un grupo que se le pase como paramétro
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class Grupo_EDIT{  // declaración de clase
	
	var $grupo;//Usuario a editar
	
	function __construct($grupo){
		$this->grupo = $grupo;
		$this->toString();//Imprimir por pantalla el formulario
	} // fin del constructor

	// declaración de método pinta()
	// muestra por pantall html con los valores de los atributos de la clase
	// y un hiperenlace para volver al script php que la invocó
	function toString(){
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';?>
		<div class="general">		
		<fieldset><legend class="TituloFormulario"><?php echo $strings['Editar']; ?></legend>
			<form method="POST" accept-charset="UTF-8" id="formularioEdit" name="formularioEdit" style="display: inline-block;" action="../Controllers/Grupo_CONTROLLER.php">
				<table class="formulario">
					<tri>
						<tdi><?php echo $strings['IdGrupo']; ?></tdi><tdi><input readonly type="text" id="IdGrupo" name="IdGrupo" size="6" maxlength="6" value="<?php echo $this->grupo->_getIdGrupo(); ?>"/></tdi><tdi><img id="loginABot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="loginABotText"></texto-correccion></tdi>

					</tri>
					<trp>
						<tdp><?php echo $strings['NombreGrupo']; ?></tdp><tdp><input required type="text" id="NombreGrupo" name="NombreGrupo" size="60" maxlength="60" value="<?php echo $this->grupo->_getNombreGrupo() ?>"/></tdp><tdp><img id="passwordABot" height="20px" src="../img/red-button.png"/></tdp><tdp><texto-correccion id="passwordABotText"></texto-correccion></tdp>

					</trp>
					<tri>
						<tdi><?php echo $strings['DescripcionGrupo']; ?></tdi><tdi><input required type="text" id="DescripGrupo" name="DescripGrupo" size="100" maxlength="100" value="<?php echo $this->grupo->_getDescripcionGrupo(); ?>"/></tdi><tdi><img id="DNIABot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="DNIABotText"></texto-correccion></tdi>

					</tri>
					<br/><br/>
					<button onClick="document.getElementById('IdGrupo').value='<?php echo $this->grupo->_getIdGrupo(); ?>'; return validarFormularioGAE(document.getElementById('formularioEdit'), 'EDIT');" name="orden" value="EDIT"/><img src="../img/edit.png" height="30px"/>
				</table>
			</form>
		</fieldset>
		</div><?php
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
 ?>