<?php
/* Clase vista showall, para mostrar tuplas y datos que se le pasen a mostrar
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class Usuario_SHOWALL{  // declaración de clase
	
	var $resultado;//Tuplas a mostrar
	var $grupos;
	var $datosAMostrar;//Los datos de esas tuplas a mostrar

	// declaración constructor de la clase
	// se inicializa con los valores del formulario y el valor del botón submit pulsado
	function __construct($respuesta, $datosAMostrar){
		$this->datosAMostrar = $datosAMostrar;
		
		if(is_string($respuesta)){
			$this->resultado = $respuesta;
		}else{
			$resultado = array();
			
			$USU_GRUPO = array();
			$contadorFilas = 0;
			while($contadorFilas < mysqli_num_rows($respuesta[1])){
				$fila = $respuesta[1]->fetch_row();
				$USU_GRUPO[$contadorFilas][0] = $fila[0];
				$USU_GRUPO[$contadorFilas][1] = $fila[1];
				$contadorFilas++;
			}
			
			$i = 0;
			while($fila = $respuesta[0]->fetch_row()){//Se recorre el array de usuarios y se van rellenando datos
				$resultado[$i] = array();
				$resultado[$i]["login"] = $fila[0];
				$resultado[$i]["password"] = $fila[1];
				$resultado[$i]["DNI"] = $fila[2];
				$resultado[$i]["Nombre"] = $fila[3];
				$resultado[$i]["Apellidos"] = $fila[4];
				$resultado[$i]["Correo"] = $fila[5];
				$resultado[$i]["Direccion"] = $fila[6];
				$resultado[$i]["Telefono"] = $fila[7];
				$resultado[$i]["Grupos"] = array();
				$j = 0;//Iterador de USU_GRUPO
				$posicion = 0;
				while($j < sizeof($USU_GRUPO)){//Mientras haya filas con la posibildad de que esté este usuario en algun grupo
					if($USU_GRUPO[$j][0] == $resultado[$i]["login"]){//Se comprueba si los logins coinciden		
						$resultado[$i]["Grupos"][$posicion] = $USU_GRUPO[$j][1];//Y se inserta un grupo
						$posicion++;
					}
					$j++;
				}
				
				$i++;
			}
			
			$this->resultado = $resultado;
			
			$grupos = array();
			
			while($fila = $respuesta[2]->fetch_row()){
				$grupos[$fila[0]] = $fila[1];//$i es el grupo y 0 y 1 son id y nombre respectivamente
			}
			
			$this->grupos = $grupos;
		}
		$this->toString();
	} // fin del constructor

	// declaración de método pinta()
	// muestra por pantall html con los valores de los atributos de la clase
	// y un hiperenlace para volver al script php que la invocó
	function toString(){
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';
		if(is_string($this->resultado)){
			echo '<table id="tablaDatos" name="SHOWALL">
						<tr style="margin-bottom: 20px">
							<td style="background-color: white;"></td>
							<td style="background-color: white;"></td>
							<td style="background-color: white;"></td>
							<td style="background-color: white;"><center><img src="../img/add.png" onClick=document.getElementById("ADD").submit() height="40px"> <form id="ADD" onSubmit="controlador.php"><input type="hidden" name="orden" value="ADD"></form></center></td>
							<td style="background-color: white;"></td>
							<td style="background-color: white;"></td>
							<td style="background-color: white;"><center><img src="../img/search.png" onClick=document.getElementById("SEARCH").submit() height="40px"> <form id="SEARCH" onSubmit="controlador.php"><input type="hidden" name="orden" value="SEARCH"></form> </center></td>
							<td style="background-color: white;"></td>
							<td style="background-color: white;"></td>
							<td style="background-color: white;"></td>
						</tr>';
		}else{
			echo "<div class='general'>";
					
				echo '<table id="tablaDatos" name="SHOWALL">
						<tr style="margin-bottom: 20px">
							<td style="background-color: white;"></td>
							<td style="background-color: white;"></td>
							<td style="background-color: white;"></td>
							<td style="background-color: white;"><center><img src="../img/add.png" onClick=document.getElementById("ADD").submit() height="40px"> <form id="ADD" onSubmit="controlador.php"><input type="hidden" name="orden" value="ADD"></form></center></td>
							<td style="background-color: white;"></td>
							<td style="background-color: white;"></td>
							<td style="background-color: white;"><center><img src="../img/search.png" onClick=document.getElementById("SEARCH").submit() height="40px"> <form id="SEARCH" onSubmit="controlador.php"><input type="hidden" name="orden" value="SEARCH"></form> </center></td>
							<td style="background-color: white;"></td>
							<td style="background-color: white;"></td>
							<td style="background-color: white;"></td>
						</tr>';
				echo   '<tr>
							<th>'. $strings['Login'] .'</th>
							<th>'. $strings['password'] .'</th>
							<th>'. $strings['DNI'] .'</th>
							<th>'. $strings['Nombre'] .'</th>
							<th>'. $strings['Apellidos'] .'</th>
							<th>'. $strings['Correo'] .'</th>
							<th>'. $strings['Direccion'] .'</th>
							<th>'. $strings['Telefono'] .'</th>
							<th>'. $strings['Grupos'] .'</th>
							
							
							<th>'. $strings['Acciones'] .'</th>
						</tr>';/*Nombre de los datos*/
						
				$i = 0; //Variable para saber el numero de iteraciones e identificar formularios e inputs
				while($i < sizeof($this->resultado) && $fila = $this->resultado[$i]){//Mientras haya filas, se coje una y se muestra;
					?>
					<form id='formularioOpcion<?php echo $i ?>' method='GET' action='../Controllers/Usuario_CONTROLLER.php'>
						<input type='hidden' id="oculto<?php echo $i ?>" name='orden' value=''/>
						<tr>
							<input type='hidden' name='login' value="<?php echo $fila["login"]; ?>">
							<td id='login'><?php echo $fila["login"]; ?></td></input></form>
							<td id='password'><?php echo $fila["password"]; ?></td>
							<td id='DNI'><?php echo $fila["DNI"]; ?></td>
							<td id='Nombre'><?php echo $fila["Nombre"]; ?></td>
							<td id='Apellidos'><?php echo $fila["Apellidos"]; ?></td>
							<td id='Correo'><?php echo $fila["Correo"]; ?></td>
							<td id='Direccion'><?php echo $fila["Direccion"]; ?></td>
							<td id='Telefono'><?php echo $fila["Telefono"]; ?></td>
							<td id='Grupo'>
									<table>
										<?php 
										$j = 0;
										while($j < sizeof($fila["Grupos"])){//Para cada grupo se muestra por pantalla con una - delante y un salto de linea
											?>
											<tr>
												<td>
													<form id='formularioEliminarGrupo<?php echo $i; echo $j; ?>' method='POST' action='../Controllers/Usuario_CONTROLLER.php'>
														<input type='hidden' name='login' value="<?php echo $fila["login"]; ?>"></input>
														<input type='hidden' name='IdGrupo' value="<?php echo $fila["Grupos"][$j]; ?>"></input>
														<input type='hidden' name='orden' value="DESASI"></input>
													</form>
													<?php
													echo "- ";
													while($nombreGrupo = current($this->grupos)){//Iterador para poner el nombre del grupo
														if($fila["Grupos"][$j] == key($this->grupos)){
															echo $nombreGrupo;
														}
														next($this->grupos);
													}
													echo " ";
													reset($this->grupos); //Para realizar el siguiente while
													?>
												</td>
												
												<td>
													<img onClick="document.getElementById('formularioEliminarGrupo<?php echo $i; echo $j; ?>').submit()" src="../img/delete.png" height="12px" style='cursor: pointer'></img>
													<?php
													echo "\n";
													$j++;
													?>
												</td>
											</tr>
											<?php
										}
										?>
										<tr>
											<td>
												<form id='formularioAnadirGrupo<?php echo $i;?>' method="POST" action="../Controllers/Usuario_CONTROLLER.php">
													<input type='hidden' name='login' value="<?php echo $fila["login"]; ?>"></input>
													<input type='hidden' name='orden' value="ASIGNA"></input>
													<select name="IdGrupo"  style="margin-top: 15%">
													<?php
													while($nombreGrupo = current($this->grupos)){//Mientras haya grupos que permitir opcionar
														?>
														<img src="../img/erase.png"></img>
														<option value=<?php echo key($this->grupos); ?>><?php echo $nombreGrupo;?></option>
														<?php
														next($this->grupos);
													}
													reset($this->grupos);
													?>
													</select>
													
												</form>
											</td>
											<td>
												<img onClick="document.getElementById('formularioAnadirGrupo<?php echo $i; ?>').submit()" src="../img/add.png" height="12px" style='cursor: pointer'></img>
											</td>	
										</tr>
									</table>
							</td>
							<td>
								<img onMouseOver="document.getElementById('oculto<?php echo $i ?>').value='EDIT'" onClick="document.getElementById('formularioOpcion<?php echo $i ?>').submit()" src='../img/edit.png' height='20px;' style='cursor: pointer'/>
								<img onMouseOver="document.getElementById('oculto<?php echo $i ?>').value='DELETE'" onClick="document.getElementById('formularioOpcion<?php echo $i ?>').submit()" src='../img/erase.png' height='20px;' style='cursor: pointer'/>
								<img onMouseOver="document.getElementById('oculto<?php echo $i ?>').value='SHOWCURRENT'" onClick="document.getElementById('formularioOpcion<?php echo $i ?>').submit()" src='../img/detail.png' height='20px;' style='cursor: pointer'/>
							</td> 
						</tr>
				<?php
				$i++;
				}//Escribir una celda en el orden en el que se presentan los datos del showall, ponemos un input hidden para que al ejecutar las acciones de edit, showcurrent o delete tengamos el input del login o lo que necesitemos. Las acciones al final en la ultima celda, pero además si es showCurrent se cambia a post para no tener una URL de la nasa
				echo '</table>';
			
			echo '</div>';
		}
		include '../Views/Footer.php';	
	}
		// fin método pinta()
} //fin de class muestradatos
 ?>