<?php
/* Clase vista delete, con el fin de poder eliminar un usuario que se le pase como parámetro. Con la clave es suficiente
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class Usuario_DELETE{  // declaración de clase
	
	var $usuario;//Usuario a Deletear
	
	function __construct($usuario){
		$this->usuario = $usuario;
		$this->toString();//Imprimir por pantalla el formulario
	} // fin del constructor

	function toString(){		
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';?>
		<div class="general">
		
		<form method="POST" accept-charset="UTF-8" id="formularioDelete" name="formularioDelete" style="display: inline-block;" action="../Controllers/Usuario_CONTROLLER.php">			
			<input type="hidden" id="loginA" name="login" value="<?php echo $this->usuario->_getLogin(); ?>"/>
			
			<table id="tuplaDetail">
				<tr>
					<th><?php echo $strings['Login']; ?></th><td><?php echo $this->usuario->_getLogin(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['password']; ?></th><td><?php echo $this->usuario->_getpassword(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['DNI']; ?></th><td><?php echo $this->usuario->_getDNI(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['Nombre']; ?></th><td><?php echo $this->usuario->_getNombre(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['Apellidos']; ?></th><td><?php echo $this->usuario->_getApellidos(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['Correo']; ?></th><td><?php echo $this->usuario->_getCorreo(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['Direccion']; ?></th><td><?php echo $this->usuario->_getDireccion(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['Telefono']; ?></th><td><?php echo $this->usuario->_getTelefono(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['Accion']; ?></th><td><button onClick="document.getElementById('loginA').value='<?php echo $this->usuario->_getLogin(); ?>'" onClick="submit" type="submit" name="orden" value="DELETE"/><img src="../img/erase.png" height="20px"/></td>
				</tr>
				<tr>
					<th><?php echo $strings['Volver']; ?></th><td><a href="../Controllers/Usuario_CONTROLLER.php"><img src="../img/return.png" height="27px"/></a></td>
				</tr>
			</table>
		
		</form>
		</div><?php
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
 ?>