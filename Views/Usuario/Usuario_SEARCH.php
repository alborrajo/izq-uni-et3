<?php
/* Clase vista search, con el fin de buscar usuarios por cualquier campo y cualquier longitud 
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
class Usuario_SEARCH{  // declaración de clase


	// declaración constructor de la clase
	// se inicializa con los valores del formulario y el valor del botón submit pulsado
	function __construct(){
		$this->toString();
	} // fin del constructor

	// declaración de método pinta()
	// muestra por pantall html con los valores de los atributos de la clase
	// y un hiperenlace para volver al script php que la invocó
	function toString(){
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';?>	
		<div class="general">
		<fieldset><legend class="TituloFormulario"><?php echo $strings['Buscar']; ?></legend>
			<form method="POST" accept-charset="UTF-8" id="formularioSearch" name="formularioSearch" style="display: inline-block;" action="../Controllers/Usuario_CONTROLLER.php">
				<table class="formulario">
					<tri>
						<tdi><?php echo $strings['Login']; ?></tdi><tdi><input  type="text" id="loginA" name="login" size="12" maxlength="9"/></tdi>

					</tri>
					<trp>
						<tdp><?php echo $strings['password']; ?></tdp><tdp><input type="password" id="passwordA" name="password" size="25" maxlength="20"/>

					</trp>
					<tri>
						<tdi><?php echo $strings['DNI']; ?></tdi><tdi><input  type="text" id="DNIA" name="DNI" size="15" maxlength="9"/></tdi>

					</tri>
					<trp>
						<tdp><?php echo $strings['Nombre']; ?></tdp><tdp><input  type="text" id="NombreA" name="Nombre" size="35" maxlength="30"/></tdp>
						
					</trp>
					<tri>
						<tdi><?php echo $strings['Apellidos']; ?></tdi><tdi><input  type="text" id="ApellidosA" name="Apellidos" size="60" maxlength="50"/></tdi>

					</tri>
					<trp>
						<tdp><?php echo $strings['Correo']; ?></tdp><tdp><input  type="text" id="CorreoA" name="Correo" size="50" maxlength="40"/></tdp>
						
					</trp>
					<tri>
						<tdi><?php echo $strings['Direccion']; ?></tdi><tdi><input  type="text" id="DireccionA" name="Direccion" size="20" maxlength="15"/></tdi>

					</tri>
					<trp>
						<tdp><?php echo $strings['Telefono']; ?></tdp><tdp><input  type="text" id="TelefonoA" name="Telefono" size="20" maxlength="15"/></tdp>

					</trp><br/><br/>
					<button onClick="submit" type="submit" name="orden" value="SEARCH"/><img src="../img/search.png" height="30px"/>
				</table>
			</form>
		</fieldset>
		</div><?php
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
 ?>