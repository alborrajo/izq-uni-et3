<?php
/* Clase vista edit, con el fin de poder editar un trabajo que se le pase como paramétro
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class Trabajo_Edit{  // declaración de clase
	
	var $trabajo;//Usuario a editar
	
	function __construct($trabajo){
		$this->trabajo = $trabajo;
		$this->toString();//Imprimir por pantalla el formulario
	} // fin del constructor

	// declaración de método pinta()
	// muestra por pantall html con los valores de los atributos de la clase
	// y un hiperenlace para volver al script php que la invocó
	function toString(){
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';?>
		<div class="general">		
		<fieldset><legend class="TituloFormulario"><?php echo $strings['Editar']; ?></legend>
			<form method="POST" accept-charset="UTF-8" id="formularioEdit" name="formularioEdit" style="display: inline-block;" action="../Controllers/Trabajo_CONTROLLER.php">
				<table class="formulario">
					<tri>
						<tdi><?php echo $strings['IdTrabajo']; ?></tdi><tdi><input readonly type="text" id="IdTrabajo" name="IdTrabajo" size="6" maxlength="6" onBlur="comprobarTexto('IdTrabajo',6) && comprobarVacio(this)" value="<?php echo $this->trabajo->_getIdTrabajo(); ?>"/></tdi><tdi><img id="IdTrabajoBot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="IdTrabajoBotText"></texto-correccion></tdi>

					</tri>
					<trp>
						<tdp><?php echo $strings['NombreTrabajo']; ?></tdp><tdp><input required type="text" id="NombreTrabajo" name="NombreTrabajo" size="60" maxlength="60" onBlur="comprobarTexto('NombreTrabajo',60) && comprobarVacio(this)" value="<?php echo $this->trabajo->_getNombreTrabajo() ?>"/></tdp><tdp><img id="NombreTrabajoBot" height="20px" src="../img/red-button.png"/></tdp><tdp><texto-correccion id="NombreTrabajoBotText"></texto-correccion></tdp>

					</trp>
					<tri>
						<tdi><?php echo $strings['FechaIniTrabajo']; ?></tdi><tdi><input required readonly type="text" id="FechaIniTrabajo" name="FechaIniTrabajo" class="tcal tcalInput" onblur="comprobarVacio(this)" value="<?php echo $this->trabajo->_getFechaIniTrabajo(); ?>"/></tdi><tdi><img id="FechaIniTrabajoBot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="FechaIniTrabajoBotText"></texto-correccion></tdi>

					</tri>
					<tri>
						<tdi><?php echo $strings['FechaFinTrabajo']; ?></tdi><tdi><input required readonly type="text" id="FechaFinTrabajo" name="FechaFinTrabajo" class="tcal tcalInput" onblur="comprobarVacio(this)" value="<?php echo $this->trabajo->_getFechaFinTrabajo(); ?>"/></tdi><tdi><img id="FechaFinTrabajoBot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="FechaFinTrabajoBotText"></texto-correccion></tdi>

					</tri>
					<tri>
						<tdi>
							<?php echo $strings['PorcentajeNota']; ?>
						</tdi>
						<tdi>
							<input required type="text" id="PorcentajeNota" name="PorcentajeNota" onBlur="comprobarReal(2,0) && comprobarVacio(this)" value="<?php echo $this->trabajo->_getPorcentajeNota(); ?>"/>
						</tdi>
						<tdi>
							<img id="PorcentajeNotaBot" height="20px" src="../img/red-button.png"/>
						</tdi>
						<tdi>
							<texto-correccion id="PorcentajeNotaBotText"></texto-correccion>
						</tdi>

					</tri>
					<br/><br/>
					<button onClick="document.getElementById('IdTrabajo').value='<?php echo $this->trabajo->_getIdTrabajo(); ?>'; return validarFormularioTAE(document.getElementById('formularioEdit'), 'EDIT');" name="orden" value="EDIT"/><img src="../img/edit.png" height="30px"/>
				</table>
			</form>
		</fieldset>
		</div><?php
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
 ?>