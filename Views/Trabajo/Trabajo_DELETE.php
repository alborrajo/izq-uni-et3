<?php
/* Clase vista delete, con el fin de poder eliminar un trabajo que se le pase como parámetro. Con la clave es suficiente
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class Trabajo_DELETE{  // declaración de clase
	
	var $trabajo;//Trabajo a Deletear
	
	function __construct($trabajo){
		$this->trabajo = $trabajo;
		$this->toString();//Imprimir por pantalla el formulario
	} // fin del constructor

	function toString(){		
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';?>
		<div class="general">
		
		<form method="POST" accept-charset="UTF-8" id="formularioDelete" name="formularioDelete" style="display: inline-block;" action="../Controllers/Trabajo_CONTROLLER.php">			
			<input type="hidden" id="loginA" name="login" value="<?php echo $this->trabajo->_getIdTrabajo(); ?>"/>
			
			<table id="tuplaDetail">
				<tr>
					<th><?php echo $strings['IdTrabajo']; ?></th><td><?php echo $this->trabajo->_getIdTrabajo(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['NombreTrabajo']; ?></th><td><?php echo $this->trabajo->_getNombreTrabajo(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['FechaIniTrabajo']; ?></th><td><?php echo $this->trabajo->_getFechaIniTrabajo(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['FechaFinTrabajo']; ?></th><td><?php echo $this->trabajo->_getFechaFinTrabajo(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['PorcentajeNota']; ?></th><td><?php echo $this->trabajo->_getPorcentajeNota(); ?></td>
				</tr>
				
			</table>
		
		</form>
		</div><?php
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
 ?>