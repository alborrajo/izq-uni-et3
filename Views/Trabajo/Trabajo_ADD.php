<?php
/* Clase vista add, con el fin de poder añadir un trabajo a la BD
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class Trabajo_Add{  // declaración de clase


	// declaración constructor de la clase
	// se inicializa con los valores del formulario y el valor del botón submit pulsado
	function __construct(){
		$this->toString();
	} // fin del constructor
 
	// declaración de método pinta()
	// muestra por pantall html con los valores de los atributos de la clase
	// y un hiperenlace para volver al script php que la invocó
	function toString(){
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';?>	
		<div class="general">	
		<fieldset><legend class="TituloFormulario"><?php echo $strings['Añadir']; ?></legend>
			<form method="POST" accept-charset="UTF-8" id="formularioAdd" name="formularioAdd" style="display: inline-block;" action="../Controllers/Trabajo_CONTROLLER.php">
				<table class="formulario">
					<tri>
						<tdi><?php echo $strings['IdTrabajo']; ?></tdi><tdi><input required type="text" id="IdTrabajo" name="IdTrabajo" size="6" maxlength="6" onBlur="comprobarTexto('IdTrabajo',6) && comprobarVacio(this)"/></tdi><tdi><img id="IdTrabajoBot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="loginABotText"></texto-correccion></tdi>

					</tri>
					<trp>
						<tdp><?php echo $strings['NombreTrabajo']; ?></tdp><tdp><input required type="text" id="NombreTrabajo" name="NombreTrabajo" size="60" maxlength="60" onBlur="comprobarTexto('NombreTrabajo',60) && comprobarVacio(this)"/></tdp><tdp><img id="NombreTrabajoBot" height="20px" src="../img/red-button.png"/></tdp><tdp><texto-correccion id="passwordABotText"></texto-correccion></tdp>

					</trp>
					<tri>
						<tdi><?php echo $strings['FechaIniTrabajo']; ?></tdi><tdi><input required type="text" id="FechaIniTrabajo" name="FechaIniTrabajo" class="tcal tcalInput" value="" onblur="comprobarVacio(this)"/></tdi><tdi><img id="FechaIniTrabajoBot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="DNIABotText"></texto-correccion></tdi>

					</tri>
					<tri>
						<tdi><?php echo $strings['FechaFinTrabajo']; ?></tdi><tdi><input required type="text" id="FechaFinTrabajo" name="FechaFinTrabajo"  class="tcal tcalInput" value="" onblur="comprobarVacio(this)"/></tdi><tdi><img id="FechaFinTrabajoBot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="DNIABotText"></texto-correccion></tdi>

					</tri>
					<tri>
						<tdi><?php echo $strings['PorcentajeNota']; ?></tdi><tdi><input required type="text" id="PorcentajeNota" name="PorcentajeNota" onBlur="comprobarReal(2,0) && comprobarVacio(this)"/></tdi><tdi><img id="PorcentajeNotaBot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="DNIABotText"></texto-correccion></tdi>

					</tri>
					<br/><br/>
					<button onClick="return validarFormularioTAE(document.getElementById('formularioAdd'), 'ADD')" name="orden" value="ADD"/><img src="../img/add.png" height="30px"/>
				</table>
			</form>
		</fieldset>
		</div><?php
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
 ?>