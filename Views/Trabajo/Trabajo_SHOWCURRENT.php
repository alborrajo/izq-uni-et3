<?php 
/* Clase vista showcurrent, para mostrar una tupla en detalle
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class Trabajo_Showcurrent{  // declaración de clase
	var $trabajo;//Usuario recibido
	
	// declaración constructor de la clase
	// se inicializa con los valores del formulario y el valor del botón submit pulsado
	function __construct($trabajo){
		$this->trabajo = $trabajo;
		$this->toString();
	} // fin del constructor

	function toString(){
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php'; ?>	
		<div class="general">
		<table id="tuplaDetail">
			<tr>
				<th><?php echo $strings['IdTrabajo']; ?></th><td><?php echo $this->trabajo->_getIdTrabajo(); ?></td>
			</tr>
			<tr>
				<th><?php echo $strings['NombreTrabajo']; ?></th><td><?php echo $this->trabajo->_getNombreTrabajo(); ?></td>
			</tr>
			<tr>
				<th><?php echo $strings['FechaIniTrabajo']; ?></th><td><?php echo $this->trabajo->_getFechaIniTrabajo(); ?></td>
			</tr>
			<tr>
				<th><?php echo $strings['FechaFinTrabajo']; ?></th><td><?php echo $this->trabajo->_getFechaFinTrabajo(); ?></td>
			</tr>
			<tr>
				<th><?php echo $strings['PorcentajeNota']; ?></th><td><?php echo $this->trabajo->_getPorcentajeNota(); ?></td>
			</tr>
			
			<tr>
				<th><?php echo $strings['Volver']; ?></th><td><a href="../Controllers/Trabajo_CONTROLLER.php"><img src="../img/return.png" height="27px"/></a></td>
			</tr>
		</table>
		</div>
		<?php 
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
?>