<?php
/* Clase vista showall, para mostrar tuplas y datos que se le pasen a mostrar
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class Trabajo_Showall{  // declaración de clase
	
	var $resultado;//Las tuplas a mostrar
	var $datosAMostrar;//Los datos de esas tuplas a mostrar

	// declaración constructor de la clase
	// se inicializa con los valores del formulario y el valor del botón submit pulsado
	function __construct($respuesta, $datosAMostrar){
		$this->datosAMostrar = $datosAMostrar;
		$this->resultado = $respuesta;
		$this->toString();
	} // fin del constructor

	// declaración de método pinta()
	// muestra por pantall html con los valores de los atributos de la clase
	// y un hiperenlace para volver al script php que la invocó
	function toString(){
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';
		echo "<div class='general'>";
		
		if(is_string($this->resultado)){
			echo '<table id="tablaDatos" name="SHOWALL">';
			if(tienePermisosPara('TRABAJ', 'SHOWAL')){				
				echo 	'<tr style="margin-bottom: 20px">
							<td style="background-color: white;"></td>
							<td style="background-color: white;"></td>
							<td style="background-color: white;"><center><img src="../img/add.png" onClick=document.getElementById("ADD").submit() height="40px"> <form id="ADD" onSubmit="controlador.php"><input type="hidden" name="orden" value="ADD"></form> </center></td>
							<td style="background-color: white;"><center><img src="../img/search.png" onClick=document.getElementById("SEARCH").submit() height="40px"> <form id="SEARCH" onSubmit="controlador.php"><input type="hidden" name="orden" value="SEARCH"></form> </center></td>
							<td style="background-color: white;"></td>
							<td style="background-color: white;"></td>
						</tr>';
			}
			?>
			<table id="tablaDatos" name="SHOWALL">
				<tr style="margin-bottom: 20px">
					<td style="background-color: white;"></td>
					<td style="background-color: white;"></td>
					<td style="background-color: white;"></td>
					<td style="background-color: white;"><center><img src="../img/add.png" onClick=document.getElementById("ADD").submit() height="40px"> <form id="ADD" onSubmit="controlador.php"><input type="hidden" name="orden" value="ADD"></form> </center></td>
					<td style="background-color: white;">
					<td style="background-color: white;"><center><img src="../img/search.png" onClick=document.getElementById("SEARCH").submit() height="40px"> <form id="SEARCH" onSubmit="controlador.php"><input type="hidden" name="orden" value="SEARCH"></form> </center></td>
					<td style="background-color: white;"></td>
					<td style="background-color: white;"></td>
				</tr>
			</table>

			<table id="tuplaDetail">
				<tr>
					<th><?php echo $strings['Informacion']; ?></th><td><?php echo $this->resultado; ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['Volver']; ?></th><td><a href="../Controllers/Trabajo_CONTROLLER.php"><?php echo $strings['Volver']; ?></a></td>
				</tr>
			</table>
			<?php
		}else{			
			echo '<table id="tablaDatos" name="SHOWALL">';
			if(tienePermisosPara('TRABAJ', 'SHOWAL')){				
				echo 	'<tr style="margin-bottom: 20px">
							<td style="background-color: white;"></td>
							<td style="background-color: white;"></td>
							<td style="background-color: white;"><center><img src="../img/add.png" onClick=document.getElementById("ADD").submit() height="40px"> <form id="ADD" onSubmit="controlador.php"><input type="hidden" name="orden" value="ADD"></form> </center></td>
							<td style="background-color: white;"><center><img src="../img/search.png" onClick=document.getElementById("SEARCH").submit() height="40px"> <form id="SEARCH" onSubmit="controlador.php"><input type="hidden" name="orden" value="SEARCH"></form> </center></td>
							<td style="background-color: white;"></td>
							<td style="background-color: white;"></td>
						</tr>';
			}
			echo   '<tr>
						<th>'. $strings['IdTrabajo'] .'</th>
						<th>'. $strings['NombreTrabajo'] .'</th>
						<th>'. $strings['FechaIniTrabajo'] .'</th>
						<th>'. $strings['FechaFinTrabajo'] .'</th>
						<th>'. $strings['PorcentajeNota'] .'</th>
						<th>'. $strings['Acciones'] .'</th>
					</tr>';/*Nombre de los datos*/
					
			$i = 0; //Variable para saber el numero de iteraciones e identificar formularios e inputs
			while($fila = $this->resultado->fetch_row()){//Mientras haya filas, se coje una y se muestra
				?>
				<form id='formularioOpcion<?php echo $i ?>' method='GET' action='../Controllers/Trabajo_CONTROLLER.php'>
					<tr>
						<input type='hidden' name='IdTrabajo' value="<?php echo $fila[0]; ?>">
						<input type='hidden' id="oculto<?php echo $i ?>" name='orden' value=''/></form>
						
						<td id='IdTrabajo'><?php echo $fila[0]; ?></td></input>
						<td id='NombreTrabajo'><?php echo $fila[1]; ?></td>
						<td id='FechaIniTrabajo'><?php echo $fila[2]; ?></td>
						<td id='FechaFinTrabajo'><?php echo $fila[3]; ?></td>
						<td id='PorcentajeNota'><?php echo $fila[4]; ?></td>
						<td>
						<?php if(tienePermisosPara('TRABAJ', 'SHOWAL')){?>
							<img onMouseOver="document.getElementById('oculto<?php echo $i ?>').value='EDIT'" onClick="document.getElementById('formularioOpcion<?php echo $i ?>').submit()" src='../img/edit.png' height='20px;' style='cursor: pointer'/>						
							<img onMouseOver="document.getElementById('oculto<?php echo $i ?>').value='DELETE'" onClick="document.getElementById('formularioOpcion<?php echo $i ?>').submit()" src='../img/erase.png' height='20px;' style='cursor: pointer'/>
							<img onMouseOver="document.getElementById('oculto<?php echo $i ?>').value='SHOWCURRENT'" onClick="document.getElementById('formularioOpcion<?php echo $i ?>').submit()" src='../img/detail.png' height='20px;' style='cursor: pointer'/>

							<?php if(substr($fila[0], 0, 2) == "ET") { //Si es una entrega ?>
							<img onMouseOver="document.getElementById('oculto<?php echo $i ?>').value='AUTOQA'" onClick="validarAutoQA('formularioOpcion<?php echo $i ?>');" src='../img/autoqa.png' height='20px;' style='cursor: pointer'/>
							<?php } ?>
						<?php
						} else {
							?>
								<form id='correcciones<?php echo $i ?>' method='GET' action='../Controllers/Evaluacion_CONTROLLER.php'>
									<input type='hidden' name='IdTrabajo' value="<?php echo $fila[0]; ?>">
									<input type='hidden' id="oculto2<?php echo $i ?>" name='orden' value=''/></form>
									<img onMouseOver="document.getElementById('oculto2<?php echo $i; ?>').value='CORRECCION'" onClick="document.getElementById('correcciones<?php echo $i; ?>').submit()" src='../img/detail.png' height='20px;' style='cursor: pointer'/>
						
							<?php if(substr($fila[0], 0, 2) == "ET") { //Si es una entrega ?>
								<a href="../Controllers/Entrega_CONTROLLER.php?IdTrabajo=<?php echo $fila[0]; ?>">
									<img src='../img/continue.png' height='20px;' style='cursor: pointer'/>
								</a>
							<?php } else { //Si es un QA ?>
								<a href="../Controllers/QA_CONTROLLER.php?action=SEARCH&IdTrabajo=<?php echo $fila[0]; ?>">
									<img src='../img/continue.png' height='20px;' style='cursor: pointer'/>
								</a>
							<?php }
							
						}
						?>

						</td> 
					</tr>
			<?php
			$i++;
			}//Escribir una celda en el orden en el que se presentan los datos del showall, ponemos un input hidden para que al ejecutar las acciones de edit, showcurrent o delete tengamos el input del login o lo que necesitemos. Las acciones al final en la ultima celda, pero además si es showCurrent se cambia a post para no tener una URL de la nasa
			echo '</table>';
		}
		echo '</div>';
		include '../Views/Footer.php';	
	}
		// fin método pinta()
} //fin de class muestradatos
 ?>