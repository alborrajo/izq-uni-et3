<?php
/* Clase vista delete, con el fin de poder eliminar una evaluación que se le pase como parámetro. Con la clave es suficiente
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class Evaluacion_DELETE{  // declaración de clase
	
	var $evaluacion;//Usuario a Deletear
	
	function __construct($evaluacion){
		$this->evaluacion = $evaluacion;
		$this->toString();//Imprimir por pantalla el formulario
	} // fin del constructor

	function toString(){		
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';?>
		<div class="general">
		
		<form method="POST" accept-charset="UTF-8" id="formularioDelete" name="formularioDelete" style="display: inline-block;" action="../Controllers/Evaluacion_CONTROLLER.php">			
			<input type="hidden" id="IdTrabajoA" name="IdTrabajo" value="<?php echo $this->evaluacion->_getIdTrabajo(); ?>"/>
			<input type="hidden" id="AliasEvaluadoA" name="AliasEvaluado" value="<?php echo $this->evaluacion->_getAliasEvaluado(); ?>"/>
			<input type="hidden" id="LoginEvaluadorA" name="LoginEvaluador" value="<?php echo $this->evaluacion->_getLoginEvaluador(); ?>"/>
			<input type="hidden" id="IdHistoriaA" name="IdHistoria" value="<?php echo $this->evaluacion->_getIdHistoria(); ?>"/>
			
			<table id="tuplaDetail">
				<tr>
					<th><?php echo $strings['IdTrabajo']; ?></th><td><?php echo $this->evaluacion->_getIdTrabajo(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['LoginEvaluador']; ?></th><td><?php echo $this->evaluacion->_getLoginEvaluador(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['AliasEvaluado']; ?></th><td><?php echo $this->evaluacion->_getAliasEvaluado(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['IdHistoria']; ?></th><td><?php echo $this->evaluacion->_getIdHistoria(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['CorrectoA']; ?></th><?php if($this->evaluacion->_getCorrectoA() == 1){echo '<td>'.$strings['Correcto'] .'</td>'; }else{echo '<td>'. $strings['Incorrecto'] .'</td>';}?>
				</tr>
				<tr>
					<th><?php echo $strings['ComenIncorrectoA']; ?></th><td><?php echo $this->evaluacion->_getComenIncorrectoA(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['CorrectoP']; ?></th><?php if($this->evaluacion->_getCorrectoP() == 1){echo '<td>'.$strings['Correcto'] .'</td>'; }else{echo '<td>'. $strings['Incorrecto'] .'</td>';}?>
				</tr>
				<tr>
					<th><?php echo $strings['ComentIncorrectoP']; ?></th><td><?php echo $this->evaluacion->_getComentIncorrectoP(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['OK']; ?></th><td><?php echo $this->evaluacion->_getOK(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['Accion']; ?></th><td><button onClick="submit" type="submit" name="orden" value="DELETE"/><img src="../img/erase.png" height="20px"/></td>
				</tr>
				<tr>
					<th><?php echo $strings['Volver']; ?></th><td><a href="../Controllers/Evaluacion_CONTROLLER.php"><img src="../img/return.png" height="27px"/></a></td>
				</tr>
			</table>
		
		</form>
		</div><?php
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
 ?>