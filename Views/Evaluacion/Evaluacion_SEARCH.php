<?php
/* Clase vista search, con el fin de buscar evaluaciones por cualquier campo y cualquier longitud 
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
class Evaluacion_SEARCH{  // declaración de clase


	// declaración constructor de la clase
	// se inicializa con los valores del formulario y el valor del botón submit pulsado
	function __construct(){
		$this->toString();
	} // fin del constructor

	// declaración de método pinta()
	// muestra por pantall html con los valores de los atributos de la clase
	// y un hiperenlace para volver al script php que la invocó
	function toString(){
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';?>	
		<div class="general">
		<fieldset><legend class="TituloFormulario"><?php echo $strings['Buscar']; ?></legend>
			<form method="POST" accept-charset="UTF-8" id="formularioSearch" name="formularioSearch" style="display: inline-block;" action="../Controllers/Evaluacion_CONTROLLER.php">
				<table class="formulario">
					
					<tri>
						<tdi><?php echo $strings['IdTrabajo']; ?></tdi>
						<tdi><input type="text" id="IdTrabajoA" name="IdTrabajo" size="6" maxlength="6"/></tdi>

					</tri>
					<trp>
						<tdp><?php echo $strings['LoginEvaluador']; ?></tdp>
						<tdp><input type="text" id="LoginEvaluadorA" name="LoginEvaluador" size="9" maxlength="9"/></tdp>

					</trp>
					<tri>
						<tdi><?php echo $strings['AliasEvaluado']; ?></tdi>
						<tdi><input type="text" id="AliasEvaluadoA" name="AliasEvaluado" size="9" maxlength="9" /></tdi>

					</tri>
					<trp>
						<tdp><?php echo $strings['IdHistoria']; ?></tdp>
						<tdp><input type="number" id="IdHistoriaA" name="IdHistoria" size="2" maxlength="2"/></tdp>

					</trp>
					<tri>
						<tdi><?php echo $strings['CorrectoA']; ?></tdi>
						<tdi><input type="number" id="CorrectoAA" name="CorrectoA" size="1" maxlength="1"/></tdi>
						
					</tri>
					<trp>
						<tdp><?php echo $strings['ComenIncorrectoA']; ?></tdp>
						<tdp><input  type="text" id="ComenIncorrectoAA" name="ComenIncorrectoA" size="60" maxlength="300"/></tdp>

					</trp>
					<tri>
						<tdi><?php echo $strings['CorrectoP']; ?></tdi>
						<tdi><input type="number" id="CorrectoPA" name="CorrectoP" size="1" maxlength="1"/></tdi>
						
					</tri>
					<trp>
						<tdp><?php echo $strings['ComentIncorrectoP']; ?></tdp>
						<tdp><input type="text" id="ComentIncorrectoPA" name="ComentIncorrectoP" size="60" maxlength="300"/></tdp>

					</trp>
					<tri>
						<tdi><?php echo $strings['OK']; ?></tdi>
						<tdi><input type="number" id="OKA" name="OK" size="1" maxlength="1"/></tdi>
					
					</tri>

					<br/><br/>

					<button onClick="submit" type="submit" name="orden" value="SEARCH"/><img src="../img/search.png" height="30px"/>
				</table>
			</form>
		</fieldset>
		</div><?php
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
 ?>