<?php
/* Clase vista edit, con el fin de poder editar una evaluacion que se le pase como paramétro
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class Evaluacion_EDIT{  // declaración de clase
	
	var $evaluacion;//Usuario a editar
	
	function __construct($evaluacion){
		$this->evaluacion = $evaluacion;
		$this->toString();//Imprimir por pantalla el formulario
	} // fin del constructor

	// declaración de método pinta()
	// muestra por pantall html con los valores de los atributos de la clase
	// y un hiperenlace para volver al script php que la invocó
	function toString(){
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';?>
		<div class="general">		
		<fieldset><legend class="TituloFormulario"><?php echo $strings['Editar']; ?></legend>
			<form method="POST" accept-charset="UTF-8" id="formularioEdit" name="formularioEdit" style="display: inline-block;" action="../Controllers/Evaluacion_CONTROLLER.php">
				<table class="formulario">
				<tri>
					<tdi><?php echo $strings['IdTrabajo']; ?></tdi><tdi><input readonly required type="text" id="IdTrabajoA" name="IdTrabajo" size="6" maxlength="6" value="<?php echo $this->evaluacion->_getIdTrabajo(); ?>" onBlur="comprobarTexto('IdTrabajoA',6)"/></tdi><tdi><img id="IdTrabajoABot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="IdTrabajoABotText"></texto-correccion></tdi>

				</tri>
				<trp>
					<tdp><?php echo $strings['LoginEvaluador']; ?></tdp><tdp><input  readonly required type="text" id="LoginEvaluadorA" name="LoginEvaluador" size="9" maxlength="9" value="<?php echo $this->evaluacion->_getLoginEvaluador(); ?>" onBlur="comprobarTexto('LoginEvaluadorA',9)"/></tdp><tdp><img id="LoginEvaluadorABot" height="20px" src="../img/red-button.png"/></tdp><tdp><texto-correccion id="LoginEvaluadorABotText"></texto-correccion></tdp>

				</trp>
				<tri>
					<tdi><?php echo $strings['AliasEvaluado']; ?></tdi><tdi><input readonly required type="text" id="AliasEvaluadoA" name="AliasEvaluado" size="6" maxlength="6" value="<?php echo $this->evaluacion->_getAliasEvaluado(); ?>" onBlur="comprobarTexto(this,9)"/></tdi><tdi><img id="AliasEvaluadoABot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="AliasEvaluadoABotText"></texto-correccion></tdi>

				</tri>
				<trp>
					<tdp><?php echo $strings['IdHistoria']; ?></tdp><tdp><input readonly required type="number" id="IdHistoriaA" name="IdHistoria" size="2" maxlength="2" value="<?php echo $this->evaluacion->_getIdHistoria(); ?>" onBlur="comprobarReal(this,0,99,0)"/></tdp><tdp><img id="IdHistoriaABot" height="20px" src="../img/red-button.png"/></tdp><tdp><texto-correccion id="IdHistoriaABotText"></texto-correccion></tdp>

				</trp>
				<tri>
					<?php
					if($this->evaluacion->_getCorrectoA() == 1){
						?>
						<tdi><?php echo $strings['CorrectoA']; ?></tdi><tdi>
						<?php echo $strings['Correcto']; ?><input checked required type="radio" id="CorrectoAA" name="CorrectoA" size="1" maxlength="1" onBlur="comprobarReal('CorrectoAA',0,1,0)" value=1></input><br/>
						<?php echo $strings['Incorrecto']; ?><input required type="radio" id="CorrectoAA" name="CorrectoA" size="1" maxlength="1" onBlur="comprobarReal('CorrectoAA',0,1,0)" value=0></input>
						</tdi><tdi><img id="CorrectoAABot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="CorrectoAABotText"></texto-correccion></tdi>
						<?php
					}else{	
						?>
						<tdi><?php echo $strings['CorrectoA']; ?></tdi><tdi>
						<?php echo $strings['Correcto']; ?><input required type="radio" id="CorrectoAA" name="CorrectoA" size="1" maxlength="1" onBlur="comprobarReal('CorrectoAA',0,1,0)" value=1></input><br/>
						<?php echo $strings['Incorrecto']; ?><input checked required type="radio" id="CorrectoAA" name="CorrectoA" size="1" maxlength="1" onBlur="comprobarReal('CorrectoAA',0,1,0)" value=0></input>
						</tdi><tdi><img id="CorrectoAABot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="CorrectoAABotText"></texto-correccion></tdi>
						<?php
					}
					?>
				</tri>
				<trp>
					<tdp><?php echo $strings['ComenIncorrectoA']; ?></tdp><tdp><textarea cols="55" rows="7" type="text" id="ComenIncorrectoAA" name="ComenIncorrectoA" size="60" maxlength="300" onBlur="comprobarTexto('ComenIncorrectoAA',300)"><?php echo $this->evaluacion->_getComenIncorrectoA(); ?></textarea></tdp><tdp><img id="ComenIncorrectoAABot" height="20px" src="../img/red-button.png"/></tdp><tdp><texto-correccion id="ComenIncorrectoAABotText"></texto-correccion></tdp>

				</trp>

				<?php if(tienePermisosPara('EVALUA', 'EDIT')){ //Si no tiene permisos (ADMIN), Mostrar: ?>
				<tri>
					<?php
					if($this->evaluacion->_getCorrectoP() == 1){
						?>
						<tdi><?php echo $strings['CorrectoP']; ?></tdi><tdi>
						<?php echo $strings['Correcto']; ?><input checked required type="radio" id="CorrectoPA" name="CorrectoP" size="1" maxlength="1" onBlur="comprobarReal('CorrectoPA',0,1,0)" value=1></input><br/>
						<?php echo $strings['Incorrecto']; ?><input required type="radio" id="CorrectoPA" name="CorrectoP" size="1" maxlength="1" onBlur="comprobarReal('CorrectoPA',0,1,0)" value=0></input>
						</tdi><tdi><img id="CorrectoPABot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="CorrectoPABotText"></texto-correccion></tdi>
						<?php
					}else{	
						?>
						<tdi><?php echo $strings['CorrectoP']; ?></tdi><tdi>
						<?php echo $strings['Correcto']; ?><input required type="radio" id="CorrectoPA" name="CorrectoP" size="1" maxlength="1" onBlur="comprobarReal('CorrectoPA',0,1,0)" value=1></input><br/>
						<?php echo $strings['Incorrecto']; ?><input checked required type="radio" id="CorrectoPA" name="CorrectoP" size="1" maxlength="1" onBlur="comprobarReal('CorrectoPA',0,1,0)" value=0></input>
						</tdi><tdi><img id="CorrectoPABot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="CorrectoPABotText"></texto-correccion></tdi>
						<?php
					}
					?>
				</tri>
				<trp>
					<tdp><?php echo $strings['ComentIncorrectoP']; ?></tdp><tdp><textarea cols="55" rows="7" type="text" id="ComentIncorrectoPA" name="ComentIncorrectoP" size="60" maxlength="300" onBlur="comprobarTexto('ComentIncorrectoPA',300)"><?php echo $this->evaluacion->_getComentIncorrectoP(); ?></textarea></tdp><tdp><img id="ComentIncorrectoPABot" height="20px" src="../img/red-button.png"/></tdp><tdp><texto-correccion id="ComentIncorrectoPABotText"></texto-correccion></tdp>

				</trp>
				<tri>
					<tdi><?php echo $strings['OK']; ?></tdi><tdi><input type="number" id="OKA" name="OK" size="1" maxlength="1" value="<?php echo $this->evaluacion->_getOK(); ?>" onBlur="comprobarReal(this,0,1,0);"/></tdi><tdi><img id="OKABot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="OKABotText"></texto-correccion></tdi>
				
				</tri>
				<?php } ?>
			
					<br/><br/>

					<button onClick="return validarFormularioEvaluacionAER(document.getElementById('formularioEdit'), 'EDIT')" name="orden" value="EDIT"/><img src="../img/edit.png" height="30px"/>
				</table>
			</form>
		</fieldset>
		</div><?php
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
 ?>