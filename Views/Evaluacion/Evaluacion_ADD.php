<?php
/* Clase vista add, con el fin de poder añadir una evaluacion a la BD
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class Evaluacion_ADD{  // declaración de clase


	// declaración constructor de la clase
	// se inicializa con los valores del formulario y el valor del botón submit pulsado
	function __construct(){
		$this->toString();
	} // fin del constructor

	// declaración de método pinta()
	// muestra por pantall html con los valores de los atributos de la clase
	// y un hiperenlace para volver al script php que la invocó
	function toString(){
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';?>	
		<div class="general">	
		<fieldset><legend class="TituloFormulario"><?php echo $strings['Añadir']; ?></legend>
			<form method="POST" accept-charset="UTF-8" id="formularioAdd" name="formularioAdd" style="display: inline-block;" action="../Controllers/Evaluacion_CONTROLLER.php">
				<table class="formulario">
					<tri>
						<tdi><?php echo $strings['IdTrabajo']; ?></tdi><tdi><input required type="text" id="IdTrabajoA" name="IdTrabajo" size="6" maxlength="6" onBlur="comprobarTexto('IdTrabajoA',6)"/></tdi><tdi><img id="IdTrabajoABot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="IdTrabajoABotText"></texto-correccion></tdi>

					</tri>
					<trp>
						<tdp><?php echo $strings['LoginEvaluador']; ?></tdp><tdp><input required type="text" id="LoginEvaluadorA" name="LoginEvaluador" size="9" maxlength="9" onBlur="comprobarTexto('LoginEvaluadorA',9)"/></tdp><tdp><img id="LoginEvaluadorABot" height="20px" src="../img/red-button.png"/></tdp><tdp><texto-correccion id="LoginEvaluadorABotText"></texto-correccion></tdp>

					</trp>
					<tri>
						<tdi><?php echo $strings['AliasEvaluado']; ?></tdi><tdi><input required type="text" id="AliasEvaluadoA" name="AliasEvaluado" size="6" maxlength="6" onBlur="comprobarTexto(this,9)"/></tdi><tdi><img id="AliasEvaluadoABot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="AliasEvaluadoABotText"></texto-correccion></tdi>

					</tri>
					<trp>
						<tdp><?php echo $strings['IdHistoria']; ?></tdp><tdp><input required type="number" id="IdHistoriaA" name="IdHistoria" size="2" maxlength="2" onBlur="comprobarReal(this,0,99,0)"/></tdp><tdp><img id="IdHistoriaABot" height="20px" src="../img/red-button.png"/></tdp><tdp><texto-correccion id="IdHistoriaABotText"></texto-correccion></tdp>

					</trp>
					<tri>
						<tdi><?php echo $strings['CorrectoA']; ?></tdi><tdi>
						<?php echo $strings['Correcto']; ?><input required type="radio" id="CorrectoAA" name="CorrectoA" size="1" maxlength="1" onBlur="comprobarReal('CorrectoAA',0,1,0)" value=1></input><br/><?php echo $strings['Incorrecto']; ?><input required type="radio" id="CorrectoAA" name="CorrectoA" size="1" maxlength="1" onBlur="comprobarReal('CorrectoAA',0,1,0)" value=0></input>
						</tdi><tdi><img id="CorrectoAABot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="CorrectoAABotText"></texto-correccion></tdi>
					
					</tri>
					<trp>
						<tdp><?php echo $strings['ComenIncorrectoA']; ?></tdp><tdp><textarea cols="55" rows="7" type="text" id="ComenIncorrectoAA" name="ComenIncorrectoA" size="60" maxlength="300" onBlur="comprobarTexto('ComenIncorrectoAA',300)"></textarea></tdp><tdp><img id="ComenIncorrectoAABot" height="20px" src="../img/red-button.png"/></tdp><tdp><texto-correccion id="ComenIncorrectoAABotText"></texto-correccion></tdp>

					</trp>
					<tri>
						<tdi><?php echo $strings['CorrectoP']; ?></tdi><tdi>
						<?php echo $strings['Correcto']; ?><input required type="radio" id="CorrectoPA" name="CorrectoP" size="1" maxlength="1" onBlur="comprobarReal('CorrectoPA',0,1,0)" value=1></input><br/>
						<?php echo $strings['Incorrecto']; ?><input required type="radio" id="CorrectoPA" name="CorrectoP" size="1" maxlength="1" onBlur="comprobarReal('CorrectoPA',0,1,0)" value=0></input>
						</tdi><tdi><img id="CorrectoPABot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="CorrectoPABotText"></texto-correccion></tdi>
					</tri>
					<trp>
						<tdp><?php echo $strings['ComentIncorrectoP']; ?></tdp><tdp><textarea cols="55" rows="7" type="text" id="ComentIncorrectoPA" name="ComentIncorrectoP" size="60" maxlength="300" onBlur="comprobarTexto('ComentIncorrectoPA',300)"></textarea></tdp><tdp><img id="ComentIncorrectoPABot" height="20px" src="../img/red-button.png"/></tdp><tdp><texto-correccion id="ComentIncorrectoPABotText"></texto-correccion></tdp>

					</trp>
					<tri>
						<tdi><?php echo $strings['OK']; ?></tdi><tdi><input required type="number" id="OKA" name="OK" size="1" maxlength="1" onBlur="comprobarReal(this,0,1,0);"/></tdi><tdi><img id="OKABot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="OKABotText"></texto-correccion></tdi>
					
					</tri>
					
					<br/><br/>

					<button onClick="return validarFormularioEvaluacionAER(document.getElementById('formularioAdd'), 'ADD')" name="orden" value="ADD"/><img src="../img/add.png" height="30px"/>
				</table>
			</form>
		</fieldset>
		</div><?php
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
 ?>