<?php 
/* Clase vista showcurrent, para mostrar una tupla en detalle
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class Evaluacion_SHOWCURRENT{  // declaración de clase
	var $usuario;//Usuario recibido
	
	// declaración constructor de la clase
	// se inicializa con los valores del formulario y el valor del botón submit pulsado
	function __construct($usuario){
		$this->evaluacion = $usuario;
		$this->toString();
	} // fin del constructor

	function toString(){
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php'; ?>	
		<div class="general">
		<table id="tuplaDetail">
			<tr>
				<th><?php echo $strings['IdTrabajo']; ?></th><td><?php echo $this->evaluacion->_getIdTrabajo(); ?></td>
			</tr>
			<tr>
				<th><?php echo $strings['LoginEvaluador']; ?></th><td><?php echo $this->evaluacion->_getLoginEvaluador(); ?></td>
			</tr>
			<tr>
				<th><?php echo $strings['AliasEvaluado']; ?></th><td><?php echo $this->evaluacion->_getAliasEvaluado(); ?></td>
			</tr>
			<tr>
				<th><?php echo $strings['IdHistoria']; ?></th><td><?php echo $this->evaluacion->_getIdHistoria(); ?></td>
			</tr>
			<tr>
				<th><?php echo $strings['CorrectoA']; ?></th><?php if($this->evaluacion->_getCorrectoA() == 1){echo '<td>'.$strings['Correcto'] .'</td>'; }else{echo '<td>'. $strings['Incorrecto'] .'</td>';}?>
			</tr>
			<tr>
				<th><?php echo $strings['ComenIncorrectoA']; ?></th><td><?php echo $this->evaluacion->_getComenIncorrectoA(); ?></td>
			</tr>
			<tr>
				<th><?php echo $strings['CorrectoP']; ?></th><?php if($this->evaluacion->_getCorrectoP() == 1){echo '<td>'.$strings['Correcto'] .'</td>'; }else{echo '<td>'. $strings['Incorrecto'] .'</td>';}?>
			</tr>
			<tr>
				<th><?php echo $strings['ComentIncorrectoP']; ?></th><td><?php echo $this->evaluacion->_getComentIncorrectoP(); ?></td>
			</tr>
			<tr>
				<th><?php echo $strings['OK']; ?></th><td><?php echo $this->evaluacion->_getOK(); ?></td>
			</tr>
			</tr>
			<tr>
				<th><?php echo $strings['Volver']; ?></th><td><a href="../Controllers/Evaluacion_CONTROLLER.php"><img src="../img/return.png" height="27px"/></a></td>
			</tr>
		</table>
		</div>
		<?php 
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
?>