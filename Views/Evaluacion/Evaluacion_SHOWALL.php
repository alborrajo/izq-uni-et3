<?php
/* Clase vista showall, para mostrar tuplas y datos que se le pasen a mostrar
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class Evaluacion_SHOWALL{  // declaración de clase
	
	var $resultado;//Las tuplas a mostrar
	var $datosAMostrar;//Los datos de esas tuplas a mostrar

	// declaración constructor de la clase
	// se inicializa con los valores del formulario y el valor del botón submit pulsado
	function __construct($respuesta, $datosAMostrar){
		$datosAMostrarS = array();
		
		$datosAMostrarS["IdTrabajo"] = array_search('IdTrabajo', $datosAMostrar, true);
		$datosAMostrarS["LoginEvaluador"] = array_search('LoginEvaluador', $datosAMostrar, true);
		$datosAMostrarS["AliasEvaluado"] = array_search('AliasEvaluado', $datosAMostrar, true);
		$datosAMostrarS["IdHistoria"] = array_search('IdHistoria', $datosAMostrar, true);
		$datosAMostrarS["CorrectoA"] = array_search('CorrectoA', $datosAMostrar, true);
		$datosAMostrarS["ComenIncorrectoA"] = array_search('ComenIncorrectoA', $datosAMostrar, true);
		$datosAMostrarS["CorrectoP"] = array_search('CorrectoP', $datosAMostrar, true);
		$datosAMostrarS["ComentIncorrectoP"] = array_search('ComentIncorrectoP', $datosAMostrar, true);
		$datosAMostrarS["OK"] = array_search('OK', $datosAMostrar, true);
		$datosAMostrarS["EDIT"] = array_search('EDIT', $datosAMostrar, true);
		$datosAMostrarS["DELETE"] = array_search('DELETE', $datosAMostrar, true);
		$datosAMostrarS["SHOWCURRENT"] = array_search('SHOWCURRENT', $datosAMostrar, true);
		
		$this->datosAMostrar = $datosAMostrarS;
		$this->resultado = $respuesta;
		$this->toString();
	} // fin del constructor

	// declaración de método pinta()
	// muestra por pantall html con los valores de los atributos de la clase
	// y un hiperenlace para volver al script php que la invocó
	function toString(){
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';
		echo "<div class='general'>";
		
		if(is_string($this->resultado)){
			echo '<table id="tablaDatos" name="SHOWALL">';
			if(tienePermisosPara('TRABAJ', 'SHOWAL')){
				echo '<tr style="margin-bottom: 20px">
						<td style="background-color: white;"></td>
						<td style="background-color: white;"></td>
						<td style="background-color: white;"></td>
						<td style="background-color: white;"></td>						
						<td style="background-color: white;"><center><img src="../img/add.png" onClick=document.getElementById("ADD").submit() height="40px"> <form id="ADD" onSubmit="Controller/Evaluacion_CONTROLLER.php"><input type="hidden" name="orden" value="ADD"></form> </center></td>
						<td style="background-color: white;"><center><img src="../img/search.png" onClick=document.getElementById("SEARCH").submit() height="40px"> <form id="SEARCH" onSubmit="Controller/Evaluacion_CONTROLLER.php"><input type="hidden" name="orden" value="SEARCH"></form> </center></td>
						<td style="background-color: white;"></td>												
						<td style="background-color: white;"></td>
						<td style="background-color: white;"></td>
						<td style="background-color: white;"></td>
						
					</tr>';
			}
			?>
			<table id="tuplaDetail">
				<tr>
					<th><?php echo $strings['Informacion']; ?></th><td><?php echo $this->resultado; ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['Volver']; ?></th><td><a href="../Controllers/Evaluacion_CONTROLLER.php"><?php echo $strings['Volver']; ?></a></td>
				</tr>
			</table>
			<?php
		}else{		
			echo '<table id="tablaDatos" name="SHOWALL">';
			if(tienePermisosPara('TRABAJ', 'SHOWAL')){
				echo '<tr style="margin-bottom: 20px">
						<td style="background-color: white;"></td>
						<td style="background-color: white;"></td>
						<td style="background-color: white;"></td>
						<td style="background-color: white;"></td>						
						<td style="background-color: white;"><center><img src="../img/add.png" onClick=document.getElementById("ADD").submit() height="40px"> <form id="ADD" onSubmit="Controller/Evaluacion_CONTROLLER.php"><input type="hidden" name="orden" value="ADD"></form> </center></td>
						<td style="background-color: white;"><center><img src="../img/search.png" onClick=document.getElementById("SEARCH").submit() height="40px"> <form id="SEARCH" onSubmit="Controller/Evaluacion_CONTROLLER.php"><input type="hidden" name="orden" value="SEARCH"></form> </center></td>
						<td style="background-color: white;"></td>												
						<td style="background-color: white;"></td>
						<td style="background-color: white;"></td>
						<td style="background-color: white;"></td>
						
					</tr>';
			}
			echo   '<tr>';
						if($this->datosAMostrar["IdTrabajo"]){
							echo '<th>'. $strings['IdTrabajo'] .'</th>';
						}
						if($this->datosAMostrar["LoginEvaluador"]){
							echo '<th>'. $strings['LoginEvaluador'] .'</th>';
						}
						if($this->datosAMostrar["AliasEvaluado"]){
							echo '<th>'. $strings['AliasEvaluado'] .'</th>';
						}
						if($this->datosAMostrar["IdHistoria"]){
							echo '<th>'. $strings['IdHistoria'] .'</th>';
						}
						if($this->datosAMostrar["CorrectoA"]){
							echo '<th>'. $strings['CorrectoA'] .'</th>';
						}
						if($this->datosAMostrar["ComenIncorrectoA"]){
							echo '<th>'. $strings['ComenIncorrectoA'] .'</th>';
						}
						if($this->datosAMostrar["CorrectoP"]){
							echo '<th>'. $strings['CorrectoP'] .'</th>';
						}
						if($this->datosAMostrar["ComentIncorrectoP"]){
							echo '<th>'. $strings['ComentIncorrectoP'] .'</th>';
						}
						if($this->datosAMostrar["OK"]){
							echo '<th>'. $strings['OK'] .'</th>';
						}
						if($this->datosAMostrar["EDIT"] || $this->datosAMostrar["DELETE"] ||$this->datosAMostrar["SHOWCURRENT"]){
							echo '<th>'. $strings['Acciones'] .'</th>';
						}
						
			echo '</tr>';/*Nombre de los datos*/
					
			$i = 0; //Variable para saber el numero de iteraciones e identificar formularios e inputs
			while($fila = $this->resultado->fetch_row()){//Mientras haya filas, se coje una y se muestra
				?>
				<form id='formularioOpcion<?php echo $i ?>' method='GET' action='../Controllers/Evaluacion_CONTROLLER.php'>
					<tr>
						<input type='hidden' id="oculto<?php echo $i ?>" name='orden' value=''/>
						<input type="hidden" id="IdTrabajoA" name="IdTrabajo" value="<?php echo $fila[0]; ?>"/>
						<input type="hidden" id="AliasEvaluadoA" name="AliasEvaluado" value="<?php echo $fila[2]; ?>"/>
						<input type="hidden" id="LoginEvaluadorA" name="LoginEvaluador" value="<?php echo $fila[1]; ?>"/>
						<input type="hidden" id="IdHistoriaA" name="IdHistoria" value="<?php echo $fila[3]; ?>"/></form>
						 <?php
						 if($this->datosAMostrar["IdTrabajo"]){
							?>
							<td id='IdTrabajo'><?php echo $fila[0]; ?></td>
							<?php
						}
						if($this->datosAMostrar["LoginEvaluador"]){
							?>
							<td id='LoginEvaluador'><?php echo $fila[1]; ?></td>
							<?php
						}
						if($this->datosAMostrar["AliasEvaluado"]){
							?>
							<td id='AliasEvaluado'><?php echo $fila[2]; ?></td>
							<?php
						}
						if($this->datosAMostrar["IdHistoria"]){
							?>
							<td id='IdHistoria'><?php echo $fila[3]; ?></td>
							<?php
						}
						if($this->datosAMostrar["CorrectoA"]){
							if($fila[4] == 1){
								?>
								<td id='CorrectoA' style="color: white; background-color: #008800"><?php echo $strings['Correcto']; ?></td>
								<?php
							}else{
								?>
								<td id='CorrectoA' style="color: white; background-color: #880000"><?php echo $strings['Incorrecto']; ?></td>
								<?php
							}								
						}
						if($this->datosAMostrar["ComenIncorrectoA"]){
							if($fila[4] == 1){
								?>
								<td id='ComenIncorrectoA' style="color: white; background-color: #006600"><?php echo $fila[5]; ?></td>
								<?php
							}else{
								?>
								<td id='ComenIncorrectoA' style="color: white; background-color: #770000"><?php echo $fila[5]; ?></td>
								<?php
							}
						}
						if($this->datosAMostrar["CorrectoP"]){
							if($fila[6] == 1){
								?>
								<td id='CorrectoP' style="color: white; background-color: #008800"><?php echo $strings['Correcto']; ?></td>
								<?php
							}else{
								?>
								<td id='CorrectoP' style="color: white; background-color: #880000"><?php echo $strings['Incorrecto']; ?></td>
								<?php
							}
						}
						if($this->datosAMostrar["ComentIncorrectoP"]){
							if($fila[6] == 1){
								?>
								<td id='ComentIncorrectoP' style="color: white; background-color: #006600"><?php echo $fila[7]; ?></td>
								<?php
							}else{
								?>
								<td id='ComentIncorrectoP' style="color: white; background-color: #770000"><?php echo $fila[7]; ?></td>
								<?php
							}
						}
						if($this->datosAMostrar["OK"]){
							?>
							<td id='OK'><?php echo $fila[8]; ?></td>
							<?php
						}
						if($this->datosAMostrar["EDIT"] || $this->datosAMostrar["DELETE"] ||$this->datosAMostrar["SHOWCURRENT"]){
						?>
							<td>
								<?php
								if($this->datosAMostrar["EDIT"]){
									?>
									<img onMouseOver="document.getElementById('oculto<?php echo $i ?>').value='EDIT'" onClick="document.getElementById('formularioOpcion<?php echo $i ?>').submit()" src='../img/edit.png' height='20px;' style='cursor: pointer'/>
									<?php
								}
								if($this->datosAMostrar["DELETE"]){
									?>
									<img onMouseOver="document.getElementById('oculto<?php echo $i ?>').value='DELETE'" onClick="document.getElementById('formularioOpcion<?php echo $i ?>').submit()" src='../img/erase.png' height='20px;' style='cursor: pointer'/>
									<?php
								}
								if($this->datosAMostrar["SHOWCURRENT"]){
									?>
									<img onMouseOver="document.getElementById('oculto<?php echo $i ?>').value='SHOWCURRENT'" onClick="document.getElementById('formularioOpcion<?php echo $i ?>').submit()" src='../img/detail.png' height='20px;' style='cursor: pointer'/>
									<?php
								}
								?>					
							</td> 
							<?php
						}
						?>
					</tr>
			<?php
			$i++;
			}
			echo '</table>';
		}
		echo '</div>';
		include '../Views/Footer.php';	
	}
}//fin de class muestradatos
?>