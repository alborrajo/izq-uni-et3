<?php
/* Clase vista delete, con el fin de poder eliminar una funcionalidad que se le pase como parámetro. Con la clave es suficiente
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class Funcionalidad_DELETE{  // declaración de clase
	
	var $funcionalidad;//Usuario a Deletear
	
	function __construct($funcionalidad){
		$this->funcionalidad = $funcionalidad;
		$this->toString();//Imprimir por pantalla el formulario
	} // fin del constructor

	function toString(){		
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';?>
		<div class="general">
		
		<form method="POST" accept-charset="UTF-8" id="formularioDelete" name="formularioDelete" style="display: inline-block;" action="../Controllers/Funcionalidad_CONTROLLER.php">			
			<input type="hidden" id="IdFuncionalidad" name="IdFuncionalidad" value="<?php echo $this->funcionalidad->_getIdFuncionalidad(); ?>"/>
			
			<table id="tuplaDetail">
				<tr>
					<th><?php echo $strings['IdFuncionalidad']; ?></th><td><?php echo $this->funcionalidad->_getIdFuncionalidad(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['NombreFuncionalidad']; ?></th><td><?php echo $this->funcionalidad->_getNombreFuncionalidad(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['DescripFuncionalidad']; ?></th><td><?php echo $this->funcionalidad->_getDescripFuncionalidad(); ?></td>
				</tr>
				<tr>
				
					<th><?php echo $strings['Accion']; ?></th><td><button onClick="document.getElementById('IdFuncionalidad').value='<?php echo $this->funcionalidad->_getIdFuncionalidad(); ?>'" onClick="submit" type="submit" name="orden" value="DELETE"/><img src="../img/erase.png" height="20px"/></td>
				</tr>
				<tr>
					<th><?php echo $strings['Volver']; ?></th><td><a href="../Controllers/Funcionalidad_CONTROLLER.php"><img src="../img/return.png" height="27px"/></a></td>
				</tr>
				
			</table>
		
		</form>
		</div><?php
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
 ?>