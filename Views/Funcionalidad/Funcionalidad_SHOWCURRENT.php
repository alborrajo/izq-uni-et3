<?php 
/* Clase vista showcurrent, para mostrar una tupla en detalle
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class Funcionalidad_SHOWCURRENT{  // declaración de clase
	var $funcionalidad;//Usuario recibido
	
	// declaración constructor de la clase
	// se inicializa con los valores del formulario y el valor del botón submit pulsado
	function __construct($funcionalidad){
		$this->funcionalidad = $funcionalidad;
		$this->toString();
	} // fin del constructor

	function toString(){
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php'; ?>	
		<div class="general">
		<table id="tuplaDetail">
			<tr>
				<th><?php echo $strings['IdFuncionalidad']; ?></th><td><?php echo $this->funcionalidad->_getIdFuncionalidad(); ?></td>
			</tr>
			<tr>
				<th><?php echo $strings['NombreFuncionalidad']; ?></th><td><?php echo $this->funcionalidad->_getNombreFuncionalidad(); ?></td>
			</tr>
			<tr>
				<th><?php echo $strings['DescripFuncionalidad']; ?></th><td><?php echo $this->funcionalidad->_getDescripFuncionalidad(); ?></td>
			</tr>
			
			<tr>
				<th><?php echo $strings['Volver']; ?></th><td><a href="../Controllers/Funcionalidad_CONTROLLER.php"><img src="../img/return.png" height="27px"/></a></td>
			</tr>
		</table>
		</div>
		<?php 
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
?>