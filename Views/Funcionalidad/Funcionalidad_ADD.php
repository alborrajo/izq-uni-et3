<?php
/* Clase vista add, con el fin de poder añadir una funcionalidad a la BD
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class Funcionalidad_ADD{  // declaración de clase


	// declaración constructor de la clase
	// se inicializa con los valores del formulario y el valor del botón submit pulsado
	function __construct(){
		$this->toString();
	} // fin del constructor
 
	// declaración de método pinta()
	// muestra por pantall html con los valores de los atributos de la clase
	// y un hiperenlace para volver al script php que la invocó
	function toString(){
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';?>	
		<div class="general">	
		<fieldset><legend class="TituloFormulario"><?php echo $strings['Añadir']; ?></legend>
			<form method="POST" accept-charset="UTF-8" id="formularioAdd" name="formularioAdd" style="display: inline-block;" action="../Controllers/Funcionalidad_CONTROLLER.php">
				<table class="formulario">
					<tri>
						<tdi><?php echo $strings['IdFuncionalidad']; ?></tdi><tdi><input required type="text" id="IdFuncionalidad" name="IdFuncionalidad" size="6" maxlength="6" onBlur="comprobarTexto('IdFuncionalidad',6) && comprobarVacio(this)"/></tdi><tdi><img id="IdFuncionalidadBot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="loginABotText"></texto-correccion></tdi>

					</tri>
					<trp>
						<tdp><?php echo $strings['NombreFuncionalidad']; ?></tdp><tdp><input required type="text" id="NombreFuncionalidad" name="NombreFuncionalidad" size="60" maxlength="60" onBlur="comprobarTexto('NombreFuncionalidad',60) && comprobarVacio(this)"/></tdp><tdp><img id="NombreFuncionalidadBot" height="20px" src="../img/red-button.png"/></tdp><tdp><texto-correccion id="passwordABotText"></texto-correccion></tdp>

					</trp>
					<tri>
						<tdi><?php echo $strings['DescripFuncionalidad']; ?></tdi><tdi><input required type="text" id="DescripFuncionalidad" name="DescripFuncionalidad" size="100" maxlength="100" onBlur="comprobarTexto('DescripFuncionalidad',100) && comprobarVacio(this)"/></tdi><tdi><img id="DescripFuncionalidadBot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="DNIABotText"></texto-correccion></tdi>

					</tri>
					<br/><br/>
					<button onClick="return validarFormularioFAE(document.getElementById('formularioAdd'), 'ADD')" name="orden" value="ADD"/><img src="../img/add.png" height="30px"/>
				</table>
			</form>
		</fieldset>
		</div><?php
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
 ?>