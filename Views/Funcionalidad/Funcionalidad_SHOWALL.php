<?php
/* Clase vista showall, para mostrar tuplas y datos que se le pasen a mostrar
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class Funcionalidad_SHOWALL{  // declaración de clase
	
	var $resultado;//Las tuplas a mostrar
	var $acciones;//Acciones todas
	var $datosAMostrar;//Los datos de esas tuplas a mostrar

	// declaración constructor de la clase
	// se inicializa con los valores del formulario y el valor del botón submit pulsado
	function __construct($respuesta, $datosAMostrar){
		$this->datosAMostrar = $datosAMostrar;
		
		if(is_string($respuesta)){			
			$this->resultado = $respuesta;
		}else{
			$resultado = array();
			
			$FUNC_ACCION = array();
			$contadorFilas = 0;
			while($contadorFilas < mysqli_num_rows($respuesta[1])){
				$fila = $respuesta[1]->fetch_row();
				$FUNC_ACCION[$contadorFilas][0] = $fila[0];
				$FUNC_ACCION[$contadorFilas][1] = $fila[1];
				$contadorFilas++;
			}
			
			$i = 0;
			while($fila = $respuesta[0]->fetch_row()){
				$resultado[$i] = array();
				$resultado[$i]["IdFuncionalidad"] = $fila[0];
				$resultado[$i]["NombreFuncionalidad"] = $fila[1];
				$resultado[$i]["DescripFuncionalidad"] = $fila[2];
				$resultado[$i]["Acciones"] = array();
				$j = 0;//Iterador de USU_accion
				$posicion = 0;
				while($j < sizeof($FUNC_ACCION)){//Mientras haya filas con la posibildad de que esté este usuario en algun accion
					if($FUNC_ACCION[$j][0] == $resultado[$i]["IdFuncionalidad"]){//Comprobar idfunc
						$resultado[$i]["Acciones"][$posicion] = $FUNC_ACCION[$j][1];//Y se inserta un accion
						$posicion++;
					}
					$j++;
				}
				
				$i++;
			}
			
			$this->resultado = $resultado;
			
			$acciones = array();
			
			while($fila = $respuesta[2]->fetch_row()){
				$acciones[$fila[0]] = $fila[1];
			}
			
			$this->acciones = $acciones;
		}
		$this->toString();
	} // fin del constructor

	// declaración de método pinta()
	// muestra por pantall html con los valores de los atributos de la clase
	// y un hiperenlace para volver al script php que la invocó
	function toString(){
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';
		echo "<div class='general'>";
		
		if(is_string($this->resultado)){
			echo '<table id="tablaDatos" name="SHOWALL">
					<tr style="margin-bottom: 20px">
						<td style="background-color: white;"></td>
						<td style="background-color: white;"></td>
						<td style="background-color: white;"></td>
						<td style="background-color: white;"><center><img src="../img/add.png" onClick=document.getElementById("ADD").submit() height="40px"> <form id="ADD" onSubmit="controlador.php"><input type="hidden" name="orden" value="ADD"></form> </center></td>
						<td style="background-color: white;">
						<td style="background-color: white;"><center><img src="../img/search.png" onClick=document.getElementById("SEARCH").submit() height="40px"> <form id="SEARCH" onSubmit="controlador.php"><input type="hidden" name="orden" value="SEARCH"></form> </center></td>
						<td style="background-color: white;"></td>
						<td style="background-color: white;"></td>
					</tr>';
			?>
			<table id="tuplaDetail">
				<tr>
					<th><?php echo $strings['Informacion']; ?></th><td><?php echo $this->resultado; ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['Volver']; ?></th><td><a href="../Controllers/Funcionalidad_CONTROLLER.php"><?php echo $strings['Volver']; ?></a></td>
				</tr>
			</table>
			<?php
		}else{			
			echo '<table id="tablaDatos" name="SHOWALL">
					<tr style="margin-bottom: 20px">
						<td style="background-color: white;"></td>
						<td style="background-color: white;"><center><img src="../img/add.png" onClick=document.getElementById("ADD").submit() height="40px"> <form id="ADD" onSubmit="controlador.php"><input type="hidden" name="orden" value="ADD"></form> </center></td>
						<td style="background-color: white;"></td>
						<td style="background-color: white;"><center><img src="../img/search.png" onClick=document.getElementById("SEARCH").submit() height="40px"> <form id="SEARCH" onSubmit="controlador.php"><input type="hidden" name="orden" value="SEARCH"></form> </center></td>
						<td style="background-color: white;"></td>
					</tr>';
			echo   '<tr>
						<th>'. $strings['IdFuncionalidad'] .'</th>
						<th>'. $strings['NombreFuncionalidad'] .'</th>
						<th>'. $strings['DescripFuncionalidad'] .'</th>
						<th>'. $strings['Acciones'] .'</th>
						
						<th>'. $strings['Acciones'] .'</th>
					</tr>';/*Nombre de los datos*/
					
			$i = 0; //Variable para saber el numero de iteraciones e identificar formularios e inputs
			while($i < sizeof($this->resultado) && $fila = $this->resultado[$i]){//Mientras haya filas, se coje una y se muestra
				?>
				<form id='formularioOpcion<?php echo $i ?>' method='GET' action='../Controllers/Funcionalidad_CONTROLLER.php'>
					<input type='hidden' id="oculto<?php echo $i ?>" name='orden' value=''/>
					<tr>
						<input type='hidden' name='IdFuncionalidad' value="<?php echo $fila["IdFuncionalidad"]; ?>"><td id='IdFuncionalidad'><?php echo $fila["IdFuncionalidad"]; ?></td></input></form>
						<td id='NombreFuncionalidad'><?php echo $fila["NombreFuncionalidad"]; ?></td>
						<td id='DescripFuncionalidad'><?php echo $fila["DescripFuncionalidad"]; ?></td>
						<td id='Accion'>
								<table>
									<?php 
									$j = 0;
									while($j < sizeof($fila["Acciones"])){//Para cada accion se muestra por pantalla con una - delante y un salto de linea
										?>
										<tr>
											<td>
												<form id='formularioEliminarAccion<?php echo $i; echo $j; ?>' method='POST' action='../Controllers/Funcionalidad_CONTROLLER.php'>
													<input type='hidden' name='IdFuncionalidad' value="<?php echo $fila["IdFuncionalidad"]; ?>"></input>
													<input type='hidden' name='IdAccion' value="<?php echo $fila["Acciones"][$j]; ?>"></input>
													<input type='hidden' name='orden' value="DESASIGNAR"></input>
												</form>
												<?php
												echo "- ";
												while($nombreAccion = current($this->acciones)){//Iterador para poner el nombre del accion
													if($fila["Acciones"][$j] == key($this->acciones)){
														echo $nombreAccion;
													}
													next($this->acciones);
												}
												echo " ";
												reset($this->acciones); //Para realizar el siguiente while
												?>
											</td>
											
											<td>
												<img onClick="document.getElementById('formularioEliminarAccion<?php echo $i; echo $j; ?>').submit()" src="../img/delete.png" height="12px" style='cursor: pointer'></img>								
												<?php
												echo "\n";
												$j++;
												?>
											</td>
										</tr>
										<?php
									}
									?>
									<tr>
										<td>
											<form id='formularioAnadirAccion<?php echo $i;?>' method="POST" action="../Controllers/Funcionalidad_CONTROLLER.php">
												<input type='hidden' name='IdFuncionalidad' value="<?php echo $fila["IdFuncionalidad"]; ?>"></input>
												<input type='hidden' name='orden' value="ASIGNAR"></input>
												<select name="IdAccion"  style="margin-top: 15%">
												<?php
												while($nombreAccion = current($this->acciones)){//Mientras haya accions que permitir opcionar
													?>
													<img src="../img/erase.png"></img>
													<option value=<?php echo key($this->acciones); ?>><?php echo $nombreAccion;?></option>
													<?php
													next($this->acciones);
												}
												reset($this->acciones);
												?>
												</select>
											</form>
										</td>
									
									
										<td>
											<img onClick="document.getElementById('formularioAnadirAccion<?php echo $i; ?>').submit()" src="../img/add.png" height="12px" style='cursor: pointer'></img>
										</td>
									</tr>									
								</table>
						</td>
						<td>
							<img onMouseOver="document.getElementById('oculto<?php echo $i ?>').value='EDIT'" onClick="document.getElementById('formularioOpcion<?php echo $i ?>').submit()" src='../img/edit.png' height='20px;' style='cursor: pointer'/>
							<img onMouseOver="document.getElementById('oculto<?php echo $i ?>').value='DELETE'" onClick="document.getElementById('formularioOpcion<?php echo $i ?>').submit()" src='../img/erase.png' height='20px;' style='cursor: pointer'/>
							<img onMouseOver="document.getElementById('oculto<?php echo $i ?>').value='SHOWCURRENT'" onClick="document.getElementById('formularioOpcion<?php echo $i ?>').submit()" src='../img/detail.png' height='20px;' style='cursor: pointer'/>
						</td> 
					</tr>
			<?php
			$i++;
			}//Escribir una celda en el orden en el que se presentan los datos del showall, ponemos un input hidden para que al ejecutar las acciones de edit, showcurrent o delete tengamos el input del login o lo que necesitemos. Las acciones al final en la ultima celda, pero además si es showCurrent se cambia a post para no tener una URL de la nasa
			echo '</table>';
		}
		echo '</div>';
		include '../Views/Footer.php';	
	}
		// fin método pinta()
} //fin de class muestradatos
 ?>