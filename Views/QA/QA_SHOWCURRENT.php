<?php 
/* Clase vista showcurrent para qa, para mostrar una tupla en detalle
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class QA_SHOWCURRENT{  // declaración de clase
	var $qa;//Usuario recibido
	
	// declaración constructor de la clase
	// se inicializa con los valores del formulario y el valor del botón submit pulsado
	function __construct($qa){
		$this->qa = $qa;
		$this->toString();
	} // fin del constructor

	function toString(){
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php'; ?>	
		<div class="general">
		<table id="tuplaDetail">
			<tr>
				<th><?php echo $strings['IdTrabajo']; ?></th><td><?php echo $this->qa->_getIdTrabajo(); ?></td>
			</tr>
			<tr>
				<th><?php echo $strings['LoginEvaluador']; ?></th><td><?php echo $this->qa->_getLoginEvaluador(); ?></td>
			</tr>
			<tr>
				<th><?php echo $strings['LoginEvaluado']; ?></th><td><?php echo $this->qa->_getLoginEvaluado(); ?></td>
			</tr>
			<tr>
				<th><?php echo $strings['AliasEvaluado']; ?></th><td><?php echo $this->qa->_getAliasEvaluado(); ?></td>
			</tr>
 			<tr>
				<th><?php echo $strings['Volver']; ?></th><td><a href="../Controllers/QA_CONTROLLER.php"><img src="../img/return.png" height="27px"/></a></td>
			</tr>
		</table>
		</div>
		<?php 
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
?>