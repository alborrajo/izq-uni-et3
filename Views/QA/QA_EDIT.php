<?php
/* Clase vista edit para qa, con el fin de poder editar un qa que se le pase como paramétro
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class QA_EDIT{  // declaración de clase
	
	var $qa;//Usuario a editar
	
	function __construct($qa){
		$this->qa = $qa;
		$this->toString();//Imprimir por pantalla el formulario
	} // fin del constructor

	// declaración de método pinta()
	// muestra por pantall html con los valores de los atributos de la clase
	// y un hiperenlace para volver al script php que la invocó
	function toString(){
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';?>
		<div class="general">		
		<fieldset><legend class="TituloFormulario"><?php echo $strings['Editar']; ?></legend>
			<form method="POST" accept-charset="UTF-8" id="formularioEdit" name="formularioEdit" style="display: inline-block;" action="../Controllers/QA_CONTROLLER.php">
				<table class="formulario">
					<tri>
						<tdi><?php echo $strings['IdTrabajo']; ?></tdi><tdi><input readonly type="text" id="IdTrabajo" name="IdTrabajo" size="6" maxlength="6" onblur="comprobarTexto(this,6) && comprobarVacio(this)" value="<?php echo $this->qa->_getIdTrabajo(); ?>"/></tdi><tdi><img id="loginABot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="loginABotText"></texto-correccion></tdi>

					</tri>
					<trp>
						<tdp><?php echo $strings['LoginEvaluador']; ?></tdp><tdp><input required type="text" id="LoginEvaluador" name="LoginEvaluador" size="9" maxlength="9" onblur="comprobarTexto(this,9) && comprobarVacio(this)" value="<?php echo $this->qa->_getLoginEvaluador(); ?>"/></tdp><tdp><img id="passwordABot" height="20px" src="../img/red-button.png"/></tdp><tdp><texto-correccion id="passwordABotText"></texto-correccion></tdp>

					</trp>
					<tri>
						<tdi><?php echo $strings['LoginEvaluado']; ?></tdi><tdi><input required type="text" id="LoginEvaluado" name="LoginEvaluado" size="9" maxlength="9" onblur="comprobarTexto(this,9) && comprobarVacio(this)"  value="<?php echo $this->qa->_getLoginEvaluado(); ?>"/></tdi><tdi><img id="DNIABot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="DNIABotText"></texto-correccion></tdi>

					</tri>
					<trp>
						<tdp><?php echo $strings['AliasEvaluado']; ?></tdp><tdp><input required type="text" id="AliasEvaluado" name="AliasEvaluado" size="6" maxlength="6" onblur="comprobarTexto(this,6) && comprobarVacio(this)" value="<?php echo $this->qa->_getAliasEvaluado(); ?>"/></tdp><tdp><img id="telefonoABot" height="20px" src="../img/red-button.png"/></tdp><tdp><texto-correccion id="telefonoABotText"></texto-correccion></tdp>

					</trp>
					<br/><br/>
					<button onClick="document.getElementById('IdTrabajo').value='<?php echo $this->qa->_getIdTrabajo(); ?>'; return validarFormularioQAE(document.getElementById('formularioEdit'), 'EDIT');" name="orden" value="EDIT"/><img src="../img/edit.png" height="30px"/>
				</table>
			</form>
		</fieldset>
		</div><?php
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
 ?>