<?php
/* Clase vista delete para qa, con el fin de poder eliminar un qa que se le pase como parámetro. Con la clave es suficiente
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class QA_DELETE{  // declaración de clase
	
	var $qa;//Usuario a Deletear
	
	function __construct($qa){
		$this->qa = $qa;
		$this->toString();//Imprimir por pantalla el formulario
	} // fin del constructor

	function toString(){		
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';?>
		<div class="general">
		
		<form method="POST" accept-charset="UTF-8" id="formularioDelete" name="formularioDelete" style="display: inline-block;" action="../Controllers/QA_CONTROLLER.php">			
			<input type="hidden" id="IdTrabajo" name="IdTrabajo" value="<?php echo $this->qa->_getIdTrabajo(); ?>"/>
			<input type="hidden" id="LoginEvaluador" name="LoginEvaluador" value="<?php echo $this->qa->_getLoginEvaluador(); ?>"/>
			<input type="hidden" id="AliasEvaluado" name="AliasEvaluado" value="<?php echo $this->qa->_getAliasEvaluado(); ?>"/>
			
			<table id="tuplaDetail">
				<tr>
					<th><?php echo $strings['IdTrabajo']; ?></th><td><?php echo $this->qa->_getIdTrabajo(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['LoginEvaluador']; ?></th><td><?php echo $this->qa->_getLoginEvaluador(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['LoginEvaluado']; ?></th><td><?php echo $this->qa->_getLoginEvaluado(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['AliasEvaluado']; ?></th><td><?php echo $this->qa->_getAliasEvaluado(); ?></td>
				</tr>
				<tr>
					<th><?php echo $strings['Accion']; ?></th><td><button onClick="document.getElementById('IdTrabajo').value='<?php echo $this->qa->_getIdTrabajo(); ?>'" onClick="submit" type="submit" name="orden" value="DELETE"/><img src="../img/erase.png" height="20px"/></td>
				</tr>
				<tr>
					<th><?php echo $strings['Volver']; ?></th><td><a href="../Controllers/QA_CONTROLLER.php"><img src="../img/return.png" height="27px"/></a></td>
				</tr>
			</table>
		
		</form>
		</div><?php
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
 ?>