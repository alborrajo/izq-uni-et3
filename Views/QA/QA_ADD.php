<?php
/* Clase vista add para qa, con el fin de poder añadir un qa a la BD
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
	
class QA_ADD{  // declaración de clase


	// declaración constructor de la clase
	// se inicializa con los valores del formulario y el valor del botón submit pulsado
	function __construct(){
		$this->toString();
	} // fin del constructor

	// declaración de método pinta()
	// muestra por pantall html con los valores de los atributos de la clase
	// y un hiperenlace para volver al script php que la invocó
	function toString(){
		include '../Views/Header.php';
		include '../Views/MenuNavHorizontal.php';
		include '../Views/MenuLatIzq.php';?>	
		<div class="general">	
		<fieldset><legend class="TituloFormulario"><?php echo $strings['Añadir']; ?></legend>
			<form method="POST" accept-charset="UTF-8" id="formularioAdd" name="formularioAdd" style="display: inline-block;" action="../Controllers/QA_CONTROLLER.php">
				<table class="formulario">
					<tri>
						<tdi><?php echo $strings['IdTrabajo']; ?></tdi><tdi><input required type="text" id="IdTrabajo" name="IdTrabajo" size="6" maxlength="6" onblur="comprobarTexto(this,6) && comprobarVacio(this)" /></tdi><tdi><img id="loginABot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="loginABotText"></texto-correccion></tdi>

					</tri>
					<trp>
						<tdp><?php echo $strings['LoginEvaluador']; ?></tdp><tdp><input required type="text" id="LoginEvaluador" name="LoginEvaluador" size="9" maxlength="9" onblur="comprobarTexto(this,9) && comprobarVacio(this)" /></tdp><tdp><img id="passwordABot" height="20px" src="../img/red-button.png"/></tdp><tdp><texto-correccion id="passwordABotText"></texto-correccion></tdp>

					</trp>
					<tri>
						<tdi><?php echo $strings['LoginEvaluado']; ?></tdi><tdi><input required type="text" id="LoginEvaluado" name="LoginEvaluado" size="9" maxlength="9" onblur="comprobarTexto(this,9) && comprobarVacio(this)" /></tdi><tdi><img id="DNIABot" height="20px" src="../img/red-button.png"/></tdi><tdi><texto-correccion id="DNIABotText"></texto-correccion></tdi>

					</tri>
					<trp>
						<tdp><?php echo $strings['AliasEvaluado']; ?></tdp><tdp><input required type="text" id="AliasEvaluado" name="AliasEvaluado" size="6" maxlength="6" onblur="comprobarTexto(this,6) && comprobarVacio(this)" /></tdp><tdp><img id="telefonoABot" height="20px" src="../img/red-button.png"/></tdp><tdp><texto-correccion id="telefonoABotText"></texto-correccion></tdp>

					</trp>
					<br/><br/>
					<button onClick="return validarFormularioQAE(document.getElementById('formularioAdd'), 'ADD')" name="orden" value="ADD"/><img src="../img/add.png" height="30px"/>
				</table>
			</form>
		</fieldset>
		</div><?php
		include '../Views/Footer.php';
	} // fin método pinta()
} //fin de class muestradatos
 ?>