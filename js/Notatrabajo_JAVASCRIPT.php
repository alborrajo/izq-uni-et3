/* Php del javascript con el fin de poder poner los $strings dentro de los mensajes de error, es básicamente un .js con formato .php que se carga en el head
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
<?php 
	session_start();
	include_once '../Locales/Strings_'. $_SESSION['idioma'].'.php';//Idioma
?>
/* 	Javascript de funciones para la entidad NOTATRABAJO
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/

function validarNota(campo){
	//Función para validar la nota del campo nota
	var tamano = 5; //2 entero , o . y 2 decimales
	if(campo.readOnly){//Si el campo es readonly, la validacion es correcta pues es ineditable, y mensaje sobre ello
		campoCorrecto(campo.id, "<?php echo $strings['No se puede editar este campo']; ?>");//
		return true;
	}else if(comprobarVacio(campo)){//Si el campo login es vacío, retornamos false pues necesita algun caracter
		campoIncorrecto(campo.id, "<?php echo $strings['Rellena el campo']; ?>");//Mensaje de error
		return false;	
	}else if(!comprobarTexto(campo, tamano)){//Si el campo login tiene una cantidad de caracteres mayor al límite permitido, retornamos false
		campoIncorrecto(campo.id, "<?php echo $strings['El tamaño de la nota sobrepasa el límite de']; ?>" + size);//Mensaje de error
		return false;
	}else if(!comprobarReal2(campo, 0, 10)){
		campoIncorrecto(campo.id, "<?php echo $strings['El valor no está entre los especificados (valormenor y valormayor)']; ?>");//Mensaje de error
		return false;
	}else{
		//Si ha llegado hasta aquí, debe estar bien. Retornar true
		campoCorrecto(campo.id);//Poner el mensaje de error a correcto (sin errores)
		return true;		
	}
}

function validarIdTrabajo(campo){
	var tamano = 6; //2 entero , o . y 2 decimales
	if(campo.readOnly){//Si el campo es readonly, la validacion es correcta pues es ineditable, y mensaje sobre ello
		campoCorrecto(campo.id, "<?php echo $strings['No se puede editar este campo']; ?>");//
		return true;
	}else if(comprobarVacio(campo)){//Si el campo login es vacío, retornamos false pues necesita algun caracter
		campoIncorrecto(campo.id, "<?php echo $strings['Rellena el campo']; ?>");//Mensaje de error
		return false;	
	}else if(!comprobarTexto(campo, tamano)){//Si el campo login tiene una cantidad de caracteres mayor al límite permitido, retornamos false
		campoIncorrecto(campo.id, "<?php echo $strings['El tamaño del IdTrabajo sobrepasa el límite de ']; ?>" + size);//Mensaje de error
		return false;
	}else{
		//Si ha llegado hasta aquí, debe estar bien. Retornar true
		campoCorrecto(campo.id);//Poner el mensaje de error a correcto (sin errores)
		return true;		
	}
}

function validarFormularioNotatrabajoAER(formulario, orden){/*Add, Edit o Registro*/
	//Validación completa de un formulario pasando un formulario (formulario). Comprobará todos los elementos del formulario comparandolos con las posibilidades, y si da alguna, validarla.
	var elementos = formulario.elements;//Todos los inputs del formulario
	var tamano = elementos.length;//Cantidad de elementos del array elementos
	var validaciones = 0; //Cantidad de validaciones que se han realizado sobre los inputs
	var bucle = 0;//Cantidad de iteraciones del while principal. Si sobrepasa el tamano de elementos, no se ha validado todos
	var posibilidadesElementos = ["login", "IdTrabajo", "NotaTrabajo", "orden"];//Todas las posibildades de nombre de inputs
	
	while(validaciones < tamano && bucle < tamano){//Mientras no se hayan hecho todas las validaciones del formulario
		var elemento = elementos[bucle];//Elemento que aun no se ha validado
		var i = 0; //Index del array posibildadesElementos
		
		if(!elemento.required && comprobarVacio(elemento)){//Si el elemento no es requerido Y es nulo, no necesitamos validarlo, puede ser cualquier cosa
			validaciones++;
		}else{//Sino, validación
			while(elemento.name != posibilidadesElementos[i] && i < posibilidadesElementos.length){//Mientras no se encuentre qué campo es elementos O i sobrepase el tamaño de posibilidadesElementos
				i++;//Incrementamos i
			}
			if(i >= posibilidadesElementos.length){//Si i sobrepasa o iguala el tamaño de posibildadesElementos, no se ha encontrado un campo, implica error
				alert(<?php echo "'"; echo $strings['Error con uno de los campos']; echo "'"; ?>);//Mensaje de error
				return false;//Abortar proceso
			}else{//Sino, validamos el campo que sea
				switch(posibilidadesElementos[i]){//Dado el campo que sea, validar ese campo
					case posibilidadesElementos[0]://Si es el login
						if(validarLogin(elemento.id)){validaciones++;}//Si se valida el login, se aumenta el numero de validaciones
						break;
					
					case posibilidadesElementos[1]://IdTrabajo
						if(validarIdTrabajo(elemento)){validaciones++;}/*Ditto...*/
						break;
						
					case posibilidadesElementos[2]://NotaTrabajo
						if(validarNota(elemento)){validaciones++;}
						break;
						
					case posibilidadesElementos[3]:
						if(elemento.value == orden){//Si es la orden, mirar que coincida con la que se le pasa por argumento
							validaciones++;
						}
					break;
				}
			}
		}
		bucle++;
	}
	
	if(tamano > validaciones && validaciones > 0){//Si el tamaño supera a la cantidad de validaciones y las validaciones no son 0, algun campo falla
		alert(<?php echo "'"; echo $strings['Alguno de los campos no tiene valores correctos']; echo "'"; ?>);
		return false;
	}else if(tamano == validaciones){//Por ultimo, si se han realizado tantas validaciones como campos tiene el formulario, significa que se han validado todos los campos, haciendo submit del formulario
		if(confirm(<?php echo "'"; echo $strings['Formulario correcto, ¿desea enviarlo?']; echo "'"; ?>)){//Si confirma que desea enviarlo
			elementos[indexContrasena].maxlength = 128;//Cambiamos el tamaño a 128 bits para que la encriptación quepa
			elementos[indexContrasena].value = hex_md5(elementos[indexContrasena].value);//Se encripta la contraseña (guardarla por si se envía el formulario)
			formulario.submit();//Se envía
			return true;
		}else{//No se envía.
			alert(<?php echo "'"; echo $strings['Se ha cancelado enviar el formulario']; echo "'"; ?>);
			return false;
		}		
	}else{//Alguno de las validaciones ha fallado, retornamos false y mensaje de error
		alert(<?php echo "'"; echo $strings['No se han podido comprobar todos los campos o hay un error extraño']; echo "'"; ?>);//Llegar a esta opción debería ser imposible
		return false;
	}
}


function limpiarCampos(formulario){
	//LImpia los campos del formulario, de esta manera, sin meter un input reset, puede pedirse confirmación
	if(confirm(<?php echo "'"; echo $strings['¿Seguro que desea limpiar los campos del formulario?']; echo "'"; ?>)){ //Si confirma el borrado, se borran.
		document.getElementById(formulario).reset(); //El formulario con su id.reset() para resetearlo
	}else{//Sino
		return false;//No
	}
	
}

