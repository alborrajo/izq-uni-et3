/* Php del javascript con el fin de poder poner los $strings dentro de los mensajes de error, es básicamente un .js con formato .php que se carga en el head
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
<?php 
	session_start();
	include_once '../Locales/Strings_'. $_SESSION['idioma'].'.php';//Idioma
?>
/* 	Javascript de funciones para la entidad NOTATRABAJO
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/

function validarAutoQA(autoqaId) {
	confirm("<?php echo $strings["autoqa"]; ?>") && document.getElementById(autoqaId).submit();
}

function comprobarVacio(campo){
	//Comprueba si el campo pasado a la función es vacío, devolviendo TRUE en tal caso
  	if((campo === undefined) || (campo.value === null) || (campo.value === undefined) || (campo.value.length == 0)){ //Si el campo es null (no existe) o no tiene longitud, retornar true como que es vacío. Considerar undefined como vacio es necesario para el html
  		return true;
  	}
  	else{
		//Sino, el campo tiene "algo", no (false) es vacío
  		return false;
  	}
}  

function esVacio(campo){
	//Lo mismo que comprobarVacio pero introduciendo campo.value, un valor de un campo no un campo en sí.
	if((campo === undefined) || (campo === null) || (campo.length == 0)){ //Si el campo es null (no existe) o no tiene longitud, retornar true como que es vacío. Considerar undefined como vacio es necesario para el html
  		return true;
  	}
  	else{
		//Sino, el campo tiene "algo", no (false) es vacío
  		return false;
  	}
} 

function validarTelefono(campo){
	//Funcion para comprobar si el telefono introducido es español (nacional o internacional, lo que implica que puede empezar por 34, 0034 o +34), no se aceptan guiones ni nada parecido, solo numeros
	var x = /^0034[0-9]{9}$/;/*Numero internacional que empiece por 0034*/
	var y = /^\+34[0-9]{9}$/;/*Numero internacional que empiece por +34*/
	var z = /^[0-9]{9}$/;/*Numero nacional*/
	if(!comprobarVacio(campo)){//Si el campo no es vacío, realiza el test
		if(x.test(campo.value) || y.test(campo.value) ||z.test(campo.value)){
			campoCorrecto(campo.id)
			return true;
		}else{
			campoIncorrecto(campo.id, <?php echo "'"; echo $strings['Formato del telefono incorrecto o contiene caracteres no válidos']; echo "'"; ?>);
		}//Comprueba que campo siga la regex x del telefono, siendo así un numero válido.
	}else{
		campoIncorrecto(campo.id, <?php echo "'"; echo $strings['El campo es vacío']; echo "'"; ?>);
	}		
}

function desprefijarTelefono(campo){
	//Funcion para quitar el prefijo del telefono o no se podrá guardar en la BD
	var x = /^0034[0-9]{9}$/;/*Numero internacional que empiece por 0034*/
	var y = /^\+34[0-9]{9}$/;/*Numero internacional que empiece por +34*/
	var z = /^[0-9]{9}$/;/*Numero nacional*/
}

function comprobarTexto(campo,size){
	//Compara el tamaño de un campo dado (campo) con el tamaño dado (size), retornando false si el tamaño de campo es mayor que el size y true en caso contrario
	if (campo.value.length>size){ //Si el tamaño del campo es mayor que el que se pasa como argumento
    	return false;
  	}
  	return true;
}

function comprobarAlfabetico(campo, size){
	//Comprueba que un campo (campo) dado sea solo letras, *significado de Alfabetico*, de un tamaño size
	var x = /^[a-zA-ZáéíóúÁÉÍÓÚüÜñç]*$/; //Expresión regular de cualquier letra entre a-z y A-Z tantas veces como se quiera (significado del *)
	
	if(comprobarTexto(campo, size)){ //Si el texto (campo) introducido no sobrepasa el tamaño (size) solicitado, se procede
		return x.test(campo.value);//Comprueba que siga la regex x de un campo alfabetico, siendo true si es valido
	}else{//Si sobrepasa el tamaño (size) retornase false
		return false;
	}	
}

function show(id){
	//Muestra (display: inline-block) un elemento con el id introducido
	document.getElementById(id).style='display: inline-block;'; //Se pone el display a inline-block
}

function hide(id){
	//Oculta un elemento con el id introducido
	document.getElementById(id).style='display: none;';//se pone el display a none
}

function campoIncorrecto(id, msg){
	//Cambia el circulo al lado del input (id) a rojo y el mensaje a su derecha en rojo y valor msg.
	document.getElementById(id+'Bot').src='../img/red-button.png'; //Imagen circulo rojo
	document.getElementById(id+'BotText').style.color='#AB0000'; //Texto color rojo
	if(msg === undefined){//Si no se pasa un msg como parámetro implica que el mensaje sea undefined
		msg = 'Error no identificado';//Dado el caso, poner un error "por defecto"
	}
	document.getElementById(id+'BotText').innerHTML=msg; //Texto = msg
}

function campoCorrecto(id, msg){
	//Cambia el circulo al lado del input id a verde y el mensaje a su derecha en verde y palabra "correcto"
	document.getElementById(id+'Bot').src='../img/green-button.png'; //Imagen circulo verde
	document.getElementById(id+'BotText').style.color='#006400'; //Texto color verde
	if(msg === undefined){//Si no se pasa un msg como parámetro implica que el mensaje sea undefined
		msg = <?php echo "'"; echo $strings['Correcto']; echo "'"; ?>;//Dado el caso, poner un mensaje "por defecto"
	}
	document.getElementById(id+'BotText').innerHTML=msg; //Eliminar texto, quizá cambiar a mensaje de "correcto" Edit: Sí, daltonismo...
}

function setTuplaErase(fila){
	//Muestra la tabla de eliminar en detalle unto con los datos de la fila numero (fila)
	var tabla = document.getElementById('tablaDatos');
	var tablaDetail = document.getElementById('tuplaErase'); //tabla de la tupla detallada
	var div = 'divTuplaErase'; //id de la div de la tabla
	
	for(i = 0; i <= tabla.rows.length; i++){//Recorrer todos los elementos de la fila (fila) de tabla
		tablaDetail.rows[i].cells[1].innerHTML = tabla.rows[fila].cells[i].innerHTML;//Poner la celda [i][1] de la tuplaErase al valor de [fila][i] de la tabla de datos
	}
	
	document.getElementById('divTuplaErase').style='display: inline-block'; //Muestra la tupla a borrar
}

function setTuplaDetail(fila){
	//Muestra la tabla de detalle junto con los datos de la fila numero (fila)
	var tabla = document.getElementById('tablaDatos');
	var tablaDetail = document.getElementById('tuplaDetail'); //tabla de la tupla detallada
	var div = 'divTuplaDetail'; //id de la div de la tabla
	
	for(i = 0; i <= tabla.rows.length; i++){//Recorrer todos los elementos de la fila (fila) de tabla
		tablaDetail.rows[i].cells[1].innerHTML = tabla.rows[fila].cells[i].innerHTML;//Poner la celda [i][1] de la tuplaDetail al valor de [fila][i] de la tabla de datos
	}
	
	document.getElementById(div).style='display: inline-block'; //Muestra la tupla a detallar
}


function validarLogin(id){
	//Comprobación entera del login. El orden importa, comprobarTexto tiene que ir antes de comprobarAlfabético pues ambos hacen lo mismo una vez, y el mensaje de error no sería correcto si comprobarAlfabetico estuviese primero
	var campo = document.getElementById(id); //Campo login
	var size = 25; //Tamaño máximo del login
	
	if(campo.readOnly){//Si el campo es readonly, la validacion es correcta pues es ineditable, y mensaje sobre ello
		campoCorrecto(id, <?php echo "'"; echo $strings['No se puede editar este campo']; echo "'"; ?>);//
		return true;
	}else if(comprobarVacio(campo)){//Si el campo login es vacío, retornamos false pues necesita algun caracter
		campoIncorrecto(id, <?php echo "'"; echo $strings['Rellena el campo']; echo "'"; ?>);//Mensaje de error
		return false;	
	}else if(!comprobarTexto(campo, size)){//Si el campo login tiene una cantidad de caracteres mayor al límite permitido, retornamos false
		campoIncorrecto(id, <?php echo "'"; echo $strings['El tamaño del login sobrepasa el límite de ']; echo "'"; ?> + size);//Mensaje de error
		return false;
	}else{//Si ha llegado hasta aquí, debe estar bien. Retornar true
		campoCorrecto(id);//Poner el mensaje de error a correcto (sin errores)
		return true;		
	}
}

function validarPassword(id){
	//Comprobación entera de la password. El único problema puede ser que no tenga el tamaño adecuado o sea vacío, pues se acepta cualquier caracter
	var campo = document.getElementById(id);//Campo password
	var size = 20;//Tamaño limite de la contraseña
	
	if(campo.readOnly){//Si el campo es readonly, la validacion es correcta pues es ineditable, y mensaje sobre ello
		campoCorrecto(id, <?php echo "'"; echo $strings['No se puede editar este campo']; echo "'"; ?>);//
		return true;
	}else if(comprobarVacio(campo)){//Si el campo password es vacío, error retornamos false
		campoIncorrecto(id, <?php echo "'"; echo $strings['Rellena el campo']; echo "'"; ?>);//Mensaje de error
		return false;	
	}else if(!comprobarTexto(campo, size)){//Si el campo contraseña es mayor del límite de 20 caracteres error, retornamos false
		campoIncorrecto(id, <?php echo "'"; echo $strings['El tamaño de la contraseña sobrepasa el límite de ']; echo "'"; ?> + size);//Mensaje de error
		return false;
	}else{//Si ha llegado hasta aquí, debe estar bien. Retornar true
		campoCorrecto(id);//Poner el mensaje de error a correcto (sin errores)
		return true;
	}
}

function validarNombre(id){
	//Comprobación entera del nombre. Debería ser un problema equivalente a validar login, pero con un size diferente
	var campo = document.getElementById(id);//Campo del nombre
	var size = 25;//Tamaño maximo del nombre
	
	if(campo.readOnly){//Si el campo es readonly, la validacion es correcta pues es ineditable, y mensaje sobre ello
		campoCorrecto(id, <?php echo "'"; echo $strings['No se puede editar este campo']; echo "'"; ?>);//
		return true;
	}else if(comprobarVacio(campo)){//Si el campo nombre es vacío, retornamos false pues necesita algun caracter
		campoIncorrecto(id, <?php echo "'"; echo $strings['Rellena el campo']; echo "'"; ?>);//Mensaje de error
		return false;	
	}else if(!comprobarTexto(campo, size)){//Si el campo nombre tiene una cantidad de caracteres mayor al límite permitido, retornamos false
		campoIncorrecto(id, <?php echo "'"; echo $strings['El tamaño del nombre sobrepasa el límite de ']; echo "'"; ?> + size);//Mensaje de error
		return false;
	}else if(!comprobarAlfabetico(campo, size)){//Si el campo nombre toma valores no alfabéticos, retornamos false
		campoIncorrecto(id, 'Sólo caracteres alfabéticos');//Mensaje de error
		return false;
	}else{//Si ha llegado hasta aquí, debe estar bien. Retornar true
		campoCorrecto(id);//Poner el mensaje de error a correcto (sin errores)
		return true;
	}
}

function validarApellido(id){
	//Comprobación entera del apellido. Debería ser un problema equivalente a validar nombre, excepto que se permite mas de 1 apellido. No nos vale la funcion comprobarAlfabético, hay que definir en este caso una nueva regex
	var campo = document.getElementById(id);//Campo del apellido
	var size = 50;//Tamaño maximo del apellido
	var regex = /^[[áéíóúüÁÉÍÓÚÜa-zA-Z]+[\s]*]*/; //Regex como nombre pero con espacios (\s) 
	
	if(campo.readOnly){//Si el campo es readonly, la validacion es correcta pues es ineditable, y mensaje sobre ello
		campoCorrecto(id, <?php echo "'"; echo $strings['No se puede editar este campo']; echo "'"; ?>);//
		return true;
	}else if(comprobarVacio(campo)){//Si el campo apellido es vacío, retornamos false pues necesita algun caracter
		campoIncorrecto(id, <?php echo "'"; echo $strings['Rellena el campo']; echo "'"; ?>);//Mensaje de error
		return false;	
	}else if(!comprobarTexto(campo, size)){//Si el campo apellido tiene una cantidad de caracteres mayor al límite permitido, retornamos false
		campoIncorrecto(id, <?php echo "'"; echo $strings['El tamaño de los apellidos sobrepasa el límite de ']; echo "'"; ?> + size);//Mensaje de error
		return false;
	}else if(!regex.test(campo.value)){//Si el campo apellido toma valores no alfabéticos excepto espacio, retornamos false
		campoIncorrecto(id, <?php echo "'"; echo $strings['Sólo caracteres alfabéticos o espacios']; echo "'"; ?>);//Mensaje de error
		return false;
	}else{//Si ha llegado hasta aquí, debe estar bien. Retornar true
		campoCorrecto(id);//Poner el mensaje de error a correcto (sin errores)
		return true;
	}
}

function validarEmail(campo){
	//Comprobacion entera del email. Problemas: vacío, tamaño, validez
	var size = 50;//Tamño del email
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; //Expresión regular de un email
	
	if(campo.readOnly){//Si el campo es readonly, la validacion es correcta pues es ineditable, y mensaje sobre ello
		campoCorrecto(id, <?php echo "'"; echo $strings['No se puede editar este campo']; echo "'"; ?>);//
		return true;
	}else if(comprobarVacio(campo)){//Si el campo está vacío, retornamos false junto con el mensaje de error
		campoIncorrecto(campo.id, <?php echo "'"; echo $strings['Rellena el campo']; echo "'"; ?>);//Mensaje de error
		return false;
	}else if(!comprobarTexto(campo, size)){ //Si el email sobrepasa el limite de tamaño permitido, return false con mensaje
		campoIncorrecto(id, <?php echo "'"; echo $strings['El tamaño del email sobrepasa el límite de ']; echo "'"; ?> + size);//Mensaje de error
		return false;
	}else if(!re.test(campo.value)){ //No hace falta comprobar los alfanumericos en el email. Si no es válido, retornamos false
		campoIncorrecto(campo.id, <?php echo "'"; echo $strings['Formato del email incorrecto']; echo "'"; ?>);//Mensaje de error
		return false;
	}else{//Si llega hasta aquí debería estar correcto
		campoCorrecto(campo.id);
		return true;
	}
}

function validarCurso(id){
	//Comprobación entera del curso. Problemas: vacio
	var campo = document.getElementById(id);//Campo del curso
	
	if(campo.readOnly){//Si el campo es readonly, la validacion es correcta pues es ineditable, y mensaje sobre ello
		campoCorrecto(id, <?php echo "'"; echo $strings['No se puede editar este campo']; echo "'"; ?>);//
		return true;
	}else if(comprobarVacio(campo)){//Si no se ha seleccionado nada, retornamos false
		campoIncorrecto(id, <?php echo "'"; echo $strings['Rellena el campo']; echo "'"; ?>);//Mensaje de error
		return false;		
	}else{//El curso debería ser correcto llegados a este punto, retornamos true
		campoCorrecto(id);
		return true;
	}
}

function validarGrupo(id){
	//Comprobación entera del grupo. Problemas: vacio
	var campo = document.getElementById(id);//Campo del grupo
	
	if(campo.readOnly){//Si el campo es readonly, la validacion es correcta pues es ineditable, y mensaje sobre ello
		campoCorrecto(id, <?php echo "'"; echo $strings['No se puede editar este campo']; echo "'"; ?>);//
		return true;
	}else if(comprobarVacio(campo)){//Si no se ha seleccionado nada, retornamos false
		campoIncorrecto(id, <?php echo "'"; echo $strings['Rellena el campo']; echo "'"; ?>);//Mensaje de error
		return false;		
	}else{//El grupo debería ser correcto llegados a este punto, retornamos true
		campoCorrecto(id);
		return true;
	}
}

function validarTitulacion(id){
	//Comprobación entera de la titulación. Problemas: vacío o tamaño. Puede que se puedan poner "," o incluso "+", así que no haremos comprobación de alfabetico
	var campo = document.getElementById(id);//Campo de la titulacion
	var size = 60; //Tamaño del campo titulación máximo
	
	if(campo.readOnly){//Si el campo es readonly, la validacion es correcta pues es ineditable, y mensaje sobre ello
		campoCorrecto(id, <?php echo "'"; echo $strings['No se puede editar este campo']; echo "'"; ?>);//
		return true;
	}else if(comprobarVacio(campo)){//Si el campo es vacio, retornamos false
		campoIncorrecto(id, <?php echo "'"; echo $strings['Rellena el campo']; echo "'"; ?>);//Mensaje de error
		return false;
	}else if (!comprobarTexto(campo,size)){//Si el campo sobrepasa el tamaño límite, retornamos false
		campoIncorrecto(id, <?php echo "'"; echo $strings['El tamaño de la titulación sobrepasa el límite de ']; echo "'"; ?> + size);//Mensaje de error
		return false;
	}else{//Si llega hasta aquí, debería estar bien, retornamos true
		campoCorrecto(id);
		return true;
	}
}

function validarFecha(id){
	//Comprobación entera de la fecha. El calendario tigra ya trae su "validacion", además de al ser readonly y no cambiable, no puede tener otro problema que no sea estar vacio
	var campo = document.getElementById(id);
	
	if(comprobarVacio(campo)){//Si el campo es vacío, retornamos false
		campoIncorrecto(id, <?php echo "'"; echo $strings['Rellena el campo']; echo "'"; ?>);//Mensaje de error
		return false;
	}else{//Si llega hasta aquí, debería estar correcto, retornaoms true
		campoCorrecto(id);
		return true;
	}
}
function validarFormularioAER(formulario, orden){/*Add Edit o Registro*/
	//Validación completa de un formulario pasando un formulario (formulario). Comprobará todos los elementos del formulario comparandolos con las posibilidades, y si da alguna, validarla.
	var elementos = formulario.elements;//Todos los inputs del formulario
	var tamano = elementos.length;//Cantidad de elementos del array elementos
	var validaciones = 0; //Cantidad de validaciones que se han realizado sobre los inputs
	var bucle = 0;//Cantidad de iteraciones del while principal. Si sobrepasa el tamano de elementos, no se ha validado todos
	var posibilidadesElementos = ["login", "password", "DNI", "telefono", "nomuser", "apellido", "email", "fechnac", "fotopersonal", "sexo", "orden"];//Todas las posibildades de nombre de inputs
	var indexContrasena = '';//Index dentro de elementos de la contrasena para futura encriptación
	
	while(validaciones < tamano && bucle < tamano){//Mientras no se hayan hecho todas las validaciones del formulario
		var elemento = elementos[bucle];//Elemento que aun no se ha validado
		var i = 0; //Index del array posibildadesElementos
		
		if(!elemento.required && comprobarVacio(elemento)){//Si el elemento no es requerido Y es nulo, no necesitamos validarlo, puede ser cualquier cosa
			validaciones++;
		}else{//Sino, validación
			while(elemento.name != posibilidadesElementos[i] && i < posibilidadesElementos.length){//Mientras no se encuentre qué campo es elementos O i sobrepase el tamaño de posibilidadesElementos
				i++;//Incrementamos i
			}
			if(i >= posibilidadesElementos.length){//Si i sobrepasa o iguala el tamaño de posibildadesElementos, no se ha encontrado un campo, implica error
				alert(<?php echo "'"; echo $strings['Error con uno de los campos']; echo "'"; ?>);//Mensaje de error
				return false;//Abortar proceso
			}else{//Sino, validamos el campo que sea
				switch(posibilidadesElementos[i]){//Dado el campo que sea, validar ese campo
					case posibilidadesElementos[0]://Si es el login
						if(validarLogin(elemento.id)){validaciones++;}//Si se valida el login, se aumenta el numero de validaciones
						break;
					
					case posibilidadesElementos[1]://Password
						if(validarPassword(elemento.id)){validaciones++;}/*Ditto...*/
						indexContrasena = bucle;//Guardar index de la contrasena, para encriptarla
						break;
						
					case posibilidadesElementos[2]://DNI
						if(validarDNI(elemento)){validaciones++;}
						break;
						
					case posibilidadesElementos[3]://telefono
						if(validarTelefono(elemento)){validaciones++;}
						break;
					
					case posibilidadesElementos[4]://nomuser
						if(validarNombre(elemento.id)){validaciones++;}
						break;
					
					case posibilidadesElementos[5]://apellido
						if(validarApellido(elemento.id)){validaciones++;}
						break;
						
					case posibilidadesElementos[6]://email
						if(validarEmail(elemento)){validaciones++;}
						break;
						
					case posibilidadesElementos[7]://fechnac
						if(validarFecha(elemento.id)){validaciones++;}
						break;
						
					case posibilidadesElementos[8]://fotopersonal
						if(validarFoto(elemento)){validaciones++;}
						break;		

					case posibilidadesElementos[9]://sexo
						if(validarSexo(elemento)){validaciones++;}
						break;
						
					case posibilidadesElementos[10]://orden
						if(elemento.value == orden){validaciones++;}
						break;
				}
			}
		}
		bucle++;
	}
	
	if(tamano > validaciones && validaciones > 0){//Si el tamaño supera a la cantidad de validaciones y las validaciones no son 0, algun campo falla
		alert(<?php echo "'"; echo $strings['Alguno de los campos no tiene valores correctos']; echo "'"; ?>);
		return false;
	}else if(tamano == validaciones){//Por ultimo, si se han realizado tantas validaciones como campos tiene el formulario, significa que se han validado todos los campos, haciendo submit del formulario
		if(confirm(<?php echo "'"; echo $strings['Formulario correcto, ¿desea enviarlo?']; echo "'"; ?>)){//Si confirma que desea enviarlo
			elementos[indexContrasena].maxlength = 128;//Cambiamos el tamaño a 128 bits para que la encriptación quepa
			elementos[indexContrasena].value = hex_md5(elementos[indexContrasena].value);//Se encripta la contraseña (guardarla por si se envía el formulario)
			formulario.submit();//Se envía
			return true;
		}else{//No se envía.
			alert(<?php echo "'"; echo $strings['Se ha cancelado enviar el formulario']; echo "'"; ?>);
			return false;
		}		
	}else{//Alguno de las validaciones ha fallado, retornamos false y mensaje de error
		alert(<?php echo "'"; echo $strings['No se han podido comprobar todos los campos o hay un error extraño']; echo "'"; ?>);//Llegar a esta opción debería ser imposible
		return false;
	}
}


function limpiarCampos(formulario){
	//LImpia los campos del formulario, de esta manera, sin meter un input reset, puede pedirse confirmación
	if(confirm(<?php echo "'"; echo $strings['¿Seguro que desea limpiar los campos del formulario?']; echo "'"; ?>)){ //Si confirma el borrado, se borran.
		document.getElementById(formulario).reset(); //El formulario con su id.reset() para resetearlo
	}else{//Sino
		return false;//No
	}
	
}

function comprobarEntero(campo, valormenor, valormayor){
	//Comprueba que un valor dado sea menor que (valormayor) y menor que (valormayor)
	var regex = /^[\-\+]?\d*/; //Expresión regular de cualquier número, no introducir caracteres extraños
	var min = 0;//Valor representativo de valormenor
	var max = 0;//Valor representativo de valormayor
	
	if(comprobarVacio(valormenor)){//El valor es nulo, por lo tanto el usuario quizá piense que no introducir nada equivale a 0. Se lo dejamos claro
		alert(<?php echo "'"; echo $strings['No ha introducido el numero de valormenor, se pondrá como "0"']; echo "'"; ?>);//Mensaje informativo
		min = 0;
	}else{//El valormenor no es vacío, seguimos
		if(!regex.test(valormenor.value, 10)){//Comprobar que el numero valormenor sea un numero válido
			alert(<?php echo "'"; echo $strings['El numero menor no es válido']; echo "'"; ?>);//mensaje de error
			return false;//Fin de función			
		}else{
			min = parseInt(valormenor.value, 10);			
		}
	}
	
	if(comprobarVacio(valormayor)){//El valor es nulo, por lo tanto el usuario quizá piense que no introducir nada equivale a 0. Se lo dejamos claro
		alert(<?php echo "'"; echo $strings['No ha introducido el numero de valormayor, se pondrá como "0"']; echo "'"; ?>);//Mensaje informativo
		max = 0;
	}else{//El valormayor no es vacío, seguimos
		if(!regex.test(valormayor.value)){//Comprobar que el numero valormayor sea un numero válidof
			alert(<?php echo "'"; echo $strings['El numero mayor no es válido']; echo "'"; ?>);//mensaje de error
			return false;//Fin de función			
		}else{
			max = parseInt(valormenor.value, 10);			
		}
	}
	
	if(comprobarVacio(campo) || !regex.test(campo.value)){//Comprueba que el campo introducido no sea un numero o sea un vacio
		alert(<?php echo "'"; echo $strings['Solo numeros enteros con un "-" o un "+" delante']; echo "'"; ?>);//Mensaje de error
		return false; //Si lo es, retornamos false
	}else{//Si es un numero válido
		var valor = parseInt(campo.value, 10);//Parse de campo a un integer, por si acaso no es un int lo introducido
		
		if(valor >= min && valor <= max){//Por ultimo comprobar si el valor está entre los valores dados
			return true;
		}else{//Sino, el valor no está entre los dados, retornamos false
			alert(<?php echo "'"; echo $strings['El valor no está entre los especificados (valormenor y valormayor)']; echo "'"; ?>);//Mensaje de error
			return false;
		}
	}
}


function validarDNI(campo){
	//Función para comprobar la validez de un DNI introducido, tanto NIE como NIF
	var regexNIF = /^[0-9]{8}[A-Za-z]$/;//Expresión regular del NIF
    var regexNIE = /^[XYZxyz][0-9]{7}[A-Za-z]$/;//Expresión regular del NIE
	var letrasDNI = ["T","R","W","A","G","M","Y","F","P","D","X","B","N","J","Z","S","Q","V","H","L","C","K","E"];//Array con las posiciones de las letras que al dividir por 23 el resto debería dar
	
	if(comprobarVacio(campo)){//Si no se introduce nada
		campoIncorrecto(campo.id, <?php echo "'"; echo $strings['El campo es vacío']; echo "'"; ?>);//Mensaje de error
		return false;//Fin de funcion
		
	}else if(regexNIF.test(campo.value)){//Comprobar si es un NIF
		var numero = campo.value.substr(0,8);//Numero del NIF, desde el caracter 0 al 8º
		var letra = campo.value.charAt(8).toUpperCase();//Letra, ultimo caracter, el 9º, del DNI introducido, en mayusculas para compararlas con el array de letras posbiles
		
		if(letrasDNI[parseInt(numero)%23] === letra){//Si la letra en la posicion (resto de numero dividido por 23) es igual a la letra del DNI introducido, el DNI es correcto
			campoCorrecto(campo.id);
			return true;
		}else{//Sino, es incorrecto
			campoIncorrecto(campo.id, <?php echo "'"; echo $strings['El DNI introducido no es válido']; echo "'"; ?>);//Mensaje de error
			return false;
		}
		
	}else if(regexNIE.test(campo.value)){
		var letraInicial = campo.value.charAt(0).toUpperCase();//Primera letra del NIE
		var numero = '';//Numero del NIF, que cojeremos despues de darle valor al primer caracter pues depende de si es x y o z
		var anadirNumero = campo.value.substr(1,8);//Numero del NIF, desde el caracter 1º al 8º, para añadir despues al numero
		var letra = campo.value.charAt(8).toUpperCase();//Letra, ultimo caracter, el 9º, del DNI introducido
		
		switch(letraInicial){//Valor de la letra inicial a numero
			case "X"://Si es X vale 0
				numero = '0';
				break;
				
			case "Y"://Si es Y vale 1
				numero = '1';
				break;
				
			case "Z"://Si es Z vale 2
				numero = '2';
				break;
		}
		
		numero += anadirNumero;//Concatenar cadena numero con añadirNumero
		
		if(letrasDNI[parseInt(numero)%23] === letra){//Si la letra en la posicion (resto de numero dividido por 23) es igual a la letra del DNI introducido, el DNI es correcto
			campoCorrecto(campo.id);
			return true;
		}else{//Sino, es incorrecto
			campoIncorrecto(campo.id, <?php echo "'"; echo $strings['El DNI introducido no es válido']; echo "'"; ?>);//Mensaje de error
			return false;
		}		
		
	}else{//Si llega hasta aquí, no coincide el DNI con ninguna regex, por lo tanto debería ser incorrecto
		campoIncorrecto(campo.id, <?php echo "'"; echo $strings['El DNI introducido no es válido']; echo "'"; ?>);//Mensaje de error
		return false;
	}	
}

function validarSexo(campo){
	//Esta función sirver para validar el sexo escogido por el usuario en algún formulario
	var sexo = campo.value.toLowerCase();//El valor del campo que se envía en minúsuclas para comprarrlo con los 2 posibles
	
	if(sexo == 'hombre' || sexo == 'mujer'){//Si se parece a alguno de los posibles es valido
		campoCorrecto(campo.id);//Se marca como válido
		return true;//Retornamos true
	}else{//Sino
		campoIncorrecto(campo.id, <?php echo "'"; echo $strings['El sexo tiene que ser hombre o mujer']; echo "'"; ?>);//Msg de error
		return false;//No
	}
}

function validarFoto(campo){
	//Para comprobar que la foto es válida. No sabría cómo comprobarlo, así que mientras no sea vacío se devolverá true
	if(comprobarVacio(campo)){//Si es vacío, error
		campoIncorrecto(campo.id, <?php echo "'"; echo $strings['Rellena el campo']; echo "'"; ?>);//Msg de error
		return false;
	}else{//Sino, es correcto
		campoCorrecto(campo.id);//se indica
		return true;//Retornamos true
	}
}


//Función validar submit formulario de trabajo 
function validarFormularioTAE(formulario, orden){

	var elementos = formulario.elements;//Todos los inputs del formulario
	var numeroElementos = elementos.length;//Numero de elementos
	var i = 0;/*Bucles*/
	var validaciones = 0; //Numero de validaciones correctas
	
	while(i < numeroElementos){//Mientras no se recorran todos los elementos del formulario
		var elemento = elementos[i];//Cojemos un elemento
		switch(elemento.name){//Y dependiendo de cual sea
			case 'IdTrabajo'://Si es el login
				if(comprobarTexto(elemento.id,6) && comprobarVacio(elemento.id)){validaciones++;}//Si se valida el login, se aumenta el numero de validaciones
			break;

			case 'NombreTrabajo'://Password
				if(comprobarTexto(elemento.id,60) && comprobarVacio(elemento.id)){validaciones++;}/*Ditto...*/
			break;

			case 'FechaIniTrabajo'://Password
				if(validarFecha(elemnto.id) && comprobarVacio(elemento.id)){validaciones++;}/*Ditto...*/
			break;

			case 'FechaFinTrabajo'://Password
				if(validarFecha(elemnto.id) && comprobarVacio(elemento.id)){validaciones++;}/*Ditto...*/
			break;

			case 'PorcentajeNota'://Password
				if(validarReal(2,0) && comprobarVacio(elemento.id)){validaciones++;}/*Ditto...*/
			break;
			
			case 'orden'://Orden
				if(elemento.value == orden){validaciones++;}/*Ditto...*/
			break;
		}
		i++;
	}
	if(numeroElementos == validaciones){//Si se han validad tantos campos como hay, significa que el formulario está correcto y habrá que enviarlo, sin confirmaciones que hay que logearse rapidito
		formulario.submit();//Se envía
		return true;
	}else{//Alguno de las validaciones ha fallado, retornamos false y mensaje de error
		alert('Algún campo incorrecto');//Llegar a esta opción debería ser imposible
		return false;
	}	
	
}