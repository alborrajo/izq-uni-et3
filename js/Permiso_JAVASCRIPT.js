/* 	Javascript de funciones para la entidad PERMISO
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/

function accionesPosibles(idFuncs,idAccs) {
	var IdFuncionalidad = document.getElementById(idFuncs); //Elemento HTML <select> para elegir la Funcionalidad
	var IdAccion = document.getElementById(idAccs);	//Elemento HTML <select> para elegir la Accion
	
	//Desactivar todas las opciones del <select< de IdAccion
	for (var j = 0; j <  IdAccion.length; j++){
		//Activar opción
		IdAccion.options[j].disabled = true;
	}

	//Por cada par funcionalidad-acción
	for(var i = 0;i < func_accion.length; i++) {
		//Si en el par actual la funcionalidad es la del select
		if(func_accion[i][0] == IdFuncionalidad.value) {
			//Por cada opción del select IdAccion
			for (var j = 0; j <  IdAccion.length; j++){
				//Si la opción actual, el IdAccion coincide con el del par funcionalidad-acción
				if(func_accion[i][1] == IdAccion.options[j].value) {
					//Activar opción
					IdAccion.options[j].disabled = false;
				}
			}

		}		
	}
}