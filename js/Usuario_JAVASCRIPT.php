<?php 
	session_start();
	include_once '../Locales/Strings_'. $_SESSION['idioma'].'.php';//Idioma
?>
/* Javascript de funciones de validacion para la entidad USUARIO
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
function comprobarVacio(campo){
	//Comprueba si el campo pasado a la función es vacío, devolviendo TRUE en tal caso
  	if((campo === undefined) || (campo.value === null) || (campo.value === undefined) || (campo.value.length == 0)){ //Si el campo es null (no existe) o no tiene longitud, retornar true como que es vacío. Considerar undefined como vacio es necesario para el html
  		return true;
  	}
  	else{
		//Sino, el campo tiene "algo", no (false) es vacío
  		return false;
  	}
}  

function esVacio(campo){
	//Lo mismo que comprobarVacio pero introduciendo campo.value, un valor de un campo no un campo en sí.
	if((campo === undefined) || (campo === null) || (campo.length == 0)){ //Si el campo es null (no existe) o no tiene longitud, retornar true como que es vacío. Considerar undefined como vacio es necesario para el html
  		return true;
  	}
  	else{
		//Sino, el campo tiene "algo", no (false) es vacío
  		return false;
  	}
} 

function comprobarTexto(campo,size){
	//Compara el tamaño de un campo dado (campo) con el tamaño dado (size), retornando false si el tamaño de campo es mayor que el size y true en caso contrario
	if (campo.value.length>size){ //Si el tamaño del campo es mayor que el que se pasa como argumento
    	return false;
  	}
  	return true;
}

function comprobarVacio(campo){
	//Comprueba si el campo pasado a la función es vacío, devolviendo TRUE en tal caso
  	if((campo === undefined) || (campo.value === null) || (campo.value === undefined) || (campo.value.length == 0)){ //Si el campo es null (no existe) o no tiene longitud, retornar true como que es vacío. Considerar undefined como vacio es necesario para el html
  		return true;
  	}
  	else{
		//Sino, el campo tiene "algo", no (false) es vacío
  		return false;
  	}
} 

function comprobarAlfabetico(campo, size){
	//Comprueba que un campo (campo) dado sea solo letras, *significado de Alfabetico*, de un tamaño size
	var x = /^[a-zA-ZáéíóúÁÉÍÓÚüÜñç]*$/; //Expresión regular de cualquier letra entre a-z y A-Z tantas veces como se quiera (significado del *)
	
	if(comprobarTexto(campo, size)){ //Si el texto (campo) introducido no sobrepasa el tamaño (size) solicitado, se procede
		return x.test(campo.value);//Comprueba que siga la regex x de un campo alfabetico, siendo true si es valido
	}else{//Si sobrepasa el tamaño (size) retornase false
		return false;
	}	
}

function validarLoginUsuario(campo) {
	//Comprobación entera del login. El orden importa, comprobarTexto tiene que ir antes de comprobarAlfabético pues ambos hacen lo mismo una vez, y el mensaje de error no sería correcto si comprobarAlfabetico estuviese primero
	var size = 9; //Tamaño máximo del login
	
	if(campo.readOnly){//Si el campo es readonly, la validacion es correcta pues es ineditable, y mensaje sobre ello
		campoCorrecto(campo.id, "<?php echo $strings['No se puede editar este campo']; ?>");//
		return true;
	}else if(comprobarVacio(campo)){//Si el campo login es vacío, retornamos false pues necesita algun caracter
		campoIncorrecto(campo.id, "<?php echo $strings['Rellena el campo']; ?>");//Mensaje de error
		return false;	
	}else if(!comprobarTexto(campo, size)){//Si el campo login tiene una cantidad de caracteres mayor al límite permitido, retornamos false
		campoIncorrecto(campo.id, "<?php echo $strings['El tamaño del login sobrepasa el límite de ']; ?>" + size);//Mensaje de error
		return false;
	}else{//Si ha llegado hasta aquí, debe estar bien. Retornar true
		campoCorrecto(campo.id);//Poner el mensaje de error a correcto (sin errores)
		return true;		
	}
}

function validarPasswordUsuario(campo){
	//Comprobación entera de la password. El único problema puede ser que no tenga el tamaño adecuado o sea vacío, pues se acepta cualquier caracter
	var size = 20;//Tamaño limite de la contraseña
	
	if(campo.readOnly){//Si el campo es readonly, la validacion es correcta pues es ineditable, y mensaje sobre ello
		campoCorrecto(campo.id, "<?php echo $strings['No se puede editar este campo']; ?>");//
		return true;
	}else if(comprobarVacio(campo)){//Si el campo password es vacío, error retornamos false
		campoIncorrecto(campo.id, "<?php echo $strings['Rellena el campo']; ?>");//Mensaje de error
		return false;	
	}else if(!comprobarTexto(campo, size)){//Si el campo contraseña es mayor del límite de 20 caracteres error, retornamos false
		campoIncorrecto(campo.id, "<?php echo $strings['El tamaño de la contraseña sobrepasa el límite de ']; ?>" + size);//Mensaje de error
		return false;
	}else{//Si ha llegado hasta aquí, debe estar bien. Retornar true
		campoCorrecto(campo.id);//Poner el mensaje de error a correcto (sin errores)
		return true;
	}
}

function validarNombreUsuario(id){
	//Comprobación entera del nombre. Debería ser un problema equivalente a validar login, pero con un size diferente
	var campo = document.getElementById(id);//Campo del nombre
	var size = 30;//Tamaño maximo del nombre
	
	if(campo.readOnly){//Si el campo es readonly, la validacion es correcta pues es ineditable, y mensaje sobre ello
		campoCorrecto(id, "<?php echo $strings['No se puede editar este campo']; ?>");//
		return true;
	}else if(comprobarVacio(campo)){//Si el campo nombre es vacío, retornamos false pues necesita algun caracter
		campoIncorrecto(id, "<?php echo $strings['Rellena el campo']; ?>");//Mensaje de error
		return false;	
	}else if(!comprobarTexto(campo, size)){//Si el campo nombre tiene una cantidad de caracteres mayor al límite permitido, retornamos false
		campoIncorrecto(id, "<?php echo $strings['El tamaño del nombre sobrepasa el límite de ']; ?>" + size);//Mensaje de error
		return false;
	}else if(!comprobarAlfabetico(campo, size)){//Si el campo nombre toma valores no alfabéticos, retornamos false
		campoIncorrecto(id, "<?php echo $strings['Sólo caracteres alfabéticos']; ?>");//Mensaje de error
		return false;
	}else{//Si ha llegado hasta aquí, debe estar bien. Retornar true
		campoCorrecto(id);//Poner el mensaje de error a correcto (sin errores)
		return true;
	}
}

function validarApellidosUsuario(id){
	//Comprobación entera del apellido. Debería ser un problema equivalente a validar nombre, excepto que se permite mas de 1 apellido. No nos vale la funcion comprobarAlfabético, hay que definir en este caso una nueva regex
	var campo = document.getElementById(id);//Campo del apellido
	var size = 50;//Tamaño maximo del apellido
	var regex = /^[[áéíóúüÁÉÍÓÚÜa-zA-Z]+[\s]*]*/; //Regex como nombre pero con espacios (\s) 
	
	if(campo.readOnly){//Si el campo es readonly, la validacion es correcta pues es ineditable, y mensaje sobre ello
		campoCorrecto(id, "<?php echo $strings['No se puede editar este campo']; ?>");//
		return true;
	}else if(comprobarVacio(campo)){//Si el campo apellido es vacío, retornamos false pues necesita algun caracter
		campoIncorrecto(id, "<?php echo $strings['Rellena el campo']; ?>");//Mensaje de error
		return false;	
	}else if(!comprobarTexto(campo, size)){//Si el campo apellido tiene una cantidad de caracteres mayor al límite permitido, retornamos false
		campoIncorrecto(id, "<?php echo $strings['El tamaño de los apellidos sobrepasa el límite de ']; ?>" + size);//Mensaje de error
		return false;
	}else if(!regex.test(campo.value)){//Si el campo apellido toma valores no alfabéticos excepto espacio, retornamos false
		campoIncorrecto(id, "<?php echo $strings['Sólo caracteres alfabéticos o espacios']; ?>");//Mensaje de error
		return false;
	}else{//Si ha llegado hasta aquí, debe estar bien. Retornar true
		campoCorrecto(id);//Poner el mensaje de error a correcto (sin errores)
		return true;
	}
}

function validarDNIUsuario(campo){
	//Función para comprobar la validez de un DNI introducido, tanto NIE como NIF
	var regexNIF = /^[0-9]{8}[A-Za-z]$/;//Expresión regular del NIF
    var regexNIE = /^[XYZxyz][0-9]{7}[A-Za-z]$/;//Expresión regular del NIE
	var letrasDNI = ["T","R","W","A","G","M","Y","F","P","D","X","B","N","J","Z","S","Q","V","H","L","C","K","E"];//Array con las posiciones de las letras que al dividir por 23 el resto debería dar
	
	if(comprobarVacio(campo)){//Si no se introduce nada
		campoIncorrecto(campo.id, "<?php echo $strings['El campo está vacío']; ?>");//Mensaje de error
		return false;//Fin de funcion
		
	}else if(regexNIF.test(campo.value)){//Comprobar si es un NIF
		var numero = campo.value.substr(0,8);//Numero del NIF, desde el caracter 0 al 8º
		var letra = campo.value.charAt(8).toUpperCase();//Letra, ultimo caracter, el 9º, del DNI introducido, en mayusculas para compararlas con el array de letras posbiles
		
		if(letrasDNI[parseInt(numero)%23] === letra){//Si la letra en la posicion (resto de numero dividido por 23) es igual a la letra del DNI introducido, el DNI es correcto
			campoCorrecto(campo.id);
			return true;
		}else{//Sino, es incorrecto
			campoIncorrecto(campo.id, "<?php echo $strings['El DNI introducido no es válido']; ?>");//Mensaje de error
			return false;
		}
		
	}else if(regexNIE.test(campo.value)){
		var letraInicial = campo.value.charAt(0).toUpperCase();//Primera letra del NIE
		var numero = '';//Numero del NIF, que cojeremos despues de darle valor al primer caracter pues depende de si es x y o z
		var anadirNumero = campo.value.substr(1,8);//Numero del NIF, desde el caracter 1º al 8º, para añadir despues al numero
		var letra = campo.value.charAt(8).toUpperCase();//Letra, ultimo caracter, el 9º, del DNI introducido
		
		switch(letraInicial){//Valor de la letra inicial a numero
			case "X"://Si es X vale 0
				numero = '0';
				break;
				
			case "Y"://Si es Y vale 1
				numero = '1';
				break;
				
			case "Z"://Si es Z vale 2
				numero = '2';
				break;
		}
		
		numero += anadirNumero;//Concatenar cadena numero con añadirNumero
		
		if(letrasDNI[parseInt(numero)%23] === letra){//Si la letra en la posicion (resto de numero dividido por 23) es igual a la letra del DNI introducido, el DNI es correcto
			campoCorrecto(campo.id);
			return true;
		}else{//Sino, es incorrecto
			campoIncorrecto(campo.id, "<?php echo $strings['El DNI introducido no es válido']; ?>");//Mensaje de error
			return false;
		}		
		
	}else{//Si llega hasta aquí, no coincide el DNI con ninguna regex, por lo tanto debería ser incorrecto
		campoIncorrecto(campo.id, "<?php echo $strings['El DNI introducido no es válido']; ?>");//Mensaje de error
		return false;
	}	
}

function validarCorreoUsuario(campo){
	//Comprobacion entera del email. Problemas: vacío, tamaño, validez
	var size = 40;//Tamño del email
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; //Expresión regular de un email
	
	if(campo.readOnly){//Si el campo es readonly, la validacion es correcta pues es ineditable, y mensaje sobre ello
		campoCorrecto(id, "<?php echo $strings['No se puede editar este campo']; ?>");//
		return true;
	}else if(comprobarVacio(campo)){//Si el campo está vacío, retornamos false junto con el mensaje de error
		campoIncorrecto(campo.id, "<?php echo $strings['Rellena el campo']; ?>");//Mensaje de error
		return false;
	}else if(!comprobarTexto(campo, size)){ //Si el email sobrepasa el limite de tamaño permitido, return false con mensaje
		campoIncorrecto(id, "<?php echo $strings['El tamaño del email sobrepasa el límite de ']; ?>" + size);//Mensaje de error
		return false;
	}else if(!re.test(campo.value)){ //No hace falta comprobar los alfanumericos en el email. Si no es válido, retornamos false
		campoIncorrecto(campo.id, "<?php echo $strings['Formato del email incorrecto']; ?>");//Mensaje de error
		return false;
	}else{//Si llega hasta aquí debería estar correcto
		campoCorrecto(campo.id);
		return true;
	}
}

function validarDireccionUsuario(campo){
	//Funcion para comprobar si la dirección introducida es válida
	var size = 60; //Tamño de la direccion
	var regex = /^[A-Za-záéíóúÁÉÍÓÚäÄëËïÏöÖüÜ.\-#,]*/; //Expresión regular de una posible direccion
	
	if(campo.readOnly){//Si el campo es readonly, la validacion es correcta pues es ineditable, y mensaje sobre ello
		campoCorrecto(campo.id, "<?php echo $strings['No se puede editar este campo']; ?>");//
		return true;
	}else if(comprobarVacio(campo)){//Si el campo apellido es vacío, retornamos false pues necesita algun caracter
		campoIncorrecto(campo.id, "<?php echo $strings['Rellena el campo']; ?>");//Mensaje de error
		return false;	
	}else if(!comprobarTexto(campo, size)){//Si el campo apellido tiene una cantidad de caracteres mayor al límite permitido, retornamos false
		campoIncorrecto(campo.id, "<?php echo $strings['El tamaño de la direccion sobrepasa el límite de ']; ?>" + size);//Mensaje de error
		return false;
	}else if(!regex.test(campo.value)){//Si el campo apellido toma valores no alfabéticos excepto espacio, retornamos false
		campoIncorrecto(campo.id, "<?php echo $strings['Sólo caracteres alfabéticos, espacios comas o puntos']; ?>");//Mensaje de error
		return false;
	}else{//Si ha llegado hasta aquí, debe estar bien. Retornar true
		campoCorrecto(campo.id);//Poner el mensaje de error a correcto (sin errores)
		return true;
	}
}

function validarTelefonoUsuario(campo){
	//Funcion para comprobar si el telefono introducido es español (nacional o internacional, lo que implica que puede empezar por 34, 0034 o +34), no se aceptan guiones ni nada parecido, solo numeros
	var x = /^0034[0-9]{9}$/;/*Numero internacional que empiece por 0034*/
	var y = /^\+34[0-9]{9}$/;/*Numero internacional que empiece por +34*/
	var w = /^34[0-9]{9}$/;
	var z = /^[0-9]{9}$/;/*Numero nacional*/
	if(!comprobarVacio(campo)){//Si el campo no es vacío, realiza el test
		if(x.test(campo.value) || y.test(campo.value) ||z.test(campo.value) || w.test(campo.value)){
			campoCorrecto(campo.id)
			return true;
		}else{
			campoIncorrecto(campo.id, "<?php echo $strings['Formato del telefono incorrecto o contiene caracteres no válidos']; ?>");
		}//Comprueba que campo siga la regex x del telefono, siendo así un numero válido.
	}else{
		campoIncorrecto(campo.id, "<?php echo $strings['El campo es vacío']; ?>");
	}		
}

function desprefijarTelefono(campo){
	//Funcion para quitar el prefijo del telefono o no se podrá guardar en la BD
	var x = /^0034[0-9]{9}$/;/*Numero internacional que empiece por 0034*/
	var y = /^\+34[0-9]{9}$/;/*Numero internacional que empiece por +34*/
	
	if(validarTelefono(campo)){
		if(x.test(campo.value)){
			return campo.value.substr(2);
		}else if(y.test(campo.value)){
			return campo.value.substr(1);
		}else{
			return campo.value;
		}
	}
}

function validarFormularioUsuarioAER(formulario, orden){
/*Función con el fin de validar los formularios ADD y EDIT (y no se el REGISTRO) de la tabla USUARIO*/
var elementos = formulario.elements;//Todos los inputs del formulario
var tamano = elementos.length;//Cantidad de elementos del array elementos
var validaciones = 0; //Cantidad de validaciones que se han realizado sobre los inputs
var posibilidadesElementos = ["login", "password", "DNI", "Nombre", "Apellidos", "Correo", "Direccion", "Telefono", "orden"];//Todas las posibildades de nombre de inputs
var bucle = 0;//Para ir recorriendo el array de posibilidades
var indexContrasena = '';//Index de la contraseña para tranformarlo al enviar
var indexTelefono = '';//Index del telefono para desprefijarlo al final

	while(bucle < tamano){//Recorrer el array de posibilidadesElementos
		var elemento = elementos[bucle];//Coger un elemento del formulario
		switch(elemento.name){//Dependiendo del nombre del elemento, que debería coincidir con uno del array de posibilidades, validamos una u otra cosa

			case posibilidadesElementos[0]:
				if(validarLoginUsuario(elemento)){//Si es el IdAccion validamos dicho
					validaciones++;
				}
			break;
			
			case posibilidadesElementos[1]:
				if(validarPasswordUsuario(elemento)){//Si es NombreAccion validamos dicho
					indexContrasena = bucle;//Guardar el index de la contraseña
					validaciones++;
				}
			break;
			
			case posibilidadesElementos[2]:
				if(validarDNIUsuario(elemento)){//Si es DNI validamos dicho
					validaciones++;
				}
			break;
			
			case posibilidadesElementos[3]:
				if(validarNombreUsuario(elemento.id)){//Si es Nombre validamos dicho
					validaciones++;
				}
			break;
			
			case posibilidadesElementos[4]:
				if(validarApellidosUsuario(elemento.id)){//Si es Apellidos validamos dicho
					validaciones++;
				}
			break;
			
			case posibilidadesElementos[5]:
				if(validarCorreoUsuario(elemento)){//Si es Correo validamos dicho
					validaciones++;
				}
			break;
			
			case posibilidadesElementos[6]:
				if(validarDireccionUsuario(elemento)){//Si es Direccion validamos dicho
					validaciones++;
				}
			break;
			
			case posibilidadesElementos[7]:
				if(validarTelefonoUsuario(elemento)){//Si es Telefono validamos dicho
					indexTelefono = bucle;
					validaciones++;
				}
			break;
			
			case posibilidadesElementos[8]:
				if(elemento.value == orden){//Si es la orden, mirar que coincida con la que se le pasa por argumento
					validaciones++;
				}
			break;
			
			default: //Si llega a aquí, es que un input tiene un nombre no valido por lo que se retorna false junto con un alert de error
				alert("No se ha encontrado uno de los elementos del formulario entre las posibilidades de los inputs");
				return false;
			break;
		}
		bucle++;
	}
	
	if(tamano > validaciones && validaciones > 0){//Si el tamaño supera a la cantidad de validaciones y las validaciones no son 0, algun campo falla
		alert("<?php echo $strings['Alguno de los campos no tiene valores correctos']; ?>");
		return false;
	}else if(tamano == validaciones){//Por ultimo, si se han realizado tantas validaciones como campos tiene el formulario, significa que se han validado todos los campos, haciendo submit del formulario
		if(confirm("<?php echo $strings['Formulario correcto, ¿desea enviarlo?']; ?>")){//Si confirma que desea enviarlo
			elementos[indexContrasena].maxlength = 128;//Cambiamos el tamaño a 128 bits para que la encriptación quepa
			elementos[indexContrasena].value = hex_md5(elementos[indexContrasena].value);//Se encripta la contraseña (guardarla por si se envía el formulario)
			elementos[indexTelefono].value = desprefijarTelefono(elementos[indexTelefono]);//Se quita el prefijo que pueda tener el telefono
			formulario.submit();//Se envía
			return true;
		}else{//No se envía.
			alert("<?php echo $strings['Se ha cancelado enviar el formulario']; ?>");
			return false;
		}		
	}else{//Alguno de las validaciones ha fallado, retornamos false y mensaje de error
		alert("<?php echo $strings['No se han podido comprobar todos los campos o hay un error extraño']; ?>");//Llegar a esta opción debería ser imposible
		return false;
	}
}

function campoIncorrecto(id, msg){
	//Cambia el circulo al lado del input (id) a rojo y el mensaje a su derecha en rojo y valor msg.
	document.getElementById(id+'Bot').src='../img/red-button.png'; //Imagen circulo rojo
	document.getElementById(id+'BotText').style.color='#AB0000'; //Texto color rojo
	if(msg === undefined){//Si no se pasa un msg como parámetro implica que el mensaje sea undefined
		msg = "<?php echo $strings['Error no identificado']; ?>";//Dado el caso, poner un error "por defecto"
	}
	document.getElementById(id+'BotText').innerHTML=msg; //Texto = msg
}

function campoCorrecto(id, msg){
	//Cambia el circulo al lado del input id a verde y el mensaje a su derecha en verde y palabra "correcto"
	document.getElementById(id+'Bot').src='../img/green-button.png'; //Imagen circulo verde
	document.getElementById(id+'BotText').style.color='#006400'; //Texto color verde
	if(msg === undefined){//Si no se pasa un msg como parámetro implica que el mensaje sea undefined
		msg = "<?php echo $strings['Correcto']; ?>";//Dado el caso, poner un mensaje "por defecto"
	}
	document.getElementById(id+'BotText').innerHTML=msg; //Eliminar texto, quizá cambiar a mensaje de "correcto" Edit: Sí, daltonismo...
}