<?php 
	session_start();
	include_once '../Locales/Strings_'. $_SESSION['idioma'].'.php';//Idioma
?>
/* 	Javascript de funciones para la entidad ACCION
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
function comprobarVacio(campo){
	//Comprueba si el campo pasado a la función es vacío, devolviendo TRUE en tal caso
  	if((campo === undefined) || (campo.value === null) || (campo.value === undefined) || (campo.value.length == 0)){ //Si el campo es null (no existe) o no tiene longitud, retornar true como que es vacío. Considerar undefined como vacio es necesario para el html
  		return true;
  	}
  	else{
		//Sino, el campo tiene "algo", no (false) es vacío
  		return false;
  	}
}  

function esVacio(campo){
	//Lo mismo que comprobarVacio pero introduciendo campo.value, un valor de un campo no un campo en sí.
	if((campo === undefined) || (campo === null) || (campo.length == 0)){ //Si el campo es null (no existe) o no tiene longitud, retornar true como que es vacío. Considerar undefined como vacio es necesario para el html
  		return true;
  	}
  	else{
		//Sino, el campo tiene "algo", no (false) es vacío
  		return false;
  	}
} 

function comprobarTexto(campo,size){
	//Compara el tamaño de un campo dado (campo) con el tamaño dado (size), retornando false si el tamaño de campo es mayor que el size y true en caso contrario
	if (campo.value.length>size){ //Si el tamaño del campo es mayor que el que se pasa como argumento
    	return false;
  	}
  	return true;
}

function comprobarVacio(campo){
	//Comprueba si el campo pasado a la función es vacío, devolviendo TRUE en tal caso
  	if((campo === undefined) || (campo.value === null) || (campo.value === undefined) || (campo.value.length == 0)){ //Si el campo es null (no existe) o no tiene longitud, retornar true como que es vacío. Considerar undefined como vacio es necesario para el html
  		return true;
  	}
  	else{
		//Sino, el campo tiene "algo", no (false) es vacío
  		return false;
  	}
} 

function comprobarAlfabetico(campo, size){
	//Comprueba que un campo (campo) dado sea solo letras, *significado de Alfabetico*, de un tamaño size
	var x = /^[a-zA-ZáéíóúÁÉÍÓÚüÜñç]*$/; //Expresión regular de cualquier letra entre a-z y A-Z tantas veces como se quiera (significado del *)
	
	if(comprobarTexto(campo, size)){ //Si el texto (campo) introducido no sobrepasa el tamaño (size) solicitado, se procede
		return x.test(campo.value);//Comprueba que siga la regex x de un campo alfabetico, siendo true si es valido
	}else{//Si sobrepasa el tamaño (size) retornase false
		return false;
	}	
}

function validarIdAccion(campo){
/*Función con el fin de validar el IdAcción varchar(6) NOT NULL*/
var tamano = 6;//tamaño límite

	if(comprobarVacio(campo)){//Si el campo es vacío, mensaje de error y devolver false
		campoIncorrecto(campo.id, "<?php echo $strings['El IdAccion es vacío']; ?>");
		return false;
	}else if(!comprobarTexto(campo, tamano)){//Si el campo supera el tamaño límite, mensaje de error y retornamos false
		campoIncorrecto(campo.id, "<?php echo $strings['El IdAccion supera el tamaño límite']; ?>");
	}else{//Si llega aquí debería estar bien
		campoCorrecto(campo.id);
		return true;
	}
}

function validarNombreAccion(campo){
/*Función con el fin de validar el IdAcción varchar(60) NOT NULL*/		
var tamano = 60;//tamaño límite

	if(comprobarVacio(campo)){//Si el campo es vacío, mensaje de error y devolver false
		campoIncorrecto(campo.id, "<?php echo $strings['El NombreAccion es vacío']; ?>");
		return false;
	}else if(!comprobarTexto(campo, tamano)){//Si el campo supera el tamaño límite, mensaje de error y retornamos false
		campoIncorrecto(campo.id, "<?php echo $strings['El NombreAccion supera el tamaño límite']; ?>");
	}else{//Si llega aquí debería estar bien
		campoCorrecto(campo.id);
		return true;
	}
}

function validarDescripAccion(campo){
/*Función con el fin de validar el IdAcción varchar(100) NOT NULL*/	
var tamano = 100;//tamaño límite

	if(comprobarVacio(campo)){//Si el campo es vacío, mensaje de error y devolver false
		campoIncorrecto(campo.id, "<?php echo $strings['El NombreAccion es vacío']; ?>");
		return false;
	}else if(!comprobarTexto(campo, tamano)){//Si el campo supera el tamaño límite, mensaje de error y retornamos false
		campoIncorrecto(campo.id, "<?php echo $strings['El NombreAccion supera el tamaño límite']; ?>");
	}else{//Si llega aquí debería estar bien
		campoCorrecto(campo.id);
		return true;
	}
}

function validarFormularioAccionesAE(formulario, orden){
/*Función con el fin de validar los formularios ADD y EDIT de la tabla ACCION*/
var elementos = formulario.elements;//Todos los inputs del formulario
var tamano = elementos.length;//Cantidad de elementos del array elementos
var validaciones = 0; //Cantidad de validaciones que se han realizado sobre los inputs
var posibilidadesElementos = ["IdAccion", "NombreAccion", "DescripAccion", "orden"];//Todas las posibildades de nombre de inputs
var bucle = 0;//Para ir recorriendo el array de posibilidades

	while(bucle < tamano){//Recorrer el array de posibilidadesElementos
		var elemento = elementos[bucle];//Coger un elemento del formulario
		
		switch(elemento.name){//Dependiendo del nombre del elemento, que debería coincidir con uno del array de posibilidades, validamos una u otra cosa
			case posibilidadesElementos[0]:
				if(validarIdAccion(elemento)){//Si es el IdAccion validamos dicho
					validaciones++;
				}
			break;
			
			case posibilidadesElementos[1]:
				if(validarNombreAccion(elemento)){//Si es NombreAccion validamos dicho
					validaciones++;
				}
			break;
			
			case posibilidadesElementos[2]:
				if(validarDescripAccion(elemento)){//Si es DescripAccion validamos dicho
					validaciones++;
				}
			break;
			
			case posibilidadesElementos[3]:
				if(elemento.value == orden){//Si es la orden, mirar que coincida con la que se le pasa por argumento
					validaciones++;
				}
			break;
			default: //Si llega a aquí, es que un input tiene un nombre no valido por lo que se retorna false junto con un alert de error
				alert("No se ha encontrado uno de los elementos del formulario entre las posibilidades de los inputs");
				return false;
			break;
		}
		bucle++;
	}
	
	if(tamano > validaciones && validaciones > 0){//Si el tamaño supera a la cantidad de validaciones y las validaciones no son 0, algun campo falla
		alert("<?php echo $strings['Alguno de los campos no tiene valores correctos']; ?>");
		return false;
	}else if(tamano == validaciones){//Por ultimo, si se han realizado tantas validaciones como campos tiene el formulario, significa que se han validado todos los campos, haciendo submit del formulario
		if(confirm("<?php echo $strings['Formulario correcto, ¿desea enviarlo?']; ?>")){//Si confirma que desea enviarlo
			formulario.submit();//Se envía
			return true;
		}else{//No se envía.
			alert("<?php echo $strings['Se ha cancelado enviar el formulario']; ?>");
			return false;
		}		
	}else{//Alguno de las validaciones ha fallado, retornamos false y mensaje de error
		alert("<?php echo $strings['No se han podido comprobar todos los campos o hay un error extraño']; ?>");//Llegar a esta opción debería ser imposible
		return false;
	}
}

function campoIncorrecto(id, msg){
	//Cambia el circulo al lado del input (id) a rojo y el mensaje a su derecha en rojo y valor msg.
	document.getElementById(id+'Bot').src='../img/red-button.png'; //Imagen circulo rojo
	document.getElementById(id+'BotText').style.color='#AB0000'; //Texto color rojo
	if(msg === undefined){//Si no se pasa un msg como parámetro implica que el mensaje sea undefined
		msg = "<?php echo $strings['Error no identificado']; ?>";//Dado el caso, poner un error "por defecto"
	}
	document.getElementById(id+'BotText').innerHTML=msg; //Texto = msg
}

function campoCorrecto(id, msg){
	//Cambia el circulo al lado del input id a verde y el mensaje a su derecha en verde y palabra "correcto"
	document.getElementById(id+'Bot').src='../img/green-button.png'; //Imagen circulo verde
	document.getElementById(id+'BotText').style.color='#006400'; //Texto color verde
	if(msg === undefined){//Si no se pasa un msg como parámetro implica que el mensaje sea undefined
		msg = "<?php echo $strings['Correcto']; ?>";//Dado el caso, poner un mensaje "por defecto"
	}
	document.getElementById(id+'BotText').innerHTML=msg; //Eliminar texto, quizá cambiar a mensaje de "correcto" Edit: Sí, daltonismo...
}