<?php 
	session_start();
	include_once '../Locales/Strings_'. $_SESSION['idioma'].'.php';//Idioma
?>
/*
	Javascript de funciones para la entidad ENTREGA
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/

function generarAlias(campoAliasId) {
	var campoAlias = document.getElementById(campoAliasId);

	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  
	for (var i = 0; i < campoAlias.maxLength; i++) {
	  text += possible.charAt(Math.floor(Math.random() * possible.length));
	}

	campoAlias.value = text;
}

function validarRAR(campoFicheroId) {
	regex = /.*\.rar$/i;
	if(regex.test(document.getElementById(campoFicheroId).value)) {return true;}
	else {
		alert("<?php echo $strings['Fichero con extensión no válida']; ?>");//Mensaje de error
		document.getElementById(campoFicheroId).value = '';
		return false;
	}
}

function validarFormularioAEREntrega(formulario, orden){/*Add Edit o Registro de ENTREGAS*/
	//Validación completa de un formulario pasando un formulario (formulario). Comprobará todos los elementos del formulario comparandolos con las posibilidades, y si da alguna, validarla.
	var elementos = formulario.elements;//Todos los inputs del formulario
	var tamano = elementos.length;//Cantidad de elementos del array elementos
	var validaciones = 0; //Cantidad de validaciones que se han realizado sobre los inputs
	var bucle = 0;//Cantidad de iteraciones del while principal. Si sobrepasa el tamano de elementos, no se ha validado todos
	var posibilidadesElementos = ["login","IdTrabajo","Alias","Horas","Ruta","orden"];//Todas las posibildades de nombre de inputs
	var indexContrasena = '';//Index dentro de elementos de la contrasena para futura encriptación
	
	while(validaciones < tamano && bucle < tamano){//Mientras no se hayan hecho todas las validaciones del formulario
		var elemento = elementos[bucle];//Elemento que aun no se ha validado
		var i = 0; //Index del array posibildadesElementos
		
		if(!elemento.required && comprobarVacio(elemento)){//Si el elemento no es requerido Y es nulo, no necesitamos validarlo, puede ser cualquier cosa
			validaciones++;
		}else{//Sino, validación
			while(elemento.name != posibilidadesElementos[i] && i < posibilidadesElementos.length){//Mientras no se encuentre qué campo es elementos O i sobrepase el tamaño de posibilidadesElementos
				i++;//Incrementamos i
			}
			if(i >= posibilidadesElementos.length){//Si i sobrepasa o iguala el tamaño de posibildadesElementos, no se ha encontrado un campo, implica error
				alert(<?php echo "'"; echo $strings['Error con uno de los campos']; echo "'"; ?>);//Mensaje de error
				return false;//Abortar proceso
			}else{//Sino, validamos el campo que sea
				switch(posibilidadesElementos[i]){//Dado el campo que sea, validar ese campo
					case posibilidadesElementos[0]://Si es el login
						if(validarLogin(elemento.id)){validaciones++;}//Si se valida el login, se aumenta el numero de validaciones
						break;
					
					case posibilidadesElementos[1]://idTrabajo
						if(comprobarVacio(elemento.id) && comprobarTexto(elemento.id,6)){validaciones++;}/*Ditto...*/
						break;
						
					case posibilidadesElementos[2]://Alias
						if(comprobarVacio(elemento.id) && comprobarTexto(elemento.id,6)){validaciones++;}
						break;
						
					case posibilidadesElementos[3]://Horas
						if(validarEntero(elemento)){validaciones++;}
						break;
					
					case posibilidadesElementos[4]://Ruta
						if(validarRAR(elemento.id)){validaciones++;}
						break;
					
				}
			}
		}
		bucle++;
	}
	
	if(tamano > validaciones && validaciones > 0){//Si el tamaño supera a la cantidad de validaciones y las validaciones no son 0, algun campo falla
		alert(<?php echo "'"; echo $strings['Alguno de los campos no tiene valores correctos']; echo "'"; ?>);
		return false;
	}else if(tamano == validaciones){//Por ultimo, si se han realizado tantas validaciones como campos tiene el formulario, significa que se han validado todos los campos, haciendo submit del formulario
		if(confirm(<?php echo "'"; echo $strings['Formulario correcto, ¿desea enviarlo?']; echo "'"; ?>)){//Si confirma que desea enviarlo
	
			formulario.submit();//Se envía
			return true;
		}else{//No se envía.
			alert(<?php echo "'"; echo $strings['Se ha cancelado enviar el formulario']; echo "'"; ?>);
			return false;
		}		
	}else{//Alguno de las validaciones ha fallado, retornamos false y mensaje de error
		alert(<?php echo "'"; echo $strings['No se han podido comprobar todos los campos o hay un error extraño']; echo "'"; ?>);//Llegar a esta opción debería ser imposible
		return false;
	}
}