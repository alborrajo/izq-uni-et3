<?php
/* Clase de modelo de PERMISO, el cual accederá exclusivamente a la base de datos
	hay funciones get para poder insertarlas en formularios y set por hacer algo de simetría
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	5/12/17
*/
class Permiso{
	
	var $IdGrupo;
	var $IdFuncionalidad;
	var $IdAccion;
	var $mysqli;
	//Atributos
	
	function __construct($IdGrupo,$IdFuncionalidad,$IdAccion){
		//Asignaciones
		$this->_setIdGrupo($IdGrupo);
		$this->_setIdFuncionalidad($IdFuncionalidad);
		$this->_setIdAccion($IdAccion);
		
		include_once '../Functions/AccederBD.php';
		$this->mysqli = ConectarBD();
	}
	
	function _setIdGrupo($IdGrupo){
		$this->IdGrupo = $IdGrupo;
	}
	
	function _setIdFuncionalidad($IdFuncionalidad){
		$this->IdFuncionalidad = $IdFuncionalidad;
	}
	
	function _setIdAccion($IdAccion){
		$this->IdAccion = $IdAccion;
	}		
	
	function _getIdGrupo(){
		return $this->IdGrupo;
	}
	
	function _getIdFuncionalidad(){
		return $this->IdFuncionalidad;
	}
	
	function _getIdAccion(){
		return $this->IdAccion;
	}

	function _getNombreGrupo(){
		$IdGrupo = mysqli_real_escape_string($this->mysqli, $this->IdGrupo);
		
		$sql = $this->mysqli->query("SELECT NombreGrupo FROM PERMISO P, GRUPO G WHERE P.IdGrupo = '$IdGrupo' AND P.IdGrupo = G.IdGrupo");
		$NombreGrupo = $sql->fetch_row();
		return $NombreGrupo[0];
	}

	function grupos(){//Para obtener una lista con todos los grupos de la BD
		$sql = "SELECT * FROM GRUPO";

		$resultado = $this->mysqli->query($sql);

		if(!$resultado || $resultado->num_rows == 0) {
			return 'No se ha encontrado ningun dato';
		}else{
			return $resultado;
		}
	}
	
	function funcs(){//Para obtener una lista con todas las funcionalidades de la BD
		$sql = "SELECT * FROM FUNCIONALIDAD";

		$resultado = $this->mysqli->query($sql);

		if(!$resultado || $resultado->num_rows == 0) {
			return 'No se ha encontrado ningun dato';
		}else{
			return $resultado;
		}
	}

	function acciones(){//Para obtener una lista con todas las acciones de la BD
		$sql = "SELECT * FROM ACCION";

		$resultado = $this->mysqli->query($sql);

		if(!$resultado || $resultado->num_rows == 0) {
			return 'No se ha encontrado ningun dato';
		}else{
			return $resultado;
		}
	}

	function func_accion(){//Para obtener una lista con todas las acciones de cada funcionalidad
		$sql = "SELECT * FROM FUNC_ACCION";

		$resultado = $this->mysqli->query($sql);

		if(!$resultado || $resultado->num_rows == 0) {
			return 'No se ha encontrado ningun dato';
		}else{
			return $resultado;
		}
	}

	function ADD(){//Para añadir a la BD
		if($this->IdGrupo == '' && $this->IdFuncionalidad == '' && $this->IdAccion == ''){
			return 'Clave vacía';
		}else{
			$IdGrupo = mysqli_real_escape_string($this->mysqli, $this->IdGrupo);
			$IdFuncionalidad = mysqli_real_escape_string($this->mysqli, $this->IdFuncionalidad);
			$IdAccion = mysqli_real_escape_string($this->mysqli, $this->IdAccion);
			
			$sql = "SELECT * FROM PERMISO WHERE IdGrupo = '$IdGrupo' AND IdFuncionalidad = '$IdFuncionalidad' AND IdAccion = '$IdAccion'";
			
			$resultado = $this->mysqli->query($sql);
			
			if(!$resultado){
				return 'No se ha podido conectar con la BD';
			}else{
				if($resultado->num_rows == 0){
					$sql = "INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('$IdGrupo', '$IdFuncionalidad', '$IdAccion')";
				
					if(!$this->mysqli->query($sql)){
						return 'Ha fallado la inserción';
					}else{
						return 'Inserción correcta';
					}
				}else{
					return 'Los datos clave introducidos ya existen en la tabla';
				}
			}
		}
	}

	function DELETE(){//Para eliminar de la BD
		$IdGrupo = mysqli_real_escape_string($this->mysqli, $this->IdGrupo);
		$IdFuncionalidad = mysqli_real_escape_string($this->mysqli, $this->IdFuncionalidad);
		$IdAccion = mysqli_real_escape_string($this->mysqli, $this->IdAccion);
			
		$sql = "SELECT * FROM PERMISO WHERE IdGrupo = '$IdGrupo' AND IdFuncionalidad = '$IdFuncionalidad' AND IdAccion = '$IdAccion'";
		
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No se ha encontrado el permiso';
		}else{
			$sql = "DELETE FROM PERMISO WHERE IdGrupo = '$IdGrupo' AND IdFuncionalidad = '$IdFuncionalidad' AND IdAccion = '$IdAccion'";
			
			if(!$this->mysqli->query($sql)){
				return 'Fallo al eliminar la tupla';
			}else{
				return 'Se ha borrado la tupla';
			}
		}
	}

	function SEARCH(){//Para buscar en la base de datos, no se busca la contraseña	
		$IdGrupo = mysqli_real_escape_string($this->mysqli, $this->IdGrupo);
		$IdFuncionalidad = mysqli_real_escape_string($this->mysqli, $this->IdFuncionalidad);
		$IdAccion = mysqli_real_escape_string($this->mysqli, $this->IdAccion);
		
		$sql = "SELECT P.IdGrupo, NombreGrupo, IdFuncionalidad, IdAccion FROM PERMISO P, GRUPO G WHERE ((P.IdGrupo = G.IdGrupo) AND (P.IdGrupo LIKE '%$IdGrupo%') AND (P.IdFuncionalidad LIKE '%$IdFuncionalidad%') AND (P.IdAccion LIKE '%$IdAccion%'))";
	
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado || $resultado->num_rows == 0){
			return 'No se ha encontrado ningun dato';
		}else{
			return $resultado;
		}
	}
	
	function SHOWALL(){//Para mostrar la BD
		$sql = "SELECT P.IdGrupo, NombreGrupo, IdFuncionalidad, IdAccion FROM PERMISO P, GRUPO G WHERE P.IdGrupo = G.IdGrupo";
		
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No se ha encontrado ningun dato';
		}else{
			return $resultado;
		}
	}

}
?>