
<?php
/* Clase de modelo de HISTORIA, el cual accederá exclusivamente a la base de datos
	hay funciones get para poder insertarlas en formularios y set por hacer algo de simetría
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	que quede claro, a no ser que no sepas español
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	5/12/17
*/
class Historia{
	

	var $IdTrabajo;
	var $IdHistoria;
	var $textoHistoria;
	var $mysqli;

	//Atributos
	
	function __construct($IdTrabajo,$IdHistoria,$textoHistoria){
		//Asignaciones
	
		$this->_setIdTrabajo($IdTrabajo);
		$this->_setIdHistoria($IdHistoria);
	    $this->_setTextoHistoria($textoHistoria);
	  
		
		include_once '../Functions/AccederBD.php';
		$this->mysqli = ConectarBD();
	}
	
	
	function _setIdTrabajo($IdTrabajo){
		$this->IdTrabajo = $IdTrabajo;
	}
	
	function _setIdHistoria($IdHistoria){
		$this->IdHistoria = $IdHistoria;
	}
	
	
	function _setTextoHistoria($textoHistoria){
		$this->textoHistoria = $textoHistoria;
	}		
	
	
	function _getIdTrabajo(){
		return $this->IdTrabajo;
	}
	
	
	function _getIdHistoria(){
		return $this->IdHistoria;
	}
	
	
	function _getTextoHistoria(){
		return $this->textoHistoria;
	}

	
	function _getDatosGuardados(){//Para recuperar de la base de datos
		if(($this->IdTrabajo == '' || $this->IdHistoria == '')){
			return 'Historia vacía, introduzca una Historia';
		}else{
			$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
			$IdHistoria = mysqli_real_escape_string($this->mysqli, $this->IdHistoria);
			
			$sql = "SELECT * FROM HISTORIA WHERE IdTrabajo = '$IdTrabajo' AND IdHistoria = '$IdHistoria'";
			
			$resultado = $this->mysqli->query($sql);
			
			if(!$resultado){
				return 'No se ha podido conectar con la BD';
			}else if($resultado->num_rows == 0){
				return 'No existe la Historia';
			}else{
				$fila = $resultado->fetch_row();
				
				$this->_setTextoHistoria($fila[2]);
			
			}
		}
	}
		
	
	function ADD(){//Para añadir a la BD
		if(($this->IdTrabajo == '' && $this->IdHistoria == '')){
			return 'Historia vacía, introduzca una Historia';
		}else{
			$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
			$IdHistoria = mysqli_real_escape_string($this->mysqli, $this->IdHistoria);

			$sql = "SELECT * FROM HISTORIA WHERE IdTrabajo = '$IdTrabajo' AND IdHistoria = '$IdHistoria'";
			
			$resultado = $this->mysqli->query($sql);
			
			if(!$resultado){
				return 'No se ha podido conectar con la BD';
			}else{
				if($resultado->num_rows == 0){
					$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
					$IdHistoria = mysqli_real_escape_string($this->mysqli, $this->IdHistoria);
					$textoHistoria = mysqli_real_escape_string($this->mysqli, $this->textoHistoria);
					
					$sql = "INSERT INTO HISTORIA (IdTrabajo,IdHistoria,textoHistoria) VALUES ('$IdTrabajo', '$IdHistoria','$textoHistoria')";
				
					if(!$this->mysqli->query($sql)){
						return 'Ha fallado el insertar la Historia; IdTrabajo debe ser único';
					}else{
						return 'Inserción correcta';
					}
				}else{
					return 'El IdTrabajo introducido ya existe';
				}
			}
		}
	}
	
	function EDIT(){//Para editar de la BD
		if(($this->IdTrabajo == '' && $this->IdHistoria == '')){
			return 'Historia vacía, introduzca un Historia';
		}else{
			$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
			$IdHistoria = mysqli_real_escape_string($this->mysqli, $this->IdHistoria);

			$sql = "SELECT * FROM HISTORIA WHERE IdTrabajo = '$IdTrabajo' AND IdHistoria = '$IdHistoria'";
			
			$resultado = $this->mysqli->query($sql);
			
			if(!$resultado){
				return 'No se ha podido conectar con la BD';
			}else if($resultado->num_rows == 1){
				$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
				$IdHistoria = mysqli_real_escape_string($this->mysqli, $this->IdHistoria);
				$textoHistoria = mysqli_real_escape_string($this->mysqli, $this->textoHistoria);

				$sql = "UPDATE HISTORIA SET IdHistoria = '$IdHistoria', textoHistoria = '$textoHistoria' WHERE IdTrabajo = '$IdTrabajo' AND IdHistoria = '$IdHistoria'";
				
				if(!$this->mysqli->query($sql)){
					return 'Ha fallado la actualización de la historia';
				}else{
					return 'Modificado correcto';
				}
			}else{
				return 'IdTrabajo o IdHistoria no existen en la base de datos';
			}
		}
	}
	
	function SEARCH(){//Para buscar en la base de datos, no se busca la contraseña	
		$sql = "SELECT * FROM HISTORIA WHERE ((IdTrabajo LIKE '%$this->IdTrabajo%') AND (IdHistoria LIKE '%$this->IdHistoria%') AND (textoHistoria LIKE '%$this->textoHistoria%'))";
	
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado || $resultado->num_rows == 0){
			return 'No se ha encontrado ningun dato';
		}else{
			return $resultado;
		}
	}
	
	function DELETE(){//Para eliminar de la BD
		$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
		$IdHistoria = mysqli_real_escape_string($this->mysqli, $this->IdHistoria);
		
		$sql = "SELECT * FROM HISTORIA WHERE IdTrabajo = '$IdTrabajo' AND IdHistoria = '$IdHistoria'";
		
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No se ha encontrado ningun dato';
		}else{
			$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
			$IdHistoria = mysqli_real_escape_string($this->mysqli, $this->IdHistoria);

			$sql = "DELETE FROM HISTORIA WHERE IdTrabajo = '$IdTrabajo' AND IdHistoria = '$IdHistoria'";
			
			if(!$this->mysqli->query($sql)){
				return 'Fallo al eliminar la tupla';
			}else{
				return 'Se ha borrado la tupla';
			}
		}
	}
	
	function SHOWCURRENT(){//Para mostrar de la base de datos
		$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
		$IdHistoria = mysqli_real_escape_string($this->mysqli, $this->IdHistoria);

		$sql = "SELECT * FROM HISTORIA WHERE IdTrabajo = '$IdTrabajo' AND IdHistoria = '$IdHistoria'";
		
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No existe la Historia';
		}else{
			return $resultado;
		}
	}
	
	function SHOWALL(){//Para mostrar la BD
		$sql = "SELECT * FROM HISTORIA";
	
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No se ha encontrado ningun dato';
		}else{
			return $resultado;
		}
	}
	
}
?>