<?php
/* Clase de modelo de ACCION, el cual accederá exclusivamente a la base de datos
	hay funciones get para poder insertarlas en formularios y set por hacer algo de simetría
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	que quede claro, a no ser que no sepas español
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	5/12/17
*/
class Accion{
	
	var $IdAccion;
	var $NombreAccion;
	var $DescripAccion;
	var $mysqli;
	//Atributos
	
	function __construct($IdAccion, $NombreAccion, $DescripAccion){
		//Asignaciones
		$this->_setIdAccion($IdAccion);
		$this->_setNombreAccion($NombreAccion);
		$this->_setDescripAccion($DescripAccion);
		
		include_once '../Functions/AccederBD.php';
		$this->mysqli = ConectarBD();
	}
	
		function _setIdAccion($IdAccion){
			$this->IdAccion = $IdAccion;
		}
		
		function _setNombreAccion($NombreAccion){
			$this->NombreAccion = $NombreAccion;
		}
		
		function _setDescripAccion($DescripAccion){
			$this->DescripAccion = $DescripAccion;
		}
		
		
		function _getIdAccion(){
			return $this->IdAccion;
		}
		
		function _getNombreAccion(){
			return $this->NombreAccion;
		}
		
		function _getDescripAccion(){
			return $this->DescripAccion;
		}
		
		function _getDatosGuardados(){//Para recuperar de la base de datos
			if(($this->IdAccion == '')){
				return 'IdAccion vacío, introduzca un IdAccion';
			}else{
				$idaccion = mysqli_real_escape_string($this->mysqli, $this->IdAccion);
				$sql = "SELECT * FROM ACCION WHERE IdAccion = '$idaccion'";
				
				$resultado = $this->mysqli->query($sql);
				
				if(!$resultado){
					return 'No se ha podido conectar con la BD';
				}else if($resultado->num_rows == 0){
					return 'No existe el IdAccion';
				}else{
					$fila = $resultado->fetch_row();
					
					$this->_setNombreAccion($fila[1]);
					$this->_setDescripAccion($fila[2]);
				}
			}
		}
		
	
	function ADD(){//Para añadir a la BD
		if(($this->IdAccion == '')){
			return 'IdAccion vacío, introduzca un IdAccion';
		}else{
			$idaccion = mysqli_real_escape_string($this->mysqli, $this->IdAccion);
			$sql = "SELECT * FROM ACCION WHERE IdAccion = '$idaccion'";
			
			$resultado = $this->mysqli->query($sql);
			
			if(!$resultado){
				return 'No se ha podido conectar con la BD';
			}else{
				if($resultado->num_rows == 0){
					$idaccion = mysqli_real_escape_string($this->mysqli, $this->IdAccion);
					$nombreaccion = mysqli_real_escape_string($this->mysqli, $this->NombreAccion);
					$descripaccion = mysqli_real_escape_string($this->mysqli, $this->DescripAccion);

					$sql = "INSERT INTO ACCION (IdAccion, NombreAccion, DescripAccion) VALUES ('$idaccion', '$nombreaccion', '$descripaccion')";
				
					if(!$this->mysqli->query($sql)){
						return 'Ha fallado el insertar la acción; IdAccion debe ser único';
					}else{
						return 'Inserción correcta';
					}
				}else{
					return 'El IdAccion introducido ya existe';
				}
			}
		}
	}
	
	function EDIT(){//Para editar de la BD
		if(($this->IdAccion == '')){
			return 'IdAccion vacío, introduzca un IdAccion';
		}else{
			$idaccion = mysqli_real_escape_string($this->mysqli, $this->IdAccion);
			$sql = "SELECT * FROM ACCION WHERE IdAccion = '$idaccion'";
			
			$resultado = $this->mysqli->query($sql);
			
			if(!$resultado){
				return 'No se ha podido conectar con la BD';
			}else if($resultado->num_rows == 1){
				$idaccion = mysqli_real_escape_string($this->mysqli, $this->IdAccion);
				$nombreaccion = mysqli_real_escape_string($this->mysqli, $this->NombreAccion);
				$descripaccion = mysqli_real_escape_string($this->mysqli, $this->DescripAccion);

				$sql = "UPDATE ACCION SET NombreAccion = '$nombreaccion', DescripAccion = '$descripaccion' WHERE IdAccion = '$idaccion'";
				
				if(!$this->mysqli->query($sql)){
					return 'Ha fallado la actualización de la acción';
				}else{
					return 'Modificado correcto';
				}
			}else{
				return 'IdAccion no existe en la base de datos';
			}
		}
	}
	
	function SEARCH(){//Para buscar en la base de datos, no se busca la contraseña	
		$idaccion = mysqli_real_escape_string($this->mysqli, $this->IdAccion);
		$nombreaccion = mysqli_real_escape_string($this->mysqli, $this->NombreAccion);
		$descripaccion = mysqli_real_escape_string($this->mysqli, $this->DescripAccion);

		$sql = "SELECT * FROM ACCION WHERE ((IdAccion LIKE '%$idaccion%') AND (NombreAccion LIKE '%$nombreaccion%') AND (DescripAccion LIKE '%$descripaccion%'))";
	
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado || $resultado->num_rows == 0){
			return 'No se ha encontrado ningun dato';
		}else{
			return $resultado;
		}
	}
	
	function DELETE(){//Para eliminar de la BD
		$idaccion = mysqli_real_escape_string($this->mysqli, $this->IdAccion);		
		$sql = "SELECT * FROM ACCION WHERE IdAccion = '$idaccion'";
		
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No se ha encontrado la acción';
		}else{
			$idaccion = mysqli_real_escape_string($this->mysqli, $this->IdAccion);			
			$sql = "DELETE FROM ACCION WHERE IdAccion = '$idaccion'";
			
			if(!$this->mysqli->query($sql)){
				return 'Fallo al eliminar la tupla';
			}
						
			$sql = "DELETE FROM FUNC_ACCION WHERE IdAccion = '$idaccion'";
			
			if(!$this->mysqli->query($sql)){
				return 'Fallo al eliminar tuplas';
			}else{
				return 'Accion eliminada correctamente';
			}
		}
	}
	
	function SHOWCURRENT(){//Para mostrar de la base de datos
		$idaccion = mysqli_real_escape_string($this->mysqli, $this->IdAccion);			
		
		$sql = "SELECT * FROM ACCION WHERE IdAccion = '$idaccion'";
		
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No existe el IdAccion';
		}else{
			return $resultado;
		}
	}
	
	function SHOWALL(){//Para mostrar la BD
		$sql = "SELECT * FROM ACCION";
	
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else{
			return $resultado;
		}
	}
}
?>