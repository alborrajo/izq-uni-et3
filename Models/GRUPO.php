<?php
/* Clase de modelo de GRUPO, el cual accederá exclusivamente a la base de datos
	hay funciones get para poder insertarlas en formularios y set por hacer algo de simetría
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	que quede claro, a no ser que no sepas español
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	5/12/17
*/
class Grupo{
	
	var $IdGrupo;
	var $NombreGrupo;
	var $DescripGrupo;
	
	//Atributos
	
	function __construct($IdGrupo, $NombreGrupo, $DescripGrupo){
		//Asignaciones
		$this->_setIdGrupo($IdGrupo);
		$this->_setNombreGrupo($NombreGrupo);
		$this->_setDescripcionGrupo($DescripGrupo);
		
		include_once '../Functions/AccederBD.php';
			$this->mysqli = ConectarBD();
		}
	
		function _setIdGrupo($IdGrupo){
			$this->IdGrupo = $IdGrupo;
		}
		
		function _setNombreGrupo($NombreGrupo){
			$this->NombreGrupo = $NombreGrupo;
		}
		
		function _setDescripcionGrupo($DescripGrupo){
			$this->DescripGrupo = $DescripGrupo;
		}
		
		

		function _getIdGrupo(){
			return $this->IdGrupo;
		}
		
		function _getNombreGrupo(){
			return $this->NombreGrupo;
		}
		
		function _getDescripcionGrupo(){
			return $this->DescripGrupo;
		}
		
	
		
		function _getDatosGuardados(){//Para recuperar de la base de datos
			if(($this->IdGrupo == '')){
				return 'IdGrupo vacío, introduzca un IdGrupo';
			}else{
				$IdGrupo = mysqli_real_escape_string($this->mysqli, $this->IdGrupo);
				
				$sql = "SELECT * FROM GRUPO WHERE IdGrupo = '$IdGrupo'";
				
				$resultado = $this->mysqli->query($sql);
				
				if(!$resultado){
					return 'No se ha podido conectar con la BD';
				}else if($resultado->num_rows == 0){
					return 'No existe el IdGrupo';
				}else{
					$fila = $resultado->fetch_row();
					
					$this->_setNombreGrupo($fila[1]);
					$this->_setDescripcionGrupo($fila[2]);
				}
			}
		}
		
	
	function ADD(){//Para añadir a la BD
		if(($this->IdGrupo == '')){
			return 'IdGrupo vacío, introduzca un IdGrupo';
		}else{
			$IdGrupo = mysqli_real_escape_string($this->mysqli, $this->IdGrupo);
			
			$sql = "SELECT * FROM GRUPO WHERE IdGrupo = '$IdGrupo'";
			
			$resultado = $this->mysqli->query($sql);
			
			if(!$resultado){
				return 'No se ha podido conectar con la BD';
			}else{
				if($resultado->num_rows == 0){
					$NombreGrupo = mysqli_real_escape_string($this->mysqli, $this->NombreGrupo);
					$DescripGrupo = mysqli_real_escape_string($this->mysqli, $this->DescripGrupo);
			
					$sql = "INSERT INTO GRUPO (IdGrupo, NombreGrupo, DescripGrupo) VALUES ('$IdGrupo', '$NombreGrupo', '$DescripGrupo')";
				
					if(!$this->mysqli->query($sql)){
						return 'Ha fallado el insertar el grupo; IdGrupo debe ser único';
					}else{
						return 'Inserción correcta';
					}
				}else{
					return 'El IdGrupo introducido ya existe';
				}
			}
		}
	}
	
	function EDIT(){//Para editar de la BD
		if(($this->IdGrupo == '')){
			return 'IdGrupo vacío, introduzca un IdGrupo';
		}else{
			$IdGrupo = mysqli_real_escape_string($this->mysqli, $this->IdGrupo);
			
			$sql = "SELECT * FROM GRUPO WHERE IdGrupo = '$IdGrupo'";
			
			$resultado = $this->mysqli->query($sql);
			
			if(!$resultado){
				return 'No se ha podido conectar con la BD';
			}else if($resultado->num_rows == 1){
				$NombreGrupo = mysqli_real_escape_string($this->mysqli, $this->NombreGrupo);
				$DescripGrupo = mysqli_real_escape_string($this->mysqli, $this->DescripGrupo);
					
				$sql = "UPDATE GRUPO SET NombreGrupo = '$NombreGrupo', DescripGrupo = '$DescripGrupo' WHERE IdGrupo = '$IdGrupo'";
				
				if(!$this->mysqli->query($sql)){
					return 'Ha fallado la actualización del usuario';
				}else{
					return 'Modificado correcto';
				}
			}else{
				return 'IdGrupo no existe en la base de datos';
			}
		}
	}
	
	function SEARCH(){//Para buscar en la base de datos, no se busca la contraseña	
		$IdGrupo = mysqli_real_escape_string($this->mysqli, $this->IdGrupo);
		$NombreGrupo = mysqli_real_escape_string($this->mysqli, $this->NombreGrupo);
		$DescripGrupo = mysqli_real_escape_string($this->mysqli, $this->DescripGrupo);
				
		$sql = "SELECT * FROM GRUPO WHERE (IdGrupo LIKE '%$IdGrupo%') AND (NombreGrupo LIKE '%$NombreGrupo%') AND (DescripGrupo LIKE '%$DescripGrupo%')";
	
		$resultado1 = $this->mysqli->query($sql);
		
		if(!$resultado1 || $resultado1->num_rows == 0){
			return 'No se ha encontrado ningun dato';
		}else{
		
			$sql = "SELECT IdAccion, NombreAccion FROM ACCION";
			
			$ACCIONES = $this->mysqli->query($sql);
			
			if(!$ACCIONES){
				return 'No se ha podido conectar con la BD';
			}
			
			$sql = "SELECT IdFuncionalidad, NombreFuncionalidad FROM FUNCIONALIDAD";
			
			$FUNCIONALIDADES = $this->mysqli->query($sql);
			
			if(!$FUNCIONALIDADES){
				return 'No se ha podido conectar con la BD';
			}
			
			$sql = "SELECT * FROM PERMISO";
			
			$permisos = $this->mysqli->query($sql);
			
			if(!$permisos){
				return 'Nose ha podido conectar con la BD';
			}else{
				return array ($resultado1, $ACCIONES, $FUNCIONALIDADES, $permisos);
			}
		}	
	}
	
	function DELETE(){//Para eliminar de la BD
		$IdGrupo = mysqli_real_escape_string($this->mysqli, $this->IdGrupo);
		
		$sql = "SELECT * FROM GRUPO WHERE IdGrupo = '$IdGrupo'";
		
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No se ha encontrado al grupo';
		}else{
			$sql = "DELETE FROM GRUPO WHERE IdGrupo = '$IdGrupo'";
			
			if(!$this->mysqli->query($sql)){
				return 'Fallo al eliminar la tupla';
			}
			
			$sql = "DELETE FROM USU_GRUPO WHERE IdGrupo = '$IdGrupo'";
			
			if(!$this->mysqli->query($sql)){
				return 'Fallo al eliminar tuplas';
			}
			
			$sql = "DELETE FROM PERMISO WHERE IdGrupo = '$IdGrupo'";
			
			if(!$this->mysqli->query($sql)){
				return 'Fallo al eliminar tuplas';
			}else{
				return 'Grupo eliminado correctamente';
			}
		}
	}
	
	function SHOWCURRENT(){//Para mostrar de la base de datos
		$IdGrupo = mysqli_real_escape_string($this->mysqli, $this->IdGrupo);
		
		$sql = "SELECT * FROM GRUPO WHERE IdGrupo = '$IdGrupo'";
		
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No existe el grupo';
		}else{
			return $resultado;
		}
	}
	
	function SHOWALL(){//Para mostrar la BD
		$sql = "SELECT * FROM GRUPO";
	
		$resultado1 = $this->mysqli->query($sql);
		
		if(!$resultado1){
			return 'No se ha podido conectar con la BD';
		}else if($resultado1->num_rows == 0){
			return 'No se ha encontrado ningun dato';
		}else{
		
			$sql = "SELECT IdAccion, NombreAccion FROM ACCION";
			
			$ACCIONES = $this->mysqli->query($sql);
			
			if(!$ACCIONES){
				return 'No se ha podido conectar con la BD';
			}
			
			$sql = "SELECT IdFuncionalidad, NombreFuncionalidad FROM FUNCIONALIDAD";
			
			$FUNCIONALIDADES = $this->mysqli->query($sql);
			
			if(!$FUNCIONALIDADES){
				return 'No se ha podido conectar con la BD';
			}
			
			$sql = "SELECT * FROM PERMISO";
			
			$permisos = $this->mysqli->query($sql);
			
			if(!$permisos){
				return 'Nose ha podido conectar con la BD';
			}else{
				return array ($resultado1, $ACCIONES, $FUNCIONALIDADES, $permisos);
			}
		}		
	}
	
	function DARPERMISO($IdFuncionalidad, $IdAccion){
		$IdGrupo = mysqli_real_escape_string($this->mysqli, $this->IdGrupo);
		
		$sql = "SELECT * FROM PERMISO WHERE IdGrupo = '$IdGrupo' AND IdFuncionalidad = '$IdFuncionalidad' AND IdAccion = '$IdAccion'";
		
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows != 0){
			return 'El grupo ya el permiso';
		}else{
			$sql = "SELECT * FROM ACCION WHERE IdAccion = '$IdAccion'";
			
			$resultado = $this->mysqli->query($sql);

			if(!$resultado){
				return 'No se ha podido conectar con la BD';
			}else if($resultado->num_rows == 0){
				return 'No existe el IdAccion';
			}else{
				$sql = "SELECT * FROM FUNCIONALIDAD WHERE IdFuncionalidad = '$IdFuncionalidad'";
			
				$resultado = $this->mysqli->query($sql);

				if(!$resultado){
					return 'No se ha podido conectar con la BD';
				}else if($resultado->num_rows == 0){
					return 'No existe el IdFuncionalidad';
				}else{
					$sql = "SELECT * FROM FUNC_ACCION WHERE IdFuncionalidad = '$IdFuncionalidad' AND IdAccion = '$IdAccion'";
					
					$resultado = $this->mysqli->query($sql);

					if(!$resultado){
						return 'No se ha podido conectar con la BD';
					}else if($resultado->num_rows == 0){
						return 'No existe la accion para la funcionalidad';
					}else{
						$sql = "INSERT INTO PERMISO (IdGrupo, IdFuncionalidad, IdAccion) VALUES ('$IdGrupo', '$IdFuncionalidad', '$IdAccion')";
						
						$resultado = $this->mysqli->query($sql);
						
						if(!$resultado){
							return 'No se ha podido conectar con la BD';
						}else{
							return 'Permiso dado con exito';
						}
					}
				}
			}
		}		
	}
	
	function QUITARPERMISO($IdFuncionalidad, $IdAccion){
		$IdGrupo = mysqli_real_escape_string($this->mysqli, $this->IdGrupo);
		
		$sql = "SELECT * FROM PERMISO WHERE IdGrupo = '$IdGrupo' AND IdFuncionalidad = '$IdFuncionalidad' AND IdAccion = '$IdAccion'";
		
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'El grupo no tiene el permiso';
		}else{
			$sql = "DELETE FROM PERMISO WHERE IdGrupo = '$IdGrupo' AND IdFuncionalidad = '$IdFuncionalidad' AND IdAccion = '$IdAccion'";
			
			$resultado = $this->mysqli->query($sql);
			
			if(!$resultado){
				return 'No se ha podido conectar con la BD';
			}else{
				return 'Permiso quitado con exito';
			}
		}	
	}
}
?>