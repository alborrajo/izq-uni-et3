<?php
/* Clase de modelo de EVALUACION, el cual accederá exclusivamente a la base de datos
	hay funciones get para poder insertarlas en formularios y set por hacer algo de simetría
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	que quede claro, a no ser que no sepas español
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	5/12/17
*/
class Evaluacion{
	
	var $IdTrabajo;
	var $AliasEvaluado;
	var $LoginEvaluador;
	var $IdHistoria;
	var $CorrectoA;
	var $ComenIncorrectoA;
	var $CorrectoP;
	var $ComentIncorrectoP;
	var $OK;
	//Atributos
	
	function __construct($IdTrabajo, $AliasEvaluado, $LoginEvaluador, $IdHistoria, $CorrectoA, $ComenIncorrectoA, $CorrectoP, $ComentIncorrectoP, $OK){
		//Asignaciones
		$this->_setIdTrabajo($IdTrabajo);
		$this->_setAliasEvaluado($AliasEvaluado);
		$this->_setLoginEvaluador($LoginEvaluador);
		$this->_setIdHistoria($IdHistoria);
		$this->_setCorrectoA($CorrectoA);
		$this->_setComenIncorrectoA($ComenIncorrectoA);
		$this->_setCorrectoP($CorrectoP);
		$this->_setComentIncorrectoP($ComentIncorrectoP);
		$this->_setOK($OK);

		include_once '../Functions/AccederBD.php';
		$this->mysqli = ConectarBD();
	}
		
	function _setIdTrabajo($IdTrabajo){
		$this->IdTrabajo = $IdTrabajo;
	}
	
	function _setAliasEvaluado($AliasEvaluado){
		$this->AliasEvaluado = $AliasEvaluado;
	}
	
	function _setLoginEvaluador($LoginEvaluador){
		$this->LoginEvaluador = $LoginEvaluador;
	}
	
	function _setIdHistoria($IdHistoria){
		$this->IdHistoria = $IdHistoria;
	}

	function _setCorrectoA($CorrectoA){
		$this->CorrectoA = $CorrectoA;
	}

	function _setComenIncorrectoA($ComenIncorrectoA){
		$this->ComenIncorrectoA = $ComenIncorrectoA;
	}

	function _setCorrectoP($CorrectoP){
		$this->CorrectoP = $CorrectoP;
	}

	function _setComentIncorrectoP($ComentIncorrectoP){
		$this->ComentIncorrectoP = $ComentIncorrectoP;
	}

	function _setOK($OK){
		$this->OK = $OK;
	}

	
	function _getIdTrabajo(){
		return $this->IdTrabajo;
	}
	
	function _getAliasEvaluado(){
		return $this->AliasEvaluado;
	}
	
	function _getLoginEvaluador(){
		return $this->LoginEvaluador;
	}
	
	function _getIdHistoria(){
		return $this->IdHistoria;
	}

	function _getCorrectoA(){
		return $this->CorrectoA;
	}

	function _getComenIncorrectoA(){
		return $this->ComenIncorrectoA;
	}

	function _getCorrectoP(){
		return $this->CorrectoP;
	}

	function _getComentIncorrectoP(){
		return $this->ComentIncorrectoP;
	}

	function _getOK(){
		return $this->OK;
	}
	
	
	function _getDatosGuardados(){//Para recuperar de la base de datos
		if($this->IdTrabajo == '' && $this->AliasEvaluado == '' && $this->LoginEvaluador == '' && $this->IdHistoria == ''){
			return 'Clave vacía';
		}else{
			$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
			$AliasEvaluado = mysqli_real_escape_string($this->mysqli, $this->AliasEvaluado);
			$LoginEvaluador = mysqli_real_escape_string($this->mysqli, $this->LoginEvaluador);
			$IdHistoria = mysqli_real_escape_string($this->mysqli, $this->IdHistoria);

			$sql = "SELECT * FROM EVALUACION WHERE ((IdTrabajo = '$IdTrabajo') AND (AliasEvaluado = '$AliasEvaluado') AND (LoginEvaluador = '$LoginEvaluador') AND (IdHistoria = '$IdHistoria'))";
			
			$resultado = $this->mysqli->query($sql);
			
			
			if(!$resultado){
				return 'No se ha podido conectar con la BD';
			}else if($resultado->num_rows == 0){
				return 'No existe la clave';
			}else{

				$fila = $resultado->fetch_row();
				
				$this->_setCorrectoA($fila[4]);
				$this->_setComenIncorrectoA($fila[5]);
				$this->_setCorrectoP($fila[6]);
				$this->_setComentIncorrectoP($fila[7]);
				$this->_setOK($fila[8]);

			}
		}
	}
		
	
	function ADD(){//Para añadir a la BD
		if($this->IdTrabajo == '' && $this->AliasEvaluado == '' && $this->LoginEvaluador == '' && $this->IdHistoria == ''){
			return 'Clave vacía';
		}else{
			$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
			$AliasEvaluado = mysqli_real_escape_string($this->mysqli, $this->AliasEvaluado);
			$LoginEvaluador = mysqli_real_escape_string($this->mysqli, $this->LoginEvaluador);
			$IdHistoria = mysqli_real_escape_string($this->mysqli, $this->IdHistoria);

			$sql = "SELECT * FROM EVALUACION WHERE IdTrabajo = '$IdTrabajo' AND AliasEvaluado = '$AliasEvaluado' AND LoginEvaluador = '$LoginEvaluador' AND IdHistoria = '$IdHistoria'";
			
			$resultado = $this->mysqli->query($sql);
			
			if(!$resultado){
				return 'No se ha podido conectar con la BD';
			}else{
				if($resultado->num_rows == 0){
					$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
					$AliasEvaluado = mysqli_real_escape_string($this->mysqli, $this->AliasEvaluado);
					$LoginEvaluador = mysqli_real_escape_string($this->mysqli, $this->LoginEvaluador);
					$IdHistoria = mysqli_real_escape_string($this->mysqli, $this->IdHistoria);
					$CorrectoA = mysqli_real_escape_string($this->mysqli, $this->CorrectoA);
					$ComenIncorrectoA = mysqli_real_escape_string($this->mysqli, $this->ComenIncorrectoA);
					$CorrectoP = mysqli_real_escape_string($this->mysqli, $this->CorrectoP);
					$ComentIncorrectoP = mysqli_real_escape_string($this->mysqli, $this->ComentIncorrectoP);
					$OK = mysqli_real_escape_string($this->mysqli, $this->OK);

					$sql = "INSERT INTO EVALUACION (IdTrabajo, AliasEvaluado, LoginEvaluador, IdHistoria, CorrectoA, ComenIncorrectoA, CorrectoP, ComentIncorrectoP, OK) VALUES ('$IdTrabajo', '$AliasEvaluado', '$LoginEvaluador', '$IdHistoria', '$CorrectoA', '$ComenIncorrectoA', '$CorrectoP', '$ComentIncorrectoP', '$OK')";
				
					if(!$this->mysqli->query($sql)){
						return 'Ha fallado la inserción';
					}else{
						return 'Inserción correcta';
					}
				}else{
					return 'Los datos clave introducidos ya existen en la tabla';
				}
			}
		}
	}
	
	function EDIT(){//Para editar de la BD
		if($this->IdTrabajo == '' && $this->AliasEvaluado == '' && $this->LoginEvaluador == '' && $this->IdHistoria == ''){
			return 'Clave vacía';
		}else{
			$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
			$AliasEvaluado = mysqli_real_escape_string($this->mysqli, $this->AliasEvaluado);
			$LoginEvaluador = mysqli_real_escape_string($this->mysqli, $this->LoginEvaluador);
			$IdHistoria = mysqli_real_escape_string($this->mysqli, $this->IdHistoria);
			
			$sql = "SELECT * FROM EVALUACION WHERE IdTrabajo = '$IdTrabajo' AND AliasEvaluado = '$AliasEvaluado' AND LoginEvaluador = '$LoginEvaluador' AND IdHistoria = '$IdHistoria'";
			
			$resultado = $this->mysqli->query($sql);
			
			if(!$resultado){
				return 'No se ha podido conectar con la BD';
			}else if($resultado->num_rows == 1){
				$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
				$AliasEvaluado = mysqli_real_escape_string($this->mysqli, $this->AliasEvaluado);
				$LoginEvaluador = mysqli_real_escape_string($this->mysqli, $this->LoginEvaluador);
				$IdHistoria = mysqli_real_escape_string($this->mysqli, $this->IdHistoria);
				$CorrectoA = mysqli_real_escape_string($this->mysqli, $this->CorrectoA);
				$ComenIncorrectoA = mysqli_real_escape_string($this->mysqli, $this->ComenIncorrectoA);
				$CorrectoP = mysqli_real_escape_string($this->mysqli, $this->CorrectoP);
				$ComentIncorrectoP = mysqli_real_escape_string($this->mysqli, $this->ComentIncorrectoP);
				$OK = mysqli_real_escape_string($this->mysqli, $this->OK);

				$sql = "UPDATE EVALUACION SET CorrectoA = '$CorrectoA', ComenIncorrectoA = '$ComenIncorrectoA', CorrectoP = '$CorrectoP', ComentIncorrectoP = '$ComentIncorrectoP', OK = '$OK' WHERE IdTrabajo = '$IdTrabajo' AND AliasEvaluado = '$AliasEvaluado' AND LoginEvaluador = '$LoginEvaluador' AND IdHistoria = '$IdHistoria'";
				
				if(!$this->mysqli->query($sql)){
					return 'Ha fallado la actualización';
				}else{
					return 'Modificado correcto';
				}
			}else{
				return 'Los datos clave no existen en la tabla';
			}
		}
	}

	function EDIT_USER(){//Para editar de la BD siendo USER regular
		if($this->IdTrabajo == '' && $this->AliasEvaluado == '' && $this->LoginEvaluador == '' && $this->IdHistoria == ''){
			return 'Clave vacía';
		}else{
			$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
			$AliasEvaluado = mysqli_real_escape_string($this->mysqli, $this->AliasEvaluado);
			$LoginEvaluador = mysqli_real_escape_string($this->mysqli, $this->LoginEvaluador);
			$IdHistoria = mysqli_real_escape_string($this->mysqli, $this->IdHistoria);
			
			$sql = "SELECT * FROM EVALUACION WHERE IdTrabajo = '$IdTrabajo' AND AliasEvaluado = '$AliasEvaluado' AND LoginEvaluador = '$LoginEvaluador' AND IdHistoria = '$IdHistoria'";
			
			$resultado = $this->mysqli->query($sql);
			
			if(!$resultado){
				return 'No se ha podido conectar con la BD';
			}else if($resultado->num_rows == 1){
				$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
				$AliasEvaluado = mysqli_real_escape_string($this->mysqli, $this->AliasEvaluado);
				$LoginEvaluador = mysqli_real_escape_string($this->mysqli, $this->LoginEvaluador);
				$IdHistoria = mysqli_real_escape_string($this->mysqli, $this->IdHistoria);
				$CorrectoA = mysqli_real_escape_string($this->mysqli, $this->CorrectoA);
				$ComenIncorrectoA = mysqli_real_escape_string($this->mysqli, $this->ComenIncorrectoA);
				
				//Cambiar solo los campos que puede tocar el usuario (CorrectoA y ComenIncorrectoA)
				$sql = "UPDATE EVALUACION SET CorrectoA = '$CorrectoA', ComenIncorrectoA = '$ComenIncorrectoA' WHERE IdTrabajo = '$IdTrabajo' AND AliasEvaluado = '$AliasEvaluado' AND LoginEvaluador = '$LoginEvaluador' AND IdHistoria = '$IdHistoria'";
				
				if(!$this->mysqli->query($sql)){
					return 'Ha fallado la actualización';
				}else{
					return 'Modificado correcto';
				}
			}else{
				return 'Los datos clave no existen en la tabla';
			}
		}
	}
	
	function SEARCH(){//Para buscar en la base de datos
		$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
		$AliasEvaluado = mysqli_real_escape_string($this->mysqli, $this->AliasEvaluado);
		$LoginEvaluador = mysqli_real_escape_string($this->mysqli, $this->LoginEvaluador);
		$IdHistoria = mysqli_real_escape_string($this->mysqli, $this->IdHistoria);
		$CorrectoA = mysqli_real_escape_string($this->mysqli, $this->CorrectoA);
		$ComenIncorrectoA = mysqli_real_escape_string($this->mysqli, $this->ComenIncorrectoA);
		$CorrectoP = mysqli_real_escape_string($this->mysqli, $this->CorrectoP);
		$ComentIncorrectoP = mysqli_real_escape_string($this->mysqli, $this->ComentIncorrectoP);
		$OK = mysqli_real_escape_string($this->mysqli, $this->OK);

		$sql = "SELECT * FROM EVALUACION WHERE IdTrabajo LIKE '%$IdTrabajo%' AND AliasEvaluado LIKE '%$AliasEvaluado%' AND LoginEvaluador LIKE '%$LoginEvaluador%' AND IdHistoria LIKE '%$IdHistoria%' AND CorrectoA LIKE '%$CorrectoA%' AND ComenIncorrectoA LIKE '%$ComenIncorrectoA%' AND CorrectoP LIKE '%$CorrectoP%' AND ComentIncorrectoP LIKE '%$ComentIncorrectoP%' AND OK LIKE '%$OK%'";
	
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado || $resultado->num_rows == 0){
			return 'No se ha encontrado ningun dato';
		}else{
			return $resultado;
		}
	}
	
	function DELETE(){//Para eliminar de la BD
		$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
		$AliasEvaluado = mysqli_real_escape_string($this->mysqli, $this->AliasEvaluado);
		$LoginEvaluador = mysqli_real_escape_string($this->mysqli, $this->LoginEvaluador);
		$IdHistoria = mysqli_real_escape_string($this->mysqli, $this->IdHistoria);
		
		$sql = "SELECT * FROM EVALUACION WHERE IdTrabajo = '$IdTrabajo' AND AliasEvaluado = '$AliasEvaluado' AND LoginEvaluador = '$LoginEvaluador' AND IdHistoria = '$IdHistoria'";
		
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No se ha encontrado la tupla';
		}else{
			$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
			$AliasEvaluado = mysqli_real_escape_string($this->mysqli, $this->AliasEvaluado);
			$LoginEvaluador = mysqli_real_escape_string($this->mysqli, $this->LoginEvaluador);
			$IdHistoria = mysqli_real_escape_string($this->mysqli, $this->IdHistoria);
			
			$sql = "DELETE FROM EVALUACION WHERE IdTrabajo = '$IdTrabajo' AND AliasEvaluado = '$AliasEvaluado' AND LoginEvaluador = '$LoginEvaluador' AND IdHistoria = '$IdHistoria'";
			
			if(!$this->mysqli->query($sql)){
				return 'Fallo al eliminar la tupla';
			}else{
				return 'Se ha borrado la tupla';
			}
		}
	}
	
	function SHOWCURRENT(){//Para mostrar de la base de datos
		$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
		$AliasEvaluado = mysqli_real_escape_string($this->mysqli, $this->AliasEvaluado);
		$LoginEvaluador = mysqli_real_escape_string($this->mysqli, $this->LoginEvaluador);
		$IdHistoria = mysqli_real_escape_string($this->mysqli, $this->IdHistoria);
		
		$sql = "SELECT * FROM EVALUACION WHERE IdTrabajo = '$IdTrabajo' AND AliasEvaluado = '$AliasEvaluado' AND LoginEvaluador = '$LoginEvaluador' AND IdHistoria = '$IdHistoria'";
		
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No existe la tupla';
		}else{
			return $resultado;
		}
	}
	
	function SHOWALL(){//Para mostrar la BD
		$sql = "SELECT * FROM EVALUACION";
	
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No se ha encontrado ningun dato';
		}else{
			return $resultado;
		}
	}
	
	function CORRECCION(){
		$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
		$AliasEvaluado = mysqli_real_escape_string($this->mysqli, $this->AliasEvaluado);
		
		$sql = "SELECT Alias FROM ENTREGA WHERE login = '$AliasEvaluado' AND IdTrabajo = '$IdTrabajo'";
		
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No has hecho ninguna entrega';
		}else{
			$alias = mysqli_fetch_assoc($resultado)["Alias"];
			$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
			
			$sql = "SELECT * FROM EVALUACION WHERE IdTrabajo = '$IdTrabajo' AND AliasEvaluado = '$alias'";
			
			$evaluaciones = $this->mysqli->query($sql);
			
			if(!$evaluaciones){
			return 'No se ha podido conectar con la BD';
			}else if($evaluaciones->num_rows == 0){
				return 'No hay evaluaciones';
			}else{
				return $evaluaciones;
			}
		}
	}
	
	function CORRECCIONQA(){
		$IdTrabajo = 'NombreTrabajoParaLasQas';
		if($this->IdTrabajo == 'QA1'){
			$IdTrabajo = 'ET1';
		}else if($this->IdTrabajo == 'QA2'){
			$IdTrabajo = 'ET2';
		}else if($this->IdTrabajo == 'QA3'){
			$IdTrabajo = 'ET3';
		}
		
		$LoginEvaluador = mysqli_real_escape_string($this->mysqli, $this->LoginEvaluador);

		$sql = "SELECT * FROM EVALUACION WHERE IdTrabajo = '$IdTrabajo' AND LoginEvaluador = '$LoginEvaluador'";
			
		$evaluaciones = $this->mysqli->query($sql);
		
		if(!$evaluaciones){
			return 'No se ha podido conectar con la BD';
		}else if($evaluaciones->num_rows == 0){
			return 'No hay evaluaciones';
		}else{
			return $evaluaciones;
		}		
	}
	
	function ENPLAZO(){
		$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
		$sql = "SELECT FechaIniTrabajo, FechaFinTrabajo FROM TRABAJO WHERE IdTrabajo = '$IdTrabajo'";
		
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No existe la Entrega';
		}else{
			$hoy = new DateTime("now");
			$fechas = mysqli_fetch_assoc($resultado);
			
			if(new DateTime($fechas["FechaIniTrabajo"]) < $hoy && $hoy < new DateTime($fechas["FechaFinTrabajo"]) ){
				return true;
			}else{
				return false;
			}			
		}		
	}

}
?>