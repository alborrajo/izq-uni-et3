<?php
/* Clase de modelo de USUARIO, el cual accederá exclusivamente a la base de datos
	hay funciones get para poder insertarlas en formularios y set por hacer algo de simetría
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	que quede claro, a no ser que no sepas español
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	5/12/17
*/
class Usuario{
	
	var $login;
	var $password;
	var $DNI;
	var $Nombre;
	var $Apellidos;
	var $Correo;
	var $Direccion;
	var $Telefono;
	var $mysqli;
	//Atributos
	
	function __construct($login, $password, $DNI, $Nombre, $Apellidos, $Correo, $Direccion, $Telefono){
		//Asignaciones
		$this->_setLogin($login);
		$this->_setPassword($password);
		$this->_setDNI($DNI);
		$this->_setNombre($Nombre);
		$this->_setApellidos($Apellidos);
		$this->_setCorreo($Correo);
		$this->_setDireccion($Direccion);
		$this->_setTelefono($Telefono);
		
		include_once '../Functions/AccederBD.php';
		$this->mysqli = ConectarBD();
	}
	
		function _setLogin($login){
			$this->login = $login;
		}
		
		function _setPassword($password){
			$this->password = $password;
		}
		
		function _setDNI($DNI){
			$this->DNI = $DNI;
		}
		
		function _setNombre($Nombre){
			$this->Nombre = $Nombre;
		}
		
		function _setApellidos($Apellidos){
			$this->Apellidos = $Apellidos;
		}
		
		function _setDireccion($Direccion){
			$this->Direccion = $Direccion;
		}
		
		function _setCorreo($Correo){
			$this->Correo = $Correo;
		}
		
		function _setTelefono($Telefono){
			$this->Telefono = $Telefono;
		}
		
		
		
		function _getLogin(){
			return $this->login;
		}
		
		function _getPassword(){
			return $this->password;
		}
		
		function _getDNI(){
			return $this->DNI;
		}
		
		function _getNombre(){
			return $this->Nombre;
		}
		
		function _getApellidos(){
			return $this->Apellidos;
		}
		
		function _getCorreo(){
			return $this->Correo;
		}
		
		function _getDireccion(){
			return $this->Direccion;
		}
		
		function _getTelefono(){
			return $this->Telefono;
		}
		
		
		function _getDatosGuardados(){//Para recuperar de la base de datos
			if(($this->login == '')){
				return 'Login vacío, introduzca un login';
			}else{
				$login = mysqli_real_escape_string($this->mysqli, $this->login);
				$sql = "SELECT * FROM USUARIO WHERE login = '$login'";
				
				$resultado = $this->mysqli->query($sql);
				
				if(!$resultado){
					return 'No se ha podido conectar con la BD';
				}else if($resultado->num_rows == 0){
					return 'No existe el login';
				}else{
					$fila = $resultado->fetch_row();
					
					$this->_setPassword($fila[1]);
					$this->_setDNI($fila[2]);
					$this->_setNombre($fila[3]);
					$this->_setApellidos($fila[4]);
					$this->_setCorreo($fila[5]);
					$this->_setDireccion($fila[6]);
					$this->_setTelefono($fila[7]);
				}
			}
		}
		
	
	function ADD(){//Para añadir a la BD
		if(($this->login == '')){
			return 'Login vacío, introduzca un login';
		}else{
			$login = mysqli_real_escape_string($this->mysqli, $this->login);
			$sql = "SELECT * FROM USUARIO WHERE login = '$login'";
			
			$resultado = $this->mysqli->query($sql);
			
			if(!$resultado){
				return 'No se ha podido conectar con la BD';
			}else{
				if($resultado->num_rows == 0){
					$password = mysqli_real_escape_string($this->mysqli, $this->password);
					$DNI = mysqli_real_escape_string($this->mysqli, $this->DNI);
					$Nombre = mysqli_real_escape_string($this->mysqli, $this->Nombre);
					$Apellidos = mysqli_real_escape_string($this->mysqli, $this->Apellidos);
					$Correo = mysqli_real_escape_string($this->mysqli, $this->Correo);
					$Direccion = mysqli_real_escape_string($this->mysqli, $this->Direccion);
					$Telefono = mysqli_real_escape_string($this->mysqli, $this->Telefono);
					
					$sql = "INSERT INTO USUARIO (login, password, DNI, Nombre, Apellidos, Correo, Direccion, Telefono) VALUES ('$login', '$password', '$DNI', '$Nombre', '$Apellidos', '$Correo', '$Direccion', '$Telefono');";

					$grupoPorDefecto = "INSERT INTO USU_GRUPO (login, IdGrupo) VALUES ('$login', 'USER');";
				
					if(!$this->mysqli->query($sql)){
						return 'Ha fallado el insertar al usuario';
					}else{
						$this->mysqli->query($grupoPorDefecto); //Si falla, es porque ya existe el usuario en el grupo, así que el problema se resuelve solo.
						return 'Inserción correcta';
					}
				}else{
					return 'El login introducido ya existe';
				}
			}
		}
	}
	
	function EDIT(){//Para editar de la BD
		if(($this->login == '')){
			return 'Login vacío, introduzca un login';
		}else{
			$login = mysqli_real_escape_string($this->mysqli, $this->login);
			$sql = "SELECT * FROM USUARIO WHERE login = '$login'";
			
			$resultado = $this->mysqli->query($sql);
			
			if(!$resultado){
				return 'No se ha podido conectar con la BD';
			}else if($resultado->num_rows == 1){				
				$password = mysqli_real_escape_string($this->mysqli, $this->password);
				$DNI = mysqli_real_escape_string($this->mysqli, $this->DNI);
				$Nombre = mysqli_real_escape_string($this->mysqli, $this->Nombre);
				$Apellidos = mysqli_real_escape_string($this->mysqli, $this->Apellidos);
				$Correo = mysqli_real_escape_string($this->mysqli, $this->Correo);
				$Direccion = mysqli_real_escape_string($this->mysqli, $this->Direccion);
				$Telefono = mysqli_real_escape_string($this->mysqli, $this->Telefono);
				
				$sql = "UPDATE USUARIO SET password = '$password', DNI = '$DNI', Nombre = '$Nombre', Apellidos = '$Apellidos', Correo = '$Correo', Direccion = '$Direccion', Telefono = '$Telefono' WHERE login = '$login'";
				
				if(!$this->mysqli->query($sql)){
					return 'Ha fallado la actualización del usuario';
				}else{
					return 'Modificado correcto';
				}
			}else{
				return 'Login no existe en la base de datos';
			}
		}
	}
	
	function SEARCH(){//Para buscar en la base de datos, no se busca la password	
		$login = mysqli_real_escape_string($this->mysqli, $this->login);				
		$password = mysqli_real_escape_string($this->mysqli, $this->password);
		$DNI = mysqli_real_escape_string($this->mysqli, $this->DNI);
		$Nombre = mysqli_real_escape_string($this->mysqli, $this->Nombre);
		$Apellidos = mysqli_real_escape_string($this->mysqli, $this->Apellidos);
		$Correo = mysqli_real_escape_string($this->mysqli, $this->Correo);
		$Direccion = mysqli_real_escape_string($this->mysqli, $this->Direccion);
		$Telefono = mysqli_real_escape_string($this->mysqli, $this->Telefono);
		
		$sql = "SELECT * FROM USUARIO WHERE ((login LIKE '%$login%') AND (DNI LIKE '%$DNI%') AND (Nombre LIKE '%$Nombre%') AND (Apellidos LIKE '%$Apellidos%') AND (Correo LIKE '%$Correo%') AND (Direccion LIKE '%$Direccion%') AND (Telefono LIKE '%$Telefono%'))";
	
		$resultado1 = $this->mysqli->query($sql);
		
		if(!$resultado1 || $resultado1->num_rows == 0){
			return 'No se ha encontrado ningun dato';
		}
		
		$sql = "SELECT * FROM USU_GRUPO";
	
		$resultado2 = $this->mysqli->query($sql);
		
		if(!$resultado2){
			return 'No se ha podido conectar con la BD';
		}
		
		$sql = "SELECT IdGrupo, NombreGrupo FROM GRUPO";
		
		$grupos = $this->mysqli->query($sql);
		
		if(!$grupos){
			return 'No se ha podido conectar con la BD';
		}
		
		return array($resultado1, $resultado2, $grupos);
	}
	
	function DELETE(){//Para eliminar de la BD//Hay que eliminarlo tmb de la tabla USU_GRUPO
		$login = mysqli_real_escape_string($this->mysqli, $this->login);			
		
		$sql = "SELECT * FROM USUARIO WHERE login = '$login'";
		
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No se ha encontrado al usuario';
		}else{
			$sql = "DELETE FROM USUARIO WHERE login = '$login'";
			
			if(!$this->mysqli->query($sql)){
				return 'Fallo al eliminar al usuario';
			}			
			//Si llega hasta aquí, es que ha borrado al usuario, por lo tanto podemos borrarlo de USU_GRUPO
			
			$sql = "DELETE FROM USU_GRUPO WHERE login = '$login'";
			
			if(!$this->mysqli->query($sql)){
				return 'Fallo al eliminar tuplas';
			}
			
			$sql = "DELETE FROM ENTREGA WHERE login = '$login'";
			
			if(!$this->mysqli->query($sql)){
				return 'Fallo al eliminar tuplas';
			}
			
			$sql = "DELETE FROM NOTA_TRABAJO WHERE login = '$login'";
			
			if(!$this->mysqli->query($sql)){
				return 'Fallo al eliminar tuplas';
			}else{
				return 'Usuario eliminado correctamente';
			}
		}
	}
	
	function SHOWCURRENT(){//Para mostrar de la base de datos
		$login = mysqli_real_escape_string($this->mysqli, $this->login);	
		
		$sql = "SELECT * FROM USUARIO WHERE login = '$login'";
		
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No existe el login';
		}else{
			return $resultado;
		}
	}
	
	function SHOWALL(){//Para mostrar la BD
		$sql = "SELECT * FROM USUARIO";
	
		$resultado1 = $this->mysqli->query($sql);
		
		if(!$resultado1){
			return 'No se ha podido conectar con la BD';
		}
		
		$sql = "SELECT * FROM USU_GRUPO";
	
		$resultado2 = $this->mysqli->query($sql);
		
		if(!$resultado2){
			return 'No se ha podido conectar con la BD';
		}
		
		$sql = "SELECT IdGrupo, NombreGrupo FROM GRUPO";
		
		$grupos = $this->mysqli->query($sql);
		
		if(!$grupos){
			return 'No se ha podido conectar con la BD';
		}
		
		return array($resultado1, $resultado2, $grupos);
	}
	
	function LOGIN(){//Para buscar de la BD y comparar con la pass
		$login = mysqli_real_escape_string($this->mysqli, $this->login);	
		
		$sql = "SELECT password FROM USUARIO WHERE login = '$login'";
		
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'Login incorrecto';
		}else{
			if($this->password == $resultado->fetch_assoc()["password"]){
				return 'true';
			}else{
				return 'Contraseña incorrecta';
			}
		}
		
	}

	function REGISTRAR(){//Para comprobar que se puede registrar de la BD, no se porque no unir esta con REGISTRAR(), había una razón relacionada con la comprobación
		if(($this->login == '')){
			return 'Login vacío, introduzca un login';
		}else{
			$login = mysqli_real_escape_string($this->mysqli, $this->login);	
		
			$sql = "SELECT * FROM USUARIO WHERE login = '$login'";
			
			$resultado = $this->mysqli->query($sql);
			
			if($resultado->num_rows != 0){
				return 'Ya existe un usuario con ese login';
			}else{	
				$password = mysqli_real_escape_string($this->mysqli, $this->password);
				$DNI = mysqli_real_escape_string($this->mysqli, $this->DNI);
				$Nombre = mysqli_real_escape_string($this->mysqli, $this->Nombre);
				$Apellidos = mysqli_real_escape_string($this->mysqli, $this->Apellidos);
				$Correo = mysqli_real_escape_string($this->mysqli, $this->Correo);
				$Direccion = mysqli_real_escape_string($this->mysqli, $this->Direccion);
				$Telefono = mysqli_real_escape_string($this->mysqli, $this->Telefono);
				
				$sql = "INSERT INTO USUARIO (login, password, DNI, Nombre, Apellidos, Correo, Direccion, Telefono) VALUES ('$login', '$password', '$DNI', '$Nombre', '$Apellidos', '$Correo', '$Direccion', '$Telefono')";
					
				$respuesta = $this->mysqli->query($sql);
				
				if(!$respuesta){
					return 'No se ha podido conectar con la BD';
				}else{
					return 'Usuario añadido correctamente';
				}
			}
		}
	}
	
	function ASIGNARGRUPO($IdGrupo){
		$sql = "SELECT * FROM GRUPO WHERE IdGrupo = '$IdGrupo'"; //Comprobar que existe el grupo
		
		$resultado = $this->mysqli->query($sql);
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No existe el grupo indicado';
		}else{		
			$login = mysqli_real_escape_string($this->mysqli, $this->login);	
			
			$sql = "SELECT * FROM USU_GRUPO WHERE login = '$login' AND IdGrupo = '$IdGrupo'";
			
			$resultado = $this->mysqli->query($sql);
			
			if(!$resultado){
				return 'No se ha podido conectar con la BD';
			}else if($resultado->num_rows != 0){
				return 'El usuario ya está asignado a ese grupo';
			}else{
				$sql = "INSERT INTO USU_GRUPO (login, IdGrupo) VALUES ('$login', '$IdGrupo')";
				
				if(!$this->mysqli->query($sql)){
					return 'Fallo al insertar el grupo al usuario';
				}else{
					return 'Se ha añadido al usuario al grupo';
				}
			}
		}
	}
	
	function DESASIGNARGRUPO($IdGrupo){
		$login = mysqli_real_escape_string($this->mysqli, $this->login);	
			
		$sql = "SELECT * FROM USU_GRUPO WHERE login = '$this->login' AND IdGrupo = '$IdGrupo'";
		
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conecctar con la BD';
		}else if($resultado->num_rows == 0){
			return 'El usuario no pertenece al grupo';
		}else{
			$sql = "DELETE FROM USU_GRUPO WHERE login = '$login' AND IdGrupo = '$IdGrupo'";
			
			if(!$this->mysqli->query($sql)){
				return 'Fallo al quitar al usuario del grupo';
			}else{
				return 'Se ha eliminado al usuario del grupo';
			}
		}
	}
	
	function PERMISOPARA($IdFuncionalidad, $IdAccion){
		if(($this->login == '')){
			return 'Login vacío, introduzca un login';
		}else{
			$login = mysqli_real_escape_string($this->mysqli, $this->login);	
			
			$sql = "SELECT IdGrupo FROM USU_GRUPO WHERE login = '$login'";
			
			$resultado = $this->mysqli->query($sql);
			
			if(!$resultado){
				return 'No se ha podido conecctar con la BD';
			}else if($resultado->num_rows == 0){
				return 'El usuario no pertenece a ningun grupo';
			}else{
				$retorno = false;
				$i = 0;
				$grupos = mysqli_fetch_all($resultado);
				while($i < sizeof($grupos) && $retorno == false){
					$grupo = $grupos[$i][0];
					$sql = "SELECT IdFuncionalidad, IdAccion FROM PERMISO WHERE IdGrupo = '$grupo'";
					
					$permiso = $this->mysqli->query($sql);
					
					if(!$permiso){
						return 'No se ha podido conectar con la BD';
					}else{
						$j = 0;
						$permisos = mysqli_fetch_all($permiso);
						while($j < sizeof($permisos) && $retorno == false){
							$acceso = $permisos[$j];
							if($IdFuncionalidad == $acceso[0] && $IdAccion == $acceso[1]){
								$retorno = true;
							}
							
							if($acceso[0] == 'TODO' && $acceso[1] == 'TODO'){
								$retorno = true;
							}
							$j++;
						}
					}
					$i++;
				}
				return $retorno;
			}
		}
	}
}
?>