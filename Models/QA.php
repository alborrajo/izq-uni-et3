<?php
/* Clase de modelo de QA, el cual accederá exclusivamente a la base de datos
	hay funciones get para poder insertarlas en formularios y set por hacer algo de simetría
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error

	que quede claro, a no ser que no sepas español
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	5/12/17
*/

include_once "../Models/EVALUACION.php";

class QA{
	
	var $IdTrabajo;
	var $LoginEvaluador;
	var $LoginEvaluado;
	var $AliasEvaluado;
	//Atributos
	
	function __construct($IdTrabajo, $LoginEvaluador, $LoginEvaluado, $AliasEvaluado){
		//Asignaciones
		$this->_setIdTrabajo($IdTrabajo);
		$this->_setLoginEvaluador($LoginEvaluador);
		$this->_setLoginEvaluado($LoginEvaluado);
		$this->_setAliasEvaluado($AliasEvaluado);
		
		include_once '../Functions/AccederBD.php';
		$this->mysqli = ConectarBD();
	}

	function _setIdTrabajo($IdTrabajo){
		$this->IdTrabajo = $IdTrabajo;
	}
	
	function _setLoginEvaluador($LoginEvaluador){
		$this->LoginEvaluador = $LoginEvaluador;
	}
	
	function _setLoginEvaluado($LoginEvaluado){
		$this->LoginEvaluado = $LoginEvaluado;
	}
	
	function _setAliasEvaluado($AliasEvaluado){
		$this->AliasEvaluado = $AliasEvaluado;
	}
	
	
	
	
	
	function _getIdTrabajo(){
		return $this->IdTrabajo;
	}
	
	function _getLoginEvaluador(){
		return $this->LoginEvaluador;
	}
	
	function _getLoginEvaluado(){
		return $this->LoginEvaluado;
	}
	
	function _getAliasEvaluado(){
		return $this->AliasEvaluado;
	}
	
	
	function _getDatosGuardados(){//Para recuperar de la base de datos
		if(($this->IdTrabajo == '' OR $this->LoginEvaluador == '' OR $this->AliasEvaluado == '')){
			return 'Datos de la clave primaria vacíos';
		}else{
			$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
			$LoginEvaluador = mysqli_real_escape_string($this->mysqli, $this->LoginEvaluador);
			$AliasEvaluado = mysqli_real_escape_string($this->mysqli, $this->AliasEvaluado);
		
			$sql = "SELECT * FROM ASIGNAC_QA WHERE ((IdTrabajo = '$IdTrabajo') AND (LoginEvaluador = '$LoginEvaluador') AND (AliasEvaluado = '$AliasEvaluado'))";
			
			$resultado = $this->mysqli->query($sql);
			
			if(!$resultado){
				return 'No se ha podido conectar con la BD';
			}else if($resultado->num_rows == 0){
				return 'No existe la clave primaria';
			}else{
				$fila = $resultado->fetch_row();
				
				$this->_setLoginEvaluado($fila[2]);
				
			}
		}
	}
		
	
	function ADD(){//Para añadir a la BD
		if(($this->IdTrabajo == '')){
			return 'IdTrabajo vacío, introduzca un IdTrabajo';
		}else if(($this->LoginEvaluador == '')){
			return 'LoginEvaluador vacío, introduzca un valor';
		}else if(($this->AliasEvaluado == '')){
			return 'AliasEvaluado vacío, introduzca un valor';
		}else{
			$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
			$LoginEvaluador = mysqli_real_escape_string($this->mysqli, $this->LoginEvaluador);
			$AliasEvaluado = mysqli_real_escape_string($this->mysqli, $this->AliasEvaluado);
			
			$sql = "SELECT * FROM ASIGNAC_QA WHERE ((IdTrabajo = '$IdTrabajo') AND (LoginEvaluador = '$LoginEvaluador') AND (AliasEvaluado = '$AliasEvaluado'))";
			
			$resultado = $this->mysqli->query($sql);
			
			if(!$resultado){
				return 'No se ha podido conectar con la BD';
			}else{
				if($resultado->num_rows == 0){
					$LoginEvaluado = mysqli_real_escape_string($this->mysqli, $this->LoginEvaluado);
			
					$sql = "INSERT INTO ASIGNAC_QA (IdTrabajo, LoginEvaluador, LoginEvaluado, AliasEvaluado) VALUES ('$IdTrabajo', '$LoginEvaluador',  '$LoginEvaluado', '$AliasEvaluado')";
				
					if(!$this->mysqli->query($sql)){
						return 'Ha fallado el insertar el QA';
					}else{
						//Si se inserta, insertar también para esta QA las tuplas EVALUACION de historia correspondientes (Esto no seria necesario si se usasen triggers)

						//Obtener todas las historias correspondiente al trabajo asignado
						$sqlhistorias = "SELECT * FROM HISTORIA WHERE IdTrabajo = '$IdTrabajo'";
						$historias = $this->mysqli->query($sqlhistorias);

						//Borrar tuplas de EVALUACION anteriores, correspondientes a la asignación
						$sqlDeleteEval = "DELETE FROM EVALUACION WHERE IdTrabajo = '$IdTrabajo' AND AliasEvaluado = '$AliasEvaluado' AND LoginEvaluador = '$LoginEvaluador'";
						$this->mysqli->query($sqlDeleteEval);

						//Si no se han podido obtener las historias
						if(!$historias || $historias->num_rows == 0) {
							return 'Error obteniendo Historias, los usuarios no podrán evaluar.';
						}
						else {
							while($historia = $historias->fetch_assoc()){ //Mientras queden tuplas		
								//$historia -> Tupla actual;		$historias -> Todas
								//Crear tupla EVALUACION con los datos de la asignación de QA
								$evaluacion = new Evaluacion($IdTrabajo,$AliasEvaluado,$LoginEvaluador,$historia["IdHistoria"],1,'',1,'',2);
								$evaluacion->ADD();
							}
							return 'Inserción correcta';
						}

					}
				}else{
					return 'El QA introducido ya existe';
				}
			}
		}
	}
	
	function EDIT(){//Para editar de la BD
		if(($this->IdTrabajo == '')){
			return 'IdTrabajo vacío, introduzca un IdTrabajo';
		}else if(($this->LoginEvaluador == '')){
			return 'LoginEvaluador vacío, introduzca un valor';
		}else if(($this->AliasEvaluado == '')){
			return 'AliasEvaluado vacío, introduzca un valor';
		}else{
			$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
			$LoginEvaluador = mysqli_real_escape_string($this->mysqli, $this->LoginEvaluador);
			$AliasEvaluado = mysqli_real_escape_string($this->mysqli, $this->AliasEvaluado);
			
			$sql = "SELECT * FROM ASIGNAC_QA WHERE ((IdTrabajo = '$IdTrabajo') AND (LoginEvaluador = '$LoginEvaluador') AND (AliasEvaluado = '$AliasEvaluado'))";
			
			$resultado = $this->mysqli->query($sql);
			
			if(!$resultado){
				return 'No se ha podido conectar con la BD';
			}else if($resultado->num_rows == 1){
				$LoginEvaluado = mysqli_real_escape_string($this->mysqli, $this->LoginEvaluado);
				
				$sql = "UPDATE ASIGNAC_QA SET LoginEvaluador = '$LoginEvaluador', LoginEvaluado = '$LoginEvaluado', AliasEvaluado = '$AliasEvaluado' WHERE ((IdTrabajo = '$IdTrabajo') AND (LoginEvaluador = '$LoginEvaluador') AND (AliasEvaluado = '$AliasEvaluado'))";
				
				if(!$this->mysqli->query($sql)){
					return 'Ha fallado la actualización del QA';
				}else{
					return 'Modificado correcto';
				}
			}else{
				return 'Clave primaria no existe en la base de datos';
			}
		}
	}
	
	function SEARCH(){//Para buscar en la base de datos, no se busca la contraseña	
		$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
		$LoginEvaluador = mysqli_real_escape_string($this->mysqli, $this->LoginEvaluador);
		$AliasEvaluado = mysqli_real_escape_string($this->mysqli, $this->AliasEvaluado);
		$LoginEvaluado = mysqli_real_escape_string($this->mysqli, $this->LoginEvaluado);
				
		$sql = "SELECT * FROM ASIGNAC_QA WHERE ASIGNAC_QA.IdTrabajo LIKE '%$IdTrabajo%' AND ASIGNAC_QA.LoginEvaluador LIKE '%$LoginEvaluador%' AND ASIGNAC_QA.LoginEvaluado LIKE '%$LoginEvaluado%' AND ASIGNAC_QA.AliasEvaluado LIKE '%$AliasEvaluado%'";
	
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No se ha encontrado ningun dato';
		}else{
			return $resultado;
		}
	}
	
	function DELETE(){//Para eliminar de la BD
		$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
		$LoginEvaluador = mysqli_real_escape_string($this->mysqli, $this->LoginEvaluador);
		$AliasEvaluado = mysqli_real_escape_string($this->mysqli, $this->AliasEvaluado);
			
		$sql = "SELECT * FROM ASIGNAC_QA WHERE ((IdTrabajo = '$IdTrabajo') AND (LoginEvaluador = '$LoginEvaluador') AND (AliasEvaluado = '$AliasEvaluado'))";
		
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No se ha encontrado al QA';
		}else{
			$sql = "DELETE FROM ASIGNAC_QA WHERE ((IdTrabajo = '$IdTrabajo') AND (LoginEvaluador = '$LoginEvaluador') AND (AliasEvaluado = '$AliasEvaluado'))";
			
			if(!$this->mysqli->query($sql)){
				return 'Fallo al eliminar la tupla';
			}else{
				return 'Se ha borrado al QA';
			}
		}
	}
	
	function SHOWCURRENT(){//Para mostrar de la base de datos
		$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
		$LoginEvaluador = mysqli_real_escape_string($this->mysqli, $this->LoginEvaluador);
		$AliasEvaluado = mysqli_real_escape_string($this->mysqli, $this->AliasEvaluado);
		
		$sql = "SELECT * FROM ASIGNAC_QA WHERE ((IdTrabajo = '$IdTrabajo') AND (LoginEvaluador = '$LoginEvaluador') AND (AliasEvaluado = '$AliasEvaluado'))";
		
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No existe el QA';
		}else{
			return $resultado;
		}
	}

	function SHOWALL(){//Para mostrar la BD
		$sql = "SELECT * FROM ASIGNAC_QA";
	
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No se ha encontrado ningun dato';
		}else{
			return $resultado;
		}
	}

	function SHOWALL_USER(){//Muestra todas las QA asignadas al LoginEvaluador junto con un link para descargar la entrega del LoginEvaluado
		//SOLO MUESTRA LAS QA QUE TIENEN ENTREGA ASOCIADA
		//Esto no da problemas dado que al asignar QA automáticamente, solo se asignan las QA con entrega asociada
		$LoginEvaluador = mysqli_real_escape_string($this->mysqli, $this->LoginEvaluador);
		
		$sql = "SELECT ENTREGA.IdTrabajo, ASIGNAC_QA.LoginEvaluador, ASIGNAC_QA.LoginEvaluado, ASIGNAC_QA.AliasEvaluado, ENTREGA.Ruta FROM ASIGNAC_QA, ENTREGA WHERE ASIGNAC_QA.IdTrabajo = ENTREGA.IdTrabajo AND ASIGNAC_QA.LoginEvaluado = ENTREGA.login AND ASIGNAC_QA.AliasEvaluado = ENTREGA.Alias AND  ASIGNAC_QA.LoginEvaluador = '$LoginEvaluador'";
	
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No se ha encontrado ningun dato';
		}else{
			return $resultado;
		}
	}
}
?>