<?php
/* Clase de modelo de NOTATRABAJO, el cual accederá exclusivamente a la base de datos
	hay funciones get para poder insertarlas en formularios y set por hacer algo de simetría
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	que quede claro, a no ser que no sepas español
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	5/12/17
*/
class NotaTrabajo{
	
	var $login;
	var $IdTrabajo;
	var $NotaTrabajo;
	var $mysqli;
	//Atributos
	
	function __construct($login, $IdTrabajo, $NotaTrabajo){
		//Asignaciones
		$this->_setLogin($login);
		$this->_setIdTrabajo($IdTrabajo);
		$this->_setNotaTrabajo($NotaTrabajo);
		
		include_once '../Functions/AccederBD.php';
		$this->mysqli = ConectarBD();
	}
	
		function _setLogin($login){
			$this->login = $login;
		}
		
		function _setIdTrabajo($IdTrabajo){
			$this->IdTrabajo = $IdTrabajo;
		}
		
		function _setNotaTrabajo($NotaTrabajo){
			$this->NotaTrabajo = $NotaTrabajo;
		}
		
		
		function _getLogin(){
			return $this->login;
		}
		
		function _getIdTrabajo(){
			return $this->IdTrabajo;
		}
		
		function _getNotaTrabajo(){
			return $this->NotaTrabajo;
		}
		
		
		function _getDatosGuardados(){//Para recuperar de la base de datos
			if(($this->login == '' && $this->IdTrabajo == '' )){
				return 'Login e IdTrabajo vacíos';
			}else{
				$login = mysqli_real_escape_string($this->mysqli, $this->login);
				$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
			
				$sql = "SELECT * FROM NOTA_TRABAJO WHERE login = '$login' AND IdTrabajo = '$IdTrabajo'";
				
				$resultado = $this->mysqli->query($sql);
				
				if(!$resultado){
					return 'No se ha podido conectar con la BD';
				}else if($resultado->num_rows == 0){
					return 'No existe la clave';
				}else{
					$fila = $resultado->fetch_row();
					
					$this->_setIdTrabajo($fila[1]);
					$this->_setNotaTrabajo($fila[2]);
				}
			}
		}
		
	
	function ADD(){//Para añadir a la BD
		if(($this->login == '' && $this->IdTrabajo == '' )){
			return 'Login e IdTrabajo vacíos';
		}else{
			$login = mysqli_real_escape_string($this->mysqli, $this->login);
			$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
			
			$sql = "SELECT * FROM USUARIO WHERE login = '$login'";
			
			$resultado = $this->mysqli->query($sql);
			
			if(!$resultado){
				return 'No se ha podido conectar con la BD';				
			}else if($resultado->num_rows != 1){
				return 'No se ha encontrado al usuario';
			}else{
				$sql = "SELECT * FROM TRABAJO WHERE IdTrabajo = '$IdTrabajo'";
				
				$resultado = $this->mysqli->query($sql);
				
				if(!$resultado){
					return 'No se ha podido conectar con la BD';				
				}else if($resultado->num_rows != 1){
					return 'No se ha encontrado el trabajo';
				}else{				
			
					$sql = "SELECT * FROM NOTA_TRABAJO WHERE login = '$login' AND IdTrabajo = '$IdTrabajo'";
					
					$resultado = $this->mysqli->query($sql);
					
					if(!$resultado){
						return 'No se ha podido conectar con la BD';
					}else{
						if($resultado->num_rows == 0){
							$NotaTrabajo = mysqli_real_escape_string($this->mysqli, $this->NotaTrabajo);
							
							$sql = "INSERT INTO NOTA_TRABAJO (login, IdTrabajo, NotaTrabajo) VALUES ('$login', '$IdTrabajo', '$NotaTrabajo')";
						
							if(!$this->mysqli->query($sql)){
								return 'Ha fallado la inserción';
							}else{
								return 'Inserción correcta';
							}
						}else{
							return 'Los datos clave introducidos ya existen en la tabla';
						}
					}
				
				}
			}
			
			
		}
	}
	
	function EDIT(){//Para editar de la BD
		if(($this->login == '' && $this->IdTrabajo == '' )){
			return 'Login e IdTrabajo vacíos';
		}else{
			$login = mysqli_real_escape_string($this->mysqli, $this->login);
			$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
			
			$sql = "SELECT * FROM NOTA_TRABAJO WHERE login = '$login' AND IdTrabajo = '$IdTrabajo'";
			
			$resultado = $this->mysqli->query($sql);
			
			if(!$resultado){
				return 'No se ha podido conectar con la BD';
			}else if($resultado->num_rows == 1){
				$NotaTrabajo = mysqli_real_escape_string($this->mysqli, $this->NotaTrabajo);
							
				$sql = "UPDATE NOTA_TRABAJO SET NotaTrabajo = '$NotaTrabajo' WHERE login = '$login' AND IdTrabajo = '$IdTrabajo'";
				
				if(!$this->mysqli->query($sql)){
					return 'Ha fallado la actualización';
				}else{
					return 'Modificado correcto';
				}
			}else{
				return 'Los datos clave no existen en la tabla';
			}
		}
	}
	
	function SEARCH(){//Para buscar en la base de datos
		$login = mysqli_real_escape_string($this->mysqli, $this->login);
		$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
		$NotaTrabajo = mysqli_real_escape_string($this->mysqli, $this->NotaTrabajo);
				
		$sql = "SELECT login, N.IdTrabajo, NotaTrabajo, PorcentajeNota FROM NOTA_TRABAJO N, TRABAJO T WHERE N.IdTrabajo = T.IdTrabajo AND (N.login LIKE '%$login%') AND (N.NotaTrabajo LIKE '%$NotaTrabajo%') AND (N.IdTrabajo LIKE '%$IdTrabajo%') ORDER BY login";
	
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado || $resultado->num_rows == 0){
			return 'No se ha encontrado ningun dato';
		}else{
			return $resultado;
		}
	}
	
	function DELETE(){//Para eliminar de la BD
		$login = mysqli_real_escape_string($this->mysqli, $this->login);
		$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
		
		$sql = "SELECT * FROM NOTA_TRABAJO WHERE login = '$login' AND IdTrabajo = '$IdTrabajo'";
		
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No se ha encontrado la tupla';
		}else{
			$sql = "DELETE FROM NOTA_TRABAJO WHERE login = '$login' AND IdTrabajo = '$IdTrabajo'";
			
			if(!$this->mysqli->query($sql)){
				return 'Fallo al eliminar la tupla';
			}else{
				return 'Se ha borrado la tupla';
			}
		}
	}
	
	function SHOWCURRENT(){//Para mostrar de la base de datos
		$login = mysqli_real_escape_string($this->mysqli, $this->login);
		$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
		
		$sql = "SELECT * FROM NOTA_TRABAJO WHERE login = '$login' AND IdTrabajo = '$IdTrabajo'";
		
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No existe la tupla';
		}else{
			return $resultado;
		}
	}
	
	//Showall admin
	function SHOWALL(){//Para mostrar la BD
		$sql = "SELECT login, N.IdTrabajo, NotaTrabajo, PorcentajeNota FROM NOTA_TRABAJO N, TRABAJO T WHERE N.IdTrabajo = T.IdTrabajo ORDER BY login";
	
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No se ha encontrado ningun dato';
		}else{
			return $resultado;
		}
	}

	//Showall usuario
	function SHOWALL_U(){//Para mostrar la BD
		$usuario=$_SESSION['login'];
		$sql = "SELECT login, N.IdTrabajo, NotaTrabajo, PorcentajeNota FROM NOTA_TRABAJO N, TRABAJO T WHERE login = '$usuario' AND N.IdTrabajo = T.IdTrabajo";
	
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No se ha encontrado ningun dato';
		}else{
			return $resultado;
		}
	}

	


	function GENOTAS(){
		//Borrar datos
		$this->mysqli->query("DELETE FROM NOTA_TRABAJO");
		//Boolean para controlar errores
		$boolean=false;
		//Seleccionamos todos los usuarios con entregas
		$usuarios= $this->mysqli->query("SELECT DISTINCT login FROM ENTREGA");
		while ($fila = $usuarios->fetch_row()) {
			$respuesta=$this->GENOTAS_AUX($fila[0]);
			if($respuesta == 'Error'){
				$boolean=true;		

			}
		}

		if($boolean == true){
			return 'Error, no hay evaluaciones para los trabajos';
		}
		else{
			return 'Notas generadas con éxito';
		}
	}

	function GENOTAS_AUX($usuario){//Borra los datos de la tabla y genera notas para todos los trabajos.
		$toret='Correcto';
		//Obtener lista con todas los trabajos
		$trabajos = $this->mysqli->query("SELECT DISTINCT IdTrabajo FROM ENTREGA");

		//Por cada trabajo
		while($fila = $trabajos->fetch_row()) {
			if($fila[0] == 'ET1' || $fila[0] == 'ET2' || $fila[0] == 'ET3'){
				$alias= $this->mysqli->query("SELECT Alias FROM ENTREGA WHERE IdTrabajo = '$fila[0]' AND login = '$usuario'");
				if(!(!$alias)){
				$alias2=$alias->fetch_row();
				//Obetener todas las tuplas de evaluacion correspondientes al trabajo
				$evaluacion = $this->mysqli->query("SELECT * FROM EVALUACION WHERE IdTrabajo = '$fila[0]' AND AliasEvaluado = '$alias2[0]'");
				//Contador de historias totales
				$totalhistorias=0;
				//Contador para historias correctas
				$historiascorrectas=0;
				while($fila2 = $evaluacion->fetch_row()) {
					$totalhistorias= ($totalhistorias+1);
					//Comprobamos que la historia sea correcta y si lo es incrementamos en uno el contador de historias correctas
					if ($fila2[6] == 1) {
						$historiascorrectas= ($historiascorrectas + 1);
					}
				}
				$login = $usuario; //Obtenemos el login a través de la tabla ENTREGA
				$IdTrabajo = $fila[0];
				if ($totalhistorias != 0){
					$NotaTrabajo = (($historiascorrectas *10) / $totalhistorias); //Calculamos la nota con una regla de tres

					//Insertar en la BD
					$sql = "INSERT INTO NOTA_TRABAJO (login, IdTrabajo, NotaTrabajo) VALUES ('$login', '$IdTrabajo',  '$NotaTrabajo')";

					$resultado = $this->mysqli->query($sql);
				
					if(!$resultado){
						return 'Ha fallado el insertar la nota';
						break;
					}
				}
				else {
					$toret='Error';
				}
				}
			}
			else{
				//Obetener todas las tuplas de evaluacion correspondientes al trabajo
				$evaluacion = $this->mysqli->query("SELECT * FROM EVALUACION WHERE IdTrabajo = '$fila[0]' AND LoginEvaluador = '$usuario' ORDER BY AliasEvaluado");
				if (!(!$evaluacion)) {
				if ($fila[0] == 'QA1') {
					$array = array(
    					0 => 0,
    					1 => 0,
    					2 => 0,
    					3 => 0,
    					4 => 0,
					);
					for ($i=0; $i <5 ; $i++) { 
						$historiascorrectas=0;
						$contador=0;
						for ($i=0; $i <28 ; $i++) { 
							$fila2 = $evaluacion->fetch_row();
							$contador=($contador + 1);
							if($fila2[8] == 1){
								$historiascorrectas= ($historiascorrectas +1);
							}
						}
						if(contador != 0){
							$array[$i]= (($historiascorrectas*10) / 28);
						}

						else {
							$toret='Error';
						}

					}
					$NotaTrabajo=($array[0]+$array[1]+$array[2]+$array[3]+$array[4])/5;
					$login = $usuario;
					$IdTrabajo = $fila[0];

					//Insertar en la BD
					$sql = "INSERT INTO NOTA_TRABAJO (login, IdTrabajo, NotaTrabajo) VALUES ('$login', '$IdTrabajo',  '$NotaTrabajo')";

					$resultado = $this->mysqli->query($sql);
				
					if(!$resultado){
						return 'Ha fallado el insertar la nota';
						break;
					}


				}

				else{
					$array = array(
    					0 => 0,
    					1 => 0,
    					2 => 0,
    					3 => 0,
    					4 => 0,
					);
					for ($i=0; $i <5 ; $i++) { 
						$historiascorrectas=0;
						$contador=0;

						for ($i=0; $i <46 ; $i++) { 
							$fila2 = $evaluacion->fetch_row();
							$contador=($contador+1);
							if($fila2[8] == 1){
								$historiascorrectas= ($historiascorrectas +1);
							}
						}
						if(contador != 0){
							$array[$i]= (($historiascorrectas*10) / 46);
						}

						else {
							$toret='Error';
						}

					}
					$NotaTrabajo=($array[0]+$array[1]+$array[2]+$array[3]+$array[4])/5;
					$login = $usuario;
					$IdTrabajo = $fila[0];

					//Insertar en la BD
					$sql = "INSERT INTO NOTA_TRABAJO (login, IdTrabajo, NotaTrabajo) VALUES ('$login', '$IdTrabajo',  '$NotaTrabajo')";

					$resultado = $this->mysqli->query($sql);
				
					if(!$resultado){
						return 'Ha fallado el insertar la nota';
						break;
					}


				}


			}
			}
		}
		return $toret;
			
	}
}
?>