<?php
/* Clase de modelo de TRABAJO, el cual accederá exclusivamente a la base de datos
	hay funciones get para poder insertarlas en formularios y set por hacer algo de simetría
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	que quede claro, a no ser que no sepas español
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	5/12/17
*/

include_once "../Models/QA.php";

class Trabajo{
	
	var $IdTrabajo;
	var $NombreTrabajo;
	var $FechaIniTrabajo;
	var $FechaFinTrabajo;
	var $PorcentajeNota;

	//Atributos
	
	function __construct($IdTrabajo, $NombreTrabajo, $FechaIniTrabajo, $FechaFinTrabajo, $PorcentajeNota){
		//Asignaciones
		$this->_setIdTrabajo($IdTrabajo);
		$this->_setNombreTrabajo($NombreTrabajo);
		$this->_setFechaIniTrabajo($FechaIniTrabajo);
		$this->_setFechaFinTrabajo($FechaFinTrabajo);
		$this->_setPorcentajeNota($PorcentajeNota);
		
		include_once '../Functions/AccederBD.php';
		$this->mysqli = ConectarBD();
	}
	
		function _setIdTrabajo($IdTrabajo){
			$this->IdTrabajo = $IdTrabajo;
		}
		
		function _setNombreTrabajo($NombreTrabajo){
			$this->NombreTrabajo = $NombreTrabajo;
		}
		
		function _setFechaIniTrabajo($FechaIniTrabajo){
			$this->FechaIniTrabajo = $FechaIniTrabajo;
		}

		function _setFechaFinTrabajo($FechaFinTrabajo){
			$this->FechaFinTrabajo = $FechaFinTrabajo;
		}

		function _setPorcentajeNota($PorcentajeNota){
			$this->PorcentajeNota = $PorcentajeNota;
		}
		
		
		
		function _getIdTrabajo(){
			return $this->IdTrabajo;
		}
		
		function _getNombreTrabajo(){
			return $this->NombreTrabajo;
		}
		
		function _getFechaIniTrabajo(){
			return $this->FechaIniTrabajo;
		}

		function _getFechaFinTrabajo(){
			return $this->FechaFinTrabajo;
		}
		
		function _getPorcentajeNota(){
			return $this->PorcentajeNota;
		}
		
		

		
		function _getDatosGuardados(){//Para recuperar de la base de datos
			if(($this->IdTrabajo == '')){
				return 'Trabajo vacio, introduzca un Trabajo';
			}else{
				$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
				
				$sql = "SELECT * FROM TRABAJO WHERE IdTrabajo = '$IdTrabajo'";
				
				$resultado = $this->mysqli->query($sql);
				
				if(!$resultado){
					return 'No se ha podido conectar con la BD';
				}else if($resultado->num_rows == 0){
					return 'No existe un Trabajo';
				}else{
					$fila = $resultado->fetch_row();
					
					$this->_setIdTrabajo($fila[0]);
					$this->_setNombreTrabajo($fila[1]);
					$this->_setFechaIniTrabajo($fila[2]);
					$this->_setFechaFinTrabajo($fila[3]);
					$this->_setPorcentajeNota($fila[4]);
	
				}
			}
		}
		
	
	function ADD(){//Para añadir a la BD
		if(($this->IdTrabajo == '')){
			return 'Trabajo vacio, introduzca un Trabajo';
		}else{
			$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
			
			$sql = "SELECT * FROM TRABAJO WHERE IdTrabajo = '$IdTrabajo'";
			
			$resultado = $this->mysqli->query($sql);
			
			if(!$resultado){
				return 'No se ha podido conectar con la BD';
			}else{
				if($resultado->num_rows == 0){
					$NombreTrabajo = mysqli_real_escape_string($this->mysqli, $this->NombreTrabajo);
					$FechaIniTrabajo = mysqli_real_escape_string($this->mysqli, $this->FechaIniTrabajo);
					$FechaFinTrabajo = mysqli_real_escape_string($this->mysqli, $this->FechaFinTrabajo);
					$PorcentajeNota = mysqli_real_escape_string($this->mysqli, $this->PorcentajeNota);
			
					$sql = "INSERT INTO TRABAJO (IdTrabajo, NombreTrabajo, FechaIniTrabajo, FechaFinTrabajo, PorcentajeNota) VALUES ('$IdTrabajo', '$NombreTrabajo', '$FechaIniTrabajo', '$FechaFinTrabajo', '$PorcentajeNota')";
				
					if(!$this->mysqli->query($sql)){
						return 'Ha fallado el insertar un Trabajo; IdTrabajo debe ser única';
					}else{
						return 'Inserción correcta';
					}
				}else{
					return 'El IdTrabajo introducido ya existe';
				}
			}
		}
	}
	
	function EDIT(){//Para editar de la BD
		if(($this->IdTrabajo == '')){
			return 'IdTrabajo vacía, introduzca un IdTrabajo';
		}else{
			$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
			
			$sql = "SELECT * FROM TRABAJO WHERE IdTrabajo = '$IdTrabajo'";
			
			$resultado = $this->mysqli->query($sql);
			
			if(!$resultado){
				return 'No se ha podido conectar con la BD';
			}else if($resultado->num_rows == 1){
				$NombreTrabajo = mysqli_real_escape_string($this->mysqli, $this->NombreTrabajo);
				$FechaIniTrabajo = mysqli_real_escape_string($this->mysqli, $this->FechaIniTrabajo);
				$FechaFinTrabajo = mysqli_real_escape_string($this->mysqli, $this->FechaFinTrabajo);
				$PorcentajeNota = mysqli_real_escape_string($this->mysqli, $this->PorcentajeNota);
					
				$sql = "UPDATE TRABAJO SET NombreTrabajo = '$NombreTrabajo', FechaIniTrabajo = '$FechaIniTrabajo', FechaFinTrabajo = '$FechaFinTrabajo', PorcentajeNota = '$PorcentajeNota' WHERE IdTrabajo = '$IdTrabajo'";
				
				if(!$this->mysqli->query($sql)){
					return 'Ha fallado la actualización del Trabajo';
				}else{
					return 'Modificado correcto';
				}
			}else{
				return 'IdTrabajo no existe en la base de datos';
			}
		}
	}
	
	function SEARCH(){//Para buscar en la base de datos, no se busca la contraseña	
				$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
				$NombreTrabajo = mysqli_real_escape_string($this->mysqli, $this->NombreTrabajo);
				$FechaIniTrabajo = mysqli_real_escape_string($this->mysqli, $this->FechaIniTrabajo);
				$FechaFinTrabajo = mysqli_real_escape_string($this->mysqli, $this->FechaFinTrabajo);
				$PorcentajeNota = mysqli_real_escape_string($this->mysqli, $this->PorcentajeNota);
		$sql = "SELECT * FROM TRABAJO WHERE ((IdTrabajo LIKE '%$IdTrabajo%') AND (NombreTrabajo LIKE '%$NombreTrabajo%') AND (FechaIniTrabajo LIKE '%$FechaIniTrabajo%') AND (FechaFinTrabajo LIKE '%$FechaFinTrabajo%') AND (PorcentajeNota LIKE '%$PorcentajeNota%'))";
	
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado || $resultado->num_rows == 0){
			return 'No se ha encontrado ningun dato';
		}else{
			return $resultado;
		}
	}
	
	function DELETE(){//Para eliminar de la BD
		$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
				
		$sql = "SELECT * FROM TRABAJO WHERE IdTrabajo = '$IdTrabajo'";
		
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No se ha encontrado el Trabajo';
		}else{
			$sql = "DELETE FROM TRABAJO WHERE IdTrabajo = '$IdTrabajo'";
			
			if(!$this->mysqli->query($sql)){
				return 'Fallo al eliminar la tupla';
			}else{
				return 'Se ha borrado el Trabajo';
			}
		}
	}
	
	function SHOWCURRENT(){//Para mostrar de la base de datos
		$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
				
		$sql = "SELECT * FROM TRABAJO WHERE IdTrabajo = '$IdTrabajo'";
		
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No existe el IdTrabajo';
		}else{
			return $resultado;
		}
	}
	
	function SHOWALL(){//Para mostrar la BD
		$sql = "SELECT * FROM TRABAJO";
	
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No se ha encontrado ningun dato';
		}else{
			return $resultado;
		}
	}
	

	function AUTOQA(){//Borra los datos de la tabla y asigna automáticamente 5 QAs a cada usuario.
		//Borrar datos
		$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
				
		$this->mysqli->query("DELETE FROM ASIGNAC_QA WHERE IdTrabajo = '$IdTrabajo'");
		
		//Obtener lista con todas las entregas activas (Con fichero suido al servidor)
		$entregas = $this->mysqli->query("SELECT * FROM ENTREGA WHERE IdTrabajo = '$IdTrabajo' AND Ruta <> ''");
		$entregasArr = $entregas->fetch_all(MYSQLI_NUM); //Convertir a Array

		//Si hay 5 entregas o menos, no se pueden asignar 5 a cada usuario sin que se repitan/se evaluen a si mismos
		if(count($entregasArr) <= 5) {
			return 'Entregas insuficientes';
		}
		
		//Teniendo en cuenta que cada usuario tiene una entrega:
		//Por cada entrega, se le asigna al usuario que hizo esa entrega las 5 entregas siguientes de la lista
		//De esa forma, cada usuario tendrá asignadas 5 entregas, y cada entrega estará asignada a 5 usuarios
		//Usuario 1			Usuario 2		Usuario 3		Usuario 4		Usuario 5
		//	-Entrega 2		 -Entrega 3		 -Entrega 4		 -Entrega 5		 -Entrega >6<
		//	-Entrega 3		 -Entrega 4		 -Entrega 5		 -Entrega >6<	 -Entrega 7
		//	-Entrega 4		 -Entrega 5		 -Entrega >6<	 -Entrega 7		 -Entrega 8
		//	-Entrega 5		 -Entrega >6<	 -Entrega 7		 -Entrega 8		 -Entrega 9
		//	-Entrega >6<	 -Entrega 7		 -Entrega 8		 -Entrega 9		 -Entrega 10
		//
		//Como se puede ver, la Entrega 6 ha sido asignada a 6 usuarios, y así sucesivamente
		//Para los 5 últimos usuarios de la lista, se le asignaran las ultimas QAs que van despues de ellos
		//Y las primeras QAs de la lista. Dando la vuelta a la lista

		//Por cada entrega (Por tanto, por cada usuario)
		for($i = 0; $i < count($entregasArr); $i++) {
			//Asignarle las 5 QAs siguientes
			for($j = 1; $j <= 5; $j++) {
				//Si j es 1, $asignado es el nº del siguiente; si es 2, es el nº del que va despues, etc.
				$asignado = ($i + $j)%count($entregasArr); //Módulo para que la lista de la vuelta y de out of bounds

				//Insertar en la BD: IdTrabajo del asignado ($asignado), LoginEvaluador del Evaluador ($i), etc.
				$qa = new QA($entregasArr[$asignado][1], $entregasArr[$i][0], $entregasArr[$asignado][0], $entregasArr[$asignado][2] );
				$resultado = $qa->ADD();
				
				if(!$resultado){
					return 'Ha fallado el insertar el QA';
					break;
				}
			}
		}

		return 'Asignación automática realizada con éxito';
			
	}

}
?>