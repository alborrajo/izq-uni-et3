<?php
/* Clase de modelo de FUNCIONALIDAD, el cual accederá exclusivamente a la base de datos
	hay funciones get para poder insertarlas en formularios y set por hacer algo de simetría
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	que quede claro, a no ser que no sepas español
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	5/12/17
*/
class Funcionalidad{
	
	var $IdFuncionalidad;
	var $NombreFuncionalidad;
	var $DescripFuncionalidad;

	//Atributos
	
	function __construct($IdFuncionalidad, $NombreFuncionalidad, $DescripFuncionalidad){
		//Asignaciones
		$this->_setIdFuncionalidad($IdFuncionalidad);
		$this->_setNombreFuncionalidad($NombreFuncionalidad);
		$this->_setDescripFuncionalidad($DescripFuncionalidad);
		
		include_once '../Functions/AccederBD.php';
		$this->mysqli = ConectarBD();
	}
	
		function _setIdFuncionalidad($IdFuncionalidad){
			$this->IdFuncionalidad = $IdFuncionalidad;
		}
		
		function _setNombreFuncionalidad($NombreFuncionalidad){
			$this->NombreFuncionalidad = $NombreFuncionalidad;
		}
		
		function _setDescripFuncionalidad($DescripFuncionalidad){
			$this->DescripFuncionalidad = $DescripFuncionalidad;
		}
		
		
		
		function _getIdFuncionalidad(){
			return $this->IdFuncionalidad;
		}
		
		function _getNombreFuncionalidad(){
			return $this->NombreFuncionalidad;
		}
		
		function _getDescripFuncionalidad(){
			return $this->DescripFuncionalidad;
		}
		

		
		function _getDatosGuardados(){//Para recuperar de la base de datos
			if($this->IdFuncionalidad == ''){
				return 'Funcionalidad vacía, introduzca una Funcionalidad';
			}else{
				$IdFuncionalidad = mysqli_real_escape_string($this->mysqli, $this->IdFuncionalidad);
				$sql = "SELECT * FROM FUNCIONALIDAD WHERE IdFuncionalidad = '$IdFuncionalidad'";
				
				$resultado = $this->mysqli->query($sql);
				
				if(!$resultado){
					return 'No se ha podido conectar con la BD';
				}else if($resultado->num_rows == 0){
					return 'No existe la Funcionalidad';
				}else{
					$fila = $resultado->fetch_row();
					
					$this->_setNombreFuncionalidad($fila[1]);
					$this->_setDescripFuncionalidad($fila[2]);
	
				}
			}
		}
		
	
	function ADD(){//Para añadir a la BD
		if(($this->IdFuncionalidad == '')){
			return 'Funcionalidad vacía, introduzca una Funcionalidad';
		}else{
			$IdFuncionalidad = mysqli_real_escape_string($this->mysqli, $this->IdFuncionalidad);
			$sql = "SELECT * FROM FUNCIONALIDAD WHERE IdFuncionalidad = '$IdFuncionalidad'";
			
			$resultado = $this->mysqli->query($sql);
			
			if(!$resultado){
				return 'No se ha podido conectar con la BD';
			}else{
				if($resultado->num_rows == 0){
					$IdFuncionalidad = mysqli_real_escape_string($this->mysqli, $this->IdFuncionalidad);
					$NombreFuncionalidad = mysqli_real_escape_string($this->mysqli, $this->NombreFuncionalidad);
					$DescripFuncionalidad = mysqli_real_escape_string($this->mysqli, $this->DescripFuncionalidad);

					$sql = "INSERT INTO FUNCIONALIDAD (IdFuncionalidad, NombreFuncionalidad, DescripFuncionalidad) VALUES ('$IdFuncionalidad', '$NombreFuncionalidad', '$DescripFuncionalidad')";
				
					if(!$this->mysqli->query($sql)){
						return 'Ha fallado el insertar la Funcionalidad; IdFuncionalidad debe ser única';
					}else{
						return 'Inserción correcta';
					}
				}else{
					return 'El IdFuncionalidad introducido ya existe';
				}
			}
		}
	}
	
	function EDIT(){//Para editar de la BD
		if(($this->IdFuncionalidad == '')){
			return 'IdFuncionalidad vacía, introduzca un IdFuncionalidad';
		}else{
			$IdFuncionalidad = mysqli_real_escape_string($this->mysqli, $this->IdFuncionalidad);

			$sql = "SELECT * FROM FUNCIONALIDAD WHERE IdFuncionalidad = '$IdFuncionalidad'";
			
			$resultado = $this->mysqli->query($sql);
			
			if(!$resultado){
				return 'No se ha podido conectar con la BD';
			}else if($resultado->num_rows == 1){
				$IdFuncionalidad = mysqli_real_escape_string($this->mysqli, $this->IdFuncionalidad);
				$NombreFuncionalidad = mysqli_real_escape_string($this->mysqli, $this->NombreFuncionalidad);
				$DescripFuncionalidad = mysqli_real_escape_string($this->mysqli, $this->DescripFuncionalidad);

				$sql = "UPDATE FUNCIONALIDAD SET NombreFuncionalidad = '$NombreFuncionalidad', DescripFuncionalidad = '$DescripFuncionalidad' WHERE IdFuncionalidad = '$IdFuncionalidad'";
				
				if(!$this->mysqli->query($sql)){
					return 'Ha fallado la actualización de la Funcionalidad';
				}else{
					return 'Modificado correcto';
				}
			}else{
				return 'IdFuncionalidad no existe en la base de datos';
			}
		}
	}
	
	function SEARCH(){//Para buscar en la base de datos, no se busca la contraseña
		$IdFuncionalidad = mysqli_real_escape_string($this->mysqli, $this->IdFuncionalidad);
		$NombreFuncionalidad = mysqli_real_escape_string($this->mysqli, $this->NombreFuncionalidad);
		$DescripFuncionalidad = mysqli_real_escape_string($this->mysqli, $this->DescripFuncionalidad);

		$sql = "SELECT * FROM FUNCIONALIDAD WHERE ((IdFuncionalidad LIKE '%$IdFuncionalidad%') AND (NombreFuncionalidad LIKE '%$NombreFuncionalidad%') AND (DescripFuncionalidad LIKE '%$DescripFuncionalidad%'))";
	
		$resultado1 = $this->mysqli->query($sql);
		
		if(!$resultado1 || $resultado1->num_rows == 0){
			return 'No se ha encontrado ningun dato';
		}
		
		$sql = "SELECT * FROM FUNC_ACCION";
	
		$resultado2 = $this->mysqli->query($sql);
		
		if(!$resultado2){
			return 'No se ha podido conectar con la BD';
		}
		
		$sql = "SELECT IdAccion, NombreAccion FROM ACCION";
		
		$Acciones = $this->mysqli->query($sql);
		
		if(!$Acciones){
			return 'No se ha podido conectar con la BD';
		}
		
		return array($resultado1, $resultado2, $Acciones);		
	}
	
	function DELETE(){//Para eliminar de la BD
		$IdFuncionalidad = mysqli_real_escape_string($this->mysqli, $this->IdFuncionalidad);
		$sql = "SELECT * FROM FUNCIONALIDAD WHERE IdFuncionalidad = '$IdFuncionalidad'";
		
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No se ha encontrado la Funcionalidad';
		}else{
			$IdFuncionalidad = mysqli_real_escape_string($this->mysqli, $this->IdFuncionalidad);
			$sql = "DELETE FROM FUNCIONALIDAD WHERE IdFuncionalidad = '$IdFuncionalidad'";
			
			if(!$this->mysqli->query($sql)){
				return 'Fallo al eliminar la tupla';
			}
			
			$IdFuncionalidad = mysqli_real_escape_string($this->mysqli, $this->IdFuncionalidad);
			$sql = "DELETE FROM FUNC_ACCION WHERE IdFuncionalidad = '$IdFuncionalidad'";
			
			if(!$this->mysqli->query($sql)){
				return 'Fallo al eliminar tuplas';
			}else{
				return 'Funcionalidad eliminada correctamente';
			}
		}
	}
	
	function SHOWCURRENT(){//Para mostrar de la base de datos
		$IdFuncionalidad = mysqli_real_escape_string($this->mysqli, $this->IdFuncionalidad);
		$sql = "SELECT * FROM FUNCIONALIDAD WHERE IdFuncionalidad = '$IdFuncionalidad'";
		
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No existe el IdFuncionalidad';
		}else{
			return $resultado;
		}
	}
	
	function SHOWALL(){//Para mostrar la BD
		$sql = "SELECT * FROM FUNCIONALIDAD";
	
		$resultado1 = $this->mysqli->query($sql);
		
		if(!$resultado1){
			return 'No se ha podido conectar con la BD';
		}else if($resultado1->num_rows == 0){
			return 'No se ha encontrado ningun dato';
		}
		
		$sql = "SELECT * FROM FUNC_ACCION";
	
		$resultado2 = $this->mysqli->query($sql);
		
		if(!$resultado2){
			return 'No se ha podido conectar con la BD';
		}
		
		$sql = "SELECT IdAccion, NombreAccion FROM ACCION";
		
		$Acciones = $this->mysqli->query($sql);
		
		if(!$Acciones){
			return 'No se ha podido conectar con la BD';
		}
		
		return array($resultado1, $resultado2, $Acciones);		
	}
	
	function ASIGNARACCION($IdAccion){
		$sql = "SELECT * FROM ACCION WHERE IdAccion = '$IdAccion'"; //Comprobar que existe el accion
		
		$resultado = $this->mysqli->query($sql);
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No existe la accion indicada';
		}else{		
			$IdFuncionalidad = mysqli_real_escape_string($this->mysqli, $this->IdFuncionalidad);
			$sql = "SELECT * FROM FUNC_ACCION WHERE IdFuncionalidad = '$IdFuncionalidad' AND IdAccion = '$IdAccion'";
			
			$resultado = $this->mysqli->query($sql);
			
			if(!$resultado){
				return 'No se ha podido conectar con la BD';
			}else if($resultado->num_rows != 0){
				return 'La acción ya está asignada a la funcionalidad';
			}else{
				$IdFuncionalidad = mysqli_real_escape_string($this->mysqli, $this->IdFuncionalidad);
				$sql = "INSERT INTO FUNC_ACCION (IdFuncionalidad, IdAccion) VALUES ('$IdFuncionalidad', '$IdAccion')";
				
				if(!$this->mysqli->query($sql)){
					return 'Fallo al insertar la accion a la funcionalidad';
				}else{
					return 'Se ha añadido la accion a la funcionalidad';
				}
			}
		}
	}
	
	function DESASIGNARACCION($IdAccion){
		$IdFuncionalidad = mysqli_real_escape_string($this->mysqli, $this->IdFuncionalidad);
		$sql = "SELECT * FROM FUNC_ACCION WHERE IdFuncionalidad = '$IdFuncionalidad' AND IdAccion = '$IdAccion'";
		
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conecctar con la BD';
		}else if($resultado->num_rows == 0){
			return 'La accion no pertenece a la funcionalidad';
		}else{
			$IdFuncionalidad = mysqli_real_escape_string($this->mysqli, $this->IdFuncionalidad);
			$sql = "DELETE FROM FUNC_ACCION WHERE IdFuncionalidad = '$IdFuncionalidad' AND IdAccion = '$IdAccion'";
			
			if(!$this->mysqli->query($sql)){
				return 'Fallo al quitar la accion de la funcionalidad';
			}else{
				return 'Se ha eliminado la accion de la funcionalidad';
			}
		}
	}
}
?>