
<?php
/* Clase de modelo de ENTREGA, el cual accederá exclusivamente a la base de datos
	hay funciones get para poder insertarlas en formularios y set por hacer algo de simetría
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	todas las funciones son describibiles por si mismas gracias a los cuantiosos mensajes de error
	que quede claro, a no ser que no sepas español
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	5/12/17
*/
class Entrega{
	
	var $login;
	var $IdTrabajo;
	var $Alias;
	var $Horas;
	var $Ruta;
	var $mysqli;

	//Atributos
	
	function __construct($login,$IdTrabajo,$Alias,$Horas,$Ruta){
		//Asignaciones
		$this->_setLogin($login);
		$this->_setIdTrabajo($IdTrabajo);
		$this->_setAlias($Alias);
	    $this->_setHoras($Horas);
	    $this->_setRuta($Ruta);
		
		include_once '../Functions/AccederBD.php';
		$this->mysqli = ConectarBD();
	}
	
		function _setLogin($login){
			$this->login = $login;
		}
		
		function _setIdTrabajo($IdTrabajo){
			$this->IdTrabajo = $IdTrabajo;
		}
		
		function _setAlias($Alias){
			$this->Alias = $Alias;
		}
		
		
		function _setHoras($Horas){
			$this->Horas = $Horas;
		}
	
		function _setRuta($Ruta){
			$this->Ruta = $Ruta;
		}
	
		
		function _getLogin(){
			return $this->login;
		}
		
		function _getIdTrabajo(){
			return $this->IdTrabajo;
		}
		
		function _getAlias(){
			return $this->Alias;
		}
		
		
		function _getHoras(){
			return $this->Horas;
		}
	
		function _getRuta(){
			return $this->Ruta;
		}
		
		

		
		function _getDatosGuardados(){//Para recuperar de la base de datos
			if(($this->login == '')){
				return 'login vacío, introduzca un login';
			}else if($this->IdTrabajo == ''){
				return 'IdTrabajo vacío, introduzca un IdTrabajo';
			}else{
				$login = mysqli_real_escape_string($this->mysqli, $this->login);
				$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);

				$sql = "SELECT * FROM ENTREGA WHERE login = '$login' AND IdTrabajo = '$IdTrabajo'";
				
				$resultado = $this->mysqli->query($sql);
				
				if(!$resultado){
					return 'No se ha podido conectar con la BD';
				}else if($resultado->num_rows == 0){
					return 'No existe una entrega con dicho login e IdTrabajo';
				}else{
					$fila = $resultado->fetch_row();
					
					$this->_setAlias($fila[2]);
					$this->_setHoras($fila[3]);
					$this->_setRuta($fila[4]);	
				}
			}
		}
		
	function genAlias() {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < 6; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		$this->Alias = $randomString;
		return $randomString;
	}
	
	function ADD(){//Para añadir a la BD
		if(($this->login == '')){
			return 'login vacío, introduzca un login';
		}else if($this->IdTrabajo == ''){
			return 'IdTrabajo vacío, introduzca un IdTrabajo';
		}else{
			$login = mysqli_real_escape_string($this->mysqli, $this->login);
			$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);

			$sql = "SELECT * FROM ENTREGA WHERE login = '$login' AND IdTrabajo = '$IdTrabajo'";
			
			$resultado = $this->mysqli->query($sql);
			
			if(!$resultado){
				return 'No se ha podido conectar con la BD';
			}else{
				if($resultado->num_rows == 0){
					$login = mysqli_real_escape_string($this->mysqli, $this->login);
					$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
					$Alias = mysqli_real_escape_string($this->mysqli, $this->Alias);
					$Horas = mysqli_real_escape_string($this->mysqli, $this->Horas);
					$Ruta = mysqli_real_escape_string($this->mysqli, $this->Ruta);

					$sql = "INSERT INTO ENTREGA (login, IdTrabajo, Alias, Horas, Ruta) 
							VALUES ('$login', '$IdTrabajo', '$Alias','$Horas','$Ruta')";
				
					if(!$this->mysqli->query($sql)){
						return 'Ha fallado el insertar la Entrega; login e IdTrabajo deben ser únicos';
					}else{
						return 'Inserción correcta';
					}
				}else{
					return 'El login e IdTrabajo introducidos ya existen';
				}
			}
		}
	}
	
	function EDIT(){//Para editar de la BD
		if(($this->login == '')){
			return 'login vacío, introduzca un login';
		}else if($this->IdTrabajo == ''){
			return 'IdTrabajo vacío, introduzca un IdTrabajo';
		}else{
			$login = mysqli_real_escape_string($this->mysqli, $this->login);
			$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);

			$sql = "SELECT * FROM ENTREGA WHERE login = '$login' AND IdTrabajo = '$IdTrabajo'";
			
			$resultado = $this->mysqli->query($sql);
			
			if(!$resultado){
				return 'No se ha podido conectar con la BD';
			}else if($resultado->num_rows == 1){
				$login = mysqli_real_escape_string($this->mysqli, $this->login);
				$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
				$Alias = mysqli_real_escape_string($this->mysqli, $this->Alias);
				$Horas = mysqli_real_escape_string($this->mysqli, $this->Horas);
				$Ruta = mysqli_real_escape_string($this->mysqli, $this->Ruta);

				$sql = "UPDATE ENTREGA SET Alias = '$Alias', Horas = '$Horas' , Ruta = '$Ruta 
                 'WHERE login = '$login' AND IdTrabajo = '$IdTrabajo'";
				
				if(!$this->mysqli->query($sql)){
					return 'Ha fallado la actualización de la Entrega';
				}else{
					return 'Modificado correcto';
				}
			}else{
				return 'login e IdTrabajo no existen en la base de datos';
			}
		}
	}
	
	function SEARCH(){//Para buscar en la base de datos, no se busca la contraseña	
		$login = mysqli_real_escape_string($this->mysqli, $this->login);
		$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
		$Alias = mysqli_real_escape_string($this->mysqli, $this->Alias);
		$Horas = mysqli_real_escape_string($this->mysqli, $this->Horas);
		$Ruta = mysqli_real_escape_string($this->mysqli, $this->Ruta);

		$sql = "SELECT * FROM ENTREGA WHERE ((login LIKE '%$login%') AND (IdTrabajo LIKE '%$IdTrabajo%') AND (Alias LIKE '%$Alias%') AND (Horas LIKE '%$Horas%') AND (Ruta LIKE '%$Ruta%'))";
	
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado || $resultado->num_rows == 0){
			return 'No se ha encontrado ningun dato';
		}else{
			return $resultado;
		}
	}
	
	function DELETE(){//Para eliminar de la BD
		$login = mysqli_real_escape_string($this->mysqli, $this->login);
		$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
		
		$sql = "SELECT * FROM ENTREGA WHERE login = '$login' AND IdTrabajo = '$IdTrabajo'";
		
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No se ha encontrado la Entrega';
		}else{
			$sql = "DELETE FROM ENTREGA WHERE login = '$login' AND IdTrabajo = '$IdTrabajo'";
			
			if(!$this->mysqli->query($sql)){
				return 'Fallo al eliminar la tupla';
			}else{
				return 'Se ha borrado la Entrega';
			}
		}
	}
	
	function SHOWCURRENT(){//Para mostrar de la base de datos
		$login = mysqli_real_escape_string($this->mysqli, $this->login);
		$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
		
		$sql = "SELECT * FROM ENTREGA WHERE login = '$login' AND IdTrabajo = '$IdTrabajo'";
		
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No existe la Entrega';
		}else{
			return $resultado;
		}
	}
	
	function SHOWALL(){//Para mostrar la BD
		$sql = "SELECT * FROM ENTREGA";
	
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No se ha encontrado ningun dato';
		}else{
			return $resultado;
		}
	}
	
	function ENPLAZO(){
		$IdTrabajo = mysqli_real_escape_string($this->mysqli, $this->IdTrabajo);
		$sql = "SELECT FechaIniTrabajo, FechaFinTrabajo FROM TRABAJO WHERE IdTrabajo = '$IdTrabajo'";
		
		$resultado = $this->mysqli->query($sql);
		
		if(!$resultado){
			return 'No se ha podido conectar con la BD';
		}else if($resultado->num_rows == 0){
			return 'No existe la Entrega';
		}else{
			$hoy = new DateTime("now");
			$fechas = mysqli_fetch_assoc($resultado);
			
			if(new DateTime($fechas["FechaIniTrabajo"]) < $hoy && $hoy < new DateTime($fechas["FechaFinTrabajo"]) ){
				return true;
			}else{
				return false;
			}			
		}		
	}
}
?>