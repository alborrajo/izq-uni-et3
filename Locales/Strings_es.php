<?php 
/* Strings del idioma español
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
$strings = 
array(
	'Incorrecto' => 'Incorrecto',
	'No se ha encontrado el permiso' => 'No se ha encontrado el permiso',
	'No se pueden ver las correcciones mientras se puede cambiar la entrega' => 'No se pueden ver las correcciones mientras se puede cambiar la entrega',
	'Fuera de plazo' => 'Fuera de plazo',
	'Trabajo no válido' => 'Trabajo no válido',
	'No hay evaluaciones' => 'No hay evaluaciones',
	'No has hecho ninguna entrega' => 'No has hecho ninguna entrega',
	'El tamaño de la nota sobrepasa el límite de' => 'El tamaño de la nota sobrepasa el límite de ',
	'Error con la extensión del fichero o nombre no válido' => 'Error con la extensión del fichero o nombre no válido',
	'Bienvenido: ' => 'Bienvenido: ',
	'Error no identificado' => 'Error no identificado',
	'Grupo eliminado correctamente' => 'Grupo eliminado correctamente',
	'DescripGrupo' => 'Descripción del grupo',
	'La acción ya está asignada a la funcionalidad' => 'La acción ya está asignada a la funcionalidad',
	'Se ha añadido la accion a la funcionalidad' => 'Se ha añadido la accion a la funcionalidad',
	'No se ha encontrado la Funcionalidad' => 'No se ha encontrado la funcionalidad',
	'Funcionalidad eliminada correctamente' => 'Funcionalidad eliminada correctamente',
	'La accion no pertenece a la funcionalidad' => 'La accion no pertenece a la funcionalidad',
	'Se ha eliminado la accion de la funcionalidad' => 'Se ha eliminado la accion de la funcionalidad',
	'Permisos insuficientes' => 'Permisos insuficientes',
	'Correcciones' => 'Correcciones',
	'IdTrabajo vacío, introduzca un IdTrabajo' => 'IdTrabajo vacío, introduzca un IdTrabajo',
	'El IdFuncionalidad introducido ya existe' => 'El IdFuncionalidad introducido ya existe',
	'TextoHistoria' => 'TextoHistoria',
	'Entrega' => 'Entrega',
	'Entregas' => 'Entregas',
	'Evaluaciones' => 'Evaluaciones',
	'No se ha encontrado el trabajo' => 'No se ha encontrado el trabajo',
	'Funcionalidades' => 'Funcionalidades',
	'Historias' => 'Historias',
	'Notas' => 'Notas',
	'Permisos' => 'Permisos',

	//QA
	'QA' => 'QA',
	'QAs' => 'QAs',
	'Trabajos' => 'Trabajos',
	'Usuarios' => 'Usuarios',
	'Generar QA' => 'Generar QA',
	'Ha fallado el insertar el QA' => 'Ha fallado el insertar el QA',

	'Generar historias' => 'Generar historias',
	'El tamaño del ruta sobrepasa el límite de ' => 'O tamaño da ruta sobrepasa o límite de ',
	'IdHistoria' => 'IdHistoria',
	'Ruta' => 'Ruta',
	'Horas' => 'Horas', 
	'Alias' => 'Alias', 
	'IdTrabajo' => 'IdTrabajo', 
	'Accion' => 'Acción', 
	'Acciones' => 'Acciones',
	'Alguno de los campos no tiene valores correctos' => 'Alguno de los campos no tiene valores correctos',
	'Apellidos' => 'Apellidos', 
	'Añadir' => 'Añadir',
	'Buscar' => 'Buscar',
	'Contraseña incorrecta' => 'Contraseña incorrecta',
	'Contraseña' => 'Contraseña', 
	'Correcto' => 'Correcto',
	'Creado' => 'Creado',
	'DNI vacío, introduzca un valor' => 'DNI vacío, introduzca un valor',
	'DNI' => 'DNI/NIF', 
	'Desconectado' => 'Desconectado',
	'Desconectarse' => 'Desconectarse',
	'E-mail vacío, introduzca un valor' => 'E-mail vacío, introduzca un valor',
	'Editar' => 'Editar',
	'El DNI introducido no es válido' => 'El DNI introducido no es válido',
	'El DNI introducido ya existe' => 'El DNI introducido ya existe',
	'El E-Mail existe en la BD' => 'El E-Mail existe en la BD',
	'El campo es vacío' => 'El campo es vacío',
	'El login introducido ya existe' => 'El login introducido ya existe',
	'El login no existe' => 'El login no existe',
	'El numero de decimales no es válido' => 'El numero de decimales no es válido',
	'El numero mayor no es válido' => 'El numero mayor no es válido',
	'El numero menor no es válido' => 'El numero menor no es válido',
	'El sexo tiene que ser hombre o mujer' => 'El sexo tiene que ser hombre o mujer',
	'El tamaño de la contraseña sobrepasa el límite de ' => 'El tamaño de la contraseña sobrepasa el límite de ',
	'El tamaño del IdTrabajo sobrepasa el límite de ' => 'El tamaño del IdTrabajo sobrepasa el límite de ',
	'El tamaño del IdHistoria sobrepasa el límite de ' => 'El tamaño del IdHistoria sobrepasa el límite de ',
	'El tamaño del TextoHistoria sobrepasa el límite de ' => 'El tamaño del TextoHistoria sobrepasa el límite de ',
	'El tamaño del alias sobrepasa el límite de ' => 'El tamaño del alias sobrepasa el límite de ',
	'El tamaño de la hora sobrepasa el límite de ' => 'El tamaño de la hora sobrepasa el límite de ',
	'El tamaño de la ruta sobrepasa el límite de ' => 'El tamaño de la ruta sobrepasa el límite de ',
	'El tamaño de la titulación sobrepasa el límite de ' => 'El tamaño de la titulación sobrepasa el límite de ',
	'El tamaño de los apellidos sobrepasa el límite de ' => 'El tamaño de los apellidos sobrepasa el límite de ',
	'El tamaño del email sobrepasa el límite de ' => 'El tamaño del email sobrepasa el límite de ',
	'El tamaño del login sobrepasa el límite de ' => 'El tamaño del login sobrepasa el límite de ',
	'El tamaño del nombre sobrepasa el límite de ' => 'El tamaño del nombre sobrepasa el límite de ',
	'El valor no está entre los especificados (valormenor y valormayor)' => 'El valor no está entre los especificados (valormenor y valormayor)',
	'Eliminar' => 'Eliminar',
	'Email' => 'E-mail', 
	'Error con uno de los campos' => 'Error con uno de los campos',
	'Error en la inserción' => 'Error en la inserción',
	'Error, la base de datos no ha recibido la petición de borrado' => 'Error, la base de datos no ha recibido la petición de borrado',
	'Fallo al eliminar la tupla' => 'Fallo al eliminar la tupla',
	'FechaNacimiento' => 'Fecha de nacimiento', 
	'Formato del email incorrecto' => 'Formato del email incorrecto',
	'Formato del telefono incorrecto o contiene caracteres no válidos' => 'Formato del telefono incorrecto o contiene caracteres no válidos',
	'Formulario correcto, ¿desea enviarlo?' => 'Formulario correcto, ¿desea enviarlo?',
	'FotoPersonal' => 'Foto personal', 
	'Ha fallado el insertar al usuario; Login, DNI y E-Mail deben ser únicos' => 'Ha fallado el insertar al usuario; Login, DNI y E-Mail deben ser únicos',
	'Ha fallado la actualización del usuario, puede que el DNI o E-Mail ya estén en la base de datos (Tienen que ser únicos)' => 'Ha fallado la actualización del usuario, puede que el DNI o E-Mail ya estén en la base de datos (Tienen que ser únicos)',
	'Ha fallado la actualización del usuario, puede que el DNI o E-Mail ya estén en la base de datos' => 'Ha fallado la actualización del usuario, puede que el DNI o E-Mail ya estén en la base de datos',
	'Idioma' => 'Idioma',
	'Informacion' => 'Información',
	'Inserción correcta' => 'Inserción realizada con éxito',
	'Login incorrecto' => 'Login incorrecto',
	'Login no existe en la base de datos' => 'Login no existe en la base de datos',
	'Login vacío, introduzca un login' => 'Login vacío, introduzca un login',
	'No existe una entrega con dicho login e IdTrabajo' => 'No existe una entrega con dicho login e IdTrabajo',
	'El login e IdTrabajo introducidos ya existen' => 'El login e IdTrabajo introducidos ya existen',
	'Login e IdTrabajo no existen en la base de datos' => 'Login e IdTrabajo no existen en la base de datos',
	'Ha fallado el insertar la Entrega; login e IdTrabajo deben ser únicos' => 'Ha fallado el insertar la Entrega; login e IdTrabajo deben ser únicos',
	'Login' => 'Login', 
	'Main' => 'Página Principal',
	'Man' => 'Hombre', 
	'Menu' => 'Menú',
	'Modificado correcto' => 'Modificado correcto',
	'No existe el login en la BD o ésta está inaccesible' => 'No existe el login en la BD o ésta está inaccesible',
	'No existe el login' => 'No existe el login',
	'No ha introducido el numero de decimales, se pondrá como que no tiene' => 'No ha introducido el numero de decimales, se pondrá como que no tiene',
	'No ha introducido el numero de valormayor, se pondrá como "0"' => 'No ha introducido el numero de valormayor, se pondrá como "0"',
	'No ha introducido el numero de valormenor, se pondrá como "0"' => 'No ha introducido el numero de valormenor, se pondrá como "0"',
	'No se ha encontrado al usuario en la BD' => 'No se ha encontrado al usuario en la BD',	
	'No se ha encontrado al usuario' => 'No se ha encontrado al usuario',
	'No se ha encontrado ningun dato' => 'No se ha encontrado ningun dato',
	'No se ha podido conectar con la BD' => 'No se ha podido conectar con la BD',
	'No se han podido comprobar todos los campos o hay un error extraño' => 'No se han podido comprobar todos los campos o hay un error extraño',
	'No se puede editar este campo' => 'No se puede editar este campo',
	'Nombre' => 'Nombre', 
	'Opcion' => 'Opción',
	'Registrar' => 'Registrar',
	'Registrarse' => 'Registrarse',
	'Rellena el campo' => 'Rellena el campo',
	'Se ha borrado al usuario con dicha clave' => 'Se ha borrado al usuario con dicha clave',
	'Se ha borrado al usuario' => 'Usuario borrado',
	'Se ha cancelado enviar el formulario' => 'Se ha cancelado enviar el formulario',
	'Sexo' => 'Sexo', 
	'Solo numeros enteros con un "-" o un "+" delante y el numero de decimales' => 'Solo numeros enteros con un "-" o un "+" delante y el numero de decimales',
	'Solo numeros enteros con un "-" o un "+" delante' => 'Solo numeros enteros con un "-" o un "+" delante',
	'Sólo caracteres alfabéticos o espacios' => 'Sólo caracteres alfabéticos o espacios',
	'Telefono' => 'Teléfono', 
	'Titulo' => 'Título',
	'Usuario añadido correctamente' => 'Usuario añadido correctamente',
	'Usuario no autenticado' => 'Usuario no autenticado',
	'Usuario' => 'Usuario',
	'Volver' => 'Volver',
	'Woman' => 'Mujer', 
	'español' => 'Español',
	'gallego' => 'Gallego',
	'header' => 'Interfaces de Usuario',
	'hombre' => 'Hombre', 
	'ingles' => 'Inglés',
	'mujer' => 'Mujer', 
	'true' => 'Esto no debería aparecer aquí',
	'¿Seguro que desea limpiar los campos del formulario?' => '¿Seguro que desea limpiar los campos del formulario?',
	'password' => 'Contraseña',
	'Correo' => 'Correo electrónico',
	'Direccion' => 'Dirección',
	'Grupos' => 'Grupos',
	'IdAccion' => 'Identificador Acción',
	'NombreAccion' => 'Nombre Acción',
	'DescripAccion' => 'Descripción Acción',
	'Ha fallado el insertar al usuario' => 'Ha fallado el insertar al usuario',
	'Ha fallado la actualización del usuario' => 'Ha fallado la actualización del usuario',
	'Fallo al eliminar al usuario' => 'Fallo al eliminar al usuario',
	'Fallo al eliminar tuplas' => 'Fallo al eliminar datos',
	'Usuario eliminado correctamente' => 'Usuario eliminado correctamente',
	'Ya existe un usuario con ese login' => 'Ya existe un usuario con ese login',
	'No existe el grupo indicado' => 'No existe el grupo indicado',
	'El usuario ya está asignado a ese grupo' => 'El usuario ya está asignado a ese grupo',
	'Fallo al insertar el grupo al usuario' => 'Fallo al insertar el grupo al usuario',
	'Fallo al quitar al usuario del grupo' => 'Fallo al quitar al usuario del grupo',
	'Se ha añadido al usuario al grupo' => 'Se ha añadido al usuario al grupo',
	'El usuario no pertenece al grupo' => 'El usuario no pertenece al grupo',
	'Se ha eliminado al usuario del grupo' => 'Se ha eliminado al usuario del grupo',
	
	//ACCION
	'IdAccion vacío, introduzca un IdAccion' => 'IdAccion vacío, introduzca un IdAccion',
	'No existe el IdAccion' => 'No existe el IdAccion',
	'IdAccion vacío, introduzca un IdAccion' => 'IdAccion vacío, introduzca un IdAccion',
	'Ha fallado el insertar la acción; IdAccion debe ser único' => 'Ha fallado el insertar la acción; IdAccion debe ser único',
	'El IdAccion introducido ya existe' => 'El IdAccion introducido ya existe',
	'Ha fallado la actualización de la acción' => 'Ha fallado la actualización de la acción',
	'IdAccion no existe en la base de datos' => 'IdAccion no existe en la base de datos',
	'No se ha encontrado la acción' => 'No se ha encontrado la acción',
	'Se ha borrado la acción' => 'Se ha borrado la acción',
	'Accion eliminada correctamente' => 'Accion eliminada correctamente',
	

	//PERMISO
	'IdGrupo' => 'ID del grupo',
	'IdFuncionalidad' => 'ID de la funcionalidad',
	'IdAccion' => 'ID de la acción',

	'Grupo' => 'Grupo',
	'Funcionalidad' => 'Funcionalidad',
	'Accion' => 'Acción',

	//NOTATRABAJO
	'IdTrabajo' => 'ID del trabajo',
	'NotaTrabajo' => 'Nota del trabajo',

	'Se ha borrado la tupla' => 'Se ha borrado la tupla',
	'Los datos clave introducidos ya existen en la tabla' => 'Los datos clave introducidos ya existen en la tabla',	
	'Los datos clave no existen en la tabla' => 'Los datos clave no existen en la tabla',
	'Notas generadas con éxito' => 'Notas generadas con éxito',
	'Ha fallado el insertar la nota'=>'Ha fallado el insertar la nota',
	'Error, no hay evaluaciones para los trabajos' => 'Error, no hay evaluaciones para algunos trabajos, por lo que no se calularon las notas para eses trabajos',

	//EVALUACION
	'LoginEvaluador' => 'Login del evaluador',
	'AliasEvaluado' => 'Alias del evaluado',
	'IdHistoria' => 'Id de la historia',
	'CorrectoA' => 'Correcto',
	'ComenIncorrectoA' => 'Explicación del incorrecto',
	'CorrectoP' => 'Correcto del Profesor',
	'ComentIncorrectoP' => 'Explicación del incorrecto del Profesor',
	'OK' => 'OK',
	'No existe la tupla' => 'No existe la tupla',

	'IdGrupo' => 'Id Grupo',
	'NombreGrupo' => 'Nombre grupo',
	'DescripcionGrupo' => 'Descripcion grupo',
	'IdTrabajo' => 'Id Trabajo',
	'LoginEvaluador' => 'Login Evaluador',
	'LoginEvaluado' => 'Login Evaluado',
	'AliasEvaluado' => 'Alias Evaluado',
	'Se ha borrado el grupo' => 'Se ha borrado el grupo',
	'Se ha borrado al QA' => 'Se ha borrado el QA',

	'NombreFuncionalidad' => 'Nombre funcionalidad',
	'DescripFuncionalidad' => 'Descripción funcionalidad',

	'NombreTrabajo' => 'Nombre trabajo',
	'FechaIniTrabajo' => 'Fecha inicio trabajo',
	'FechaFinTrabajo' => 'Fecha fin trabajo',
	'PorcentajeNota' => 'Porcentaje trabajo',
	'NotaFinal' => 'Nota final',
	'Se ha borrado el Trabajo' => 'Se ha borrado el trabajo',
	'Se ha borrado la Funcionalidad' => 'Se ha borrado la funcionalidad',

	//Asignacion automatica QA
	'Asignación automática realizada con éxito' => 'Asignación automática realizada con éxito',
	'Entregas insuficientes' => 'Entregas insuficientes',
	'autoqa' => 'La asignación automática de QAs eliminará los datos actuales y los reemplazará por unos nuevos. ¿Está usted seguro de que desea continuar?',
	'Error obteniendo Historias, los usuarios no podrán evaluar.' => 'Error obteniendo Historias, los usuarios no podrán evaluar.',

	//Asignacion manual QA
	'El LoginEvaluador no puede ser igual que LoginEvaluado' => 'El LoginEvaluador no puede ser igual que LoginEvaluado',

	//HISTORIA
	'Ha fallado la actualización de la historia' => 'Ha fallado la actualización de la historia',
	'Historia vacía, introduzca una Historia' => 'Historia vacía, introduzca una Historia',
	'IdTrabajo o IdHistoria no existen en la base de datos' => 'IdTrabajo o IdHistoria no existen en la base de datos',
	

	//JAVASCRIPT
	'Sólo caracteres alfabéticos' => 'Sólo caracteres alfabéticos',
	'El tamaño de los apellidos sobrepasa el límite de ' => 'El tamaño de los apellidos sobrepasa el límite de ',
	'El campo está vacío' => 'El campo está vacío',
	'El tamaño de la direccion sobrepasa el límite de ' => 'El tamaño de la direccion sobrepasa el límite de ',
	'Sólo caracteres alfabéticos, espacios comas o puntos' => 'Sólo caracteres alfabéticos, espacios comas o puntos',
	'Error no identificado' => 'Error no identificado',

	'El IdAccion es vacío' => 'El IdAccion es vacío',
	'El IdAccion supera el tamaño límite' => 'El IdAccion supera el tamaño límite',
	'El NombreAccion es vacío' => 'El NombreAccion es vacío',
	'El NombreAccion supera el tamaño límite' => 'El NombreAccion supera el tamaño límite',
	'El valor no está entre los especificados (valormenor y valormayor)' => 'El valor no está entre los especificados (valormenor y valormayor)',
	
	//Entrega
	'Se ha borrado la Entrega' => 'Se ha borrado la Entrega',
	'Fichero' => 'Fichero',
	'Fichero con extensión no válida' => 'Fichero con extensión no válida',


);
?>
