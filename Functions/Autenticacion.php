<?php
/*Función que tiene como propósito comprobar que el usuario está logeado, es decir, existe un $_SESSION['login'];
	por 3hh731, kch3f4, j7g9n1, ymh5sa, hgdnog 
	28/11/17
*/
function autenticado(){
	if(isset($_SESSION['login'])){//Si existe, existe
		return true;
	}else{//Si no existe, no existe
		return false;
	}
}

function tienePermisosPara($IdFuncionalidad, $IdAccion){	
	include_once '../Models/USUARIO.php';
	
	if(!autenticado()){
		return false;
	}
	
	$usuario = new Usuario($_SESSION['login'],'','','','','','','','','');
	
	$resultado = $usuario->PERMISOPARA($IdFuncionalidad, $IdAccion);
	
	if(is_string($resultado)){
		return false;
	}
	
	return $resultado;
}
?>
